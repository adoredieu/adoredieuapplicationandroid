REM mdpi
..\TxpImageFactory\bin\TxpImageFactory.exe -configurationFile=config_white_pictograms.xml -xResolutionFactor=0,5 -yResolutionFactor=0,5 -outputDirectory=app\src\main\res\drawable-mdpi
..\TxpImageFactory\bin\TxpImageFactory.exe -configurationFile=config_music_player.xml -xResolutionFactor=0,5 -yResolutionFactor=0,5 -outputDirectory=app\src\main\res\drawable-mdpi
..\TxpImageFactory\bin\TxpImageFactory.exe -configurationFile=config_gestures.xml -xResolutionFactor=0,5 -yResolutionFactor=0,5 -outputDirectory=app\src\main\res\drawable-mdpi
..\TxpImageFactory\bin\TxpImageFactory.exe -configurationFile=config_pictograms.xml -xResolutionFactor=0,5 -yResolutionFactor=0,5 -outputDirectory=app\src\main\res\drawable-mdpi
REM hdpi
..\TxpImageFactory\bin\TxpImageFactory.exe -configurationFile=config_white_pictograms.xml -xResolutionFactor=0,75 -yResolutionFactor=0,75 -outputDirectory=app\src\main\res\drawable-hdpi
..\TxpImageFactory\bin\TxpImageFactory.exe -configurationFile=config_music_player.xml -xResolutionFactor=0,75 -yResolutionFactor=0,75 -outputDirectory=app\src\main\res\drawable-hdpi
..\TxpImageFactory\bin\TxpImageFactory.exe -configurationFile=config_gestures.xml -xResolutionFactor=0,75 -yResolutionFactor=0,75 -outputDirectory=app\src\main\res\drawable-hdpi
..\TxpImageFactory\bin\TxpImageFactory.exe -configurationFile=config_pictograms.xml -xResolutionFactor=0,75 -yResolutionFactor=0,75 -outputDirectory=app\src\main\res\drawable-hdpi
REM xhdpi
..\TxpImageFactory\bin\TxpImageFactory.exe -configurationFile=config_white_pictograms.xml -xResolutionFactor=1 -yResolutionFactor=1 -outputDirectory=app\src\main\res\drawable-xhdpi
..\TxpImageFactory\bin\TxpImageFactory.exe -configurationFile=config_music_player.xml -xResolutionFactor=1 -yResolutionFactor=1 -outputDirectory=app\src\main\res\drawable-xhdpi
..\TxpImageFactory\bin\TxpImageFactory.exe -configurationFile=config_gestures.xml -xResolutionFactor=1 -yResolutionFactor=1 -outputDirectory=app\src\main\res\drawable-xhdpi
..\TxpImageFactory\bin\TxpImageFactory.exe -configurationFile=config_pictograms.xml -xResolutionFactor=1 -yResolutionFactor=1 -outputDirectory=app\src\main\res\drawable-xhdpi
REM xxhdpi
..\TxpImageFactory\bin\TxpImageFactory.exe -configurationFile=config_white_pictograms.xml -xResolutionFactor=1,5 -yResolutionFactor=1,5 -outputDirectory=app\src\main\res\drawable-xxhdpi
..\TxpImageFactory\bin\TxpImageFactory.exe -configurationFile=config_music_player.xml -xResolutionFactor=1,5 -yResolutionFactor=1,5 -outputDirectory=app\src\main\res\drawable-xxhdpi
..\TxpImageFactory\bin\TxpImageFactory.exe -configurationFile=config_gestures.xml -xResolutionFactor=1,5 -yResolutionFactor=1,5 -outputDirectory=app\src\main\res\drawable-xxhdpi
..\TxpImageFactory\bin\TxpImageFactory.exe -configurationFile=config_pictograms.xml -xResolutionFactor=1,5 -yResolutionFactor=1,5 -outputDirectory=app\src\main\res\drawable-xxhdpi
REM xxxhdpi
..\TxpImageFactory\bin\TxpImageFactory.exe -configurationFile=config_white_pictograms.xml -xResolutionFactor=2 -yResolutionFactor=2 -outputDirectory=app\src\main\res\drawable-xxxhdpi
..\TxpImageFactory\bin\TxpImageFactory.exe -configurationFile=config_music_player.xml -xResolutionFactor=2 -yResolutionFactor=2 -outputDirectory=app\src\main\res\drawable-xxxhdpi
..\TxpImageFactory\bin\TxpImageFactory.exe -configurationFile=config_gestures.xml -xResolutionFactor=2 -yResolutionFactor=2 -outputDirectory=app\src\main\res\drawable-xxxhdpi
..\TxpImageFactory\bin\TxpImageFactory.exe -configurationFile=config_pictograms.xml -xResolutionFactor=2 -yResolutionFactor=2 -outputDirectory=app\src\main\res\drawable-xxxhdpi
