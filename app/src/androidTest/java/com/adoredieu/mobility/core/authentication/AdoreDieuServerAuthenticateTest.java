package com.adoredieu.mobility.core.authentication;

import android.support.test.filters.SmallTest;

import com.adoredieu.mobility.core.persistence.ApplicationContextPersistence;

import org.junit.Assert;

import static android.support.test.InstrumentationRegistry.getContext;

@SmallTest
public class AdoreDieuServerAuthenticateTest {
    public void testUserSignIn() throws Exception {
        AdoreDieuServerAuthenticate adoreDieuServerAuthenticate = new AdoreDieuServerAuthenticate(
                getContext(),
                new ApplicationContextPersistence(getContext()));

        String token = adoreDieuServerAuthenticate.userSignIn("zainal",
                "$X9Z120$");
        Assert.assertFalse(token.isEmpty());
    }
}