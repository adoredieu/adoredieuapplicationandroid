package com.adoredieu.mobility.core.helpers;

import android.support.test.filters.SmallTest;

import junit.framework.Assert;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static android.support.test.InstrumentationRegistry.getContext;

@SmallTest
public class IOHelperTest {
    public void testCacheDirDelete() throws IOException {
        File cacheDir = getContext().getCacheDir();
        File file = new File(cacheDir, "test.txt");
        FileWriter fileWriter = new FileWriter(file);
        fileWriter.append(' ');
        fileWriter.close();

        Assert.assertTrue(cacheDir.list().length > 0);

        IOHelpers.deleteDir(cacheDir);
        Assert.assertFalse(cacheDir.exists());
    }

    public void testLuceneIndexDirDelete() throws IOException {
        File filesDir = getContext().getFilesDir();
        File lucene_index = new File(filesDir, "lucene_index");
        lucene_index.mkdirs();
        File file = new File(lucene_index, "test.txt");
        FileWriter fileWriter = new FileWriter(file);
        fileWriter.append(' ');
        fileWriter.close();

        Assert.assertTrue(filesDir.list().length > 0);

        IOHelpers.deleteDir(filesDir);
        Assert.assertFalse(filesDir.exists());
    }
}
