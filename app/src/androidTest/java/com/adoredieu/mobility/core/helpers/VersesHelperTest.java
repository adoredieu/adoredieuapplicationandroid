package com.adoredieu.mobility.core.helpers;

import android.support.test.filters.SmallTest;

import com.adoredieu.mobility.core.dao.BooksDao;
import com.adoredieu.mobility.core.dao.VerseDao;
import com.adoredieu.mobility.core.db.OrmBibleDatabase;
import com.adoredieu.mobility.core.interfaces.VerseDefinition;
import com.adoredieu.mobility.core.model.bible.Book;
import com.adoredieu.mobility.core.model.bible.Verse;
import com.adoredieu.mobility.core.persistence.UserPreferencesPersistence;
import com.adoredieu.mobility.core.services.BibleSearchService;
import com.adoredieu.mobility.core.services.BibleServices;

import junit.framework.Assert;

import java.util.ArrayList;
import java.util.List;

import static android.support.test.InstrumentationRegistry.getContext;

@SmallTest
public class VersesHelperTest {
    private BibleServices bibleServices;

    public void testSimpleVerse() {
        List<VerseDefinition> verses = new ArrayList<>();

        Book book = getBibleServices().getBook(1);

        verses.add(new Verse(book,
                1,
                1,
                "verse1"));

        VersesHelper.VersesText versesAsText = VersesHelper.getVersesAsText(getContext(),
                getBibleServices(),
                verses);

        Assert.assertEquals("Book1 1:1 :",
                versesAsText.getTitle());
        Assert.assertEquals("1:1 verse1\n",
                versesAsText.getContent());
    }

    public void testVerseInterval() {
        List<VerseDefinition> verses = new ArrayList<>();

        Book book = getBibleServices().getBook(1);

        verses.add(new Verse(book,
                1,
                1,
                "verse1"));
        verses.add(new Verse(book,
                1,
                2,
                "verse2"));
        verses.add(new Verse(book,
                1,
                3,
                "verse3"));

        VersesHelper.VersesText versesAsText = VersesHelper.getVersesAsText(getContext(),
                getBibleServices(),
                verses);

        Assert.assertEquals("Book1 1:1-3 :",
                versesAsText.getTitle());
        Assert.assertEquals("1:1 verse1\n"
                        + "1:2 verse2\n"
                        + "1:3 verse3\n",
                versesAsText.getContent());
    }

    public void testVerseIntervalComplex() {
        List<VerseDefinition> verses = new ArrayList<>();

        Book book = getBibleServices().getBook(1);

        verses.add(new Verse(book,
                1,
                1,
                "verse1"));
        verses.add(new Verse(book,
                1,
                2,
                "verse2"));
        verses.add(new Verse(book,
                1,
                3,
                "verse3"));
        verses.add(new Verse(book,
                1,
                5,
                "verse5"));

        VersesHelper.VersesText versesAsText = VersesHelper.getVersesAsText(getContext(),
                getBibleServices(),
                verses);

        Assert.assertEquals("Book1 1:1-3,5 :",
                versesAsText.getTitle());
        Assert.assertEquals("1:1 verse1\n"
                        + "1:2 verse2\n"
                        + "1:3 verse3\n"
                        + "[...]\n"
                        + "1:5 verse5\n",
                versesAsText.getContent());
    }

    public void testChaptersAndVerseIntervalComplex() {
        List<VerseDefinition> verses = new ArrayList<>();

        Book book = getBibleServices().getBook(1);

        verses.add(new Verse(book,
                1,
                1,
                "verse1"));
        verses.add(new Verse(book,
                1,
                2,
                "verse2"));
        verses.add(new Verse(book,
                1,
                3,
                "verse3"));
        verses.add(new Verse(book,
                1,
                5,
                "verse5"));
        verses.add(new Verse(book,
                2,
                1,
                "verse1"));
        verses.add(new Verse(book,
                2,
                2,
                "verse2"));
        verses.add(new Verse(book,
                2,
                3,
                "verse3"));
        verses.add(new Verse(book,
                2,
                5,
                "verse5"));

        VersesHelper.VersesText versesAsText = VersesHelper.getVersesAsText(getContext(),
                getBibleServices(),
                verses);

        Assert.assertEquals("Book1 1:1-3,5; 2:1-3,5 :",
                versesAsText.getTitle());
        Assert.assertEquals("1:1 verse1\n"
                        + "1:2 verse2\n"
                        + "1:3 verse3\n"
                        + "[...]\n"
                        + "1:5 verse5\n"
                        + "\n"
                        + "2:1 verse1\n"
                        + "2:2 verse2\n"
                        + "2:3 verse3\n"
                        + "[...]\n"
                        + "2:5 verse5\n",
                versesAsText.getContent());
    }

    public BibleServices getBibleServices() {
        if (bibleServices == null) {
            OrmBibleDatabase ormBibleDatabase = new OrmBibleDatabase(getContext());
            bibleServices = new BibleServices(getContext(),
                    new BooksDao(ormBibleDatabase),
                    new VerseDao(ormBibleDatabase),
                    new BibleSearchService(),
                    new UserPreferencesPersistence(getContext()));
        }

        return bibleServices;
    }
}
