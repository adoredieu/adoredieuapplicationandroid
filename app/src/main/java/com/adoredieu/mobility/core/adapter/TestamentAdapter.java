package com.adoredieu.mobility.core.adapter;

import com.adoredieu.mobility.core.dto.TestamentDto;
import com.adoredieu.mobility.core.model.bible.Testament;

public class TestamentAdapter
{
    public static TestamentDto testamentToTestamentDto(Testament testament)
    {
        TestamentDto testamentDto = new TestamentDto();
        testamentDto.setId(testament.getId());
        testamentDto.setName(testament.getName());

        return testamentDto;
    }
}
