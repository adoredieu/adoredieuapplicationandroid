package com.adoredieu.mobility.core.dto;

import org.joda.time.DateTime;

import java.util.List;

public class VersesHighlightsSynchronisationDto
{
    public final DateTime lastSynchronizationDate;
    public final Integer userId;
    public final List<HighlightedVerseDto> highlightedVerseDtos;

    public VersesHighlightsSynchronisationDto(Integer userId,
                                              DateTime lastSynchronizationDate,
                                              List<HighlightedVerseDto> highlightedVerseDtos)
    {
        this.lastSynchronizationDate = lastSynchronizationDate;
        this.userId = userId;
        this.highlightedVerseDtos = highlightedVerseDtos;
    }
}
