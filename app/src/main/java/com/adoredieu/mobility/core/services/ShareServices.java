package com.adoredieu.mobility.core.services;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.content.FileProvider;

import com.adoredieu.mobility.old.BuildConfig;
import com.adoredieu.mobility.old.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class ShareServices {
    public void share(Context context,
                      String text) {
        try {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, text);
            sendIntent.setType("text/plain");

            context.startActivity(sendIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void shareImage(Context context,
                           Bitmap bitmap) {
        try {
            File file = getLocalBitmapFile(context,
                    bitmap);
            Uri uri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", file);

            if (uri != null) {
                // Construct a ShareIntent with link to image
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
                shareIntent.setType("image/*");
                // Launch sharing dialog for image
                context.startActivity(Intent.createChooser(shareIntent,
                        context.getString(R.string.share)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private File getLocalBitmapFile(Context context,
                                    Bitmap bitmap) {
        File file = null;
        try {
            file = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                    "share_image_" + System.currentTimeMillis() + ".jpg");
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, fileOutputStream);
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }
}
