package com.adoredieu.mobility.core.interfaces;

public interface FacebookActionResultListener
{
    void onFacebookLoginSuccess();

    void onFacebookLoginFail(String message);
}
