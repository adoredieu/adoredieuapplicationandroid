package com.adoredieu.mobility.core.helpers;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.RingtoneManager;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.core.interfaces.NotificationsTags;
import com.adoredieu.mobility.view.activities.MainActivity;

import static android.content.Context.NOTIFICATION_SERVICE;

public class NotificationHelper {
    public static final String CHANNEL_ID = "ad_channel_01";
    public static final String MUSIC_PLAYER_CHANNEL_ID = "ad_channel_02";

    public static void publishTextNotification(Context context,
                                               String title,
                                               String content,
                                               String action,
                                               NotificationsTags tag) {
        try {
            Intent notificationIntent = new Intent(context,
                    MainActivity.class);
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            notificationIntent.setAction(action);

            PendingIntent intent = PendingIntent.getActivity(context,
                    0,
                    notificationIntent,
                    0);

            NotificationCompat.Builder builder =
                    new NotificationCompat.Builder(context, CHANNEL_ID)
                            .setSmallIcon(R.drawable.ic_notification)
                            .setAutoCancel(true)
                            .setContentTitle(title)
                            .setContentText(content)
                            .setStyle(new NotificationCompat.BigTextStyle().bigText(content))
                            .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                            .setContentIntent(intent); //Required on Gingerbread and below

            Notification notification = builder.build();
            notification.flags |= Notification.FLAG_AUTO_CANCEL;

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(
                    NOTIFICATION_SERVICE);
            notificationManager.notify(tag.ordinal(),
                    notification);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void publishImageNotification(Context context,
                                                String title,
                                                String text,
                                                String action,
                                                Bitmap image,
                                                NotificationsTags tag) {
        try {
            Intent notificationIntent = new Intent(context,
                    MainActivity.class);
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            notificationIntent.setAction(action);

            PendingIntent intent = PendingIntent.getActivity(context,
                    0,
                    notificationIntent,
                    0);

            NotificationCompat.BigPictureStyle notificationStyle = new NotificationCompat.BigPictureStyle();
            notificationStyle.setBigContentTitle(title);
            notificationStyle.setSummaryText(text);
            notificationStyle.bigPicture(image);

            NotificationCompat.Builder builder =
                    new NotificationCompat.Builder(context, CHANNEL_ID)
                            .setSmallIcon(R.drawable.ic_notification)
                            .setAutoCancel(true)
                            .setLargeIcon(image)
                            .setContentTitle(title)
                            .setContentText(text)
                            .setStyle(new NotificationCompat.BigTextStyle().bigText(text))
                            .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                            .setStyle(notificationStyle)
                            .setContentIntent(intent); //Required on Gingerbread and below

            Notification notification = builder.build();
            notification.flags |= Notification.FLAG_AUTO_CANCEL;

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(
                    NOTIFICATION_SERVICE);
            notificationManager.notify(tag.ordinal(),
                    notification);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void createNotificationChannel(final Context context,
                                                 final CharSequence channelName,
                                                 final String channelDescription,
                                                 final int importance,
                                                 final boolean showBadge) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                new Runnable() {
                    @Override
                    public void run() {
                        NotificationManager notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
                        NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, channelName, importance);
                        notificationChannel.setDescription(channelDescription);
                        notificationChannel.setShowBadge(showBadge);

                        notificationManager.createNotificationChannel(notificationChannel);

                    }
                }.run();
            }
        } catch (Throwable t) {
            t.printStackTrace();
        }

    }

    public static void createMusicPlayerNotificationChannel(final Context context,
                                                            final CharSequence channelName,
                                                            final String channelDescription,
                                                            final int importance,
                                                            final boolean showBadge) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                new Runnable() {
                    @Override
                    public void run() {
                        NotificationManager notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
                        NotificationChannel notificationChannel = new NotificationChannel(MUSIC_PLAYER_CHANNEL_ID, channelName, importance);
                        notificationChannel.setDescription(channelDescription);
                        notificationChannel.setShowBadge(showBadge);

                        notificationManager.createNotificationChannel(notificationChannel);

                    }
                }.run();
            }
        } catch (Throwable t) {
            t.printStackTrace();
        }

    }
}
