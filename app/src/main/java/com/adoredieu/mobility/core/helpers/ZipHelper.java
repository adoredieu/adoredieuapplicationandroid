package com.adoredieu.mobility.core.helpers;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;

import java.io.File;

public class ZipHelper
{
    /**
     * Unzip it
     *
     * @param zipFilename  input zip file
     * @param outputFolder zip file output folder
     */
    public static void unzip(File zipFilename,
                             String outputFolder,
                             String password)
    {
        try
        {
            ZipFile zipFile = new ZipFile(zipFilename);
            if (zipFile.isEncrypted())
            {
                zipFile.setPassword(password);
            }
            zipFile.extractAll(outputFolder);
        }
        catch (ZipException e)
        {
            e.printStackTrace();
        }
    }
}
