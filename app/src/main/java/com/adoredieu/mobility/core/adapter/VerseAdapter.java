package com.adoredieu.mobility.core.adapter;

import com.adoredieu.mobility.core.dto.VerseDto;
import com.adoredieu.mobility.core.model.bible.Book;
import com.adoredieu.mobility.core.model.bible.Verse;

import java.util.ArrayList;
import java.util.List;

public class VerseAdapter
{
    private static VerseDto verseToVerseDto(Verse verse)
    {
        VerseDto verseDto = new VerseDto();
        verseDto.setId(verse.getId());
        verseDto.setVerse(verse.getVerse());
        verseDto.setChapter(verse.getChapter());
        Book book = verse.getBook();
        if (book != null)
        {
            verseDto.setBook(BookAdapter.bookToBookDto(book));
        }
        verseDto.setText(verse.getText());

        return verseDto;
    }

    public static List<VerseDto> versesToVerseDtos(List<Verse> verses)
    {
        List<VerseDto> verseDtos = new ArrayList<>();
        for (int i = 0; i < verses.size(); i++)
        {
            verseDtos.add(verseToVerseDto(verses.get(i)));
        }

        return verseDtos;
    }
}
