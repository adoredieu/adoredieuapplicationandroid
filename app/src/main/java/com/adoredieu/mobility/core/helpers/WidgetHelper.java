package com.adoredieu.mobility.core.helpers;

import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;

import com.adoredieu.mobility.controllers.widget.BibleVerseWidgetProvider;

public class WidgetHelper
{
    public static void refreshWidget(Context context)
    {
        Intent intent = new Intent(context,
                                   BibleVerseWidgetProvider.class);
        intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        // Use an array and EXTRA_APPWIDGET_IDS instead of AppWidgetManager.EXTRA_APPWIDGET_ID,
        // since it seems the onUpdate() is only fired on that:
        int ids[] = AppWidgetManager.getInstance(context)
                                    .getAppWidgetIds(new ComponentName(context,
                                                                       BibleVerseWidgetProvider.class));
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS,
                        ids);
        context.sendBroadcast(intent);
    }
}
