package com.adoredieu.mobility.core.model.website;

import android.graphics.Bitmap;
import android.net.Uri;
import android.support.annotation.Nullable;

public class AudioTrack
{
    public final String artist;
    public final String title;
    public final Uri fileUri;
    public final String coverUri;
    @Nullable
    public Bitmap cover;

    public AudioTrack(String artist,
                      String title,
                      Uri fileUri,
                      String coverUri)
    {
        this.artist = artist;
        this.title = title;
        this.fileUri = fileUri;
        this.coverUri = coverUri;
        this.cover = null;
    }
}
