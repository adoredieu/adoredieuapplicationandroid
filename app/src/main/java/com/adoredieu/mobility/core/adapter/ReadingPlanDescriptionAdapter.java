package com.adoredieu.mobility.core.adapter;

import com.adoredieu.mobility.core.dto.ReadingPlanDescriptionDto;
import com.adoredieu.mobility.core.model.bible.ReadingPlanDescription;

import java.util.ArrayList;
import java.util.List;

public class ReadingPlanDescriptionAdapter
{
    public static ReadingPlanDescriptionDto readingPlanDescriptionToReadingPlanDescriptionDto(ReadingPlanDescription readingPlanDescription)
    {
        return new ReadingPlanDescriptionDto(
                readingPlanDescription.get_id(),
                readingPlanDescription.getName(),
                readingPlanDescription.getDescription());
    }

    public static List<ReadingPlanDescriptionDto> readingPlanDescriptionsToReadingPlanDescriptionDtos(List<ReadingPlanDescription> readingPlanDescriptions)
    {
        List<ReadingPlanDescriptionDto> readingPlanDescriptionDtos = new ArrayList<>();
        for (int i = 0; i < readingPlanDescriptions.size(); i++)
        {
            readingPlanDescriptionDtos.add(readingPlanDescriptionToReadingPlanDescriptionDto(
                    readingPlanDescriptions.get(i)));
        }

        return readingPlanDescriptionDtos;
    }
}

