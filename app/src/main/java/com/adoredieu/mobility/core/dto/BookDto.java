package com.adoredieu.mobility.core.dto;

import android.os.Parcel;
import android.os.Parcelable;

import com.adoredieu.mobility.core.model.bible.Book;
import com.adoredieu.mobility.core.model.bible.Testament;

public class BookDto
        implements Parcelable
{
    public static final Creator<BookDto> CREATOR = new Creator<BookDto>()
    {
        public BookDto createFromParcel(Parcel source)
        {
            return new BookDto(source);
        }

        public BookDto[] newArray(int size)
        {
            return new BookDto[size];
        }
    };
    private Integer id;
    private TestamentDto testament;

    public BookDto()
    {
    }

    public BookDto(Integer id,
                   TestamentDto testament)
    {
        this.id = id;
        this.testament = testament;
    }

    private BookDto(Parcel in)
    {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.testament = in.readParcelable(Testament.class.getClassLoader());
    }

    public TestamentDto getTestament()
    {
        return testament;
    }

    public void setTestament(TestamentDto testament)
    {
        this.testament = testament;
    }

    public int getId()
    {
        return id;
    }

    public void setId(Integer _id)
    {
        this.id = _id;
    }

    @Override

    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest,
                              int flags)
    {
        dest.writeValue(this.id);
        dest.writeParcelable(this.testament,
                             flags);
    }

    public static BookDto fromBook(Book book)
    {
        return new BookDto(book.getId(),
                           TestamentDto.fromTestament(book.getTestament()));
    }
}
