package com.adoredieu.mobility.core.helpers;

public class TimeHelper
{
    public static void TryWait(long time)
    {
        try
        {
            Thread.sleep(time);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }
}
