package com.adoredieu.mobility.core.dao;

import com.adoredieu.mobility.core.db.OrmBibleDatabase;
import com.adoredieu.mobility.core.dto.StrongDto;
import com.adoredieu.mobility.core.model.bible.Greek;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;

import javax.inject.Inject;

public class GreekDao
{
    private final RuntimeExceptionDao<Greek, Integer> dao;

    @Inject
    public GreekDao(OrmBibleDatabase db)
    {
        dao = db.getGreekDao();
    }

    public StrongDto getStrong(int strongCode)
    {
        try
        {
            QueryBuilder<Greek, Integer> queryBuilder = dao.queryBuilder();
            queryBuilder.where()
                        .eq(Greek.GREEK_ID_COLUMN,
                            strongCode);
            return StrongDto.fromGreek(queryBuilder.query()
                                                   .get(0));
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return null;
        }
    }
}
