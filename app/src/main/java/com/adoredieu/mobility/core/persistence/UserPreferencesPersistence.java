package com.adoredieu.mobility.core.persistence;

import android.content.Context;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.core.helpers.LocalTimeHelper;

import org.joda.time.LocalTime;

import javax.inject.Inject;

public class UserPreferencesPersistence
{
    private final TinyDbOverSharedPreferences tinyDbOverSharedPreferences;

    @Inject
    public UserPreferencesPersistence(Context context)
    {
        tinyDbOverSharedPreferences = new TinyDbOverSharedPreferences(
                context,
                context.getString(R.string.app_name));
    }

    public boolean getKeepScreenOnPreference()
    {
        return tinyDbOverSharedPreferences.getBoolean("KeepScreenOn",
                                                      true);
    }

    public void setKeepScreenOnPreference(boolean value)
    {
        tinyDbOverSharedPreferences.putBoolean("KeepScreenOn",
                                               value);
    }

    public boolean getQuoteOfTheDayNotificationEnabledPreference()
    {
        return tinyDbOverSharedPreferences.getBoolean("CitationNotificationEnabled",
                                                      false);
    }

    public void setQuoteOfTheDayNotificationEnabledPreference(boolean value)
    {
        tinyDbOverSharedPreferences.putBoolean("CitationNotificationEnabled",
                                               value);
    }

    public boolean getImageQuoteOfTheDayNotificationEnabledPreference()
    {
        return tinyDbOverSharedPreferences.getBoolean("CitationImageOfTheDayNotificationEnabled",
                                                      false);
    }

    public void setImageQuoteOfTheDayNotificationEnabledPreference(boolean value)
    {
        tinyDbOverSharedPreferences.putBoolean("CitationImageOfTheDayNotificationEnabled",
                                               value);
    }

    public boolean getDailyVerseNotificationEnabledPreference()
    {
        return tinyDbOverSharedPreferences.getBoolean("DailyVerseNotificationEnabled",
                                                      true);
    }

    public void setDailyVerseNotificationEnabledPreference(boolean value)
    {
        tinyDbOverSharedPreferences.putBoolean("DailyVerseNotificationEnabled",
                                               value);
    }

    public boolean getThoughtsNotificationEnabledPreference()
    {
        return tinyDbOverSharedPreferences.getBoolean("ThoughtsNotificationEnabled",
                                                      true);
    }

    public void setThoughtsNotificationEnabledPreference(boolean value)
    {
        tinyDbOverSharedPreferences.putBoolean("ThoughtsNotificationEnabled",
                                               value);
    }

    public boolean getTeachingsNotificationEnabledPreference()
    {
        return tinyDbOverSharedPreferences.getBoolean("TeachingsNotificationEnabled",
                                                      false);
    }

    public void setTeachingsNotificationEnabledPreference(boolean value)
    {
        tinyDbOverSharedPreferences.putBoolean("TeachingsNotificationEnabled",
                                               value);
    }

    public boolean getFullscreenPreference()
    {
        return tinyDbOverSharedPreferences.getBoolean("Fullscreen",
                                                      false);
    }

    public void setFullscreenPreference(boolean value)
    {
        tinyDbOverSharedPreferences.putBoolean("Fullscreen",
                                               value);
    }

    public Integer getMaxSearchResults()
    {
        return tinyDbOverSharedPreferences.getInt("max_search_result",
                                                  250);
    }

    public void setMaxSearchResults(int value)
    {
        tinyDbOverSharedPreferences.putInt("max_search_result",
                                           value);
    }

    public int getWebViewFontSize()
    {
        return tinyDbOverSharedPreferences.getInt("web_view_font_size",
                                                  16);
    }

    public void setWebViewFontSize(int fontSize)
    {
        tinyDbOverSharedPreferences.putInt("web_view_font_size",
                                           fontSize);
    }

    public boolean getAudioPlayerRandomState()
    {
        return tinyDbOverSharedPreferences.getBoolean("AudioPlayerRandomState",
                                                      false);
    }

    public void setAudioPlayerRandomState(boolean randomState)
    {
        tinyDbOverSharedPreferences.putBoolean("AudioPlayerRandomState",
                                               randomState);
    }

    public boolean getNightMode()
    {
        return tinyDbOverSharedPreferences.getBoolean("NightMode",
                                                      false);
    }

    public void setNightMode(boolean nightMode)
    {
        tinyDbOverSharedPreferences.putBoolean("NightMode",
                                               nightMode);
    }

    public LocalTime getNotificationQuietStartTime()
    {
        String dateString = tinyDbOverSharedPreferences.getString("NotificationQuietStartTime");
        if (dateString.isEmpty())
        {
            dateString = "22:30:00";
        }

        return LocalTimeHelper.parse(dateString);

    }

    public void setNotificationQuietStartTime(LocalTime time)
    {
        tinyDbOverSharedPreferences.putString("NotificationQuietStartTime",
                                              LocalTimeHelper.toString(time));
    }

    public LocalTime getNotificationQuietEndTime()
    {
        String dateString = tinyDbOverSharedPreferences.getString("NotificationQuietEndTime");
        if (dateString.isEmpty())
        {
            dateString = "08:00:00";
        }

        return LocalTimeHelper.parse(dateString);
    }

    public void setNotificationQuietEndTime(LocalTime time)
    {
        tinyDbOverSharedPreferences.putString("NotificationQuietEndTime",
                                              LocalTimeHelper.toString(time));
    }

    public boolean getStrongsMode()
    {
        return tinyDbOverSharedPreferences.getBoolean("StrongsMode",
                                                      false);
    }

    public void setStrongsMode(boolean mode)
    {
        tinyDbOverSharedPreferences.putBoolean("StrongsMode",
                                               mode);
    }

    public Integer getReadingPlanIndex()
    {
        return tinyDbOverSharedPreferences.getInt("ReadingPlanIndex",
                                                  0);
    }

    public void setReadingPlanIndex(Integer readingPlanIndex)
    {
        tinyDbOverSharedPreferences.putInt("ReadingPlanIndex",
                                           readingPlanIndex);
    }

    public Integer getReadingPlanCurrentDay()
    {
        return tinyDbOverSharedPreferences.getInt("ReadingPlanCurrentDay",
                                                  1);
    }

    public void setReadingPlanCurrentDay(Integer readingPlanCurrentDay)
    {
        tinyDbOverSharedPreferences.putInt("ReadingPlanCurrentDay",
                                           readingPlanCurrentDay);
    }
}
