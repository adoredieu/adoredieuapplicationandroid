package com.adoredieu.mobility.core.dao;

import com.adoredieu.mobility.core.db.OrmBibleDatabase;
import com.adoredieu.mobility.core.model.bible.ReadingPlanEntry;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.List;

import javax.inject.Inject;

public class ReadingPlanEntriesDao
{
    private final RuntimeExceptionDao<ReadingPlanEntry, Integer> dao;

    @Inject
    public ReadingPlanEntriesDao(OrmBibleDatabase db)
    {
        dao = db.getReadingPlanEntriesDaoDao();
    }

    public List<ReadingPlanEntry> getReadingPlanEntriesForDay(Integer readingPlanIndex,
                                                              Integer readingPlanCurrentDay)
    {
        try
        {
            QueryBuilder<ReadingPlanEntry, Integer> queryBuilder = dao.queryBuilder();
            queryBuilder.where()
                        .eq(ReadingPlanEntry.READING_PLAN_ID_COLUMN,
                            readingPlanIndex)
                        .and()
                        .eq(ReadingPlanEntry.DAY_NUMBER_COLUMN,
                            readingPlanCurrentDay);
            PreparedQuery<ReadingPlanEntry> preparedQuery = queryBuilder.prepare();

            return dao.query(preparedQuery);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public long getDaysCount(Integer readingPlanIndex)
    {
        GenericRawResults<String[]> countQueryResult = dao.queryRaw(
                "SELECT MAX(" + ReadingPlanEntry.DAY_NUMBER_COLUMN + ") FROM " + ReadingPlanEntry.TABLE_NAME
                + " WHERE " + ReadingPlanEntry.READING_PLAN_ID_COLUMN + " = ?",
                readingPlanIndex.toString());
        long result = 0;

        try
        {
            String count = countQueryResult.getResults()
                                           .get(0)[0];
            result = Long.parseLong(count);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return result;
    }
}
