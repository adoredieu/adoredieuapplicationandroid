package com.adoredieu.mobility.core.services;

import android.content.Context;
import android.os.AsyncTask;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.core.helpers.AsyncTaskHelper;
import com.adoredieu.mobility.core.helpers.WebHelper;
import com.adoredieu.mobility.core.interfaces.Action;
import com.adoredieu.mobility.core.interfaces.DateProvider;
import com.adoredieu.mobility.core.persistence.ApplicationContextPersistence;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

public class QuoteOfTheDayServices {
    private final Context context;
    private final ApplicationContextPersistence applicationContextPersistence;
    private final DateProvider dateProvider;

    @Inject
    public QuoteOfTheDayServices(Context context,
                                 ApplicationContextPersistence applicationContextPersistence,
                                 DateProvider dateProvider) {
        this.context = context;
        this.applicationContextPersistence = applicationContextPersistence;
        this.dateProvider = dateProvider;
    }

    public void getQuoteOfTheDay(final Action<String> onFinished,
                                 Runnable onError,
                                 DateTime dateTime) {
        if (dateTime.toLocalDate()
                .isEqual(dateProvider.getDateTime()
                        .toLocalDate())) {
            DateTime quoteOfTheDayCacheDate = applicationContextPersistence.getQuoteOfTheDayCacheDate();

            if (quoteOfTheDayCacheDate == null
                    || quoteOfTheDayCacheDate.toLocalDate()
                    .isBefore(dateProvider.getDateTime()
                            .toLocalDate())
                    || applicationContextPersistence.getQuoteOfTheDayCache()
                    .isEmpty()) {
                DownloadQuoteOfTheDayTask downloadQuoteOfTheDayTask = new DownloadQuoteOfTheDayTask(
                        context,
                        dateTime,
                        new Action<String>() {
                            @Override
                            public void run(String result) {
                                applicationContextPersistence.setQuoteOfTheDayCacheDate(
                                        dateProvider.getDateTime());
                                applicationContextPersistence.setQuoteOfTheDayCache(result);
                                onFinished.run(result);
                            }
                        },
                        onError);
                AsyncTaskHelper.<Void>executeAsyncTask(downloadQuoteOfTheDayTask);
            } else {
                onFinished.run(applicationContextPersistence.getQuoteOfTheDayCache());
            }
        } else {
            DownloadQuoteOfTheDayTask downloadQuoteOfTheDayTask = new DownloadQuoteOfTheDayTask(
                    context,
                    dateTime,
                    onFinished,
                    onError);
            AsyncTaskHelper.<Void>executeAsyncTask(downloadQuoteOfTheDayTask);
        }
    }

    private static class DownloadQuoteOfTheDayTask
            extends AsyncTask<Void, Void, String> {
        private final WeakReference<Context> contextWeakReference;
        private final DateTime dateTime;
        private final Action<String> onFinished;
        private final Runnable onError;

        public DownloadQuoteOfTheDayTask(Context context,
                                         DateTime dateTime,
                                         Action<String> onFinished,
                                         Runnable onError) {
            this.contextWeakReference = new WeakReference<>(context);
            this.dateTime = dateTime;
            this.onFinished = onFinished;
            this.onError = onError;
        }

        @Override
        protected String doInBackground(Void... voids) {
            Context context = contextWeakReference.get();
            if (context == null)
                return "";

            DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd");

            String website_address = context.getString(R.string.citation_www_address,
                    fmt.print(dateTime));

            return WebHelper.downloadStringBlocking(context,
                    website_address);
        }

        @Override
        protected void onPostExecute(String result) {
            if (result.isEmpty()) {
                onError.run();
            } else {
                onFinished.run(result);
            }
        }
    }
}
