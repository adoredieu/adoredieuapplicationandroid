package com.adoredieu.mobility.core.services;

import android.content.Context;
import android.os.AsyncTask;
import android.util.TimingLogger;

import com.adoredieu.mobility.core.dao.BooksDao;
import com.adoredieu.mobility.core.dao.VerseDao;
import com.adoredieu.mobility.core.helpers.AsyncTaskHelper;
import com.adoredieu.mobility.core.helpers.ResourcesHelper;
import com.adoredieu.mobility.core.interfaces.Action;
import com.adoredieu.mobility.core.interfaces.SearchOperator;
import com.adoredieu.mobility.core.interfaces.VerseDefinition;
import com.adoredieu.mobility.core.model.bible.Book;
import com.adoredieu.mobility.core.model.bible.Verse;
import com.adoredieu.mobility.core.persistence.UserPreferencesPersistence;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class BibleServices {
    private final BooksDao booksDao;
    private final VerseDao verseDao;
    private final BibleSearchService bibleSearchService;
    private final UserPreferencesPersistence userPreferencesPersistence;
    private final Context context;
    private List<String> cachedLocalizedBooksNames = null;

    @Inject
    public BibleServices(Context context,
                         BooksDao booksDao,
                         VerseDao verseDao,
                         BibleSearchService bibleSearchService,
                         UserPreferencesPersistence userPreferencesPersistence) {
        this.context = context;
        this.booksDao = booksDao;
        this.verseDao = verseDao;
        this.bibleSearchService = bibleSearchService;
        this.userPreferencesPersistence = userPreferencesPersistence;
    }

    public List<VerseDefinition> getChapter(long book,
                                            long chapter) {
        List<Verse> chapterContent = verseDao.getChapter(book,
                chapter);

        return new ArrayList<VerseDefinition>(chapterContent);
    }

    public long getChaptersCount(long book) {
        return verseDao.getChaptersCount(book);
    }

    public List<Book> getBooks() {
        return booksDao.getBooks();
    }

    public List<String> getLocalizedBooksNames() {
        if (cachedLocalizedBooksNames == null) {
            cachedLocalizedBooksNames = new ArrayList<>();
            cachedLocalizedBooksNames.add("");

            List<Book> books = getBooks();

            for (Book book : books) {
                cachedLocalizedBooksNames.add(ResourcesHelper.getStringResourceByName(context,
                        book.getName()));
            }
        }

        return cachedLocalizedBooksNames;
    }

    public Book getBook(int bookId) {
        return booksDao.getBook(bookId);
    }

    public void startSearch(String searchQuery,
                            SearchOperator searchOperator,
                            Book bookFilter,
                            Action<List<BibleSearchService.HighlightedHit>> onFinished) {
        SearchAsyncTask searchAsyncTask = new SearchAsyncTask(
                context,
                searchQuery,
                searchOperator,
                bookFilter,
                verseDao,
                bibleSearchService,
                userPreferencesPersistence,
                onFinished);

        AsyncTaskHelper.<Void>executeAsyncTask(searchAsyncTask);
    }

    private static class SearchAsyncTask
            extends AsyncTask<Void, Void, List<BibleSearchService.HighlightedHit>> {
        private final WeakReference<Context> contextWeakReference;
        private final String searchQuery;
        private final SearchOperator searchOperator;
        private final Book bookFilter;
        private final Action<List<BibleSearchService.HighlightedHit>> onFinished;
        private final VerseDao verseDao;
        private final BibleSearchService bibleSearchService;
        private final UserPreferencesPersistence userPreferencesPersistence;

        public SearchAsyncTask(Context context,
                               String searchQuery,
                               SearchOperator searchOperator,
                               Book bookFilter,
                               VerseDao verseDao,
                               BibleSearchService bibleSearchService,
                               UserPreferencesPersistence userPreferencesPersistence,
                               Action<List<BibleSearchService.HighlightedHit>> onFinished) {
            this.contextWeakReference = new WeakReference<>(context);
            this.searchQuery = searchQuery;
            this.searchOperator = searchOperator;
            this.bookFilter = bookFilter;
            this.onFinished = onFinished;
            this.verseDao = verseDao;
            this.bibleSearchService = bibleSearchService;
            this.userPreferencesPersistence = userPreferencesPersistence;
        }

        @Override
        protected List<BibleSearchService.HighlightedHit> doInBackground(Void... voids) {
            TimingLogger timings = new TimingLogger("PERFORMANCE", "BibleSearchService : search");
            try {
                List<BibleSearchService.HighlightedHit> highlightedHits = bibleSearchService.search(
                        searchQuery,
                        searchOperator,
                        searchOperator == SearchOperator.EXACTLY ? 0 : userPreferencesPersistence.getMaxSearchResults(),
                        bookFilter);
                timings.addSplit("query executed");

                timings.dumpToLog();
                return highlightedHits;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(List<BibleSearchService.HighlightedHit> highlightedHits) {
            onFinished.run(highlightedHits);
        }
    }
}
