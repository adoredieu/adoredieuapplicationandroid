package com.adoredieu.mobility.core.helpers;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;

import com.adoredieu.mobility.core.interfaces.Action;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.ProgressCallback;

import java.io.File;
import java.net.HttpURLConnection;
import java.net.URL;

public class WebHelper {
    private static final String UTF8_BOM = "\uFEFF";

    private static String removeUTF8BOM(String s) {
        if (s.startsWith(UTF8_BOM)) {
            s = s.substring(1);
        }
        return s;
    }

    public static void openUrlInBrowser(Context context,
                                        String uri) {
        try {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                    Uri.parse(uri));
            context.startActivity(browserIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void downloadFile(Context context,
                                    String uri,
                                    final ProgressCallback progressCallback,
                                    final Action<File> onFinished) {
        if (NetworkHelper.isNetworkAvailable(context)) {
            try {
                String destination = context.getExternalCacheDir() + uri.substring(uri.lastIndexOf(
                        "/"));
                Ion.with(context)
                        .load(uri)
                        .noCache()
                        .progress(progressCallback)
                        .write(new File(destination))
                        .setCallback(new FutureCallback<File>() {
                            @Override
                            public void onCompleted(Exception e,
                                                    File result) {
                                onFinished.run(result);
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void downloadJSonObject(Context context,
                                          String uri,
                                          final Action<JsonObject> onFinished) {
        if (NetworkHelper.isNetworkAvailable(context)) {
            try {
                Ion.with(context)
                        .load(uri)
                        .noCache()
                        .asJsonObject()
                        .setCallback(new FutureCallback<JsonObject>() {
                            @Override
                            public void onCompleted(Exception e,
                                                    JsonObject result) {
                                onFinished.run(result);
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static JsonObject downloadJSonObjectBlocking(Context context,
                                                        String uri) {
        if (!NetworkHelper.isNetworkAvailable(context)) {
            return null;
        }

        try {
            return Ion.with(context)
                    .load(uri)
                    .noCache()
                    .asJsonObject().get();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void downloadJSonArray(Context context,
                                         String uri,
                                         final Action<JsonArray> onFinished) {
        try {
            Ion.with(context)
                    .load(uri)
                    .noCache()
                    .asJsonArray()
                    .setCallback(new FutureCallback<JsonArray>() {
                        @Override
                        public void onCompleted(Exception e,
                                                JsonArray result) {
                            onFinished.run(result);
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static JsonArray downloadJSonArrayBlocking(Context context,
                                                      String uri) {
        if (!NetworkHelper.isNetworkAvailable(context)) {
            return null;
        }

        try {
            return Ion.with(context)
                    .load(uri)
                    .noCache()
                    .asJsonArray().get();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String downloadStringBlocking(Context context,
                                                String uri) {
        if (!NetworkHelper.isNetworkAvailable(context)) {
            return "";
        }

        try {
            String result = Ion.with(context)
                    .load(uri)
                    .noCache()
                    .asString()
                    .get();

            return removeUTF8BOM(result).trim();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static void downloadString(Context context,
                                      String uri,
                                      final Action<String> onFinished) {
        if (NetworkHelper.isNetworkAvailable(context)) {
            try {
                Ion.with(context)
                        .load(uri)
                        .noCache()
                        .asString()
                        .setCallback(new FutureCallback<String>() {
                            @Override
                            public void onCompleted(Exception e,
                                                    String result) {
                                onFinished.run(result);
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void downloadImage(Context context,
                                     String uri,
                                     final Action<Bitmap> onFinished) {
        if (NetworkHelper.isNetworkAvailable(context)) {
            try {
                Ion.with(context)
                        .load(uri)
                        .asBitmap()
                        .setCallback(new FutureCallback<Bitmap>() {
                            @Override
                            public void onCompleted(Exception e,
                                                    Bitmap result) {
                                onFinished.run(result);
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static boolean isURLReachable(Uri uri) {
        HttpURLConnection httpUrlConn;
        try {
            httpUrlConn = (HttpURLConnection) new URL(uri.toString()).openConnection();

            // A HEAD request is just like a GET request, except that it asks
            // the server to return the response headers only, and not the
            // actual resource (i.e. no message body).
            // This is useful to check characteristics of a resource without
            // actually downloading it,thus saving bandwidth. Use HEAD when
            // you don't actually need a file's contents.
            httpUrlConn.setRequestMethod("HEAD");

            // Set timeouts in milliseconds
            httpUrlConn.setConnectTimeout(30000);
            httpUrlConn.setReadTimeout(30000);

            // Print HTTP status code/message for your information.
            System.out.println("Response Code: "
                    + httpUrlConn.getResponseCode());
            System.out.println("Response Message: "
                    + httpUrlConn.getResponseMessage());

            return (httpUrlConn.getResponseCode() == HttpURLConnection.HTTP_OK);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            return false;
        }
    }
}
