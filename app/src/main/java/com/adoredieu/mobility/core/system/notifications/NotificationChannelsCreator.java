package com.adoredieu.mobility.core.system.notifications;

import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;

import com.adoredieu.mobility.core.helpers.NotificationHelper;

public class NotificationChannelsCreator {
    public static void create(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationHelper.createNotificationChannel(context,
                    "Verset du jour, pensées, enseignements",
                    "Notifications des verset du jour, pensées, enseignements.",
                    NotificationManager.IMPORTANCE_DEFAULT,
                    true);

            NotificationHelper.createMusicPlayerNotificationChannel(context,
                    "Lecteur de musique",
                    "Notification du lecteur audio",
                    NotificationManager.IMPORTANCE_LOW,
                    true);
        }
    }
}
