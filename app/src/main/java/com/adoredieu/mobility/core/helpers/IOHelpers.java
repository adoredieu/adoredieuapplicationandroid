package com.adoredieu.mobility.core.helpers;

import java.io.File;

public class IOHelpers
{
    public static boolean deleteDir(File dir)
    {
        if (dir == null)
        {
            return false;
        }

        if (!dir.exists())
        {
            return true;
        }

        if (dir.isDirectory())
        {
            String[] children = dir.list();
            if (children != null)
            {
                for (String aChildren : children)
                {
                    boolean success = deleteDir(new File(dir,
                                                         aChildren));
                    if (!success)
                    {
                        return false;
                    }
                }
            }
        }

        // The directory is now empty so delete it
        return dir.delete();
    }
}
