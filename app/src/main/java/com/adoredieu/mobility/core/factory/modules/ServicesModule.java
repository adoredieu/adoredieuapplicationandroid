package com.adoredieu.mobility.core.factory.modules;

import android.content.Context;

import com.adoredieu.mobility.core.dao.BooksDao;
import com.adoredieu.mobility.core.dao.GreekDao;
import com.adoredieu.mobility.core.dao.HebrewDao;
import com.adoredieu.mobility.core.dao.VerseDao;
import com.adoredieu.mobility.core.dao.VerseStrongDao;
import com.adoredieu.mobility.core.dao.VersesHighlightsDao;
import com.adoredieu.mobility.core.db.BibleDatabase;
import com.adoredieu.mobility.core.db.OrmBibleDatabase;
import com.adoredieu.mobility.core.helpers.StrongsHelper;
import com.adoredieu.mobility.core.interfaces.DateProvider;
import com.adoredieu.mobility.core.persistence.ApplicationContextPersistence;
import com.adoredieu.mobility.core.persistence.UserPreferencesPersistence;
import com.adoredieu.mobility.core.services.AdoreDieuServices;
import com.adoredieu.mobility.core.services.ArticleSearchServices;
import com.adoredieu.mobility.core.services.ArticleServices;
import com.adoredieu.mobility.core.services.AudioTracksServices;
import com.adoredieu.mobility.core.services.BibleInstallationServices;
import com.adoredieu.mobility.core.services.BibleSearchService;
import com.adoredieu.mobility.core.services.BibleServices;
import com.adoredieu.mobility.core.services.BibleStrongServices;
import com.adoredieu.mobility.core.services.ClipboardServices;
import com.adoredieu.mobility.core.services.FacebookServices;
import com.adoredieu.mobility.core.services.FavouriteServices;
import com.adoredieu.mobility.core.services.HighlightedVersesServices;
import com.adoredieu.mobility.core.services.ImageQuotesServices;
import com.adoredieu.mobility.core.services.MusicServices;
import com.adoredieu.mobility.core.services.QuoteOfTheDayServices;
import com.adoredieu.mobility.core.services.ShareServices;
import com.adoredieu.mobility.core.services.VerseOfTheDayServices;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ServicesModule {
    @Provides
    @Singleton
    public ShareServices provideShareServices() {
        return new ShareServices();
    }

    @Provides
    @Singleton
    public ClipboardServices provideClipboardServices() {
        return new ClipboardServices();
    }

    @Provides
    @Singleton
    public StrongsHelper provideStrongsHelper(HebrewDao hebrewDao,
                                              GreekDao greekDao) {
        return new StrongsHelper(hebrewDao,
                greekDao);
    }

    @Provides
    @Singleton
    public ArticleSearchServices provideArticleSearchServices(Context context) {
        return new ArticleSearchServices(context);
    }

    @Provides
    @Singleton
    public ArticleServices provideArticleServices(Context context) {
        return new ArticleServices(context);
    }

    @Provides
    @Singleton
    public AudioTracksServices provideAudioServices(Context context) {
        return new AudioTracksServices(context);
    }

    @Provides
    @Singleton
    public MusicServices provideMusicServices(Context context) {
        return new MusicServices(context);
    }

    @Provides
    @Singleton
    public BibleSearchService provideBibleSearchService(BibleDatabase bibleDatabase) {
        return new BibleSearchService(bibleDatabase);
    }

    @Provides
    @Singleton
    public BibleInstallationServices provideBibleDownloadServices(ApplicationContextPersistence applicationContextPersistence) {
        return new BibleInstallationServices(applicationContextPersistence);
    }

    @Provides
    @Singleton
    public BibleServices provideBibleServices(Context context,
                                              BooksDao booksDao,
                                              VerseDao verseDao,
                                              BibleSearchService bibleSearchService,
                                              UserPreferencesPersistence userPreferencesPersistence) {
        return new BibleServices(context,
                booksDao,
                verseDao,
                bibleSearchService,
                userPreferencesPersistence);
    }

    @Provides
    @Singleton
    public BibleStrongServices provideBibleStrongServices(BooksDao booksDao,
                                                          VerseStrongDao verseStrongDao) {
        return new BibleStrongServices(booksDao,
                verseStrongDao);
    }

    @Provides
    @Singleton
    public ImageQuotesServices provideImageQuotesServices(Context context) {
        return new ImageQuotesServices(context);
    }

    @Provides
    @Singleton
    public QuoteOfTheDayServices provideQuoteOfTheDayServices(Context context,
                                                              ApplicationContextPersistence applicationContextPersistence,
                                                              DateProvider dateProvider) {
        return new QuoteOfTheDayServices(context,
                applicationContextPersistence,
                dateProvider);
    }

    @Provides
    @Singleton
    public AdoreDieuServices provideAdoreDieuServices(Context context,
                                                      ApplicationContextPersistence applicationContextPersistence) {
        return new AdoreDieuServices(context,
                applicationContextPersistence);
    }

    @Provides
    @Singleton
    public FacebookServices provideFacebookServices(Context context) {
        return new FacebookServices(context);
    }

    @Provides
    @Singleton
    public VerseOfTheDayServices provideVerseOfTheDayServices(Context context,
                                                              ApplicationContextPersistence applicationContextPersistence,
                                                              DateProvider dateProvider) {
        return new VerseOfTheDayServices(context,
                applicationContextPersistence,
                dateProvider);
    }

    @Provides
    @Singleton
    public HighlightedVersesServices provideHighlightedVersesServices(VersesHighlightsDao versesHighlightsDao) {
        return new HighlightedVersesServices(versesHighlightsDao);
    }

    @Provides
    @Singleton
    public FavouriteServices provideFavouriteServices(Context context,
                                                      ApplicationContextPersistence applicationContextPersistence) {
        return new FavouriteServices(context,
                applicationContextPersistence);
    }
}
