package com.adoredieu.mobility.core.authentication;

import android.accounts.AbstractAccountAuthenticator;
import android.accounts.Account;
import android.accounts.AccountAuthenticatorResponse;
import android.accounts.AccountManager;
import android.accounts.AuthenticatorException;
import android.accounts.NetworkErrorException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import com.adoredieu.mobility.core.interfaces.authentication.ServerAuthenticate;
import com.adoredieu.mobility.view.activities.AuthenticatorActivity;

import javax.inject.Inject;

public class AdoreDieuAccountAuthenticator
        extends AbstractAccountAuthenticator
{
    private final Context context;
    private final ServerAuthenticate serverAuthenticate;

    @Inject
    public AdoreDieuAccountAuthenticator(Context context,
                                         ServerAuthenticate serverAuthenticate)
    {
        super(context);
        this.context = context;
        this.serverAuthenticate = serverAuthenticate;
    }

    @Override
    public Bundle editProperties(AccountAuthenticatorResponse response,
                                 String accountType)
    {
        return null;
    }

    @Override
    public Bundle addAccount(AccountAuthenticatorResponse response,
                             String accountType,
                             String authTokenType,
                             String[] requiredFeatures,
                             Bundle options) throws NetworkErrorException
    {
        final Intent intent = new Intent(context,
                                         AuthenticatorActivity.class);
        intent.putExtra(AccountManager.KEY_ACCOUNT_TYPE,
                        accountType);
        intent.putExtra(AccountManager.KEY_AUTHENTICATOR_TYPES,
                        authTokenType);
        intent.putExtra(AuthenticatorActivity.ARG_IS_ADDING_NEW_ACCOUNT,
                        true);
        intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE,
                        response);

        final Bundle bundle = new Bundle();
        bundle.putParcelable(AccountManager.KEY_INTENT,
                             intent);

        return bundle;
    }

    @Override
    public Bundle confirmCredentials(AccountAuthenticatorResponse response,
                                     Account account,
                                     Bundle options) throws NetworkErrorException
    {
        return null;
    }

    @Override
    public Bundle getAuthToken(AccountAuthenticatorResponse response,
                               Account account,
                               String authTokenType,
                               Bundle options) throws NetworkErrorException
    {
        // Extract the username and password from the Account Manager, and ask
        // the server for an appropriate AuthToken.
        final AccountManager am = AccountManager.get(context);
        String authToken = am.peekAuthToken(account,
                                            authTokenType);

        // Lets give another try to authenticate the user
        if (TextUtils.isEmpty(authToken))
        {
            final String password = am.getPassword(account);
            if (password != null)
            {
                try
                {
                    authToken = serverAuthenticate.userSignIn(account.name,
                                                              password);
                }
                catch (AuthenticatorException e)
                {
                    e.printStackTrace();
                    authToken = "";
                }
            }
        }

        // If we get an authToken - we return it
        if (!TextUtils.isEmpty(authToken))
        {
            final Bundle result = new Bundle();
            result.putString(AccountManager.KEY_ACCOUNT_NAME,
                             account.name);
            result.putString(AccountManager.KEY_ACCOUNT_TYPE,
                             account.type);
            result.putString(AccountManager.KEY_AUTHTOKEN,
                             authToken);
            return result;
        }

        // If we get here, then we couldn't access the user's password - so we
        // need to re-prompt them for their credentials. We do that by creating
        // an intent to display our AuthenticatorActivity.
        final Intent intent = new Intent(context,
                                         AuthenticatorActivity.class);
        intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE,
                        response);
        intent.putExtra(AccountManager.KEY_ACCOUNT_TYPE,
                        account.type);
        intent.putExtra(AccountManager.KEY_AUTHENTICATOR_TYPES,
                        authTokenType);
        final Bundle bundle = new Bundle();
        bundle.putParcelable(AccountManager.KEY_INTENT,
                             intent);
        return bundle;
    }

    @Override
    public String getAuthTokenLabel(String authTokenType)
    {
        return null;
    }

    @Override
    public Bundle updateCredentials(AccountAuthenticatorResponse response,
                                    Account account,
                                    String authTokenType,
                                    Bundle options) throws NetworkErrorException
    {
        return null;
    }

    @Override
    public Bundle hasFeatures(AccountAuthenticatorResponse response,
                              Account account,
                              String[] features) throws NetworkErrorException
    {
        return null;
    }
}
