package com.adoredieu.mobility.core.interfaces;

public interface Action<T>
{
    void run(T object);
}
