package com.adoredieu.mobility.core.model.bible;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "books")
public class Book
{
    public static final String BOOK_ID_COLUMN = "_id";
    public static final String BOOK_NAME_COLUMN = "name";
    private static final String BOOK_TESTAMENT_REF_COLUMN = "testament_ref";
    private static final String BOOK_ABBREVIATION_COLUMN = "abbreviation";

    @DatabaseField(id = true, columnName = BOOK_ID_COLUMN)
    private final Integer _id;
    @DatabaseField(columnName = BOOK_TESTAMENT_REF_COLUMN, canBeNull = false, foreign = true, foreignAutoRefresh = true)
    private Testament testament;
    @DatabaseField(columnName = BOOK_NAME_COLUMN)
    private String name;
    @DatabaseField(columnName = BOOK_ABBREVIATION_COLUMN)
    private String abbreviation;

    public Book(Testament testament,
                String name,
                String abbreviation)
    {
        _id = 0;
        this.testament = testament;
        this.name = name;
        this.abbreviation = abbreviation;
    }

    protected Book()
    {
        _id = 0;
        name = "";
        abbreviation = "";
    }

    public Testament getTestament()
    {
        return testament;
    }

    public void setTestament(Testament testament)
    {
        this.testament = testament;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getAbbreviation()
    {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation)
    {
        this.abbreviation = abbreviation;
    }

    public int getId()
    {
        return _id;
    }
}
