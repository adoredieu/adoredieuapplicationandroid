package com.adoredieu.mobility.core.helpers;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkHelper
{
    public static boolean isNetworkAvailable(Context context)
    {
        try
        {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

            return activeNetwork != null
                   && activeNetwork.isConnectedOrConnecting();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return true;
        }
    }
}
