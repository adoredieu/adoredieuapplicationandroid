package com.adoredieu.mobility.core.dto;

import android.os.Parcel;
import android.os.Parcelable;

import com.adoredieu.mobility.core.interfaces.VerseDefinition;
import com.adoredieu.mobility.core.model.bible.Verse;

public class VerseDto
        implements Parcelable, VerseDefinition
{
    private BookDto book;
    private Integer chapter;
    private Integer verse;
    private String text;
    private Integer id;

    public VerseDto()
    {
    }

    public VerseDto(
            Integer id,
            BookDto book,
            Integer chapter,
            Integer verse,
            String text)
    {
        this.book = book;
        this.chapter = chapter;
        this.verse = verse;
        this.text = text;
        this.id = id;
    }

    public BookDto getBook()
    {
        return book;
    }

    public void setBook(BookDto book)
    {
        this.book = book;
    }

    public Integer getChapter()
    {
        return chapter;
    }

    @Override
    public Integer getBookId()
    {
        return getBook().getId();
    }

    @Override
    public Integer getChapterNumber()
    {
        return chapter;
    }

    @Override
    public Integer getVerseNumber()
    {
        return verse;
    }

    public void setChapter(Integer chapter)
    {
        this.chapter = chapter;
    }

    public Integer getVerse()
    {
        return verse;
    }

    public void setVerse(Integer verse)
    {
        this.verse = verse;
    }

    public String getText()
    {
        return text;
    }

    @Override
    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public void setText(String text)
    {
        this.text = text;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest,
                              int flags)
    {
        dest.writeParcelable(this.book,
                             flags);
        dest.writeValue(this.chapter);
        dest.writeValue(this.verse);
        dest.writeString(this.text);
        dest.writeValue(this.id);
    }

    protected VerseDto(Parcel in)
    {
        this.book = in.readParcelable(BookDto.class.getClassLoader());
        this.chapter = (Integer) in.readValue(Integer.class.getClassLoader());
        this.verse = (Integer) in.readValue(Integer.class.getClassLoader());
        this.text = in.readString();
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Creator<VerseDto> CREATOR = new Creator<VerseDto>()
    {
        @Override
        public VerseDto createFromParcel(Parcel source)
        {
            return new VerseDto(source);
        }

        @Override
        public VerseDto[] newArray(int size)
        {
            return new VerseDto[size];
        }
    };

    public static VerseDto fromVerse(Verse verse)
    {
        return new VerseDto(verse.getId(),
                            BookDto.fromBook(verse.getBook()),
                            verse.getChapter(),
                            verse.getVerse(),
                            verse.getText());
    }
}
