package com.adoredieu.mobility.core.system.notifications;

import android.content.Context;
import android.graphics.Bitmap;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.core.helpers.NotificationHelper;
import com.adoredieu.mobility.core.helpers.WebHelper;
import com.adoredieu.mobility.core.interfaces.Action;
import com.adoredieu.mobility.core.interfaces.NotificationsTags;
import com.adoredieu.mobility.core.model.website.ImageQuote;
import com.adoredieu.mobility.core.persistence.ApplicationContextPersistence;
import com.adoredieu.mobility.core.services.ImageQuotesServices;

import org.joda.time.DateTime;

public class ImageQuoteOfTheDayNotification
{
    public static void pullNotificationIfNeeded(final Context context,
                                                final ApplicationContextPersistence applicationContextPersistence)
    {
        final ImageQuotesServices imageQuotesServices = new ImageQuotesServices(context);
        imageQuotesServices.getLastImageQuoteDate(
                new Action<DateTime>()
                {
                    @Override
                    public void run(DateTime dateTime)
                    {
                        if (dateTime != null)
                        {
                            DateTime lastImageQuoteOfTheDayDate = applicationContextPersistence.getLastImageQuoteOfTheDayDate();

                            if (lastImageQuoteOfTheDayDate == null
                                || dateTime.isAfter(lastImageQuoteOfTheDayDate))
                            {
                                imageQuotesServices.getLastImageQuote(
                                        new Action<ImageQuote>()
                                        {
                                            @Override
                                            public void run(ImageQuote imageQuote)
                                            {
                                                WebHelper.downloadImage(context,
                                                                        imageQuote.getThumbnailUri(),
                                                                        new Action<Bitmap>()
                                                                        {
                                                                            @Override
                                                                            public void run(Bitmap object)
                                                                            {
                                                                                NotificationHelper.publishImageNotification(
                                                                                        context,
                                                                                        context.getString(
                                                                                                R.string.nouvelle_citation_image),
                                                                                        context.getString(
                                                                                                R.string.cliquez_pour_lancer_l_application),
                                                                                        context.getString(
                                                                                                R.string.notification_action_citation_image),
                                                                                        object,
                                                                                        NotificationsTags.ImageQuoteOfTheDay);
                                                                            }
                                                                        });
                                            }
                                        });
                            }

                            applicationContextPersistence.setLastImageQuoteOfTheDayDate(dateTime);
                        }
                    }
                }
                                                 );
    }
}
