package com.adoredieu.mobility.core.system.notifications;

import android.content.Context;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.core.helpers.NotificationHelper;
import com.adoredieu.mobility.core.interfaces.Action;
import com.adoredieu.mobility.core.interfaces.DateProvider;
import com.adoredieu.mobility.core.interfaces.NotificationsTags;
import com.adoredieu.mobility.core.persistence.ApplicationContextPersistence;
import com.adoredieu.mobility.core.services.QuoteOfTheDayServices;

public class QuoteOfTheDayNotification
{
    public static void pullNotificationIfNeeded(final Context context,
                                                final ApplicationContextPersistence applicationContextPersistence,
                                                DateProvider dateProvider)
    {
        QuoteOfTheDayServices quoteOfTheDayServices = new QuoteOfTheDayServices(context,
                                                                                applicationContextPersistence,
                                                                                dateProvider);

        quoteOfTheDayServices.getQuoteOfTheDay(
                new Action<String>()
                {
                    @Override
                    public void run(String downloadedQuote)
                    {
                        if (downloadedQuote.isEmpty())
                        {
                            return;
                        }

                        String lastQuoteOfTheDay = applicationContextPersistence.getLastQuoteOfTheDay();
                        boolean notificationNeeded = false;

                        if (lastQuoteOfTheDay.isEmpty())
                        {
                            notificationNeeded = true;
                        }
                        else if (!downloadedQuote.equals(lastQuoteOfTheDay))
                        {
                            notificationNeeded = true;
                        }

                        applicationContextPersistence.setLastQuoteOfTheDay(downloadedQuote);

                        if (notificationNeeded)
                        {
                            NotificationHelper.publishTextNotification(context,
                                                                       context.getString(R.string.notification_title_citation),
                                                                       downloadedQuote,
                                                                       context.getString(R.string.notification_action_citation),
                                                                       NotificationsTags.QuoteOfTheDay);
                        }
                    }
                },
                new Runnable()
                {
                    @Override
                    public void run()
                    {

                    }
                },
                dateProvider.getDateTime());
    }
}
