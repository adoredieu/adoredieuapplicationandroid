package com.adoredieu.mobility.core.dao;

import com.adoredieu.mobility.core.db.OrmBibleDatabase;
import com.adoredieu.mobility.core.model.bible.Book;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.List;

import javax.inject.Inject;

public class BooksDao
{
    private final RuntimeExceptionDao<Book, Integer> dao;

    @Inject
    public BooksDao(OrmBibleDatabase db)
    {
        dao = db.getBookDao();
    }

    public List<Book> getBooks()
    {
        return dao.queryForAll();
    }

    public Book getBook(int bookId)
    {
        try
        {
            QueryBuilder<Book, Integer> queryBuilder = dao.queryBuilder();
            queryBuilder.where()
                        .eq(Book.BOOK_ID_COLUMN,
                            bookId);
            return queryBuilder.query()
                               .get(0);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return null;
        }
    }
}
