package com.adoredieu.mobility.core.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.adoredieu.mobility.core.model.highlightedVerses.HighlightedVerseOperation;
import javax.inject.Inject;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

/**
 * Database helper class used to manage the creation and upgrading of your database. This class also usually provides
 * the DAOs used by the other classes.
 */
public class HighlightedVersesDatabase
        extends OrmLiteSqliteOpenHelper
{
    // name of the database file for your application -- change to something appropriate for your app
    public static final String DATABASE_NAME = "highlighted_verses_operations.sqlite";

    // any time you make changes to your database objects, you may have to increase the database version
    private static final int DATABASE_VERSION = 1;

    // the DAO object we use to access the TxpContext table
    private RuntimeExceptionDao<HighlightedVerseOperation, Integer> highlightedVersesOperationsDao;

    @Inject
    public HighlightedVersesDatabase(Context context)
    {
        super(context,
              DATABASE_NAME,
              null,
              DATABASE_VERSION);
    }


    /**
     * This is called when the database is first created. Usually you should call createTable statements here to create
     * the tables that will store your data.
     */
    @Override
    public void onCreate(SQLiteDatabase db,
                         ConnectionSource connectionSource)
    {
        try
        {
            TableUtils.createTable(connectionSource,
                                   HighlightedVerseOperation.class);
        }
        catch (Exception e)
        {
            Log.e(HighlightedVersesDatabase.class.getName(),
                  "Can't create database",
                  e);
            throw new RuntimeException(e);
        }
    }

    /**
     * This is called when your application is upgraded and it has a higher version number. This allows you to adjust
     * the various data to match the new version number.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db,
                          ConnectionSource connectionSource,
                          int oldVersion,
                          int newVersion)
    {
        try
        {
            TableUtils.dropTable(connectionSource,
                                 HighlightedVerseOperation.class,
                                 true);

            // after we drop the old databases, we create the new ones
            onCreate(db,
                     connectionSource);
        }
        catch (Exception e)
        {
            Log.e(HighlightedVersesDatabase.class.getName(),
                  "Can't drop databases",
                  e);
            throw new RuntimeException(e);
        }
    }

    /**
     * Close the database connections and clear any cached DAOs.
     */
    @Override
    public void close()
    {
        super.close();

        highlightedVersesOperationsDao = null;
    }

    public RuntimeExceptionDao<HighlightedVerseOperation, Integer> getHighlightedVersesOperationsDao()
    {
        if (highlightedVersesOperationsDao == null)
        {
            try
            {
                highlightedVersesOperationsDao = RuntimeExceptionDao.createDao(getConnectionSource(),
                                                                               HighlightedVerseOperation.class);
            }
            catch (SQLException e)
            {
                e.printStackTrace();
            }
        }

        return highlightedVersesOperationsDao;
    }
}