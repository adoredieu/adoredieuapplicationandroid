package com.adoredieu.mobility.core.services;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.text.Html;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.core.helpers.AsyncTaskHelper;
import com.adoredieu.mobility.core.helpers.NetworkHelper;
import com.adoredieu.mobility.core.helpers.WebHelper;
import com.adoredieu.mobility.core.interfaces.Action;
import com.adoredieu.mobility.core.interfaces.ArticleType;
import com.adoredieu.mobility.core.model.website.Article;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ArticlesServices {
    private DownloadArticlesTask downloadArticlesTask;
    private final Context context;
    private final ArticleType articleType;

    public static final int ITEMS_PER_PAGE = 5;

    public ArticlesServices(Context context,
                            ArticleType articleType) {
        this.context = context;
        this.articleType = articleType;
    }

    public void getArticlesPage(int page,
                                Action<List<Article>> onFinished,
                                Runnable onNoMoreArticles) {
        downloadArticlesTask = new DownloadArticlesTask(context,
                articleType,
                onFinished,
                onNoMoreArticles);

        AsyncTaskHelper.<Integer>executeAsyncTask(downloadArticlesTask, page, ITEMS_PER_PAGE);
    }

    public void cancel() {
        if (downloadArticlesTask != null) {
            downloadArticlesTask.cancel(true);
            downloadArticlesTask.isDownloading = false;
        }
    }

    public boolean isDownloading() {
        if (downloadArticlesTask == null)
            return false;

        return downloadArticlesTask.isDownloading;
    }

    public void getLastArticleDate(Action<DateTime> onFinished) {
        DownloadArticlesLastDateTask downloadArticlesLastDateTask = new DownloadArticlesLastDateTask(
                context,
                articleType,
                onFinished);

        AsyncTaskHelper.<Void>executeAsyncTask(downloadArticlesLastDateTask);
    }

    public void getLastArticle(final Action<Article> onFinished) {
        downloadArticlesTask = new DownloadArticlesTask(
                context,
                articleType,
                new Action<List<Article>>() {
                    @Override
                    public void run(List<Article> object) {
                        onFinished.run(object.get(0));
                    }
                },
                new Runnable() {
                    @Override
                    public void run() {

                    }
                });

        AsyncTaskHelper.<Integer>executeAsyncTask(downloadArticlesTask, 0, 1);
    }

    private static class DownloadArticlesTask
            extends AsyncTask<Integer, Void, List<Article>> {
        private final WeakReference<Context> contextWeakReference;
        private final ArticleType articleType;
        private final Action<List<Article>> onFinished;
        private final Runnable onNoMoreArticles;
        private Integer page;
        private Integer itemsCount;
        private boolean isDownloading;

        public DownloadArticlesTask(Context context,
                                    ArticleType articleType,
                                    Action<List<Article>> onFinished,
                                    Runnable onNoMoreArticles) {
            this.contextWeakReference = new WeakReference<>(context);
            this.articleType = articleType;
            this.onFinished = onFinished;
            this.onNoMoreArticles = onNoMoreArticles;
        }

        @Override
        @SuppressLint("StringFormatInvalid")
        protected List<Article> doInBackground(Integer... params) {
            isDownloading = true;

            List<Article> articles = new ArrayList<>();
            Context context = contextWeakReference.get();
            if (context == null)
                return articles;

            DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");

            this.page = params[0];
            this.itemsCount = params[1];

            String articlesUri = "";
            switch (articleType) {
                case Thought:
                    articlesUri = context.getString(R.string.thoughts_www_address,
                            itemsCount,
                            page);
                    break;
                case Teaching:
                    articlesUri = context.getString(R.string.teachings_www_address,
                            itemsCount,
                            page);
                    break;
            }
            String downloadedString = WebHelper.downloadStringBlocking(context,
                    articlesUri);
            if (!downloadedString.isEmpty()) {
                try {
                    JSONArray jsonArray = new JSONArray(downloadedString);

                    for (int i = 0; i < jsonArray.length(); i++) {
                        try {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            long id = jsonObject.getLong("id");
                            DateTime publish_up = fmt.parseDateTime(jsonObject.getString(
                                    "publish_up"));
                            String article_link = jsonObject.getString("article_link");
                            String title = jsonObject.getString("title");
                            String intro_text = Html.fromHtml(jsonObject.getString("introtext"))
                                    .toString();
                            String thumbnailUri = jsonObject.getString("thumbnail")
                                    .equals("null") ? "" : jsonObject.getString(
                                    "thumbnail");

                            String articleDetailsBaseUri = "";
                            switch (articleType) {
                                case Thought:
                                    articleDetailsBaseUri = context.getString(R.string.thought_details_www_address,
                                            id);
                                    break;
                                case Teaching:
                                    articleDetailsBaseUri = context.getString(R.string.teaching_details_www_address,
                                            id);
                                    break;
                            }

                            articles.add(
                                    new Article(
                                            articleType,
                                            id,
                                            publish_up,
                                            title,
                                            intro_text,
                                            thumbnailUri,
                                            articleDetailsBaseUri,
                                            article_link));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return articles;
        }

        @Override
        protected void onPostExecute(List<Article> result) {
            if (result.size() == 0) {
                onNoMoreArticles.run();
            } else {
                onFinished.run(result);
            }

            isDownloading = false;
        }
    }

    public static class DownloadArticlesLastDateTask
            extends AsyncTask<Void, Void, DateTime> {
        private final WeakReference<Context> contextWeakReference;
        private final ArticleType articleType;
        private final Action<DateTime> onFinished;

        public DownloadArticlesLastDateTask(Context context,
                                            ArticleType articleType,
                                            Action<DateTime> onFinished) {
            this.contextWeakReference = new WeakReference<>(context);
            this.articleType = articleType;
            this.onFinished = onFinished;
        }

        @Override
        protected DateTime doInBackground(Void... voids) {
            Context context = contextWeakReference.get();
            if (context == null)
                return null;

            try {
                if (!NetworkHelper.isNetworkAvailable(context)) {
                    return null;
                }

                String articlesLastDateUri = "";
                switch (articleType) {
                    case Thought:
                        articlesLastDateUri = context.getString(R.string.thoughts_last_date_www_address);
                        break;
                    case Teaching:
                        articlesLastDateUri = context.getString(R.string.teachings_last_date_www_address);
                        break;
                }
                String downloadString = WebHelper.downloadStringBlocking(context,
                        articlesLastDateUri);

                if (downloadString.isEmpty()) {
                    return null;
                }

                DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
                DateTimeFormatter frenchFmt = fmt.withLocale(Locale.FRENCH);

                return DateTime.parse(downloadString,
                        frenchFmt);
            } catch (Exception e) {
                e.printStackTrace();

                return null;
            }
        }

        @Override
        protected void onPostExecute(DateTime dateTime) {
            onFinished.run(dateTime);
        }
    }
}
