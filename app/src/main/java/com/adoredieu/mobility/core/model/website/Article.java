package com.adoredieu.mobility.core.model.website;

import android.os.Parcel;
import android.os.Parcelable;

import com.adoredieu.mobility.core.interfaces.ArticleType;

import org.joda.time.DateTime;

public class Article
        implements Parcelable
{
    public final Long id;
    public final ArticleType type;
    public final String title;
    public final String introText;
    public final String thumbnailUri;
    public final String detailsUri;
    public final String articleLink;
    public final DateTime dateTime;

    public Article(ArticleType type,
                   Long id,
                   DateTime dateTime,
                   String title,
                   String introText,
                   String thumbnailUri,
                   String detailsUri,
                   String articleLink)
    {
        this.type = type;
        this.id = id;
        this.dateTime = dateTime;
        this.title = title;
        this.introText = introText;
        this.thumbnailUri = thumbnailUri;
        this.detailsUri = detailsUri;
        this.articleLink = articleLink;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest,
                              int flags)
    {
        dest.writeValue(this.id);
        dest.writeInt(this.type == null ? -1 : this.type.ordinal());
        dest.writeString(this.title);
        dest.writeString(this.introText);
        dest.writeString(this.thumbnailUri);
        dest.writeString(this.detailsUri);
        dest.writeString(this.articleLink);
        dest.writeSerializable(this.dateTime);
    }

    protected Article(Parcel in)
    {
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        int tmpType = in.readInt();
        this.type = tmpType == -1 ? null : ArticleType.values()[tmpType];
        this.title = in.readString();
        this.introText = in.readString();
        this.thumbnailUri = in.readString();
        this.detailsUri = in.readString();
        this.articleLink = in.readString();
        this.dateTime = (DateTime) in.readSerializable();
    }

    public static final Creator<Article> CREATOR = new Creator<Article>()
    {
        @Override
        public Article createFromParcel(Parcel source)
        {
            return new Article(source);
        }

        @Override
        public Article[] newArray(int size)
        {
            return new Article[size];
        }
    };
}
