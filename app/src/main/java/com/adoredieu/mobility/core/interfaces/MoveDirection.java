package com.adoredieu.mobility.core.interfaces;

public enum MoveDirection
{
    None,
    North,
    West,
    East,
    South
}
