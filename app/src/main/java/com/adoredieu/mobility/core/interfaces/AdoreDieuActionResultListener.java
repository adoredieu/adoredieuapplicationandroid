package com.adoredieu.mobility.core.interfaces;

public interface AdoreDieuActionResultListener
{
    void onAdoreDieuLoginSuccess();
}
