package com.adoredieu.mobility.core.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.adoredieu.mobility.core.model.bible.Book;
import com.adoredieu.mobility.core.model.bible.Greek;
import com.adoredieu.mobility.core.model.bible.Hebrew;
import com.adoredieu.mobility.core.model.bible.ReadingPlanDescription;
import com.adoredieu.mobility.core.model.bible.ReadingPlanEntry;
import com.adoredieu.mobility.core.model.bible.Testament;
import com.adoredieu.mobility.core.model.bible.Verse;
import com.adoredieu.mobility.core.model.bible.VerseStrong;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

import javax.inject.Inject;

/**
 * Database helper class used to manage the creation and upgrading of your database. This class also usually provides
 * the DAOs used by the other classes.
 */
public class OrmBibleDatabase
        extends OrmLiteSqliteOpenHelper {
    // name of the database file for your application -- change to something appropriate for your app
    public static final String DATABASE_NAME = "LSGS.db3";

    // any time you make changes to your database objects, you may have to increase the database version
    private static final int DATABASE_VERSION = 1;

    // the DAO object we use to access the TxpContext table
    private RuntimeExceptionDao<Book, Integer> bookDao;
    private RuntimeExceptionDao<Testament, Integer> testamentDao;
    private RuntimeExceptionDao<Verse, Integer> verseDao;
    private RuntimeExceptionDao<VerseStrong, Integer> verseStrongDao;
    private RuntimeExceptionDao<Greek, Integer> greekDao;
    private RuntimeExceptionDao<Hebrew, Integer> hebrewDao;
    private RuntimeExceptionDao<ReadingPlanDescription, Integer> readingPlanDescriptionDao;
    private RuntimeExceptionDao<ReadingPlanEntry, Integer> readingPlanEntriesDao;

    @Inject
    public OrmBibleDatabase(Context context) {
        super(context,
                DATABASE_NAME,
                null,
                DATABASE_VERSION);
    }

    public ConnectionSource getConnection() {
        if (isOpen())
            return this.getConnectionSource();

        return null;
    }

    /**
     * This is called when the database is first created. Usually you should call createTable statements here to create
     * the tables that will store your data.
     */
    @Override
    public void onCreate(SQLiteDatabase db,
                         ConnectionSource connectionSource) {
    }

    /**
     * This is called when your application is upgraded and it has a higher version number. This allows you to adjust
     * the various data to match the new version number.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db,
                          ConnectionSource connectionSource,
                          int oldVersion,
                          int newVersion) {
    }

    /**
     * Close the database connections and clear any cached DAOs.
     */
    @Override
    public void close() {
        super.close();

        bookDao = null;
        testamentDao = null;
        verseDao = null;
        verseStrongDao = null;
        greekDao = null;
        hebrewDao = null;
        readingPlanDescriptionDao = null;
        readingPlanEntriesDao = null;
    }

    public RuntimeExceptionDao<Book, Integer> getBookDao() {
        if (bookDao == null) {
            try {
                bookDao = RuntimeExceptionDao.createDao(getConnectionSource(),
                        Book.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return bookDao;
    }

    public RuntimeExceptionDao<Testament, Integer> getTestamentDao() {
        if (testamentDao == null) {
            try {
                testamentDao = RuntimeExceptionDao.createDao(getConnectionSource(),
                        Testament.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return testamentDao;
    }

    public RuntimeExceptionDao<Verse, Integer> getVerseDao() {
        if (verseDao == null) {
            try {
                verseDao = RuntimeExceptionDao.createDao(getConnectionSource(),
                        Verse.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return verseDao;
    }

    public RuntimeExceptionDao<VerseStrong, Integer> getVerseStrongDao() {
        if (verseStrongDao == null) {
            try {
                verseStrongDao = RuntimeExceptionDao.createDao(getConnectionSource(),
                        VerseStrong.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return verseStrongDao;
    }

    public RuntimeExceptionDao<Greek, Integer> getGreekDao() {
        if (greekDao == null) {
            try {
                greekDao = RuntimeExceptionDao.createDao(getConnectionSource(),
                        Greek.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return greekDao;
    }

    public RuntimeExceptionDao<Hebrew, Integer> getHebrewDao() {
        if (hebrewDao == null) {
            try {
                hebrewDao = RuntimeExceptionDao.createDao(getConnectionSource(),
                        Hebrew.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return hebrewDao;
    }

    public RuntimeExceptionDao<ReadingPlanDescription, Integer> getReadingPlanDescriptionDao() {
        if (readingPlanDescriptionDao == null) {
            try {
                readingPlanDescriptionDao = RuntimeExceptionDao.createDao(getConnectionSource(),
                        ReadingPlanDescription.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return readingPlanDescriptionDao;
    }

    public RuntimeExceptionDao<ReadingPlanEntry, Integer> getReadingPlanEntriesDaoDao() {
        if (readingPlanEntriesDao == null) {
            try {
                readingPlanEntriesDao = RuntimeExceptionDao.createDao(getConnectionSource(),
                        ReadingPlanEntry.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return readingPlanEntriesDao;
    }
}