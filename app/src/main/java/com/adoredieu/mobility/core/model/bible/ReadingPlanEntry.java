package com.adoredieu.mobility.core.model.bible;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "reading_plans")
public class ReadingPlanEntry
{
    public static final String TABLE_NAME = "reading_plans";

    public static final String ENTRY_ID_COLUMN = "_id";
    public static final String READING_PLAN_ID_COLUMN = "reading_plan_index";
    public static final String DAY_NUMBER_COLUMN = "day";
    public static final String VERSE_BOOK_COLUMN = "book";
    public static final String START_CHAPTER_COLUMN = "chapter_start";
    public static final String START_VERSE_COLUMN = "verse_start";
    public static final String END_CHAPTER_COLUMN = "chapter_end";
    public static final String END_VERSE_COLUMN = "verse_end";

    @DatabaseField(id = true, columnName = ENTRY_ID_COLUMN)
    private final Integer _id;

    @DatabaseField(columnName = READING_PLAN_ID_COLUMN)
    private final Integer readingPlanId;

    @DatabaseField(columnName = DAY_NUMBER_COLUMN)
    private final Integer dayNumber;

    @DatabaseField(columnName = VERSE_BOOK_COLUMN)
    private final Integer book;

    @DatabaseField(columnName = START_CHAPTER_COLUMN)
    private final Integer startChapter;

    @DatabaseField(columnName = START_VERSE_COLUMN)
    private final Integer startVerse;

    @DatabaseField(columnName = END_CHAPTER_COLUMN)
    private final Integer endChapter;

    @DatabaseField(columnName = END_VERSE_COLUMN)
    private final Integer endVerse;

    public ReadingPlanEntry(Integer readingPlanId,
                            Integer dayNumber,
                            Integer book,
                            Integer startChapter,
                            Integer startVerse,
                            Integer endChapter,
                            Integer endVerse)
    {
        this._id = 0;
        this.readingPlanId = readingPlanId;
        this.dayNumber = dayNumber;
        this.book = book;
        this.startChapter = startChapter;
        this.startVerse = startVerse;
        this.endChapter = endChapter;
        this.endVerse = endVerse;
    }

    public ReadingPlanEntry()
    {
        this._id = 0;
        this.readingPlanId = 0;
        this.dayNumber = 0;
        this.book = 0;
        this.startChapter = 0;
        this.startVerse = 0;
        this.endChapter = 0;
        this.endVerse = 0;
    }

    public Integer get_id()
    {
        return _id;
    }

    public Integer getReadingPlanId()
    {
        return readingPlanId;
    }

    public Integer getDayNumber()
    {
        return dayNumber;
    }

    public Integer getBook()
    {
        return book;
    }

    public Integer getStartChapter()
    {
        return startChapter;
    }

    public Integer getStartVerse()
    {
        return startVerse;
    }

    public Integer getEndChapter()
    {
        return endChapter;
    }

    public Integer getEndVerse()
    {
        return endVerse;
    }
}
