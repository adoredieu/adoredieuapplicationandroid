package com.adoredieu.mobility.core.helpers;

import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

public class SwipeDetector
        implements View.OnTouchListener
{
    final Handler handler = new Handler();
    private final View v;
    private int min_distance = 100;
    private int max_click_distance = 10;
    private float downX, downY, upX, upY;
    private OnSwipeListener swipeEventListener;
    private View.OnClickListener clickEventListener;
    private OnLongPressListener longClickEventListener;
    private final Runnable mLongPressed = new Runnable()
    {
        public void run()
        {
            onLongClick();
        }
    };
    private int min_time_for_long_press = 500;

    public SwipeDetector(View v)
    {
        this.v = v;
        v.setOnTouchListener(this);
    }

    public void setOnSwipeListener(OnSwipeListener listener)
    {
        try
        {
            swipeEventListener = listener;
        }
        catch (ClassCastException e)
        {
            Log.e("ClassCastException",
                  "please pass SwipeDetector.onSwipeEvent Interface instance",
                  e);
        }
    }

    public void setOnClickListener(View.OnClickListener listener)
    {
        try
        {
            clickEventListener = listener;
        }
        catch (ClassCastException e)
        {
            Log.e("ClassCastException",
                  "please pass View.OnClickListener Interface instance",
                  e);
        }
    }

    public void setOnLongClickListener(OnLongPressListener listener)
    {
        try
        {
            longClickEventListener = listener;
        }
        catch (ClassCastException e)
        {
            Log.e("ClassCastException",
                  "please pass View.OnClickListener Interface instance",
                  e);
        }
    }

    private void onRightToLeftSwipe()
    {
        if (swipeEventListener != null)
        {
            swipeEventListener.swipeEventDetected(v,
                                                  SwipeTypeEnum.RIGHT_TO_LEFT);
        }
        else
        {
            Log.e("SwipeDetector error",
                  "please pass SwipeDetector.onSwipeEvent Interface instance");
        }
    }

    private void onLeftToRightSwipe()
    {
        if (swipeEventListener != null)
        {
            swipeEventListener.swipeEventDetected(v,
                                                  SwipeTypeEnum.LEFT_TO_RIGHT);
        }
        else
        {
            Log.e("SwipeDetector error",
                  "please pass SwipeDetector.onSwipeEvent Interface instance");
        }
    }

    private void onTopToBottomSwipe()
    {
        if (swipeEventListener != null)
        {
            swipeEventListener.swipeEventDetected(v,
                                                  SwipeTypeEnum.TOP_TO_BOTTOM);
        }
        else
        {
            Log.e("SwipeDetector error",
                  "please pass SwipeDetector.onSwipeEvent Interface instance");
        }
    }

    private void onBottomToTopSwipe()
    {
        if (swipeEventListener != null)
        {
            swipeEventListener.swipeEventDetected(v,
                                                  SwipeTypeEnum.BOTTOM_TO_TOP);
        }
        else
        {
            Log.e("SwipeDetector error",
                  "please pass SwipeDetector.onSwipeEvent Interface instance");
        }
    }

    private void onClick()
    {
        if (clickEventListener != null)
        {
            clickEventListener.onClick(v);
        }
        else
        {
            Log.e("SwipeDetector error",
                  "please pass View.OnClickListener Interface instance");
        }
    }

    private void onLongClick()
    {
        if (longClickEventListener != null)
        {
            longClickEventListener.longPressDetected(v);
        }
        else
        {
            Log.e("SwipeDetector error",
                  "please pass View.OnClickListener Interface instance");
        }
    }

    public boolean onTouch(View v,
                           MotionEvent event)
    {
        switch (event.getAction())
        {
            case MotionEvent.ACTION_DOWN:
            {
                handler.postDelayed(mLongPressed,
                                    min_time_for_long_press);

                downX = event.getX();
                downY = event.getY();
                return true;
            }
            case MotionEvent.ACTION_UP:
            {
                handler.removeCallbacks(mLongPressed);

                upX = event.getX();
                upY = event.getY();

                float deltaX = downX - upX;
                float deltaY = downY - upY;

                //HORIZONTAL SCROLL
                if (Math.abs(deltaX) > Math.abs(deltaY))
                {
                    if (Math.abs(deltaX) > min_distance)
                    {
                        // left or right
                        if (deltaX < 0)
                        {
                            this.onLeftToRightSwipe();
                            return true;
                        }
                        if (deltaX > 0)
                        {
                            this.onRightToLeftSwipe();
                            return true;
                        }
                    }
                    else
                    {
                        //not long enough swipe...
                        return false;
                    }
                }
                //VERTICAL SCROLL
                else
                {
                    if (Math.abs(deltaY) > min_distance)
                    {
                        // top or down
                        if (deltaY < 0)
                        {
                            this.onTopToBottomSwipe();
                            return true;
                        }
                        if (deltaY > 0)
                        {
                            this.onBottomToTopSwipe();
                            return true;
                        }
                    }
                    else
                    {
                        if (Math.abs(deltaY) < max_click_distance)
                        {
                            this.onClick();
                            return true;
                        }
                        //not long enough swipe...
                        return false;
                    }
                }

                return true;
            }
        }

        return false;
    }

    public SwipeDetector setMinTimeForLongPress(int minTime)
    {
        this.min_time_for_long_press = minTime;
        return this;
    }

    public SwipeDetector setMinDistanceInPixels(int minDistance)
    {
        this.min_distance = minDistance;
        return this;
    }

    public SwipeDetector setMaxClickDistanceInPixels(int maxClickDistance)
    {
        this.max_click_distance = maxClickDistance;
        return this;
    }

    public enum SwipeTypeEnum
    {
        RIGHT_TO_LEFT,
        LEFT_TO_RIGHT,
        TOP_TO_BOTTOM,
        BOTTOM_TO_TOP
    }

    public interface OnLongPressListener
    {
        void longPressDetected(View v);
    }

    public interface OnSwipeListener
    {
        void swipeEventDetected(View v,
                                SwipeTypeEnum swipeType);
    }
}