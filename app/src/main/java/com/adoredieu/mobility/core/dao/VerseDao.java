package com.adoredieu.mobility.core.dao;

import com.adoredieu.mobility.core.db.OrmBibleDatabase;
import com.adoredieu.mobility.core.model.bible.Verse;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.List;

import javax.inject.Inject;

public class VerseDao
{
    private final RuntimeExceptionDao<Verse, Integer> dao;

    @Inject
    public VerseDao(OrmBibleDatabase db)
    {
        dao = db.getVerseDao();
    }


    public List<Verse> getChapter(long book,
                                  long chapter)
    {
        try
        {
            QueryBuilder<Verse, Integer> queryBuilder = dao.queryBuilder();
            queryBuilder.where()
                        .eq(Verse.VERSE_BOOK_REF_COLUMN,
                            book)
                        .and()
                        .eq(Verse.VERSE_CHAPTER_COLUMN,
                            chapter);
            PreparedQuery<Verse> preparedQuery = queryBuilder.prepare();

            return dao.query(preparedQuery);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public Long getChaptersCount(Long book)
    {
        GenericRawResults<String[]> countQueryResult = dao.queryRaw(
                "SELECT MAX(" + Verse.VERSE_CHAPTER_COLUMN + ") FROM " + Verse.TABLE_NAME
                + " WHERE " + Verse.VERSE_BOOK_REF_COLUMN + " = ?",
                book.toString());
        long result = 0;

        try
        {
            String count = countQueryResult.getResults()
                                           .get(0)[0];
            result = Long.parseLong(count);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return result;
    }

    public List<Verse> getAllVerses()
    {
        return dao.queryForAll();
    }

    public Verse getVerse(long book,
                          long chapter,
                          long verse)
    {
        try
        {
            QueryBuilder<Verse, Integer> queryBuilder = dao.queryBuilder();
            queryBuilder.where()
                        .eq(Verse.VERSE_BOOK_REF_COLUMN,
                            book)
                        .and()
                        .eq(Verse.VERSE_CHAPTER_COLUMN,
                            chapter).and()
                        .eq(Verse.VERSE_NUMBER_COLUMN,
                            verse);
            PreparedQuery<Verse> preparedQuery = queryBuilder.prepare();

            return dao.queryForFirst(preparedQuery);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public List<Verse> getVersesById(List<Integer> ids)
    {
        try
        {
            QueryBuilder<Verse, Integer> queryBuilder = dao.queryBuilder();
            queryBuilder.orderBy(Verse.VERSE_ID_COLUMN,
                                 true)
                        .where()
                        .in(Verse.VERSE_ID_COLUMN,
                            ids);
            PreparedQuery<Verse> preparedQuery = queryBuilder.prepare();

            return dao.query(preparedQuery);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return null;
        }
    }
}
