package com.adoredieu.mobility.core.interfaces.authentication;

import android.accounts.AuthenticatorException;
import android.accounts.NetworkErrorException;

public interface ServerAuthenticate
{
    String userSignUp(String name,
                      String username,
                      String pass,
                      String email) throws AuthenticatorException, NetworkErrorException;

    String userSignIn(String username,
                      String pass) throws AuthenticatorException, NetworkErrorException;
}
