package com.adoredieu.mobility.core.services;

import android.content.Context;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.core.helpers.WebHelper;
import com.adoredieu.mobility.core.interfaces.Action;
import com.adoredieu.mobility.core.model.website.Article;
import com.adoredieu.mobility.core.persistence.ApplicationContextPersistence;
import com.google.gson.JsonArray;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class FavouriteServices
{
    private final Context context;
    private final ApplicationContextPersistence applicationContextPersistence;

    @Inject
    public FavouriteServices(Context context,
                             ApplicationContextPersistence applicationContextPersistence)
    {
        this.context = context;
        this.applicationContextPersistence = applicationContextPersistence;
    }

    public void toggleFavourite(Article article,
                                final Runnable onSuccess,
                                final Runnable onFailed)
    {
        String operationType = "ADD";
        if (isFavourite(article))
        {
            operationType = "DELETE";
        }

        WebHelper.downloadString(
                context,
                context.getString(R.string.articles_favourites_www_address,
                                  applicationContextPersistence.getCurrentUserId(),
                                  article.id,
                                  operationType),
                new Action<String>()
                {
                    @Override
                    public void run(String content)
                    {
                        if (content != null && !content.isEmpty())
                        {
                            if (content.equals("1"))
                            {
                                onSuccess.run();
                            }
                        }
                        else
                        {
                            onFailed.run();
                        }
                    }
                });
    }

    public List<Integer> getFavourites()
    {
        List<Integer> favourites = new ArrayList<>();
        JsonArray list = WebHelper.downloadJSonArrayBlocking(
                context,
                context.getString(R.string.articles_favourites_www_address,
                                  applicationContextPersistence.getCurrentUserId(),
                                  0,
                                  "LIST"));

        if (list != null)
        {
            for (int i = 0; i < list.size(); i++)
            {
                favourites.add(list.get(i).getAsInt());
            }
        }

        return favourites;
    }

    public boolean isFavourite(Article article)
    {
        return getFavourites().contains(article.id.intValue());
    }
}
