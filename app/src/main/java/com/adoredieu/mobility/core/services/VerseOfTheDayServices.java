package com.adoredieu.mobility.core.services;


import android.content.Context;
import android.os.AsyncTask;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.core.dto.VerseOfTheDayDto;
import com.adoredieu.mobility.core.helpers.AsyncTaskHelper;
import com.adoredieu.mobility.core.helpers.WebHelper;
import com.adoredieu.mobility.core.interfaces.Action;
import com.adoredieu.mobility.core.interfaces.DateProvider;
import com.adoredieu.mobility.core.persistence.ApplicationContextPersistence;

import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

public class VerseOfTheDayServices {
    private final Context context;
    private final ApplicationContextPersistence applicationContextPersistence;
    private final DateProvider dateProvider;

    @Inject
    public VerseOfTheDayServices(Context context,
                                 ApplicationContextPersistence applicationContextPersistence,
                                 DateProvider dateProvider) {
        this.context = context;
        this.applicationContextPersistence = applicationContextPersistence;
        this.dateProvider = dateProvider;
    }

    public void getVerseOfTheDay(final Action<VerseOfTheDayDto> onFinished,
                                 Runnable onError,
                                 DateTime dateTime) {
        if (dateTime.toLocalDate()
                .isEqual(dateProvider.getDateTime()
                        .toLocalDate())) {
            DateTime verseOfTheDayCacheDate = applicationContextPersistence.getVerseOfTheDayCacheDate();
            VerseOfTheDayDto verseOfTheDayCache = applicationContextPersistence.getVerseOfTheDayCache();

            if (verseOfTheDayCacheDate == null
                    || verseOfTheDayCache == null
                    || verseOfTheDayCacheDate.toLocalDate()
                    .isBefore(dateProvider.getDateTime()
                            .toLocalDate())) {
                DownloadVerseOfTheDayTask downloadVerseOfTheDayTask = new DownloadVerseOfTheDayTask(
                        context,
                        dateTime,
                        new Action<VerseOfTheDayDto>() {
                            @Override
                            public void run(VerseOfTheDayDto result) {
                                applicationContextPersistence.setVerseOfTheDayCacheDate(dateProvider
                                        .getDateTime());
                                applicationContextPersistence.setVerseOfTheDayCache(result);
                                onFinished.run(result);
                            }
                        },
                        onError);

                AsyncTaskHelper.<Void>executeAsyncTask(downloadVerseOfTheDayTask);
            } else {
                onFinished.run(verseOfTheDayCache);
            }
        } else {
            DownloadVerseOfTheDayTask downloadVerseOfTheDayTask = new DownloadVerseOfTheDayTask(
                    context,
                    dateTime,
                    onFinished,
                    onError);

            AsyncTaskHelper.<Void>executeAsyncTask(downloadVerseOfTheDayTask);
        }
    }

    private static class DownloadVerseOfTheDayTask
            extends AsyncTask<Void, Void, VerseOfTheDayDto> {
        private final WeakReference<Context> contextWeakReference;
        private final DateTime dateTime;
        private final Action<VerseOfTheDayDto> onFinished;
        private final Runnable onError;

        public DownloadVerseOfTheDayTask(Context context,
                                         DateTime dateTime,
                                         Action<VerseOfTheDayDto> onFinished,
                                         Runnable onError) {
            this.contextWeakReference = new WeakReference<>(context);
            this.dateTime = dateTime;
            this.onFinished = onFinished;
            this.onError = onError;
        }

        @Override
        protected VerseOfTheDayDto doInBackground(Void... voids) {
            Context context = contextWeakReference.get();
            if (context == null)
                return null;

            String website_address = context.getString(R.string.daily_verse_www_address,
                    dateTime.dayOfYear().getAsString());
            String verseOfTheDayJson = WebHelper.downloadStringBlocking(context,
                    website_address);

            if (verseOfTheDayJson.isEmpty()) {
                return null;
            }

            try {
                JSONObject jsonObject = new JSONObject(verseOfTheDayJson);

                return new VerseOfTheDayDto(
                        jsonObject.getInt("_id"),
                        jsonObject.getInt("book"),
                        jsonObject.getInt("chapter_start"),
                        jsonObject.getInt("verse_start"),
                        jsonObject.getInt("chapter_end"),
                        jsonObject.getInt("verse_end"),
                        jsonObject.getString("verse"));
            } catch (JSONException e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(VerseOfTheDayDto result) {
            if (result == null) {
                onError.run();
            } else {
                onFinished.run(result);
            }
        }
    }
}
