package com.adoredieu.mobility.core.helpers;

import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.URLSpan;
import android.text.util.Linkify;

import com.adoredieu.mobility.core.dao.GreekDao;
import com.adoredieu.mobility.core.dao.HebrewDao;
import com.adoredieu.mobility.core.system.mmi.ClickSpan;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StrongsHelper
{
    private final HebrewDao hebrewDao;
    private final GreekDao greekDao;

    public StrongsHelper(HebrewDao hebrewDao,
                         GreekDao greekDao)
    {
        this.hebrewDao = hebrewDao;
        this.greekDao = greekDao;
    }

    public SpannableString formatTextWithStrongs(String text,
                                                 boolean addStrongWordToStrongNumbers,
                                                 ClickSpan.OnClickListener onClickListener)
    {
        String linksText = text;

        if (addStrongWordToStrongNumbers)
        {
            Pattern pattern = Pattern.compile("<S(H|G)>([0-9]+)</S(H|G)>");
            Matcher matcher = pattern.matcher(linksText);

            while (matcher.find())
            {
                String strongText = matcher.group(0);
                String strongType = matcher.group(1);
                int strongNumber = Integer.parseInt(matcher.group(2));

                if (strongType.equals("H"))
                {
                    linksText = linksText.replaceAll(strongText,
                                                     "<a href=\"H#" + strongNumber + "\">"
                                                     + hebrewDao.getStrong(strongNumber)
                                                                .getWord()
                                                     + " (" + strongNumber + ")</a>");
                }
                else
                {
                    linksText = linksText.replaceAll(strongText,
                                                     "<a href=\"G#" + strongNumber + "\">"
                                                     + greekDao.getStrong(strongNumber)
                                                               .getWord()
                                                     + " (" + strongNumber + ")</a>");
                }
            }
        }
        else
        {
            linksText = linksText.replaceAll("<SG>([0-9]+)</SG>",
                                             "<a href=\"G#$1\"><i>$1</i></a>");
            linksText = linksText.replaceAll("<SH>([0-9]+)</SH>",
                                             "<a href=\"H#$1\"><i>$1</i></a>");
        }

        Spanned coloredText = Html.fromHtml(linksText);
        URLSpan[] currentSpans = coloredText.getSpans(0,
                                                      coloredText.length(),
                                                      URLSpan.class);
        SpannableString buffer = new SpannableString(coloredText);
        Linkify.addLinks(buffer,
                         Linkify.ALL);

        for (URLSpan span : currentSpans)
        {
            int start = coloredText.getSpanStart(span);
            int end = coloredText.getSpanEnd(span);

            ClickSpan clickSpan = new ClickSpan(
                    span,
                    onClickListener);

            buffer.setSpan(clickSpan,
                           start,
                           end,
                           Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

        return buffer;
    }
}
