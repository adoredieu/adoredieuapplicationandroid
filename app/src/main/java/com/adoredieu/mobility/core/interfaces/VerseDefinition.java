package com.adoredieu.mobility.core.interfaces;

public interface VerseDefinition
{
    Integer getChapter();

    Integer getVerse();

    String getText();

    Integer getId();

    Integer getBookId();

    Integer getChapterNumber();

    Integer getVerseNumber();
}
