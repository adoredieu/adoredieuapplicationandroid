package com.adoredieu.mobility.core.interfaces;

public enum NotificationsTags
{
    QuoteOfTheDay,
    ImageQuoteOfTheDay,
    Teaching,
    Thought,
    VerseOfTheDay,
}
