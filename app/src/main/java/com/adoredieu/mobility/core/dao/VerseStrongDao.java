package com.adoredieu.mobility.core.dao;

import com.adoredieu.mobility.controllers.BibleStrongSearchController;
import com.adoredieu.mobility.core.db.OrmBibleDatabase;
import com.adoredieu.mobility.core.model.bible.VerseStrong;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.List;

import javax.inject.Inject;

public class VerseStrongDao {
    private final RuntimeExceptionDao<VerseStrong, Integer> dao;

    @Inject
    public VerseStrongDao(OrmBibleDatabase db) {
        dao = db.getVerseStrongDao();
    }


    public List<VerseStrong> getChapter(long book,
                                        long chapter) {
        try {
            QueryBuilder<VerseStrong, Integer> queryBuilder = dao.queryBuilder();
            queryBuilder.where()
                    .eq(VerseStrong.VERSE_BOOK_REF_COLUMN,
                            book)
                    .and()
                    .eq(VerseStrong.VERSE_CHAPTER_COLUMN,
                            chapter);
            PreparedQuery<VerseStrong> preparedQuery = queryBuilder.prepare();

            return dao.query(preparedQuery);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Long getChaptersCount(Long book) {
        GenericRawResults<String[]> countQueryResult = dao.queryRaw("SELECT MAX(" + VerseStrong.VERSE_CHAPTER_COLUMN + ") FROM " + VerseStrong.TABLE_NAME + " WHERE " + VerseStrong.VERSE_BOOK_REF_COLUMN + " = ?",
                book.toString());
        long result = 0;

        try {
            String count = countQueryResult.getResults()
                    .get(0)[0];
            result = Long.parseLong(count);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }

    public List<VerseStrong> getAllVerses() {
        return dao.queryForAll();
    }

    public List<VerseStrong> getVersesById(List<Integer> ids) {
        try {
            QueryBuilder<VerseStrong, Integer> queryBuilder = dao.queryBuilder();
            queryBuilder.orderBy(VerseStrong.VERSE_ID_COLUMN,
                    true)
                    .where()
                    .in(VerseStrong.VERSE_ID_COLUMN,
                            ids);
            PreparedQuery<VerseStrong> preparedQuery = queryBuilder.prepare();

            return dao.query(preparedQuery);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<VerseStrong> search(BibleStrongSearchController.StrongLanguage language,
                                    Integer strongCode) {
        try {
            String like = "%<S" + language.toCode() + ">" + strongCode + "</S" + language.toCode() + ">%";

            QueryBuilder<VerseStrong, Integer> queryBuilder = dao.queryBuilder();
            queryBuilder.orderBy(VerseStrong.VERSE_ID_COLUMN,
                    true)
                    .where()
                    .like(VerseStrong.VERSE_TEXT_COLUMN,
                            like);
            PreparedQuery<VerseStrong> preparedQuery = queryBuilder.prepare();

            return dao.query(preparedQuery);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
