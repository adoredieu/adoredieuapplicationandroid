package com.adoredieu.mobility.core.services;

import android.content.Context;
import android.os.AsyncTask;
import android.text.Html;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.core.helpers.AsyncTaskHelper;
import com.adoredieu.mobility.core.helpers.WebHelper;
import com.adoredieu.mobility.core.interfaces.Action;
import com.adoredieu.mobility.core.interfaces.ArticleType;
import com.adoredieu.mobility.core.model.website.Article;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.net.URLEncoder;
import java.util.ArrayList;

import javax.inject.Inject;

public class ArticleSearchServices {
    private final Context context;
    private DownloadArticlesTask downloadArticlesTask;

    @Inject
    public ArticleSearchServices(Context context) {
        this.context = context;
    }

    public void cancel() {
        if (downloadArticlesTask != null) {
            downloadArticlesTask.cancel(true);
            downloadArticlesTask.isDownloading = false;
        }
    }

    public boolean isDownloading() {
        if (downloadArticlesTask == null)
            return false;

        return downloadArticlesTask.isDownloading;
    }

    public void search(String searchText,
                       Action<ArrayList<Article>> onFinished) {
        downloadArticlesTask = new DownloadArticlesTask(context, onFinished);
        AsyncTaskHelper.<String>executeAsyncTask(downloadArticlesTask, searchText);
    }

    private static class DownloadArticlesTask
            extends AsyncTask<String, Void, ArrayList<Article>> {
        private final WeakReference<Context> contextWeakReference;
        private final Action<ArrayList<Article>> onFinished;
        private boolean isDownloading;

        public DownloadArticlesTask(Context context, Action<ArrayList<Article>> onFinished) {
            this.contextWeakReference = new WeakReference<>(context);
            this.onFinished = onFinished;
        }

        @Override
        protected ArrayList<Article> doInBackground(String... searchText) {
            isDownloading = true;

            ArrayList<Article> articles = new ArrayList<>();
            Context context = contextWeakReference.get();
            if (context == null)
                return articles;

            DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");

            String downloadedString = "";
            try {
                String articleSearchUri = context.getString(R.string.articles_search_www_address,
                        URLEncoder.encode(searchText[0],
                                "UTF-8"));

                downloadedString = WebHelper.downloadStringBlocking(
                        context,
                        articleSearchUri);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            if (!downloadedString.isEmpty()) {
                try {
                    JSONArray jsonArray = new JSONArray(downloadedString);

                    for (int i = 0; i < jsonArray.length(); i++) {
                        try {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            long id = jsonObject.getLong("id");
                            DateTime publishUp = fmt.parseDateTime(jsonObject.getString(
                                    "publish_up"));
                            String articleLink = jsonObject.getString("article_link");
                            String title = jsonObject.getString("title");
                            String type = jsonObject.getString("type");
                            String introText = Html.fromHtml(jsonObject.getString("introtext"))
                                    .toString();
                            String thumbnailUri = jsonObject.getString("thumbnail");

                            ArticleType articleType = ArticleType.Thought;
                            String articleDetailsBaseUri = context.getString(R.string.thought_details_www_address,
                                    id);
                            if (type.equals("E")) {
                                articleType = ArticleType.Teaching;
                                articleDetailsBaseUri = context.getString(R.string.teaching_details_www_address,
                                        id);
                            }

                            articles.add(
                                    new Article(
                                            articleType,
                                            id,
                                            publishUp,
                                            title,
                                            introText,
                                            thumbnailUri,
                                            articleDetailsBaseUri,
                                            articleLink));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return articles;
        }

        @Override
        protected void onPostExecute(ArrayList<Article> result) {
            onFinished.run(result);

            isDownloading = false;
        }
    }
}
