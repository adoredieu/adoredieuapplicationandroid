package com.adoredieu.mobility.core.hal;

import com.adoredieu.mobility.core.interfaces.DateProvider;

import org.joda.time.DateTime;
import org.joda.time.LocalTime;

public class RealDateProvider
        implements DateProvider
{
    @Override
    public LocalTime getTime()
    {
        return LocalTime.now();
    }

    @Override
    public DateTime getDateTime()
    {
        return DateTime.now();
    }
}
