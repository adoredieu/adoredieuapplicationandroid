package com.adoredieu.mobility.core.services;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.adoredieu.mobility.core.db.BibleDatabase;
import com.adoredieu.mobility.core.interfaces.SearchOperator;
import com.adoredieu.mobility.core.interfaces.VerseDefinition;
import com.adoredieu.mobility.core.model.bible.Book;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javax.inject.Inject;

public class BibleSearchService {
    private BibleDatabase bibleDatabase;

    @Inject
    public BibleSearchService(BibleDatabase bibleDatabase) {
        this.bibleDatabase = bibleDatabase;
    }

    private static String formatStringNormalizer(String s) {
        String temp = Normalizer.normalize(s,
                Normalizer.Form.NFD);
        return temp.replaceAll("[^\\p{ASCII}]",
                "")
                .toLowerCase();
    }

    List<HighlightedHit> search(String queryText,
                                SearchOperator searchOperator,
                                Integer maxResults,
                                Book bookFilter) {

        List<HighlightedHit> highlightedHits = new ArrayList<>();

        String formattedQueryText = formatStringNormalizer(queryText);
        SQLiteDatabase databaseConnection = bibleDatabase.getReadableDatabase();

        StringBuilder query = new StringBuilder("SELECT " +
                "searchData._id as id, " +

                "books.testament_ref as testament, " +
                "bible.book_ref as book, " +

                "bible.chapter, " +
                "bible.verse, " +
                "bible.text, " +

                "offsets(searchData) as offsets " +
                "FROM searchData " +
                "LEFT JOIN bible ON bible._id = searchData._id " +
                "LEFT JOIN books ON books._id = bible.book_ref ");

        switch (searchOperator) {
            case OR:
                query.append("WHERE ");
                String[] formattedQueryTextTokens = formattedQueryText.split(" ");
                for (int i = 0; i < formattedQueryTextTokens.length; i++) {
                    query.append("searchData.text MATCH \"").append(formattedQueryTextTokens[i].trim()).append("\" ");

                    if (i < (formattedQueryTextTokens.length - 1)) {
                        query.append(" OR ");
                    }
                }
                break;
            case EXACTLY:
            case AND:
            default:
                query.append("WHERE searchData.text MATCH \"").append(formattedQueryText).append("\" ");
                break;
        }

        if (bookFilter != null)
            query.append("AND bible.book_ref = ").append(bookFilter.getId());

        query.append(" ORDER BY bible._id;");

        Pattern pattern = Pattern.compile("\\b" + queryText.toLowerCase().trim() + "\\b");

        Cursor cursor = databaseConnection.rawQuery(query.toString(), new String[]{});
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            HighlightedHit highlightedHit = cursorToHighlightedHit(cursor);

            if (searchOperator == SearchOperator.EXACTLY) {
                if (pattern.matcher(highlightedHit.getText().toLowerCase()).find())
                    highlightedHits.add(highlightedHit);
            } else {
                highlightedHits.add(highlightedHit);
            }

            cursor.moveToNext();
        }
        cursor.close();

        return highlightedHits;
    }

    private HighlightedHit cursorToHighlightedHit(Cursor cursor) {
        HighlightedHit highlightedHit = new HighlightedHit(
                cursor.getInt(0),
                cursor.getInt(1),
                cursor.getInt(2),
                cursor.getInt(3),
                cursor.getInt(4),
                cursor.getString(5),
                cursor.getString(6));

        return highlightedHit;
    }

    public class HighlightedHit implements VerseDefinition {
        private List<Offset> offsets;
        private Integer id;
        private Integer testament;
        private Integer book;
        private Integer chapter;
        private Integer verse;
        private String text;

        HighlightedHit(Integer id,
                       Integer testament,
                       Integer book,
                       Integer chapter,
                       Integer verse,
                       String text,
                       String offsetsString) {
            this.id = id;
            this.testament = testament;
            this.book = book;
            this.chapter = chapter;
            this.verse = verse;
            this.text = text;
            offsets = new ArrayList<>();
            parseOffsets(offsetsString);
        }

        private void parseOffsets(String offsetsString) {
            String[] offsetsTokens = offsetsString.split(" ");

            offsets.clear();
            for (int i = 0; i < offsetsTokens.length / 4; i++) {
                offsets.add(new Offset(Integer.parseInt(offsetsTokens[i * 4 + 2]),
                        Integer.parseInt(offsetsTokens[i * 4 + 3])));
            }
        }

        public Integer getId() {
            return id;
        }

        @Override
        public Integer getBookId() {
            return getBook();
        }

        @Override
        public Integer getChapterNumber() {
            return getChapter();
        }

        @Override
        public Integer getVerseNumber() {
            return getVerse();
        }

        public Integer getTestament() {
            return testament;
        }

        public Integer getBook() {
            return book;
        }

        public Integer getChapter() {
            return chapter;
        }

        public Integer getVerse() {
            return verse;
        }

        public String getText() {
            return text;
        }

        public List<Offset> getOffsets() {
            return offsets;
        }

        public class Offset {
            private int start;
            private int length;

            Offset(int start, int length) {
                this.start = start;
                this.length = length;
            }

            public int getStart() {
                return start;
            }

            public int getLength() {
                return length;
            }
        }
    }
}
