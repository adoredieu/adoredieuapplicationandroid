package com.adoredieu.mobility.core.services;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.Log;

import com.adoredieu.mobility.core.interfaces.MusicServiceEventsListener;
import com.adoredieu.mobility.core.model.website.AudioTrack;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.ConcatenatingMediaSource;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import java.util.ArrayList;
import java.util.List;

import static com.google.android.exoplayer2.Player.STATE_BUFFERING;
import static com.google.android.exoplayer2.Player.STATE_ENDED;
import static com.google.android.exoplayer2.Player.STATE_IDLE;
import static com.google.android.exoplayer2.Player.STATE_READY;

public class MusicServices implements Player.EventListener {
    private SimpleExoPlayer audioPlayer = null;

    // Caller activity context, used when play local audio file.
    private final Context context;
    private MusicServiceEventsListener musicServiceEventsListener;
    private List<AudioTrack> audioTracks = new ArrayList<>();

    public MusicServices(Context context) {
        this.context = context;
        initAudioPlayer();
    }

    public void setTracks(List<AudioTrack> audioTracks) {
        this.audioTracks = audioTracks;

        // Produces DataSource instances through which media data is loaded.
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(
                context,
                Util.getUserAgent(context, "AdoreDieu"));

        ConcatenatingMediaSource concatenatedSource = new ConcatenatingMediaSource();
        for (AudioTrack audioTrack : audioTracks) {
            MediaSource mediaSource = new ExtractorMediaSource.Factory(dataSourceFactory).createMediaSource(audioTrack.fileUri);

            concatenatedSource.addMediaSource(mediaSource);
        }

        // This is the MediaSource representing the media to be played.
        // Prepare the player with the source.
        audioPlayer.prepare(concatenatedSource);
    }

    // Initialise audio player.
    private void initAudioPlayer() {
        try {
            if (audioPlayer == null) {
                audioPlayer = ExoPlayerFactory.newSimpleInstance(context, new DefaultTrackSelector());
                audioPlayer.addListener(this);
                audioPlayer.setRepeatMode(Player.REPEAT_MODE_ALL);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    // Destroy audio player.
    public void destroyAudioPlayer() {
        if (audioPlayer != null) {
            if (audioPlayer.getPlayWhenReady()) {
                audioPlayer.stop();
            }

            audioPlayer.release();

            audioPlayer = null;
        }
    }

    @Override
    public void onTimelineChanged(Timeline timeline, @Nullable Object manifest, int reason) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
        int currentPeriodIndex = getCurrentIndex();

        if (musicServiceEventsListener != null)
            musicServiceEventsListener.onTrackChanged(currentPeriodIndex);
    }

    public int getCurrentIndex() {
        return audioPlayer.getCurrentPeriodIndex();
    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        switch (playbackState) {
            case STATE_IDLE:
                break;
            case STATE_BUFFERING:
                if (musicServiceEventsListener != null)
                    musicServiceEventsListener.onPlayerBuffering();
                break;
            case STATE_READY:
                musicServiceEventsListener.onPlayerReady();

                if (playWhenReady) {
                    if (musicServiceEventsListener != null)
                        musicServiceEventsListener.onPlayerPlay();
                } else {
                    if (musicServiceEventsListener != null)
                        musicServiceEventsListener.onPlayerPause();
                }
                break;
            case STATE_ENDED:
                break;
        }
    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {
        if (musicServiceEventsListener != null)
            musicServiceEventsListener.onShuffleModeEnabledChanged(shuffleModeEnabled);
    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {
        this.audioTracks.remove(audioPlayer.getCurrentPeriodIndex());

        switch (error.type) {
            case ExoPlaybackException.TYPE_SOURCE:
                Log.e(this.getClass().getSimpleName(), "TYPE_SOURCE: " + error.getSourceException().getMessage());
                break;

            case ExoPlaybackException.TYPE_RENDERER:
                Log.e(this.getClass().getSimpleName(), "TYPE_RENDERER: " + error.getRendererException().getMessage());
                break;

            case ExoPlaybackException.TYPE_UNEXPECTED:
                Log.e(this.getClass().getSimpleName(), "TYPE_UNEXPECTED: " + error.getUnexpectedException().getMessage());
                break;
        }

        destroyAudioPlayer();
        initAudioPlayer();
        setTracks(this.audioTracks);

        if (musicServiceEventsListener != null)
            musicServiceEventsListener.onPlayerRecoverFromError();
    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {

    }

    public Player getPlayer() {
        return audioPlayer;
    }

    public void setMusicServiceEventsListener(MusicServiceEventsListener musicServiceEventsListener) {
        this.musicServiceEventsListener = musicServiceEventsListener;
    }

    public void playTrack(int index) {
        audioPlayer.seekTo(index, C.TIME_UNSET);
        audioPlayer.setPlayWhenReady(true);
    }

    public List<AudioTrack> getAudioTracks() {
        return audioTracks;
    }
}
