package com.adoredieu.mobility.core.services;

import android.content.Context;

import com.adoredieu.mobility.core.interfaces.FacebookActionResultListener;

import javax.inject.Inject;

public class FacebookServices {
    @Inject
    public FacebookServices(Context context) {
    }

    public void login(final FacebookActionResultListener facebookActionDoneListener) {
    }

    public void logout(final FacebookActionResultListener facebookActionDoneListener) {
    }

    public boolean isLoggedIn() {
        return false;
    }
}
