package com.adoredieu.mobility.core.services;

import android.content.Context;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.util.TypedValue;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.controllers.BibleStrongSearchController;
import com.adoredieu.mobility.core.adapter.BookAdapter;
import com.adoredieu.mobility.core.dao.BooksDao;
import com.adoredieu.mobility.core.dao.VerseStrongDao;
import com.adoredieu.mobility.core.dto.SearchResultBibleVerseDto;
import com.adoredieu.mobility.core.helpers.AsyncTaskHelper;
import com.adoredieu.mobility.core.interfaces.Action;
import com.adoredieu.mobility.core.interfaces.VerseDefinition;
import com.adoredieu.mobility.core.model.bible.Book;
import com.adoredieu.mobility.core.model.bible.VerseStrong;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class BibleStrongServices {
    private final BooksDao booksDao;
    private final VerseStrongDao verseStrongDao;

    @Inject
    public BibleStrongServices(BooksDao booksDao,
                               VerseStrongDao verseStrongDao) {
        this.booksDao = booksDao;
        this.verseStrongDao = verseStrongDao;
    }

    public List<VerseDefinition> getChapter(long book,
                                            long chapter) {
        List<VerseStrong> chapterContent = verseStrongDao.getChapter(book,
                chapter);

        return new ArrayList<VerseDefinition>(chapterContent);
    }

    public Book getBook(int bookId) {
        return booksDao.getBook(bookId);
    }

    public void startSearch(Context context,
                            BibleStrongSearchController.StrongLanguage language,
                            Integer strongCode,
                            Action<ArrayList<SearchResultBibleVerseDto>> onFinished) {
        try {
            SearchAsyncTask searchAsyncTask = new SearchAsyncTask(context,
                    language,
                    strongCode,
                    booksDao,
                    verseStrongDao,
                    onFinished);

            AsyncTaskHelper.<Void>executeAsyncTask(searchAsyncTask);
        } catch (Exception e) {
            e.printStackTrace();
            onFinished.run(new ArrayList<SearchResultBibleVerseDto>());
        }
    }

    private static class SearchAsyncTask
            extends AsyncTask<Void, Void, ArrayList<SearchResultBibleVerseDto>> {
        private final WeakReference<Context> contextWeakReference;
        private final BibleStrongSearchController.StrongLanguage language;
        private final Integer strongCode;
        private final Action<ArrayList<SearchResultBibleVerseDto>> onFinished;
        private final BooksDao booksDao;
        private final VerseStrongDao verseStrongDao;

        public SearchAsyncTask(Context context,
                               BibleStrongSearchController.StrongLanguage language,
                               Integer strongCode,
                               BooksDao booksDao,
                               VerseStrongDao verseStrongDao,
                               Action<ArrayList<SearchResultBibleVerseDto>> onFinished) {
            this.contextWeakReference = new WeakReference<>(context);
            this.language = language;
            this.strongCode = strongCode;
            this.booksDao = booksDao;
            this.verseStrongDao = verseStrongDao;
            this.onFinished = onFinished;
        }

        @Override
        protected ArrayList<SearchResultBibleVerseDto> doInBackground(Void... voids) {
            ArrayList<SearchResultBibleVerseDto> searchResultBibleVerseDtos = new ArrayList<>();
            Context context = contextWeakReference.get();
            if (context == null)
                return searchResultBibleVerseDtos;

            try {
                List<VerseStrong> highlightedHits = verseStrongDao.search(language,
                        strongCode);

                String strong = "<S" + language.toCode() + ">" + strongCode + "</S" + language.toCode() + ">";

                TypedValue typedValue = new TypedValue();
                Resources.Theme theme = context.getTheme();
                theme.resolveAttribute(R.attr.colorAccent,
                        typedValue,
                        true);
                String color = typedValue.coerceToString()
                        .toString();
                String numberColor = "#" + color.substring(3);

                for (int i = 0; i < highlightedHits.size(); i++) {
                    VerseDefinition verse = highlightedHits.get(i);

                    String highlightText = verse.getText();
                    highlightText = highlightText.replace(strong,
                            "<font color='" + numberColor + "'>("
                                    + strongCode
                                    + ")</font>");
                    highlightText = highlightText.replaceAll("\\(?<S(G|H)>[0-9]+</S(G|H)>\\)?",
                            "");

                    searchResultBibleVerseDtos.add(
                            new SearchResultBibleVerseDto(
                                    verse.getId(),
                                    BookAdapter.bookToBookDto(booksDao.getBook(verse.getBookId())),
                                    verse.getChapter(),
                                    verse.getVerse(),
                                    verse.getText(),
                                    highlightText));
                }

                return searchResultBibleVerseDtos;
            } catch (Exception e) {
                e.printStackTrace();
                return new ArrayList<>();
            }
        }

        @Override
        protected void onPostExecute(ArrayList<SearchResultBibleVerseDto> searchResultBibleVerseDtos) {
            onFinished.run(searchResultBibleVerseDtos);
        }
    }
}
