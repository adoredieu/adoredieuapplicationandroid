package com.adoredieu.mobility.core.android;

import android.os.Message;

import com.adoredieu.mobility.controllers.GuiController;
import com.adoredieu.mobility.view.activities.ActivityBase;

/**
 * Message Handler class that supports buffering up of messages when the
 * activity is paused i.e. in the background.
 */
public final class ConcretePauseHandler
        extends PauseHandler
{
    public final static int MSG_WHAT = ('A' << 8) + 'D';
    public final static int MSG_SHOW_HOME = 1;
    private ActivityBase activity;
    private GuiController guiController;

    @Override
    protected boolean storeMessage(Message message)
    {
        // All messages are stored by default
        return true;
    }

    @Override
    protected void processMessage(Message msg)
    {
        switch (msg.what)
        {
            case MSG_WHAT:
                switch (msg.arg1)
                {
                    case MSG_SHOW_HOME:
                        guiController.showHome(activity);
                        break;
                }
                break;
        }
    }

    public void setActivity(ActivityBase activity)
    {
        this.activity = activity;
    }

    public void setGuiController(GuiController guiController)
    {
        this.guiController = guiController;
    }
}