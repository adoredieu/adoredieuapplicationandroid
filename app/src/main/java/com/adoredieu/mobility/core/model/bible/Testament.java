package com.adoredieu.mobility.core.model.bible;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "testaments")
public class Testament
{
    private static final String TESTAMENT_NAME_COLUMN = "name";

    @DatabaseField(id = true)
    private final Integer _id;
    @DatabaseField(columnName = TESTAMENT_NAME_COLUMN)
    private String name;

    protected Testament()
    {
        _id = 0;
        name = "";
    }

    public Testament(String name)
    {
        _id = 0;
        this.name = name;
    }

    public Integer getId()
    {
        return _id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }
}
