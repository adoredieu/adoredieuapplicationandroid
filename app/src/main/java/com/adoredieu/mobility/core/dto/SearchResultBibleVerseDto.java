package com.adoredieu.mobility.core.dto;

import android.os.Parcel;
import android.os.Parcelable;

import com.adoredieu.mobility.core.interfaces.VerseDefinition;

public class SearchResultBibleVerseDto
        extends VerseDto
        implements Parcelable, VerseDefinition
{
    private String highlightedText;

    public SearchResultBibleVerseDto(Integer id,
                                     BookDto book,
                                     Integer chapter,
                                     Integer verse,
                                     String text,
                                     String highlightedText)
    {
        super(id, book, chapter, verse, text);

        setHighlightedText(highlightedText);
    }

    public String getHighlightedText()
    {
        return highlightedText;
    }

    private void setHighlightedText(String highlightedText)
    {
        this.highlightedText = highlightedText;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest,
                              int flags)
    {
        super.writeToParcel(dest,
                            flags);
        dest.writeString(this.highlightedText);
    }

    protected SearchResultBibleVerseDto(Parcel in)
    {
        super(in);
        this.highlightedText = in.readString();
    }

    public static final Creator<SearchResultBibleVerseDto> CREATOR = new Creator<SearchResultBibleVerseDto>()
    {
        @Override
        public SearchResultBibleVerseDto createFromParcel(Parcel source)
        {
            return new SearchResultBibleVerseDto(source);
        }

        @Override
        public SearchResultBibleVerseDto[] newArray(int size)
        {
            return new SearchResultBibleVerseDto[size];
        }
    };
}
