package com.adoredieu.mobility.core.interfaces;

public interface MusicServiceEventsListener {
    void onTrackChanged(int index);

    void onShuffleModeEnabledChanged(boolean mode);

    void onPlayerPause();

    void onPlayerPlay();

    void onPlayerBuffering();

    void onPlayerRecoverFromError();

    void onPlayerReady();
}
