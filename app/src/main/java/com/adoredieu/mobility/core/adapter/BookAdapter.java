package com.adoredieu.mobility.core.adapter;

import com.adoredieu.mobility.core.dto.BookDto;
import com.adoredieu.mobility.core.model.bible.Book;
import com.adoredieu.mobility.core.model.bible.Testament;

import java.util.ArrayList;
import java.util.List;

public class BookAdapter
{
    public static BookDto bookToBookDto(Book book)
    {
        BookDto bookDto = new BookDto();
        Testament testament = book.getTestament();
        if (testament != null)
        {
            bookDto.setTestament(TestamentAdapter.testamentToTestamentDto(testament));
        }
        bookDto.setId(book.getId());

        return bookDto;
    }

    public static List<BookDto> booksToBookDtos(List<Book> books)
    {
        List<BookDto> bookDtos = new ArrayList<>();
        for (int i = 0; i < books.size(); i++)
        {
            bookDtos.add(bookToBookDto(books.get(i)));
        }

        return bookDtos;
    }
}

