package com.adoredieu.mobility.core.expansion;

public class AdoreDieuDownloaderService
{
    public static final String BASE64_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqaNhnWdwEN8k4RB6cQRWPj9EEfkkXKqS5OKimpEqzQBF6ygR+VLCLGoJfyJgJKDRxZueLC5Sx8IseTqYBo2N1zqHDZrvhEjJpccJ6SYhFQSu4hijQbdqsgnhLEzFGnSld7xqfaCdtaLTlHapqoZ4694fOPlp0HhUjVVGA1IRagdnmYfY/ZAVkPGwxDmNz4H4mbNTdOQ2AwTzcRNAsX6gfY7pxidDZia0oHAV7Jp0LqNLdyrJTKSQqD6P/tP50lCB8rRsg3/PRqJ9dR3S+LaI6444VbC3Sjd5crTKHu833Fs+FkF93xITNheTwuNlFwJLume+uYT0iTRX8TetjkZyWQIDAQAB"; // TODO Add public key
    private static final byte[] SALT = new byte[]{
            83,
            49,
            89,
            (byte) 157,
            (byte) 207,
            (byte) 243,
            81,
            (byte) 141,
            (byte) 197,
            (byte) 156,
            (byte) 178,
            (byte) 244,
            (byte) 236,
            (byte) 163,
            53,
            13,
            77,
            114,
            46,
            (byte) 199,
            };

    public String getPublicKey()
    {
        return BASE64_PUBLIC_KEY;
    }

    public byte[] getSALT()
    {
        return SALT;
    }

    public String getAlarmReceiverClassName()
    {
        return DownloaderServiceBroadcastReceiver.class.getName();
    }
}
