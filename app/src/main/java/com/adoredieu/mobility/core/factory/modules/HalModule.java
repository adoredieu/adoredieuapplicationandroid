package com.adoredieu.mobility.core.factory.modules;

import com.adoredieu.mobility.core.interfaces.DateProvider;

import dagger.Module;
import dagger.Provides;

@Module
public class HalModule
{
    private final DateProvider dateProvider;

    public HalModule(DateProvider dateProvider)
    {
        this.dateProvider = dateProvider;
    }

    @Provides
    public DateProvider provideDateProvider()
    {
        return dateProvider;
    }
}
