package com.adoredieu.mobility.core.factory.modules;

import android.content.Context;

import com.adoredieu.mobility.core.db.BibleDatabase;
import com.adoredieu.mobility.core.db.HighlightedVersesDatabase;
import com.adoredieu.mobility.core.db.OrmBibleDatabase;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class DatabaseModule {
    @Provides
    @Singleton
    public HighlightedVersesDatabase provideHighlightedVersesDatabase(Context context) {
        return new HighlightedVersesDatabase(context);
    }

    @Provides
    @Singleton
    public BibleDatabase provideBibleDatabase(Context context) {
        return new BibleDatabase(context);
    }

    @Provides
    @Singleton
    public OrmBibleDatabase provideOrmBibleDatabase(Context context) {
        return new OrmBibleDatabase(context);
    }
}
