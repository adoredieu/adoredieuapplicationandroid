package com.adoredieu.mobility.core.helpers;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class WifiHelper
{
    @SuppressWarnings("deprecation")
    public static boolean isConnected(Context context)
    {
        if (context == null)
        {
            return false;
        }

        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connManager == null)
        {
            return false;
        }

        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        return mWifi.isConnected();
    }
}
