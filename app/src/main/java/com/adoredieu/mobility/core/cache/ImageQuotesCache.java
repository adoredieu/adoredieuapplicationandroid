package com.adoredieu.mobility.core.cache;

import com.adoredieu.mobility.core.model.website.ImageQuote;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ImageQuotesCache
{
    private Map<Integer, List<ImageQuote>> cache;

    public ImageQuotesCache()
    {
        cache = new HashMap<>();
    }

    public void clear()
    {
        cache = new HashMap<>();
    }

    public void add(int page,
                    List<ImageQuote> articles)
    {
        cache.put(page,
                  articles);
    }

    public boolean containsPage(int page)
    {
        return cache.containsKey(page);
    }

    public List<ImageQuote> getPageCache(int page)
    {
        return cache.get(page);
    }
}
