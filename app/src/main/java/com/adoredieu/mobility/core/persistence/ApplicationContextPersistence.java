package com.adoredieu.mobility.core.persistence;

import android.content.Context;
import android.support.annotation.Nullable;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.core.dto.VerseOfTheDayDto;
import com.adoredieu.mobility.core.helpers.DateTimeHelper;
import com.adoredieu.mobility.core.interfaces.BiblePosition;
import com.adoredieu.mobility.core.interfaces.BibleVerseWidgetContent;

import org.joda.time.DateTime;

import javax.inject.Inject;

public class ApplicationContextPersistence
{
    private final TinyDbOverSharedPreferences tinyDbOverSharedPreferences;

    @Inject
    public ApplicationContextPersistence(Context context)
    {
        tinyDbOverSharedPreferences = new TinyDbOverSharedPreferences(
                context,
                context.getString(R.string.app_name));
    }

    public String getLastQuoteOfTheDay()
    {
        return tinyDbOverSharedPreferences.getString("LastCitation");
    }

    public void setLastQuoteOfTheDay(String content)
    {
        tinyDbOverSharedPreferences.putString("LastCitation",
                                              content);
    }

    public DateTime getLastTeachingsDate()
    {
        return tinyDbOverSharedPreferences.getDateTime("LastTeachingsDate",
                                                       DateTimeHelper.MinValue);
    }

    public void setLastTeachingsDate(DateTime date)
    {
        tinyDbOverSharedPreferences.putDateTime("LastTeachingsDate",
                                                date);

    }

    public DateTime getLastImageQuoteOfTheDayDate()
    {
        return tinyDbOverSharedPreferences.getDateTime("LastCitationImageDate",
                                                       DateTimeHelper.MinValue);
    }

    public void setLastImageQuoteOfTheDayDate(DateTime date)
    {
        tinyDbOverSharedPreferences.putDateTime("LastCitationImageDate",
                                                date);
    }

    public DateTime getLastThoughtsDate()
    {
        return tinyDbOverSharedPreferences.getDateTime("LastThoughtsDate",
                                                       DateTimeHelper.MinValue);
    }

    public void setLastThoughtsDate(DateTime date)
    {
        tinyDbOverSharedPreferences.putDateTime("LastThoughtsDate",
                                                date);
    }

    @Nullable
    public BiblePosition getLastBiblePosition()
    {
        return tinyDbOverSharedPreferences.getObject("LastBiblePosition",
                                                     BiblePosition.class);
    }

    public void setLastBiblePosition(BiblePosition position)
    {
        tinyDbOverSharedPreferences.putObject("LastBiblePosition",
                                              position,
                                              BiblePosition.class);
    }

    public long getInstalledBibleVersion()
    {
        return tinyDbOverSharedPreferences.getLong("version",
                                                   0);
    }

    public void setInstalledBibleVersion(long bibleVersion)
    {
        tinyDbOverSharedPreferences.putLong("version",
                                            bibleVersion);
    }

    public boolean getHomeTutorialSeen()
    {
        return tinyDbOverSharedPreferences.getBoolean("home_tutorial_seen_2",
                                                      false);
    }

    public void setHomeTutorialSeen(boolean seen)
    {
        tinyDbOverSharedPreferences.putBoolean("home_tutorial_seen_2",
                                               seen);
    }

    public boolean getBibleTutorialSeen()
    {
        return tinyDbOverSharedPreferences.getBoolean("bible_tutorial_seen_2",
                                                      false);
    }

    public void setBibleTutorialSeen(boolean seen)
    {
        tinyDbOverSharedPreferences.putBoolean("bible_tutorial_seen_2",
                                               seen);
    }

    public DateTime getVerseOfTheDayCacheDate()
    {
        return tinyDbOverSharedPreferences.getDateTime("VerseOfTheDayCacheDate",
                                                       DateTimeHelper.MinValue);
    }

    public void setVerseOfTheDayCacheDate(DateTime verseOfTheDayCacheDate)
    {
        tinyDbOverSharedPreferences.putDateTime("VerseOfTheDayCacheDate",
                                                verseOfTheDayCacheDate);
    }

    @Nullable
    public VerseOfTheDayDto getVerseOfTheDayCache()
    {
        return tinyDbOverSharedPreferences.getObject("VerseOfTheDayCache",
                                                     VerseOfTheDayDto.class);
    }

    public void setVerseOfTheDayCache(VerseOfTheDayDto verseOfTheDay)
    {
        tinyDbOverSharedPreferences.putObject("VerseOfTheDayCache",
                                              verseOfTheDay,
                                              VerseOfTheDayDto.class);
    }

    public String getLastBibleSearch()
    {
        return tinyDbOverSharedPreferences.getString("LastBibleSearch");
    }

    public void setLastBibleSearch(String lastBibleSearch)
    {
        tinyDbOverSharedPreferences.putString("LastBibleSearch",
                                              lastBibleSearch.trim());
    }

    public DateTime getQuoteOfTheDayCacheDate()
    {
        return tinyDbOverSharedPreferences.getDateTime("QuoteOfTheDayCacheDate",
                                                       DateTimeHelper.MinValue);
    }

    public void setQuoteOfTheDayCacheDate(DateTime date)
    {
        tinyDbOverSharedPreferences.putDateTime("QuoteOfTheDayCacheDate",
                                                date);
    }

    public String getQuoteOfTheDayCache()
    {
        return tinyDbOverSharedPreferences.getString("CitationOfTheDayCache");
    }

    public void setQuoteOfTheDayCache(String quoteOfTheDay)
    {
        tinyDbOverSharedPreferences.putString("CitationOfTheDayCache",
                                              quoteOfTheDay);
    }

    public BibleVerseWidgetContent getBibleVerseWidgetVerse(Context context)
    {
        String title = tinyDbOverSharedPreferences.getString("BibleVerseWidgetTitle");
        if (title.isEmpty())
        {
            title = context.getResources().getString(R.string.bibleWidgetNoVerseTitle);
        }

        String content = tinyDbOverSharedPreferences.getString("BibleVerseWidgetContent");
        if (content.isEmpty())
        {
            content = context.getResources().getString(R.string.bibleWidgetNoVerseContent);
        }

        return new BibleVerseWidgetContent(title,
                                           content);
    }

    public void setBibleVerseWidgetVerse(BibleVerseWidgetContent bibleVerseWidgetContent)
    {
        tinyDbOverSharedPreferences.putString("BibleVerseWidgetTitle",
                                              bibleVerseWidgetContent.getTitle());
        tinyDbOverSharedPreferences.putString("BibleVerseWidgetContent",
                                              bibleVerseWidgetContent.getContent());
    }

    public String getCurrentUserUsername()
    {
        return tinyDbOverSharedPreferences.getString("CurrentUserUsername");
    }

    public void setCurrentUserUsername(String currentUserUsername)
    {
        tinyDbOverSharedPreferences.putString("CurrentUserUsername",
                                              currentUserUsername);
    }

    public String getCurrentUserName()
    {
        return tinyDbOverSharedPreferences.getString("CurrentUserName");
    }

    public void setCurrentUserName(String currentUserUsername)
    {
        tinyDbOverSharedPreferences.putString("CurrentUserName",
                                              currentUserUsername);
    }

    public Integer getCurrentUserId()
    {
        return tinyDbOverSharedPreferences.getInt("CurrentUserId",
                                                  0);
    }

    public void setCurrentUserId(Integer id)
    {
        tinyDbOverSharedPreferences.putInt("CurrentUserId",
                                           id);
    }

    public DateTime getVersesHighlightsLastSynchronisationDate()
    {
        return tinyDbOverSharedPreferences.getDateTime("VersesHighlightsLastSynchronisationDate",
                                                       DateTimeHelper.MinValue);
    }

    public void setVersesHighlightsLastSynchronisationDate(DateTime date)
    {
        tinyDbOverSharedPreferences.putDateTime("VersesHighlightsLastSynchronisationDate",
                                                date);
    }

    public String getCurrentUserPicture()
    {
        return tinyDbOverSharedPreferences.getString("CurrentUserPicture");
    }

    public void setCurrentUserPicture(String url)
    {
        tinyDbOverSharedPreferences.putString("CurrentUserPicture",
                                              url);
    }

    public Integer getLastNotifiedVerseOfTheDayId()
    {
        return tinyDbOverSharedPreferences.getInt("LastNotifiedVerseOfTheDayId",
                                                  0);
    }

    public void setLastNotifiedVerseOfTheDayId(Integer lastNotifiedVerseOfTheDayId)
    {
        tinyDbOverSharedPreferences.putInt("LastNotifiedVerseOfTheDayId",
                                           lastNotifiedVerseOfTheDayId);
    }
}
