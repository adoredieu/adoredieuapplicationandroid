package com.adoredieu.mobility.core.dao;

import com.adoredieu.mobility.core.db.OrmBibleDatabase;
import com.adoredieu.mobility.core.dto.StrongDto;
import com.adoredieu.mobility.core.model.bible.Hebrew;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;

import javax.inject.Inject;

public class HebrewDao
{
    protected final RuntimeExceptionDao<Hebrew, Integer> dao;

    @Inject
    public HebrewDao(OrmBibleDatabase db)
    {
        dao = db.getHebrewDao();
    }

    public StrongDto getStrong(int strongCode)
    {
        try
        {
            QueryBuilder<Hebrew, Integer> queryBuilder = dao.queryBuilder();
            queryBuilder.where()
                        .eq(Hebrew.HEBREW_ID_COLUMN,
                            strongCode);
            return StrongDto.fromHebrew(queryBuilder.query()
                                                    .get(0));
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return null;
        }
    }
}
