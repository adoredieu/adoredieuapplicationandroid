package com.adoredieu.mobility.core.helpers;

import android.content.Context;
import android.content.pm.ApplicationInfo;

public class Debug
{
    public static boolean isInDebug(Context context)
    {
        return (context.getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE) != 0;
    }
}
