package com.adoredieu.mobility.core.helpers;

import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.LeadingMarginSpan;

public class StringHelper
{
    public static String stripHtml(String html)
    {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N)
        {
            return Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY).toString();
        }
        else
        {
            return Html.fromHtml(html).toString();
        }
    }

    public static SpannableString createIndentedText(String text,
                                                     int marginFirstLine,
                                                     int marginNextLines)
    {
        SpannableString result = new SpannableString(text);
        result.setSpan(new LeadingMarginSpan.Standard(marginFirstLine,
                                                      marginNextLines),
                       0,
                       text.length(),
                       0);
        return result;
    }

    public static SpannableString createIndentedText(Spanned text,
                                                     int marginFirstLine,
                                                     int marginNextLines)
    {
        SpannableString result = new SpannableString(text);
        result.setSpan(new LeadingMarginSpan.Standard(marginFirstLine,
                                                      marginNextLines),
                       0,
                       text.length(),
                       0);
        return result;
    }

    public static SpannableString createColoredText(Spanned text,
                                                    int startPosition,
                                                    int endPosition,
                                                    int color)
    {
        SpannableString result = new SpannableString(text);
        result.setSpan(new ForegroundColorSpan(color),
                       startPosition,
                       endPosition,
                       Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        return result;
    }
}
