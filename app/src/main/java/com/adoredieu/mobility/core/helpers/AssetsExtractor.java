package com.adoredieu.mobility.core.helpers;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class AssetsExtractor
{
    public static void extractAsset(Context context,
                                    String assetFile,
                                    String destination)
    {
        AssetManager assetManager = context.getAssets();
        InputStream in;
        OutputStream out;
        try
        {
            in = assetManager.open(assetFile);
            File outFile = new File(destination);
            outFile.getParentFile()
                   .mkdirs();

            out = new FileOutputStream(outFile);
            copyFile(in,
                     out);
            in.close();

            out.flush();
            out.close();
        }
        catch (IOException e)
        {
            Log.e("AssetsExtractor",
                  "Failed to copy asset file " + assetFile + " to " + destination,
                  e);
        }
    }

    private static void copyFile(InputStream in,
                                 OutputStream out) throws
                                                   IOException
    {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1)
        {
            out.write(buffer,
                      0,
                      read);
        }
    }
}
