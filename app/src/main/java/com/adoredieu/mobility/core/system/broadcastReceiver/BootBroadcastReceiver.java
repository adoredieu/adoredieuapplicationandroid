package com.adoredieu.mobility.core.system.broadcastReceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * BroadcastReceiver that receives the following implicit broadcasts:
 * <ul>
 * <li>Intent.ACTION_BOOT_COMPLETED</li>
 * <li>Intent.ACTION_LOCKED_BOOT_COMPLETED</li>
 * </ul>
 * <p>
 * To receive the Intent.ACTION_LOCKED_BOOT_COMPLETED broadcast, the receiver needs to have
 * <code>directBootAware="true"</code> property in the manifest.
 */
public class BootBroadcastReceiver
        extends BroadcastReceiver {
    @Override
    public void onReceive(Context context,
                          Intent intent) {
        String action = intent.getAction();
        if (Intent.ACTION_LOCKED_BOOT_COMPLETED.equals(action) || Intent.ACTION_BOOT_COMPLETED.equals(action)) {
            Log.d(FavoritesSynchronization.class.getSimpleName(), "Boot event received !");

            NotificationsPullBroadcastReceiver.armNotificationsPullerLoop(context);
            VersesHighlightsSynchronization.armVersesHighlightsSynchronizationLoop(context);
            FavoritesSynchronization.armFavoritesSynchronizationLoop(context);
        }
    }
}