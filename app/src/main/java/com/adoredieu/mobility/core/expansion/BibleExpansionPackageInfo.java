package com.adoredieu.mobility.core.expansion;

public class BibleExpansionPackageInfo
{
    private static final XAPKFile bibleXApkFile = new XAPKFile(
            true,
            // true signifies a main file
            64,
            // the version of the APK that the file was uploaded against
            8877553L
            // the length of the file in bytes
    );

    public static XAPKFile getBibleXApkFile()
    {
        return bibleXApkFile;
    }

    public static class XAPKFile
    {
        public final boolean isMain;
        public final int fileVersion;
        public final long fileSize;

        XAPKFile(boolean isMain,
                 int fileVersion,
                 long fileSize)
        {
            this.isMain = isMain;
            this.fileVersion = fileVersion;
            this.fileSize = fileSize;
        }
    }
}
