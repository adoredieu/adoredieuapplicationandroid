package com.adoredieu.mobility.core.model.website;

import android.os.Parcel;
import android.os.Parcelable;

import org.joda.time.DateTime;

public class ImageQuote
        implements Parcelable
{
    public static final Creator<ImageQuote> CREATOR = new Creator<ImageQuote>()
    {
        public ImageQuote createFromParcel(Parcel source)
        {
            return new ImageQuote(source);
        }

        public ImageQuote[] newArray(int size)
        {
            return new ImageQuote[size];
        }
    };
    private final String title;
    private final String introtext;
    private final String thumbnailUri;
    private final String articleLink;
    private final DateTime dateTime;
    private final String facebook_link;
    private final Long id;
    private final String detailsUri;
    private final Integer page;

    public ImageQuote(Long id,
                      DateTime dateTime,
                      String title,
                      String introtext,
                      String thumbnailUri,
                      String detailsUri,
                      String articleLink,
                      Integer page,
                      String facebook_link)
    {
        this.id = id;
        this.dateTime = dateTime;
        this.title = title;
        this.introtext = introtext;
        this.thumbnailUri = thumbnailUri;
        this.facebook_link = facebook_link;
        this.detailsUri = detailsUri;
        this.articleLink = articleLink;
        this.page = page;
    }

    protected ImageQuote(Parcel in)
    {
        this.title = in.readString();
        this.introtext = in.readString();
        this.thumbnailUri = in.readString();
        this.articleLink = in.readString();
        this.dateTime = (DateTime) in.readSerializable();
        this.facebook_link = in.readString();
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.detailsUri = in.readString();
        this.page = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public String getTitle()
    {
        return title;
    }

    public String getIntrotext()
    {
        return introtext;
    }

    public String getThumbnailUri()
    {
        return thumbnailUri;
    }

    public String getArticleLink()
    {
        return articleLink;
    }

    public DateTime getDateTime()
    {
        return dateTime;
    }

    public String getFacebook_link()
    {
        return facebook_link;
    }

    public Long getId()
    {
        return id;
    }

    public String getDetailsUri()
    {
        return detailsUri;
    }

    public Integer getPage()
    {
        return page;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest,
                              int flags)
    {
        dest.writeString(this.title);
        dest.writeString(this.introtext);
        dest.writeString(this.thumbnailUri);
        dest.writeString(this.articleLink);
        dest.writeSerializable(this.dateTime);
        dest.writeString(this.facebook_link);
        dest.writeValue(this.id);
        dest.writeString(this.detailsUri);
        dest.writeValue(this.page);
    }
}
