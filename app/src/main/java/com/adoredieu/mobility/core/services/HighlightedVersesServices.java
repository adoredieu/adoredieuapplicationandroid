package com.adoredieu.mobility.core.services;

import com.adoredieu.mobility.core.dao.VersesHighlightsDao;
import com.adoredieu.mobility.core.dto.HighlightedVerseDto;
import com.adoredieu.mobility.core.model.highlightedVerses.HighlightedVerseOperation;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;

public class HighlightedVersesServices
{
    private final VersesHighlightsDao versesHighlightsDao;

    @Inject
    public HighlightedVersesServices(VersesHighlightsDao versesHighlightsDao)
    {
        this.versesHighlightsDao = versesHighlightsDao;
    }

    public HighlightedVersesCollection getHighlightedVerses(Integer bookId)
    {
        List<HighlightedVerseOperation> highlightedVerseOperationsByBook = versesHighlightsDao.getHighlightedVerseOperationsByBook(
                bookId);

        return getHighlightedVerses(highlightedVerseOperationsByBook);
    }

    private HighlightedVersesCollection getHighlightedVerses(List<HighlightedVerseOperation> highlightedVerseOperationByBook)
    {
        HighlightedVersesCollection highlightedVersesCollection = new HighlightedVersesCollection();
        for (HighlightedVerseOperation highlightedVerseOperation : highlightedVerseOperationByBook)
        {
            switch (highlightedVerseOperation.getOperationType())
            {
                case CREATE:
                    if (!highlightedVersesCollection.contains(
                            highlightedVerseOperation.getBook(),
                            highlightedVerseOperation.getChapter(),
                            highlightedVerseOperation.getVerse()))
                    {
                        highlightedVersesCollection.add(
                                new HighlightedVerseDto(
                                        new DateTime(highlightedVerseOperation.getDate()),
                                        highlightedVerseOperation.getBook(),
                                        highlightedVerseOperation.getChapter(),
                                        highlightedVerseOperation.getVerse(),
                                        highlightedVerseOperation.getColor(),
                                        highlightedVerseOperation.getOperationType()));
                    }
                    break;
                case UPDATE:
                    highlightedVersesCollection.setColor(
                            highlightedVerseOperation.getBook(),
                            highlightedVerseOperation.getChapter(),
                            highlightedVerseOperation.getVerse(),
                            highlightedVerseOperation.getColor());
                    break;
                case DELETE:
                    highlightedVersesCollection.delete(
                            highlightedVerseOperation.getBook(),
                            highlightedVerseOperation.getChapter(),
                            highlightedVerseOperation.getVerse());
                    break;
            }
        }

        return highlightedVersesCollection;
    }

    public List<HighlightedVerseDto> getHighlightedVersesCondensed()
    {
        List<HighlightedVerseOperation> highlightedVerseOperations = versesHighlightsDao.getHighlightedVerseOperations();
        HighlightedVersesCollection highlightedVersesCollection = getHighlightedVerses(
                highlightedVerseOperations);

        List<HighlightedVerseDto> highlightedVerseDtos = highlightedVersesCollection.getHighlightedVerseDtos();
        Collections.sort(highlightedVerseDtos, new Comparator<HighlightedVerseDto>()
        {
            @Override
            public int compare(HighlightedVerseDto v1,
                               HighlightedVerseDto v2)
            {
                //noinspection UseCompareMethod
                int result = ((Integer) v1.book).compareTo(v2.book);
                if (result == 0)
                {
                    //noinspection UseCompareMethod
                    result = ((Integer) v1.chapter).compareTo(v2.chapter);
                    if (result == 0)
                    {
                        //noinspection UseCompareMethod
                        result = ((Integer) v1.verse).compareTo(v2.verse);
                    }
                }

                return result;
            }
        });

        return highlightedVerseDtos;
    }

    public static class HighlightedVersesCollection
    {
        public List<HighlightedVerseDto> getHighlightedVerseDtos()
        {
            return highlightedVerseDtos;
        }

        private final List<HighlightedVerseDto> highlightedVerseDtos;

        public HighlightedVersesCollection()
        {
            this.highlightedVerseDtos = new ArrayList<>();
        }

        public boolean isHighlighted(int book,
                                     int chapter,
                                     int verse)
        {
            for (HighlightedVerseDto highlightedVerseDto : highlightedVerseDtos)
            {
                if (highlightedVerseDto.book == book
                    && highlightedVerseDto.chapter == chapter
                    && highlightedVerseDto.verse == verse)
                {
                    return true;
                }
            }
            return false;
        }

        public int getHighlightColorIndex(int book,
                                          int chapter,
                                          int verse)
        {
            for (HighlightedVerseDto highlightedVerseDto : highlightedVerseDtos)
            {
                if (highlightedVerseDto.book == book
                    && highlightedVerseDto.chapter == chapter
                    && highlightedVerseDto.verse == verse)
                {
                    return highlightedVerseDto.colorIndex;
                }
            }
            return 0;
        }

        public void add(HighlightedVerseDto highlightedVerseDto)
        {
            highlightedVerseDtos.add(highlightedVerseDto);
        }

        public void setColor(int book,
                             int chapter,
                             int verse,
                             int color)
        {
            for (HighlightedVerseDto highlightedVerseDto : highlightedVerseDtos)
            {
                if (highlightedVerseDto.book == book
                    && highlightedVerseDto.chapter == chapter
                    && highlightedVerseDto.verse == verse)
                {
                    highlightedVerseDto.colorIndex = color;
                    break;
                }
            }
        }

        public void delete(int book,
                           int chapter,
                           int verse)
        {
            for (HighlightedVerseDto highlightedVerseDto : highlightedVerseDtos)
            {
                if (highlightedVerseDto.book == book
                    && highlightedVerseDto.chapter == chapter
                    && highlightedVerseDto.verse == verse)
                {
                    highlightedVerseDtos.remove(highlightedVerseDto);
                    break;
                }
            }
        }

        public boolean contains(int book,
                                int chapter,
                                int verse)
        {
            for (HighlightedVerseDto highlightedVerseDto : highlightedVerseDtos)
            {
                if (highlightedVerseDto.book == book
                    && highlightedVerseDto.chapter == chapter
                    && highlightedVerseDto.verse == verse)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
