package com.adoredieu.mobility.core.dto;

import java.io.Serializable;

public class VerseOfTheDayDto
        implements Serializable
{
    private final Integer _id;
    private final Integer book;
    private final Integer chapter_start;
    private final Integer verse_start;
    private final Integer chapter_end;
    private final Integer verse_end;
    private final String verse;

    public VerseOfTheDayDto(Integer _id,
                            Integer book,
                            Integer chapter_start,
                            Integer verse_start,
                            Integer chapter_end,
                            Integer verse_end,
                            String verse)
    {
        this._id = _id;
        this.book = book;
        this.chapter_start = chapter_start;
        this.verse_start = verse_start;
        this.chapter_end = chapter_end;
        this.verse_end = verse_end;
        this.verse = verse;
    }

    public Integer get_id()
    {
        return _id;
    }

    public Integer getBook()
    {
        return book;
    }

    public Integer getChapter_start()
    {
        return chapter_start;
    }

    public Integer getVerse_start()
    {
        return verse_start;
    }

    public Integer getChapter_end()
    {
        return chapter_end;
    }

    public Integer getVerse_end()
    {
        return verse_end;
    }

    public String getVerse()
    {
        return verse;
    }
}
