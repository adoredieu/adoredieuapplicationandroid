package com.adoredieu.mobility.core.serializer;

import com.adoredieu.mobility.core.dto.HighlightedVerseDto;
import com.adoredieu.mobility.core.dto.VersesHighlightsSynchronisationDto;
import com.adoredieu.mobility.core.helpers.DateTimeHelper;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

public class VersesHighlightsSynchronisationDtoSerializer
        implements JsonSerializer<VersesHighlightsSynchronisationDto>
{
    @Override
    public JsonElement serialize(VersesHighlightsSynchronisationDto src,
                                 Type typeOfSrc,
                                 JsonSerializationContext context)
    {
        final JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("lastSynchronizationDate",
                               DateTimeHelper.toSqlString(src.lastSynchronizationDate));
        jsonObject.addProperty("userId",
                               src.userId);

        final JsonArray jsonArray = new JsonArray();
        for (HighlightedVerseDto highlightedVerseDto : src.highlightedVerseDtos)
        {
            final JsonObject jsonVerseHighlightObject = new JsonObject();
            jsonVerseHighlightObject.addProperty("date",
                                                 DateTimeHelper.toSqlString(highlightedVerseDto.date));
            jsonVerseHighlightObject.addProperty("book",
                                                 highlightedVerseDto.book);
            jsonVerseHighlightObject.addProperty("chapter",
                                                 highlightedVerseDto.chapter);
            jsonVerseHighlightObject.addProperty("verse",
                                                 highlightedVerseDto.verse);
            jsonVerseHighlightObject.addProperty("colorIndex",
                                                 highlightedVerseDto.colorIndex);

            String operationType = "C";
            switch (highlightedVerseDto.operationType)
            {
                case CREATE:
                    operationType = "C";
                    break;
                case READ:
                    operationType = "R";
                    break;
                case UPDATE:
                    operationType = "U";
                    break;
                case DELETE:
                    operationType = "D";
                    break;
            }
            jsonVerseHighlightObject.addProperty("operationType",
                                                 operationType);

            jsonArray.add(jsonVerseHighlightObject);
        }

        jsonObject.add("versesHighlights",
                       jsonArray);

        return jsonObject;
    }
}
