package com.adoredieu.mobility.core.factory;

import com.adoredieu.mobility.controllers.AboutController;
import com.adoredieu.mobility.controllers.ArticlesSearchController;
import com.adoredieu.mobility.controllers.AudioPlayerController;
import com.adoredieu.mobility.controllers.BibleBookChoiceController;
import com.adoredieu.mobility.controllers.BibleChapterChoiceController;
import com.adoredieu.mobility.controllers.BibleController;
import com.adoredieu.mobility.controllers.ReadingPlanChoiceController;
import com.adoredieu.mobility.controllers.ReadingPlanController;
import com.adoredieu.mobility.controllers.BibleSearchController;
import com.adoredieu.mobility.controllers.BibleSearchHelpController;
import com.adoredieu.mobility.controllers.BibleStrongController;
import com.adoredieu.mobility.controllers.BibleStrongSearchController;
import com.adoredieu.mobility.controllers.CommunityController;
import com.adoredieu.mobility.controllers.HighlightsListController;
import com.adoredieu.mobility.controllers.HomeController;
import com.adoredieu.mobility.controllers.ImageQuotesListController;
import com.adoredieu.mobility.controllers.ParametersController;
import com.adoredieu.mobility.controllers.TeachingDetailsController;
import com.adoredieu.mobility.controllers.TeachingsListController;
import com.adoredieu.mobility.controllers.ThoughtDetailsController;
import com.adoredieu.mobility.controllers.ThoughtsListController;
import com.adoredieu.mobility.controllers.base.ArticleDetailsBaseController;
import com.adoredieu.mobility.core.factory.modules.AndroidModule;
import com.adoredieu.mobility.core.factory.modules.ContextModule;
import com.adoredieu.mobility.core.factory.modules.ControllerModule;
import com.adoredieu.mobility.core.factory.modules.DaoModule;
import com.adoredieu.mobility.core.factory.modules.DatabaseModule;
import com.adoredieu.mobility.core.factory.modules.HalModule;
import com.adoredieu.mobility.core.factory.modules.PersistenceModule;
import com.adoredieu.mobility.core.factory.modules.ServicesModule;
import com.adoredieu.mobility.core.system.broadcastReceiver.BootBroadcastReceiver;
import com.adoredieu.mobility.core.system.broadcastReceiver.FavoritesSynchronization;
import com.adoredieu.mobility.core.system.broadcastReceiver.NotificationsPullBroadcastReceiver;
import com.adoredieu.mobility.core.system.broadcastReceiver.VersesHighlightsSynchronization;
import com.adoredieu.mobility.view.activities.AuthenticatorActivity;
import com.adoredieu.mobility.view.activities.LoadingActivity;
import com.adoredieu.mobility.view.activities.MainActivity;
import com.adoredieu.mobility.view.activities.SignUpActivity;
import com.adoredieu.mobility.view.fragments.FragmentBase;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {
        ContextModule.class,
        AndroidModule.class,
        DaoModule.class,
        DatabaseModule.class,
        HalModule.class,
        PersistenceModule.class,
        ServicesModule.class,
        ControllerModule.class,
})
public interface Injector
{
    HomeController provideHomeController();

    AboutController provideAboutController();

    ThoughtDetailsController provideThoughtDetailsController();

    TeachingDetailsController provideTeachingDetailsController();

    BibleSearchController provideBibleSearchController();

    CommunityController provideCommunityController();

    ThoughtsListController provideThoughtsListController();

    ImageQuotesListController provideImageQuotesListController();

    TeachingsListController provideTeachingsListController();

    BibleController provideBibleController();

    ParametersController provideParametersController();

    BibleBookChoiceController provideBibleBookChoiceController();

    BibleChapterChoiceController provideBibleChapterChoiceController();

    BibleSearchHelpController provideBibleSearchHelpController();

    AudioPlayerController provideAudioPlayerController();

    ArticlesSearchController provideArticlesSearchController();

    BibleStrongSearchController provideBibleStrongSearchController();

    BibleStrongController provideBibleStrongController();

    HighlightsListController provideHighlightListController();

    ReadingPlanChoiceController provideReadingPlanChoiceController();

    ReadingPlanController provideReadingPlanController();

    void inject(BootBroadcastReceiver bootBroadcastReceiver);

    void inject(AuthenticatorActivity authenticatorActivity);

    void inject(LoadingActivity loadingActivity);

    void inject(MainActivity mainActivity);

    void inject(SignUpActivity signUpActivity);

    void inject(AboutController aboutController);

    void inject(ArticlesSearchController articlesSearchController);

    void inject(AudioPlayerController audioPlayerController);

    void inject(BibleBookChoiceController bibleBookChoiceController);

    void inject(BibleChapterChoiceController bibleChapterChoiceController);

    void inject(BibleController bibleController);

    void inject(BibleSearchController bibleSearchController);

    void inject(BibleSearchHelpController bibleSearchHelpController);

    void inject(BibleStrongController bibleStrongController);

    void inject(BibleStrongSearchController bibleStrongSearchController);

    void inject(CommunityController communityController);

    void inject(HomeController homeController);

    void inject(ImageQuotesListController imageQuotesListController);

    void inject(ParametersController parametersController);

    void inject(TeachingDetailsController teachingDetailsController);

    void inject(TeachingsListController teachingsListController);

    void inject(ThoughtDetailsController thoughtDetailsController);

    void inject(ThoughtsListController thoughtsListController);

    void inject(ArticleDetailsBaseController articleDetailsBaseController);

    void inject(HighlightsListController highlightsListController);

    void inject(ReadingPlanChoiceController readingPlanChoiceController);

    void inject(ReadingPlanController readingPlanController);

    void inject(FragmentBase fragmentBase);

    void inject(FavoritesSynchronization favoritesSynchronization);

    void inject(VersesHighlightsSynchronization versesHighlightsSynchronization);

    void inject(NotificationsPullBroadcastReceiver notificationsPullBroadcastReceiver);
}
