package com.adoredieu.mobility.core.factory.modules;

import com.adoredieu.mobility.controllers.AboutController;
import com.adoredieu.mobility.controllers.ArticlesSearchController;
import com.adoredieu.mobility.controllers.AudioPlayerController;
import com.adoredieu.mobility.controllers.BibleBookChoiceController;
import com.adoredieu.mobility.controllers.BibleChapterChoiceController;
import com.adoredieu.mobility.controllers.BibleController;
import com.adoredieu.mobility.controllers.BibleSearchController;
import com.adoredieu.mobility.controllers.BibleSearchHelpController;
import com.adoredieu.mobility.controllers.BibleStrongController;
import com.adoredieu.mobility.controllers.BibleStrongSearchController;
import com.adoredieu.mobility.controllers.CommunityController;
import com.adoredieu.mobility.controllers.HighlightsListController;
import com.adoredieu.mobility.controllers.HomeController;
import com.adoredieu.mobility.controllers.ImageQuotesListController;
import com.adoredieu.mobility.controllers.ParametersController;
import com.adoredieu.mobility.controllers.ReadingPlanChoiceController;
import com.adoredieu.mobility.controllers.ReadingPlanController;
import com.adoredieu.mobility.controllers.TeachingDetailsController;
import com.adoredieu.mobility.controllers.TeachingsListController;
import com.adoredieu.mobility.controllers.ThoughtDetailsController;
import com.adoredieu.mobility.controllers.ThoughtsListController;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ControllerModule {
    //////////////////////////////////////////////////////////////////
    //                      NON SINGLETON
    //////////////////////////////////////////////////////////////////
    @Provides
    BibleController provideBibleController() {
        return new BibleController();
    }

    @Provides
    BibleStrongController provideBibleStrongController() {
        return new BibleStrongController();
    }

    @Provides
    ReadingPlanController provideReadingPlanController() {
        return new ReadingPlanController();
    }

    @Provides
    AboutController provideAboutController() {
        return new AboutController();
    }

    @Provides
    ReadingPlanChoiceController provideReadingPlanChoiceController() {
        return new ReadingPlanChoiceController();
    }

    @Provides
    ArticlesSearchController provideArticlesSearchController() {
        return new ArticlesSearchController();
    }

    @Provides
    BibleBookChoiceController provideBibleBookChoiceController() {
        return new BibleBookChoiceController();
    }

    @Provides
    HighlightsListController provideHighlightsListController() {
        return new HighlightsListController();
    }

    @Provides
    BibleChapterChoiceController provideBibleChapterChoiceController() {
        return new BibleChapterChoiceController();
    }

    @Provides
    BibleStrongSearchController provideBibleStrongSearchController() {
        return new BibleStrongSearchController();
    }

    @Provides
    BibleSearchController provideBibleSearchController() {
        return new BibleSearchController();
    }

    @Provides
    BibleSearchHelpController provideBibleSearchHelpController() {
        return new BibleSearchHelpController();
    }

    @Provides
    CommunityController provideCommunityController() {
        return new CommunityController();
    }

    @Provides
    HomeController provideHomeController() {
        return new HomeController();
    }

    @Provides
    ImageQuotesListController provideImageQuotesListController() {
        return new ImageQuotesListController();
    }

    @Provides
    ParametersController provideParametersController() {
        return new ParametersController();
    }

    @Provides
    TeachingDetailsController provideTeachingDetailsController() {
        return new TeachingDetailsController();
    }

    @Provides
    TeachingsListController provideTeachingsListController() {
        return new TeachingsListController();
    }

    @Provides
    ThoughtDetailsController provideThoughtDetailsController() {
        return new ThoughtDetailsController();
    }

    @Provides
    ThoughtsListController provideThoughtsListController() {
        return new ThoughtsListController();
    }

    //////////////////////////////////////////////////////////////////
    //                          SINGLETON
    //////////////////////////////////////////////////////////////////
    @Provides
    @Singleton
    AudioPlayerController provideAudioPlayerController() {
        return new AudioPlayerController();
    }
}
