package com.adoredieu.mobility.core.factory.modules;

import android.content.Context;

import com.adoredieu.mobility.core.authentication.AdoreDieuServerAuthenticate;
import com.adoredieu.mobility.core.interfaces.authentication.ServerAuthenticate;
import com.adoredieu.mobility.core.persistence.ApplicationContextPersistence;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AndroidModule
{
    @Provides
    @Singleton
    public ServerAuthenticate provideServerAuthenticate(Context context,
                                                        ApplicationContextPersistence applicationContextPersistence)
    {
        return new AdoreDieuServerAuthenticate(context,
                                               applicationContextPersistence);
    }
}
