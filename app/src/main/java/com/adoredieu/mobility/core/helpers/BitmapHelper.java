package com.adoredieu.mobility.core.helpers;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Build;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.util.Base64;
import android.util.DisplayMetrics;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class BitmapHelper
{
    public static Bitmap ByteArrayToImage(byte[] imageBytes,
                                          DisplayMetrics displayMetrics)
    {
        Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes,
                                                            0,
                                                            imageBytes.length);

        switch (displayMetrics.densityDpi)
        {
            case DisplayMetrics.DENSITY_HIGH:
                decodedImage.setDensity(DisplayMetrics.DENSITY_MEDIUM);
                break;
            case DisplayMetrics.DENSITY_XHIGH:
                decodedImage.setDensity(DisplayMetrics.DENSITY_HIGH);
                break;
            case DisplayMetrics.DENSITY_XXHIGH:
                decodedImage.setDensity(DisplayMetrics.DENSITY_XHIGH);
                break;
            default:
                // Ne rien faire, on considère que le qrcode est mdpi.
                break;
        }

        return decodedImage;
    }

    public static Bitmap Base64ToImage(String base64EncodedImage)
    {
        byte[] decodedBytes = Base64.decode(base64EncodedImage,
                                            Base64.DEFAULT);

        return BitmapFactory.decodeByteArray(decodedBytes,
                                             0,
                                             decodedBytes.length);
    }

    public static Bitmap resizeBitmap(Bitmap bitmap,
                                      int newWidth,
                                      int newHeight)
    {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth,
                         scaleHeight);

        // "RECREATE" THE NEW BITMAP
        return Bitmap.createBitmap(bitmap,
                                   0,
                                   0,
                                   width,
                                   height,
                                   matrix,
                                   false);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public static Bitmap applyBlur(Context context,
                                   Bitmap bitmap,
                                   float radius)
    {
        Bitmap outputBitmap = Bitmap.createBitmap(bitmap.getWidth(),
                                                  bitmap.getHeight(),
                                                  Bitmap.Config.ARGB_8888);
        final RenderScript rs = RenderScript.create(context);
        final Allocation inputAllocation = Allocation.createFromBitmap(rs,
                                                                       bitmap,
                                                                       Allocation.MipmapControl.MIPMAP_NONE,
                                                                       Allocation.USAGE_SCRIPT);
        final Allocation outputAllocation = Allocation.createTyped(rs,
                                                                   inputAllocation.getType());
        final ScriptIntrinsicBlur script = ScriptIntrinsicBlur.create(rs,
                                                                      Element.U8_4(rs));
        script.setRadius(radius);
        script.setInput(inputAllocation);
        script.forEach(outputAllocation);
        outputAllocation.copyTo(outputBitmap);

        return outputBitmap;
    }

    public static void saveTo(Bitmap bitmap,
                              File outputFile)
    {
        FileOutputStream out = null;
        try
        {
            out = new FileOutputStream(outputFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG,
                            90,
                            out);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                if (out != null)
                {
                    out.close();
                }
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }
}
