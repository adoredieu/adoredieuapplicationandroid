package com.adoredieu.mobility.core.services;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerFuture;
import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;

import com.adoredieu.mobility.core.helpers.AsyncTaskHelper;
import com.adoredieu.mobility.core.interfaces.AdoreDieuActionResultListener;
import com.adoredieu.mobility.core.interfaces.authentication.Authentication;
import com.adoredieu.mobility.core.persistence.ApplicationContextPersistence;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

public class AdoreDieuServices {
    private static final String accountType = "com.adoredieu.mobility";
    private final AccountManager accountManager;
    private final ApplicationContextPersistence applicationContextPersistence;

    private static class LoginAsyncTask extends AsyncTask<Void, Void, Void> {
        private final WeakReference<Activity> activityWeakReference;
        private final AdoreDieuActionResultListener adoreDieuActionResultListener;
        private final AccountManager accountManager;
        private final ApplicationContextPersistence applicationContextPersistence;

        public LoginAsyncTask(Activity activity,
                              AdoreDieuActionResultListener adoreDieuActionResultListener,
                              AccountManager accountManager,
                              ApplicationContextPersistence applicationContextPersistence) {
            this.activityWeakReference = new WeakReference<>(activity);
            this.adoreDieuActionResultListener = adoreDieuActionResultListener;
            this.accountManager = accountManager;
            this.applicationContextPersistence = applicationContextPersistence;
        }

        @Override
        protected Void doInBackground(Void... params) {
            Activity activity = activityWeakReference.get();
            if (activity == null)
                return null;

            AccountManagerFuture<Bundle> authTokenByFeatures = accountManager.getAuthTokenByFeatures(
                    accountType,
                    Authentication.AUTHTOKEN_TYPE_FULL_ACCESS,
                    null,
                    activity,
                    null,
                    null,
                    null,
                    null);

            try {
                Bundle result = authTokenByFeatures.getResult();

                if (authTokenByFeatures.isDone()) {
                    Integer currentUserId = applicationContextPersistence.getCurrentUserId();
                    if (currentUserId == 0) {
                        String authtoken = result.get("authtoken").toString();
                        String[] split = authtoken.split("\\|");

                        applicationContextPersistence.setCurrentUserId(Integer.parseInt(split[1]));
                        applicationContextPersistence.setCurrentUserName(split[2]);
                        applicationContextPersistence.setCurrentUserName(split[3]);
                    }

                    adoreDieuActionResultListener.onAdoreDieuLoginSuccess();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }
    }

    @Inject
    public AdoreDieuServices(Context context,
                             ApplicationContextPersistence applicationContextPersistence) {
        this.applicationContextPersistence = applicationContextPersistence;
        accountManager = AccountManager.get(context);
    }

    public boolean isLoggedIn() {
        List<Account> accounts = getAccounts();
        return accounts.size() > 0 && applicationContextPersistence.getCurrentUserId() != 0;
    }

    private List<Account> getAccounts() {
        Account[] accountsByType = accountManager.getAccountsByType(accountType);

        ArrayList<Account> result = new ArrayList<>();

        Collections.addAll(result, accountsByType);

        return result;
    }

    public void login(final Activity activity,
                      final AdoreDieuActionResultListener adoreDieuActionResultListener) {
        LoginAsyncTask loginAsyncTask = new LoginAsyncTask(
                activity,
                adoreDieuActionResultListener,
                accountManager,
                applicationContextPersistence);

        AsyncTaskHelper.<Void>executeAsyncTask(loginAsyncTask);
    }

    public void logout(AdoreDieuActionResultListener adoreDieuActionResultListener) {
        applicationContextPersistence.setCurrentUserId(0);
        applicationContextPersistence.setCurrentUserName("");
        applicationContextPersistence.setCurrentUserName("");

        adoreDieuActionResultListener.onAdoreDieuLoginSuccess();
    }
}
