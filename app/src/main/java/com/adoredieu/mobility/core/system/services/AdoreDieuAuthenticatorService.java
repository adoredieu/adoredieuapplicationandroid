package com.adoredieu.mobility.core.system.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.adoredieu.mobility.core.authentication.AdoreDieuAccountAuthenticator;
import com.adoredieu.mobility.core.authentication.AdoreDieuServerAuthenticate;
import com.adoredieu.mobility.core.persistence.ApplicationContextPersistence;

public class AdoreDieuAuthenticatorService
        extends Service
{
    @Override
    public IBinder onBind(Intent intent)
    {
        AdoreDieuAccountAuthenticator authenticator = new AdoreDieuAccountAuthenticator(
                this,
                new AdoreDieuServerAuthenticate(
                        this,
                        new ApplicationContextPersistence(this)));

        return authenticator.getIBinder();
    }
}