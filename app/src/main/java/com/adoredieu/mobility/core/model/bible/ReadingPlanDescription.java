package com.adoredieu.mobility.core.model.bible;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "reading_plans_desc")
public class ReadingPlanDescription
{
    public static final String PLAN_ID_COLUMN = "_id";
    public static final String PLAN_NAME_COLUMN = "name";
    public static final String PLAN_DESCRIPTION_COLUMN = "description";

    @DatabaseField(id = true, columnName = PLAN_ID_COLUMN)
    private final Integer _id;

    @DatabaseField(columnName = PLAN_NAME_COLUMN)
    private final String name;

    @DatabaseField(columnName = PLAN_DESCRIPTION_COLUMN)
    private final String description;

    public ReadingPlanDescription(String name,
                                  String description)
    {
        _id = 0;
        this.name = name;
        this.description = description;
    }

    public ReadingPlanDescription()
    {
        _id = 0;
        this.name = "";
        this.description = "";
    }

    public Integer get_id()
    {
        return _id;
    }

    public String getName()
    {
        return name;
    }

    public String getDescription()
    {
        return description;
    }
}
