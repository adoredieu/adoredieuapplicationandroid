package com.adoredieu.mobility.core.helpers;

import android.content.Context;
import android.support.annotation.DrawableRes;
import android.util.SparseBooleanArray;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.core.interfaces.VerseDefinition;
import com.adoredieu.mobility.core.services.BibleServices;
import com.adoredieu.mobility.view.adapters.BaseVersesArrayAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

public class VersesHelper {
    public static List<VerseDefinition> getCheckedVerses(StickyListHeadersListView versesListView, BaseVersesArrayAdapter<VerseDefinition> versesArrayAdapter) {
        List<VerseDefinition> checkedVerses = new ArrayList<>();

        SparseBooleanArray checkedItemPositions = versesListView.getCheckedItemPositions();
        for (int i = 0; i < checkedItemPositions.size(); i++) {
            if (!checkedItemPositions.valueAt(i)) {
                continue;
            }

            int verseListId = checkedItemPositions.keyAt(i);
            VerseDefinition item = versesArrayAdapter.getItem(verseListId);
            checkedVerses.add(item);
        }

        return checkedVerses;
    }

    @DrawableRes
    public static int getVerseBackgroundColor(int highlightIndex,
                                              boolean nightMode,
                                              boolean checked) {
        if (highlightIndex != 0) {
            int background;
            switch (highlightIndex) {
                case 1:
                    background = nightMode ? R.drawable.bg_lvi_verse_night_color_1 : R.drawable.bg_lvi_verse_color_1;
                    break;
                case 2:
                    background = nightMode ? R.drawable.bg_lvi_verse_night_color_2 : R.drawable.bg_lvi_verse_color_2;
                    break;
                case 3:
                    background = nightMode ? R.drawable.bg_lvi_verse_night_color_3 : R.drawable.bg_lvi_verse_color_3;
                    break;
                case 4:
                    background = nightMode ? R.drawable.bg_lvi_verse_night_color_4 : R.drawable.bg_lvi_verse_color_4;
                    break;
                case 5:
                    background = nightMode ? R.drawable.bg_lvi_verse_night_color_5 : R.drawable.bg_lvi_verse_color_5;
                    break;
                default:
                    background = nightMode ? R.drawable.bg_lvi_verse_night : R.drawable.bg_lvi_verse;
                    break;
            }

            if (checked) {
                return nightMode ? R.drawable.bg_lvi_verse_checked_night : R.drawable.bg_lvi_verse_checked;
            } else {
                return background;
            }
        } else {
            if (checked) {
                return nightMode ? R.drawable.bg_lvi_verse_checked_night : R.drawable.bg_lvi_verse_checked;
            } else {
                return nightMode ? R.drawable.bg_lvi_verse_night : R.drawable.bg_lvi_verse;
            }
        }

    }

    public static VersesText getVersesAsText(Context context,
                                             BibleServices bibleServices,
                                             List<VerseDefinition> verses) {
        HashMap<Integer, List<Range>> ranges = new HashMap<>();
        int previousChapter = -1;
        int previousVerse = -1;
        String bookName = "";

        StringBuilder content = new StringBuilder();
        for (int i = 0; i < verses.size(); i++) {
            VerseDefinition item = verses.get(i);
            bookName = bibleServices.getLocalizedBooksNames().get(item.getBookId());

            if (item.getChapter() != previousChapter) {
                ranges.put(item.getChapter(),
                        new ArrayList<Range>());
                previousVerse = -1;

                if (previousChapter > 0) {
                    content.append("\n");
                }
            }

            List<Range> chapterRanges = ranges.get(item.getChapter());
            if (item.getVerse() != previousVerse + 1) {
                chapterRanges.add(new Range(item.getVerse()));

                if (previousVerse > 0) {
                    content.append("[...]\n");
                }

                content.append(item.getChapter())
                        .append(":")
                        .append(item.getVerse())
                        .append(" ")
                        .append(item.getText())
                        .append("\n");
            } else {
                chapterRanges.get(ranges.get(item.getChapter())
                        .size() - 1)
                        .setEnd(item.getVerse());
                content.append(item.getChapter())
                        .append(":")
                        .append(item.getVerse())
                        .append(" ")
                        .append(item.getText())
                        .append("\n");
            }

            previousChapter = item.getChapter();
            previousVerse = item.getVerse();
        }

        StringBuilder title = new StringBuilder();
        title.append(bookName).append(" ");

        int index = 0;
        for (Map.Entry<Integer, List<Range>> entry : ranges.entrySet()) {
            if (index > 0) {
                title.append("; ");
            }

            title.append(entry.getKey())
                    .append(":");

            for (int i = 0; i < entry.getValue()
                    .size(); i++) {
                Range range = entry.getValue()
                        .get(i);

                if (range.end == null) {
                    title.append(range.getStart()
                            .toString());
                } else {
                    title.append(range.getStart())
                            .append("-")
                            .append(range.getEnd());
                }

                if (i < entry.getValue()
                        .size() - 1) {
                    title.append(",");
                }
            }

            index++;
        }
        title.append(" :");

        return new VersesText(title.toString(),
                content.toString());
    }

    public static class VersesText {
        final String title;
        final String content;

        public VersesText(String title,
                          String content) {
            this.title = title;
            this.content = content;
        }

        public String getTitle() {
            return title;
        }

        public String getContent() {
            return content;
        }
    }

    private static class Range {
        Integer start;
        Integer end;

        private Range(Integer start) {

            this.start = start;
        }

        public Integer getStart() {
            return start;
        }

        public void setStart(Integer start) {
            this.start = start;
        }

        public Integer getEnd() {
            return end;
        }

        public void setEnd(Integer end) {
            this.end = end;
        }
    }
}
