package com.adoredieu.mobility.core.interfaces;

import android.os.Bundle;
import android.view.View;

public interface FragmentEventsListener
{
    void onViewCreated(View view,
                       Bundle savedInstanceState);

    void onPause();

    void onResume();

    void onSaveInstanceState(Bundle outState);

    void onViewStateRestored(Bundle savedInstanceState);
}
