package com.adoredieu.mobility.core.helpers;

import android.content.Context;

public class ResourcesHelper
{
    public static String getStringResourceByName(Context context,
                                                 String refString)
    {
        String formatedRefString = refString.replace(" ",
                                                     "_");
        if (formatedRefString.length() > 0 && Character.isDigit(formatedRefString.charAt(0)))
        {
            formatedRefString = "_" + formatedRefString;
        }

        String packageName = context.getPackageName();
        int resId = context.getResources()
                           .getIdentifier(formatedRefString,
                                          "string",
                                          packageName);

        if (resId == 0)
        {
            return refString;
        }
        else
        {
            return context.getString(resId);
        }
    }
}
