package com.adoredieu.mobility.core.system.notifications;

import android.content.Context;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.core.helpers.Environment;
import com.adoredieu.mobility.core.helpers.NotificationHelper;
import com.adoredieu.mobility.core.interfaces.Action;
import com.adoredieu.mobility.core.interfaces.ArticleType;
import com.adoredieu.mobility.core.interfaces.NotificationsTags;
import com.adoredieu.mobility.core.model.website.Article;
import com.adoredieu.mobility.core.persistence.ApplicationContextPersistence;
import com.adoredieu.mobility.core.services.ArticlesServices;

import org.joda.time.DateTime;

public class ThoughtsNotification
{
    public static void pullNotificationIfNeeded(final Context context,
                                                final ApplicationContextPersistence applicationContextPersistence)
    {
        final ArticlesServices articlesServices = new ArticlesServices(context,
                                                                       ArticleType.Thought);
        articlesServices.getLastArticleDate(
                new Action<DateTime>()
                {
                    @Override
                    public void run(final DateTime dateTime)
                    {
                        if (dateTime != null)
                        {
                            DateTime savedLastThoughtsDate = applicationContextPersistence.getLastThoughtsDate();
                            if (dateTime.isAfter(savedLastThoughtsDate))
                            {
                                articlesServices.getLastArticle(
                                        new Action<Article>()
                                        {
                                            @Override
                                            public void run(Article article)
                                            {
                                                NotificationHelper.publishTextNotification(
                                                        context,
                                                        context.getString(R.string.nouvelle_pensee),
                                                        article.title
                                                        + Environment.NewLine
                                                        + context.getString(R.string.cliquez_pour_lancer_l_application),
                                                        context.getString(R.string.notification_action_pensee),
                                                        NotificationsTags.Thought);

                                            }
                                        });
                            }

                            applicationContextPersistence.setLastThoughtsDate(dateTime);
                        }
                    }
                });
    }
}
