package com.adoredieu.mobility.core.dao;

import com.adoredieu.mobility.core.db.OrmBibleDatabase;
import com.adoredieu.mobility.core.model.bible.ReadingPlanDescription;
import com.j256.ormlite.dao.RuntimeExceptionDao;

import java.util.List;

import javax.inject.Inject;

public class ReadingPlanDescriptionDao
{
    private final RuntimeExceptionDao<ReadingPlanDescription, Integer> dao;

    @Inject
    public ReadingPlanDescriptionDao(OrmBibleDatabase db)
    {
        dao = db.getReadingPlanDescriptionDao();
    }

    public List<ReadingPlanDescription> getReadingPlanDescriptions()
    {
        return dao.queryForAll();
    }
}
