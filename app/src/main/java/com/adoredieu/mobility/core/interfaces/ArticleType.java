package com.adoredieu.mobility.core.interfaces;

public enum ArticleType
{
    Thought,
    Teaching,
    ImageQuotes,
}
