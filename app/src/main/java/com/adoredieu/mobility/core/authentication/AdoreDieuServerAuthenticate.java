package com.adoredieu.mobility.core.authentication;

import android.accounts.AuthenticatorException;
import android.accounts.NetworkErrorException;
import android.content.Context;
import android.support.annotation.NonNull;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.core.helpers.WebHelper;
import com.adoredieu.mobility.core.interfaces.authentication.ServerAuthenticate;
import com.adoredieu.mobility.core.persistence.ApplicationContextPersistence;
import com.google.gson.JsonObject;

import java.util.UUID;

import javax.inject.Inject;

public class AdoreDieuServerAuthenticate
        implements ServerAuthenticate
{
    private final Context context;
    private final ApplicationContextPersistence applicationContextPersistence;

    @Inject
    public AdoreDieuServerAuthenticate(Context context,
                                       ApplicationContextPersistence applicationContextPersistence)
    {
        this.context = context;
        this.applicationContextPersistence = applicationContextPersistence;
    }

    @Override
    public String userSignUp(String fullName,
                             String username,
                             String password,
                             String email) throws AuthenticatorException, NetworkErrorException
    {
        JsonObject jsonObject = WebHelper.downloadJSonObjectBlocking(
                context,
                context.getString(R.string.adoredieu_signup_www_address,
                                  fullName,
                                  username,
                                  password,
                                  email));

        if (jsonObject != null)
        {
            String message = jsonObject.get("message").getAsString();
            if (message.toLowerCase().contains("success"))
            {
                return userSignIn(username,
                                  password);
            }
            else
            {
                String exceptionMessage = message.substring(message.indexOf("Error:") + "Error:"
                        .length()).trim();

                throw new AuthenticatorException(exceptionMessage);
            }
        }
        else
        {
            throw new NetworkErrorException(context.getString(R.string.pas_d_internet));
        }
    }

    @NonNull
    private String generateAuthToken(Integer userId,
                                     String username,
                                     String name)
    {
        return UUID.randomUUID().toString().toUpperCase() +
               "|" + userId +
               "|" + username +
               "|" + name;
    }

    @Override
    public String userSignIn(String username,
                             String password) throws AuthenticatorException, NetworkErrorException
    {
        JsonObject jsonObject = WebHelper.downloadJSonObjectBlocking(
                context,
                context.getString(R.string.adoredieu_login_www_address,
                                  username,
                                  password));

        if (jsonObject != null)
        {
            int success = jsonObject.get("success")
                                        .getAsInt();

            if (success == 1)
            {
                Integer userId = jsonObject.get("id")
                                           .getAsInt();
                applicationContextPersistence.setCurrentUserId(userId);

                String login = jsonObject.get("login")
                                         .getAsString();
                applicationContextPersistence.setCurrentUserUsername(login);

                String displayName = jsonObject.get("display_name")
                                               .getAsString();
                applicationContextPersistence.setCurrentUserName(displayName);

                String picture = jsonObject.get("picture")
                                           .getAsString();
                if (picture != null)
                {
                    applicationContextPersistence.setCurrentUserPicture(picture);
                }

                return generateAuthToken(userId,
                                         username,
                                         displayName);
            }
            else
            {
                throw new AuthenticatorException(context.getString(R.string.login_error));
            }
        }
        else
        {
            throw new NetworkErrorException(context.getString(R.string.pas_d_internet));
        }
    }
}
