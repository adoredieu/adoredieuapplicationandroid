package com.adoredieu.mobility.core.dto;

import android.os.Parcel;
import android.os.Parcelable;

import com.adoredieu.mobility.core.model.bible.Testament;

public class TestamentDto
        implements Parcelable
{
    public static final Creator<TestamentDto> CREATOR = new Creator<TestamentDto>()
    {
        public TestamentDto createFromParcel(Parcel source)
        {
            return new TestamentDto(source);
        }

        public TestamentDto[] newArray(int size)
        {
            return new TestamentDto[size];
        }
    };
    private Integer id;
    private String name;

    public TestamentDto()
    {
    }

    public TestamentDto(Integer id,
                        String name)
    {
        this.id = id;
        this.name = name;
    }

    private TestamentDto(Parcel in)
    {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.name = in.readString();
    }

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest,
                              int flags)
    {
        dest.writeValue(this.id);
        dest.writeString(this.name);
    }

    public static TestamentDto fromTestament(Testament testament)
    {
        return new TestamentDto(testament.getId(), testament.getName());
    }
}
