package com.adoredieu.mobility.core.interfaces;

public enum SearchOperator {
    AND,
    OR,
    EXACTLY,
}
