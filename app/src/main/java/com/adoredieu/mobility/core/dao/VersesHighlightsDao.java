package com.adoredieu.mobility.core.dao;

import com.adoredieu.mobility.core.db.HighlightedVersesDatabase;
import com.adoredieu.mobility.core.interfaces.DateProvider;
import com.adoredieu.mobility.core.model.highlightedVerses.HighlightedVerseOperation;
import javax.inject.Inject;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class VersesHighlightsDao
{
    private final RuntimeExceptionDao<HighlightedVerseOperation, Integer> dao;
    private final DateProvider dateProvider;

    @Inject
    public VersesHighlightsDao(HighlightedVersesDatabase db,
                               DateProvider dateProvider)
    {
        this.dateProvider = dateProvider;
        this.dao = db.getHighlightedVersesOperationsDao();
    }

    public List<HighlightedVerseOperation> getHighlightedVerseOperationsAfterDate(Date date)
    {
        try
        {
            QueryBuilder<HighlightedVerseOperation, Integer> queryBuilder = dao.queryBuilder();
            queryBuilder.orderBy(HighlightedVerseOperation.DATE_COLUMN, true)
                        .where()
                        .ge(HighlightedVerseOperation.DATE_COLUMN,
                            date);
            PreparedQuery<HighlightedVerseOperation> preparedQuery = queryBuilder.prepare();

            return dao.query(preparedQuery);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public List<HighlightedVerseOperation> getHighlightedVerseOperationsByBook(int bookId)
    {
        try
        {
            QueryBuilder<HighlightedVerseOperation, Integer> queryBuilder = dao.queryBuilder();
            queryBuilder.orderBy(HighlightedVerseOperation.DATE_COLUMN, true)
                        .where()
                        .eq(HighlightedVerseOperation.VERSE_BOOK_COLUMN,
                            bookId);
            PreparedQuery<HighlightedVerseOperation> preparedQuery = queryBuilder.prepare();

            return dao.query(preparedQuery);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public List<HighlightedVerseOperation> getHighlightedVerseOperations()
    {
        try
        {
            QueryBuilder<HighlightedVerseOperation, Integer> queryBuilder = dao.queryBuilder();
            queryBuilder.orderBy(HighlightedVerseOperation.DATE_COLUMN, true);
            PreparedQuery<HighlightedVerseOperation> preparedQuery = queryBuilder.prepare();

            return dao.query(preparedQuery);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public List<HighlightedVerseOperation> getHighlightedVerseOperationsForVerse(Integer bookId,
                                                                                 Integer chapterNumber,
                                                                                 Integer verseNumber)
    {
        try
        {
            QueryBuilder<HighlightedVerseOperation, Integer> queryBuilder = dao.queryBuilder();
            queryBuilder.orderBy(HighlightedVerseOperation.DATE_COLUMN, true)
                        .where()
                        .eq(HighlightedVerseOperation.VERSE_BOOK_COLUMN,
                            bookId)
                        .and()
                        .eq(HighlightedVerseOperation.VERSE_CHAPTER_COLUMN,
                            chapterNumber)
                        .and()
                        .eq(HighlightedVerseOperation.VERSE_NUMBER_COLUMN,
                            verseNumber);
            PreparedQuery<HighlightedVerseOperation> preparedQuery = queryBuilder.prepare();

            return dao.query(preparedQuery);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public void deleteVerseHighlight(Integer bookId,
                                     Integer chapterNumber,
                                     Integer verseNumber)
    {
        dao.create(new HighlightedVerseOperation(
                dateProvider.getDateTime().toDate(),
                bookId,
                chapterNumber,
                verseNumber,
                0,
                HighlightedVerseOperation.OperationType.DELETE));
    }

    public void addOrUpdateVerseHighlight(Integer bookId,
                                          Integer chapterNumber,
                                          Integer verseNumber,
                                          Integer colorIndex)
    {
        List<HighlightedVerseOperation> highlightedVerseOperation = getHighlightedVerseOperationsForVerse(
                bookId,
                chapterNumber,
                verseNumber);

        if (highlightedVerseOperation.size() <= 0
            || highlightedVerseOperation.get(highlightedVerseOperation.size() - 1)
                                        .getOperationType() == HighlightedVerseOperation.OperationType.DELETE)
        {
            // CREATE
            dao.create(new HighlightedVerseOperation(
                    dateProvider.getDateTime().toDate(),
                    bookId,
                    chapterNumber,
                    verseNumber,
                    colorIndex,
                    HighlightedVerseOperation.OperationType.CREATE));
        }
        else
        {
            // UPDATE
            dao.create(new HighlightedVerseOperation(
                    dateProvider.getDateTime().toDate(),
                    bookId,
                    chapterNumber,
                    verseNumber,
                    colorIndex,
                    HighlightedVerseOperation.OperationType.UPDATE));
        }
    }

    public void addVerseHighlight(Date dateTime,
                                  Integer bookId,
                                  Integer chapterNumber,
                                  Integer verseNumber,
                                  Integer colorIndex,
                                  HighlightedVerseOperation.OperationType operationType)
    {
        dao.create(new HighlightedVerseOperation(
                dateTime,
                bookId,
                chapterNumber,
                verseNumber,
                colorIndex,
                operationType));
    }

    public boolean contains(Date date,
                            Integer bookId,
                            Integer chapterNumber,
                            Integer verseNumber,
                            Integer colorIndex,
                            HighlightedVerseOperation.OperationType operationType)
    {
        try
        {
            QueryBuilder<HighlightedVerseOperation, Integer> queryBuilder = dao.queryBuilder();
            queryBuilder.where()
                        .eq(HighlightedVerseOperation.DATE_COLUMN,
                            date)
                        .and()
                        .eq(HighlightedVerseOperation.VERSE_BOOK_COLUMN,
                            bookId)
                        .and()
                        .eq(HighlightedVerseOperation.VERSE_CHAPTER_COLUMN,
                            chapterNumber)
                        .and()
                        .eq(HighlightedVerseOperation.VERSE_NUMBER_COLUMN,
                            verseNumber)
                        .and()
                        .eq(HighlightedVerseOperation.VERSE_COLOR_COLUMN,
                            colorIndex)
                        .and()
                        .eq(HighlightedVerseOperation.OPERATION_TYPE_COLUMN,
                            operationType);
            PreparedQuery<HighlightedVerseOperation> preparedQuery = queryBuilder.prepare();

            return dao.query(preparedQuery).size() > 0;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return false;
        }
    }
}
