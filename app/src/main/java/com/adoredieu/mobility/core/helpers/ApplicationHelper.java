package com.adoredieu.mobility.core.helpers;

import android.content.Context;
import android.support.v4.app.FragmentActivity;

import java.io.File;

public class ApplicationHelper
{
    public static void exit(FragmentActivity activityBase)
    {
        activityBase.finish();
    }

    public static void cleanCache(Context context)
    {
        try
        {
            File dir = context.getCacheDir();
            IOHelpers.deleteDir(dir);
            dir.mkdirs();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
