package com.adoredieu.mobility.core.cache;

import com.adoredieu.mobility.core.services.ArticleServices;

import java.util.HashMap;
import java.util.Map;

public class ArticlesCache
{
    private final Map<String, ArticleServices.ArticleContent> cache;

    public ArticlesCache()
    {
        cache = new HashMap<>();
    }

    public void clear()
    {
        cache.clear();
    }

    public void add(String url,
                    ArticleServices.ArticleContent content)
    {
        cache.put(url,
                  content);
    }

    public boolean containsKey(String url)
    {
        return cache.containsKey(url);
    }

    public ArticleServices.ArticleContent getArticle(String url)
    {
        if (containsKey(url))
        {
            return cache.get(url);
        }

        return null;
    }
}
