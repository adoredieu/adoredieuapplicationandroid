package com.adoredieu.mobility.core.helpers;

import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class LocalTimeHelper
{
    public static LocalTime parse(String timeString)
    {
        try
        {
            if (!timeString.isEmpty())
            {
                DateTimeFormatter fmt = DateTimeFormat.forPattern("HH:mm:ss");
                return fmt.parseLocalTime(timeString);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return new LocalTime(0, 0, 0);
    }

    public static String toShortString(LocalTime time)
    {
        return time.toString("HH:mm");
    }

    public static String toString(LocalTime time)
    {
        return time.toString("HH:mm:ss");
    }
}
