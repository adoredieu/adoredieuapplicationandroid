package com.adoredieu.mobility.core.system.broadcastReceiver;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.util.Log;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.core.dao.VersesHighlightsDao;
import com.adoredieu.mobility.core.dto.HighlightedVerseDto;
import com.adoredieu.mobility.core.dto.VersesHighlightsSynchronisationDto;
import com.adoredieu.mobility.core.helpers.DateTimeHelper;
import com.adoredieu.mobility.core.interfaces.DateProvider;
import com.adoredieu.mobility.core.interfaces.broadcast.Intents;
import com.adoredieu.mobility.core.model.highlightedVerses.HighlightedVerseOperation;
import com.adoredieu.mobility.core.persistence.ApplicationContextPersistence;
import com.adoredieu.mobility.core.serializer.VersesHighlightsSynchronisationDtoSerializer;
import com.adoredieu.mobility.core.system.application.AdoreDieuApplication;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class FavoritesSynchronization
        extends BroadcastReceiver
{
    @Inject
    DateProvider dateProvider;
    @Inject
    ApplicationContextPersistence applicationContextPersistence;
    @Inject
    VersesHighlightsDao versesHighlightsDao;

    public static void armFavoritesSynchronizationLoop(Context context)
    {
        boolean isDebug = ((context.getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE) != 0);
        Log.d(FavoritesSynchronization.class.getSimpleName(), "armed");

        // Enable receiver
        PackageManager pm = context.getPackageManager();
        ComponentName componentName = new ComponentName(context, FavoritesSynchronization.class);
        pm.setComponentEnabledSetting(componentName, PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);

        // Arm alarm
        AlarmManager alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent alarmIntent = new Intent(context, FavoritesSynchronization.class);
        alarmIntent.setAction(Intents.FAVORITES_SYNCHRONIZATION);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
                                                                 0,
                                                                 alarmIntent,
                                                                 0);

        if (alarmMgr != null)
        {
            if (isDebug & false)
            {
                alarmMgr.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                                             1000 * 60,
                                             1000 * 60,
                                             pendingIntent);
            }
            else
            {
                alarmMgr.setInexactRepeating(AlarmManager.ELAPSED_REALTIME,
                                             AlarmManager.INTERVAL_HALF_HOUR,
                                             AlarmManager.INTERVAL_HALF_HOUR,
                                             pendingIntent);
            }
        }
    }

    @Override
    public void onReceive(final Context context,
                          Intent intent)
    {
        ((AdoreDieuApplication) context.getApplicationContext()).getInjector().inject(this);

        Log.d(getClass().getSimpleName(), "onReceive");

        String action = intent.getAction();
        if (action != null && action.equals(Intents.FAVORITES_SYNCHRONIZATION))
        {
            final Integer currentUserId = applicationContextPersistence.getCurrentUserId();

            if (currentUserId != 0)
            {
                final DateTime versesHighlightsLastSynchronisationDate = applicationContextPersistence
                        .getVersesHighlightsLastSynchronisationDate();

                final List<HighlightedVerseOperation> highlightedVerseOperationAfterDate = versesHighlightsDao
                        .getHighlightedVerseOperationsAfterDate(
                                versesHighlightsLastSynchronisationDate.toDate());

                ArrayList<HighlightedVerseDto> highlightedVerseDtos = new ArrayList<>();
                for (HighlightedVerseOperation highlightedVerseOperation : highlightedVerseOperationAfterDate)
                {
                    highlightedVerseDtos.add(
                            new HighlightedVerseDto(
                                    new DateTime(highlightedVerseOperation.getDate()),
                                    highlightedVerseOperation.getBook(),
                                    highlightedVerseOperation.getChapter(),
                                    highlightedVerseOperation.getVerse(),
                                    highlightedVerseOperation.getColor(),
                                    highlightedVerseOperation.getOperationType()));
                }

                // Configure GSON
                final GsonBuilder gsonBuilder = new GsonBuilder();
                gsonBuilder.registerTypeAdapter(VersesHighlightsSynchronisationDto.class,
                                                new VersesHighlightsSynchronisationDtoSerializer());
                gsonBuilder.setPrettyPrinting();
                final Gson gson = gsonBuilder.create();

                // Format to JSON
                final String json = gson.toJson(
                        new VersesHighlightsSynchronisationDto(
                                currentUserId,
                                versesHighlightsLastSynchronisationDate,
                                highlightedVerseDtos));

                Ion.with(context)
                   .load(context.getString(R.string.verses_highlights_synchronisation_address))
                   .setBodyParameter("versesHighlightsSynchronisation", json)
                   .asString()
                   .setCallback(new FutureCallback<String>()
                   {
                       @Override
                       public void onCompleted(Exception e,
                                               String downloadedString)
                       {
                           if (e != null)
                           {
                               e.printStackTrace();
                           }
                           else
                           {
                               if (!downloadedString.isEmpty())
                               {
                                   try
                                   {
                                       JSONArray jsonArray = new JSONArray(downloadedString);

                                       for (int i = 0; i < jsonArray.length(); i++)
                                       {
                                           // "date":"2016-03-19 13:27:31","book":"1","chapter":"1","verse":"1","color_index":"1","operation_type":"C"
                                           JSONObject jsonObject = jsonArray.getJSONObject(i);

                                           String date = jsonObject.getString("date");
                                           DateTime parsedDateTime = DateTimeHelper.parseSql(date);

                                           Integer book = jsonObject.getInt("book");
                                           Integer chapter = jsonObject.getInt("chapter");
                                           Integer verse = jsonObject.getInt("verse");
                                           Integer color_index = jsonObject.getInt("color_index");

                                           String operation_type = jsonObject.getString(
                                                   "operation_type");
                                           HighlightedVerseOperation.OperationType operationType = HighlightedVerseOperation.OperationType.CREATE;
                                           switch (operation_type)
                                           {
                                               case "C":
                                                   operationType = HighlightedVerseOperation.OperationType.CREATE;
                                                   break;
                                               case "R":
                                                   operationType = HighlightedVerseOperation.OperationType.READ;
                                                   break;
                                               case "U":
                                                   operationType = HighlightedVerseOperation.OperationType.UPDATE;
                                                   break;
                                               case "D":
                                                   operationType = HighlightedVerseOperation.OperationType.DELETE;
                                                   break;
                                           }

                                           if (!versesHighlightsDao.contains(parsedDateTime.toDate(),
                                                                             book,
                                                                             chapter,
                                                                             verse,
                                                                             color_index,
                                                                             operationType))
                                           {
                                               versesHighlightsDao.addVerseHighlight(parsedDateTime.toDate(),
                                                                                     book,
                                                                                     chapter,
                                                                                     verse,
                                                                                     color_index,
                                                                                     operationType);
                                           }
                                       }

                                       applicationContextPersistence.setVersesHighlightsLastSynchronisationDate(
                                               dateProvider.getDateTime());
                                   }
                                   catch (Exception ignored)
                                   {
                                   }
                               }
                           }
                       }
                   });
            }
        }
    }
}
