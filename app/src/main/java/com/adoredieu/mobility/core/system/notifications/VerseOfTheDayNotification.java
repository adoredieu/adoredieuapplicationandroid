package com.adoredieu.mobility.core.system.notifications;

import android.content.Context;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.core.dto.VerseOfTheDayDto;
import com.adoredieu.mobility.core.helpers.NotificationHelper;
import com.adoredieu.mobility.core.interfaces.Action;
import com.adoredieu.mobility.core.interfaces.DateProvider;
import com.adoredieu.mobility.core.interfaces.NotificationsTags;
import com.adoredieu.mobility.core.persistence.ApplicationContextPersistence;
import com.adoredieu.mobility.core.services.VerseOfTheDayServices;

public class VerseOfTheDayNotification
{
    public static void pullNotificationIfNeeded(final Context context,
                                                final ApplicationContextPersistence applicationContextPersistence,
                                                DateProvider dateProvider)
    {
        VerseOfTheDayServices verseOfTheDayServices = new VerseOfTheDayServices(context,
                                                                                applicationContextPersistence,
                                                                                dateProvider);

        verseOfTheDayServices.getVerseOfTheDay(
                new Action<VerseOfTheDayDto>()
                {
                    @Override
                    public void run(VerseOfTheDayDto downloadedVerse)
                    {
                        if (downloadedVerse == null)
                        {
                            return;
                        }

                        Integer lastNotifiedVerseOfTheDayId = applicationContextPersistence.getLastNotifiedVerseOfTheDayId();
                        if (lastNotifiedVerseOfTheDayId.equals(downloadedVerse.get_id()))
                        {
                            return;
                        }

                        applicationContextPersistence.setLastNotifiedVerseOfTheDayId(downloadedVerse
                                                                                             .get_id());

                        NotificationHelper.publishTextNotification(context,
                                                                   context.getString(R.string.notification_title_daily_verse),
                                                                   downloadedVerse.getVerse(),
                                                                   "",
                                                                   NotificationsTags.VerseOfTheDay);
                    }
                },
                new Runnable()
                {
                    @Override
                    public void run()
                    {

                    }
                },
                dateProvider.getDateTime());
    }

}
