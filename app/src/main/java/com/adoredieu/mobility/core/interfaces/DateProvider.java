package com.adoredieu.mobility.core.interfaces;

import org.joda.time.DateTime;
import org.joda.time.LocalTime;

public interface DateProvider
{
    LocalTime getTime();

    DateTime getDateTime();
}

