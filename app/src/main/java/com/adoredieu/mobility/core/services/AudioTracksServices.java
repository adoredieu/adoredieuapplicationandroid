package com.adoredieu.mobility.core.services;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.core.helpers.AsyncTaskHelper;
import com.adoredieu.mobility.core.helpers.WebHelper;
import com.adoredieu.mobility.core.interfaces.Action;
import com.adoredieu.mobility.core.model.website.AudioTrack;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;

public class AudioTracksServices {
    private final Context context;

    @Inject
    public AudioTracksServices(Context context) {
        this.context = context;
    }

    public void getTracks(Action<List<AudioTrack>> onFinished,
                          Runnable onError) {
        DownloadAudioTracksTask downloadAudioTracksTask = new DownloadAudioTracksTask(
                context,
                onFinished,
                onError);

        AsyncTaskHelper.<Void>executeAsyncTask(downloadAudioTracksTask);
    }

    private static class DownloadAudioTracksTask
            extends AsyncTask<Void, Void, List<AudioTrack>> {
        private final WeakReference<Context> contextWeakReference;
        private final Action<List<AudioTrack>> onFinished;
        private final Runnable onError;

        public DownloadAudioTracksTask(Context context,
                                       Action<List<AudioTrack>> onFinished,
                                       Runnable onError) {
            this.contextWeakReference = new WeakReference<>(context);
            this.onFinished = onFinished;
            this.onError = onError;
        }

        @Override
        protected List<AudioTrack> doInBackground(Void... voids) {
            List<AudioTrack> audioTracks = new ArrayList<>();
            Context context = contextWeakReference.get();
            if (context == null)
                return audioTracks;

            String downloadString = WebHelper.downloadStringBlocking(context,
                    context.getString(R.string.audio_tracks_www_address));

            if (!downloadString.isEmpty()) {
                try {
                    JSONArray jsonArray = new JSONArray(downloadString);

                    for (int i = 0; i < jsonArray.length(); i++) {
                        try {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            String artist = jsonObject.getString("artist");
                            String title = jsonObject.getString("title");
                            Uri file = Uri.parse(jsonObject.getString("file"));
                            String cover = jsonObject.getString("cover");

                            /*
                            if (!WebHelper.isURLReachable(file))
                                throw new Resources.NotFoundException(file.toString());
                            */

                            audioTracks.add(new AudioTrack(
                                    artist,
                                    title,
                                    file,
                                    cover));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            Collections.sort(audioTracks, new Comparator<AudioTrack>() {
                @Override
                public int compare(AudioTrack lhs,
                                   AudioTrack rhs) {
                    return lhs.artist.compareTo(rhs.artist);
                }
            });

            return audioTracks;
        }

        @Override
        protected void onPostExecute(List<AudioTrack> result) {
            if (result != null && result.size() > 0) {
                onFinished.run(result);
            } else {
                onError.run();
            }
        }
    }
}
