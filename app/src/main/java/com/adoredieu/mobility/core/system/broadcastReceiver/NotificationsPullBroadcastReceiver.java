package com.adoredieu.mobility.core.system.broadcastReceiver;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.util.Log;

import com.adoredieu.mobility.core.helpers.DateTimeHelper;
import com.adoredieu.mobility.core.interfaces.DateProvider;
import com.adoredieu.mobility.core.interfaces.broadcast.Intents;
import com.adoredieu.mobility.core.persistence.ApplicationContextPersistence;
import com.adoredieu.mobility.core.persistence.UserPreferencesPersistence;
import com.adoredieu.mobility.core.system.application.AdoreDieuApplication;
import com.adoredieu.mobility.core.system.notifications.ImageQuoteOfTheDayNotification;
import com.adoredieu.mobility.core.system.notifications.QuoteOfTheDayNotification;
import com.adoredieu.mobility.core.system.notifications.TeachingsNotification;
import com.adoredieu.mobility.core.system.notifications.ThoughtsNotification;
import com.adoredieu.mobility.core.system.notifications.VerseOfTheDayNotification;

import org.joda.time.LocalTime;

import javax.inject.Inject;

public class NotificationsPullBroadcastReceiver
        extends BroadcastReceiver {
    @Inject
    DateProvider dateProvider;
    @Inject
    ApplicationContextPersistence applicationContextPersistence;
    @Inject
    UserPreferencesPersistence userPreferencesPersistence;

    public static void armNotificationsPullerLoop(Context context) {
        boolean isDebug = ((context.getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE) != 0);
        Log.d(NotificationsPullBroadcastReceiver.class.getSimpleName(), "armed");

        // Enable receiver
        PackageManager pm = context.getPackageManager();
        ComponentName componentName = new ComponentName(context, NotificationsPullBroadcastReceiver.class);
        pm.setComponentEnabledSetting(componentName, PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);

        // Arm alarm
        AlarmManager alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent alarmIntent = new Intent(context, NotificationsPullBroadcastReceiver.class);
        alarmIntent.setAction(Intents.NOTIFICATIONS_PULLER);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
                0,
                alarmIntent,
                0);

        if (alarmMgr != null) {
            if (isDebug & false) {
                alarmMgr.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                        1000 * 60,
                        1000 * 60,
                        pendingIntent);
            } else {
                alarmMgr.setInexactRepeating(AlarmManager.ELAPSED_REALTIME,
                        AlarmManager.INTERVAL_FIFTEEN_MINUTES,
                        AlarmManager.INTERVAL_FIFTEEN_MINUTES,
                        pendingIntent);
            }
        }
    }

    @Override
    public void onReceive(final Context context,
                          Intent intent) {
        ((AdoreDieuApplication) context.getApplicationContext()).getInjector().inject(this);

        Log.d(getClass().getSimpleName(), "onReceive");

        String action = intent.getAction();
        if (action != null && action.equals(Intents.NOTIFICATIONS_PULLER)) {
            LocalTime notificationQuietStartTime = userPreferencesPersistence.getNotificationQuietStartTime();
            LocalTime notificationQuietEndTime = userPreferencesPersistence.getNotificationQuietEndTime();
            LocalTime now = dateProvider.getTime();

            if (DateTimeHelper.isTimeBetweenInterval(now,
                    notificationQuietStartTime,
                    notificationQuietEndTime)) {
                return;
            }

            if (userPreferencesPersistence.getQuoteOfTheDayNotificationEnabledPreference()) {
                QuoteOfTheDayNotification.pullNotificationIfNeeded(context,
                        applicationContextPersistence,
                        dateProvider);
            }

            if (userPreferencesPersistence.getDailyVerseNotificationEnabledPreference()) {
                VerseOfTheDayNotification.pullNotificationIfNeeded(context,
                        applicationContextPersistence,
                        dateProvider);
            }

            if (userPreferencesPersistence.getImageQuoteOfTheDayNotificationEnabledPreference()) {
                ImageQuoteOfTheDayNotification.pullNotificationIfNeeded(context,
                        applicationContextPersistence);
            }

            if (userPreferencesPersistence.getThoughtsNotificationEnabledPreference()) {
                ThoughtsNotification.pullNotificationIfNeeded(context,
                        applicationContextPersistence);
            }

            if (userPreferencesPersistence.getTeachingsNotificationEnabledPreference()) {
                TeachingsNotification.pullNotificationIfNeeded(context,
                        applicationContextPersistence);
            }
        }
    }
}
