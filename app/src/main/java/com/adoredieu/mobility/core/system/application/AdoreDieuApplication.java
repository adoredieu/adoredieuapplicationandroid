package com.adoredieu.mobility.core.system.application;

import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.multidex.MultiDex;

import com.adoredieu.mobility.core.factory.DaggerInjector;
import com.adoredieu.mobility.core.factory.Injector;
import com.adoredieu.mobility.core.factory.modules.ContextModule;
import com.adoredieu.mobility.core.factory.modules.HalModule;
import com.adoredieu.mobility.core.hal.RealDateProvider;
import com.adoredieu.mobility.core.system.broadcastReceiver.BootBroadcastReceiver;

public class AdoreDieuApplication
        extends Application {
    private Injector injector;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);

        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        injector = DaggerInjector.builder()
                .contextModule(new ContextModule(this))
                .halModule(new HalModule(new RealDateProvider()))
                .build();

        // Enable boot receiver
        ComponentName receiver = new ComponentName(this, BootBroadcastReceiver.class);
        PackageManager pm = this.getPackageManager();

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);
    }

    public Injector getInjector() {
        return injector;
    }
}
