package com.adoredieu.mobility.core.model.bible;

import com.adoredieu.mobility.core.interfaces.VerseDefinition;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "bible_strongs")
public class VerseStrong
        implements VerseDefinition
{
    public static final String TABLE_NAME = "bible_strongs";

    public static final String VERSE_BOOK_REF_COLUMN = "book_ref";
    public static final String VERSE_CHAPTER_COLUMN = "chapter";
    public static final String VERSE_ID_COLUMN = "_id";
    public static final String VERSE_NUMBER_COLUMN = "verse";
    public static final String VERSE_TEXT_COLUMN = "text";

    @DatabaseField(id = true, columnName = VERSE_ID_COLUMN)
    private Integer _id;
    @DatabaseField(columnName = VERSE_BOOK_REF_COLUMN, canBeNull = false, foreign = true, foreignAutoRefresh = true, index = true)
    private Book book;
    @DatabaseField(columnName = VERSE_CHAPTER_COLUMN, index = true)
    private Integer chapter;
    @DatabaseField(columnName = VERSE_NUMBER_COLUMN, index = true)
    private Integer verse;
    @DatabaseField(columnName = VERSE_TEXT_COLUMN)
    private String text;

    public VerseStrong(Book book,
                       Integer chapter,
                       Integer verse,
                       String text)
    {
        _id = 0;
        this.book = book;
        this.chapter = chapter;
        this.verse = verse;
        this.text = text;
    }

    protected VerseStrong()
    {
        _id = 0;
        chapter = 0;
        verse = 0;
        text = "";
    }

    public Book getBook()
    {
        return book;
    }

    public void setBook(Book book)
    {
        this.book = book;
    }

    public Integer getChapter()
    {
        return chapter;
    }

    public void setChapter(Integer chapter)
    {
        this.chapter = chapter;
    }

    public Integer getVerse()
    {
        return verse;
    }

    public void setVerse(Integer verse)
    {
        this.verse = verse;
    }

    public String getText()
    {
        return text;
    }

    public void setText(String text)
    {
        this.text = text;
    }

    public Integer getId()
    {
        return _id;
    }

    public void setId(Integer id)
    {
        this._id = id;
    }

    @Override
    public Integer getBookId()
    {
        return getBook().getId();
    }

    @Override
    public Integer getChapterNumber()
    {
        return getChapter();
    }

    @Override
    public Integer getVerseNumber()
    {
        return getVerse();
    }
}
