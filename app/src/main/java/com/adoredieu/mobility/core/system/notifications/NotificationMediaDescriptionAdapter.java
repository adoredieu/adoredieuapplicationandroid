package com.adoredieu.mobility.core.system.notifications;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.Nullable;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.core.helpers.WebHelper;
import com.adoredieu.mobility.core.interfaces.Action;
import com.adoredieu.mobility.core.model.website.AudioTrack;
import com.adoredieu.mobility.view.activities.MainActivity;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.ui.PlayerNotificationManager;

public class NotificationMediaDescriptionAdapter implements PlayerNotificationManager.MediaDescriptionAdapter {
    private AudioTrack audioTrack;
    private final Context context;

    public NotificationMediaDescriptionAdapter(Context context) {
        this.context = context;
    }

    @Override
    public String getCurrentContentTitle(Player player) {
        if (audioTrack == null)
            return "";

        return audioTrack.title;
    }

    @Nullable
    @Override
    public String getCurrentContentText(Player player) {
        if (audioTrack == null)
            return "";

        return audioTrack.artist;
    }

    @Nullable
    @Override
    public Bitmap getCurrentLargeIcon(Player player,
                                      final PlayerNotificationManager.BitmapCallback callback) {
        if (audioTrack == null)
            return BitmapFactory.decodeResource(context.getResources(),
                    R.drawable.ic_album_white);

        if (this.audioTrack.cover == null) {
            if (!audioTrack.coverUri.isEmpty())
                WebHelper.downloadImage(
                        context,
                        audioTrack.coverUri,
                        new Action<Bitmap>() {
                            @Override
                            public void run(Bitmap bitmap) {
                                callback.onBitmap(bitmap);
                            }
                        });

            return BitmapFactory.decodeResource(context.getResources(),
                    R.drawable.ic_album_white);
        }

        return this.audioTrack.cover;
    }

    @Nullable
    @Override
    public PendingIntent createCurrentContentIntent(Player player) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.setAction(context.getString(R.string.notification_action_audio_player));

        return PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    public void setCurrentTrack(AudioTrack audioTrack) {
        this.audioTrack = audioTrack;
    }
}
