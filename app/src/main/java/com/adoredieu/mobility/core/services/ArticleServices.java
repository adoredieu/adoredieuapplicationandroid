package com.adoredieu.mobility.core.services;

import android.content.Context;
import android.os.AsyncTask;

import com.adoredieu.mobility.core.cache.ArticlesCache;
import com.adoredieu.mobility.core.helpers.AsyncTaskHelper;
import com.adoredieu.mobility.core.helpers.WebHelper;
import com.adoredieu.mobility.core.interfaces.Action;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

public class ArticleServices {
    private final Context context;
    private final ArticlesCache articlesCache;

    @Inject
    public ArticleServices(Context context) {
        this.context = context;
        this.articlesCache = new ArticlesCache();
    }

    public void getArticleContent(String url,
                                  Action<ArticleContent> onFinished,
                                  Runnable onError) {
        if (articlesCache.containsKey(url)) {
            onFinished.run(articlesCache.getArticle(url));
        } else {
            DownloadArticleTask downloadArticleTask = new DownloadArticleTask(
                    context,
                    articlesCache,
                    onFinished,
                    onError);
            AsyncTaskHelper.<String>executeAsyncTask(downloadArticleTask, url);
        }
    }

    private static class DownloadArticleTask
            extends AsyncTask<String, Void, ArticleContent> {
        private final WeakReference<Context> contextWeakReference;
        private final ArticlesCache articlesCache;
        private final Action<ArticleContent> onFinished;
        private final Runnable onError;

        public DownloadArticleTask(Context context,
                                   ArticlesCache articlesCache,
                                   Action<ArticleContent> onFinished,
                                   Runnable onError) {
            this.contextWeakReference = new WeakReference<>(context);
            this.articlesCache = articlesCache;
            this.onFinished = onFinished;
            this.onError = onError;
        }

        @Override
        protected ArticleContent doInBackground(String... uri) {
            ArticleContent articleContent = null;
            Context context = contextWeakReference.get();
            if (context == null)
                return articleContent;

            String downloadedString = WebHelper.downloadStringBlocking(context,
                    uri[0]);
            try {
                JSONObject jsonObject = new JSONObject(downloadedString);
                String content = jsonObject.getString("content");
                String fb_link = jsonObject.getString("fb_article_link");

                articleContent = new ArticleContent(content, fb_link);
                articlesCache.add(uri[0],
                        articleContent);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return articleContent;
        }

        @Override
        protected void onPostExecute(ArticleContent articleContent) {
            if (articleContent != null) {
                onFinished.run(articleContent);
            } else {
                onError.run();
            }
        }
    }

    public static class ArticleContent {
        final String content;
        final String facebookLikeUri;

        public ArticleContent(String content,
                              String fb_link) {

            this.content = content;
            this.facebookLikeUri = fb_link;
        }

        public String getContent() {
            return content;
        }

        public String getFacebookLikeUri() {
            return facebookLikeUri;
        }
    }
}
