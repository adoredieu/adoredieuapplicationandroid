package com.adoredieu.mobility.core.helpers;

import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.TextView;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.core.persistence.ApplicationContextPersistence;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import de.hdodenhof.circleimageview.CircleImageView;

public class NavigationDrawerHelper {
    public static void refreshUsername(FragmentActivity activityBase,
                                       ApplicationContextPersistence applicationContextPersistence) {
        NavigationView navigationView = activityBase.findViewById(R.id.nav_view);
        if (navigationView != null) {
            TextView usernameWelcomeTextView = navigationView.getHeaderView(0)
                    .findViewById(R.id.usernameWelcomeTextView);
            final CircleImageView profileCircleImageView = navigationView.getHeaderView(
                    0)
                    .findViewById(
                            R.id.profileCircleImageView);

            if (!applicationContextPersistence.getCurrentUserName().isEmpty()) {
                usernameWelcomeTextView.setText(activityBase.getString(R.string.bonjour_username,
                        applicationContextPersistence.getCurrentUserName()));
                usernameWelcomeTextView.setVisibility(View.VISIBLE);

                if (!applicationContextPersistence.getCurrentUserPicture().isEmpty()) {
                    Glide.with(activityBase)
                            .load(applicationContextPersistence.getCurrentUserPicture())
                            .listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    profileCircleImageView.setVisibility(View.VISIBLE);
                                    return false;
                                }
                            })
                            .into(profileCircleImageView);
                } else {
                    profileCircleImageView.setVisibility(View.INVISIBLE);
                }
            } else {
                usernameWelcomeTextView.setVisibility(View.INVISIBLE);
                profileCircleImageView.setVisibility(View.INVISIBLE);
            }
        }
    }

}
