package com.adoredieu.mobility.core.factory.modules;

import android.content.Context;

import com.adoredieu.mobility.core.persistence.ApplicationContextPersistence;
import com.adoredieu.mobility.core.persistence.UserPreferencesPersistence;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class PersistenceModule
{
    @Provides
    @Singleton
    public ApplicationContextPersistence provideApplicationContextPersistence(Context context)
    {
        return new ApplicationContextPersistence(context);
    }

    @Provides
    @Singleton
    public UserPreferencesPersistence provideUserPreferencesPersistence(Context context)
    {
        return new UserPreferencesPersistence(context);
    }
}
