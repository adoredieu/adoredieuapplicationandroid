package com.adoredieu.mobility.core.system.mmi;

import android.annotation.SuppressLint;
import android.text.style.URLSpan;
import android.view.View;

@SuppressLint("ParcelCreator")
public class ClickSpan
        extends URLSpan
{
    private final OnClickListener mListener;

    public ClickSpan(URLSpan url,
                     OnClickListener listener)
    {
        super(url.getURL());

        mListener = listener;
    }

    @Override
    public void onClick(View view)
    {
        if (mListener != null)
        {
            mListener.onClick(view,
                              getURL());
        }
    }

    public interface OnClickListener
    {
        void onClick(View view,
                     String url);
    }
}
