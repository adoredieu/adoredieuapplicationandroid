package com.adoredieu.mobility.core.helpers;

import org.joda.time.DateTime;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;

public class DateTimeHelper
{
    public static final DateTime MinValue = new DateTime(0);

    public static DateTime parse(String dateString)
    {
        return parseSql(dateString);
    }

    public static String toString(DateTime date)
    {
        return toSqlString(date);
    }

    /**
     * @param value DateTime value
     * @return A time yyyy-MM-dd HH:mm:ss in all locales
     */
    public static String formatInvariableDateTime(DateTime value)
    {
        org.joda.time.format.DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern(
                getInvariableDateTimeFormat());
        return dateTimeFormatter.print(value);
    }

    /**
     * @return yyyy-MM-dd HH:mm:ss
     */
    public static String getInvariableDateTimeFormat()
    {
        return "yyyy-MM-dd HH:mm:ss";
    }

    /**
     * @return A yyyy-MM-dd HH:mm:ss formatter
     */
    public static org.joda.time.format.DateTimeFormatter getInvariableDateTimeFormatter()
    {
        org.joda.time.format.DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern(
                getInvariableDateTimeFormat());
        return dateTimeFormatter;
    }

    public static boolean isTimeBetweenInterval(LocalTime time,
                                                LocalTime intervalStartTime,
                                                LocalTime intervalEndTime)
    {
        LocalTime midnightMinus = new LocalTime(23,
                                                59,
                                                59);
        LocalTime midnightPlus = new LocalTime(0,
                                               0);

        if (intervalEndTime.isBefore(intervalStartTime))
        {
            // Les heures sont à cheval sur minuit
            if (((time.isBefore(midnightMinus) || time.equals(midnightMinus)) && time.isAfter(
                    intervalStartTime))
                || ((time.isAfter(midnightPlus) || time.equals(midnightPlus)) && time.isBefore(
                    intervalEndTime)))
            {
                return true;
            }
        }
        else if (time.isAfter(intervalStartTime) && time.isBefore(intervalEndTime))
        {
            return true;
        }

        return false;
    }

    public static String toSqlString(DateTime date)
    {
        return date.toString("yyyy-MM-dd HH:mm:ss");
    }

    public static DateTime parseSql(String dateString)
    {
        try
        {
            if (!dateString.isEmpty())
            {
                return DateTime.parse(dateString,
                                      DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return null;
    }
}
