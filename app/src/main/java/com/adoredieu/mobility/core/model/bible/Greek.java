package com.adoredieu.mobility.core.model.bible;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "greek")
public class Greek
{
    public static final String GREEK_ID_COLUMN = "_id";
    public static final String GREEK_WORD_COLUMN = "word";
    public static final String GREEK_GREEK_COLUMN = "greek";
    public static final String GREEK_ORIGIN_COLUMN = "origin";
    public static final String GREEK_TYPE_COLUMN = "type";
    public static final String GREEK_DEFINITION_COLUMN = "definition";

    @DatabaseField(id = true, columnName = GREEK_ID_COLUMN)
    private final Integer _id;

    @DatabaseField(columnName = GREEK_WORD_COLUMN)
    private String word;

    @DatabaseField(columnName = GREEK_GREEK_COLUMN)
    private String greek;

    @DatabaseField(columnName = GREEK_ORIGIN_COLUMN)
    private String origin;

    @DatabaseField(columnName = GREEK_TYPE_COLUMN)
    private String type;

    @DatabaseField(columnName = GREEK_DEFINITION_COLUMN)
    private String definition;

    public Greek(String word,
                 String greek,
                 String origin,
                 String type,
                 String definition)
    {
        _id = 0;
        this.word = word;
        this.greek = greek;
        this.origin = origin;
        this.type = type;
        this.definition = definition;
    }

    protected Greek()
    {
        _id = 0;
        this.word = "";
        this.greek = "";
        this.origin = "";
        this.type = "";
        this.definition = "";
    }

    public Integer get_id()
    {
        return _id;
    }

    public String getWord()
    {
        return word;
    }

    public void setWord(String word)
    {
        this.word = word;
    }

    public String getGreek()
    {
        return greek;
    }

    public void setGreek(String greek)
    {
        this.greek = greek;
    }

    public String getOrigin()
    {
        return origin;
    }

    public void setOrigin(String origin)
    {
        this.origin = origin;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getDefinition()
    {
        return definition;
    }

    public void setDefinition(String definition)
    {
        this.definition = definition;
    }
}
