package com.adoredieu.mobility.core.dto;

import com.adoredieu.mobility.core.model.bible.Greek;
import com.adoredieu.mobility.core.model.bible.Hebrew;

public class StrongDto
{
    private final Integer code;
    private final String word;
    private final String originalWord;
    private final String origin;
    private final String type;
    private final String definition;

    public StrongDto(Integer code,
                     String word,
                     String originalWord,
                     String origin,
                     String type,
                     String definition)
    {
        this.code = code;
        this.word = word;
        this.originalWord = originalWord;
        this.origin = origin;
        this.type = type;
        this.definition = definition;
    }

    public Integer getCode()
    {
        return code;
    }

    public String getWord()
    {
        return word;
    }

    public String getOriginalWord()
    {
        return originalWord;
    }

    public String getOrigin()
    {
        return origin;
    }

    public String getType()
    {
        return type;
    }

    public String getDefinition()
    {
        return definition;
    }

    public static StrongDto fromHebrew(Hebrew hebrew)
    {
        return new StrongDto(hebrew.get_id(),
                             hebrew.getWord(),
                             hebrew.getHebrew(),
                             hebrew.getOrigin(),
                             hebrew.getType(),
                             hebrew.getDefinition());
    }

    public static StrongDto fromGreek(Greek greek)
    {
        return new StrongDto(greek.get_id(),
                             greek.getWord(),
                             greek.getGreek(),
                             greek.getOrigin(),
                             greek.getType(),
                             greek.getDefinition());
    }
}
