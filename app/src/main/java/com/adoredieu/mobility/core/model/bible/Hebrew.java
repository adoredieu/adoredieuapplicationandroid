package com.adoredieu.mobility.core.model.bible;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "hebrew")
public class Hebrew
{
    public static final String HEBREW_ID_COLUMN = "_id";
    public static final String HEBREW_WORD_COLUMN = "word";
    public static final String HEBREW_HEBREW_COLUMN = "hebrew";
    public static final String HEBREW_ORIGIN_COLUMN = "origin";
    public static final String HEBREW_TYPE_COLUMN = "type";
    public static final String HEBREW_DEFINITION_COLUMN = "definition";

    @DatabaseField(id = true, columnName = HEBREW_ID_COLUMN)
    private final Integer _id;

    @DatabaseField(columnName = HEBREW_WORD_COLUMN)
    private String word;

    @DatabaseField(columnName = HEBREW_HEBREW_COLUMN)
    private String hebrew;

    @DatabaseField(columnName = HEBREW_ORIGIN_COLUMN)
    private String origin;

    @DatabaseField(columnName = HEBREW_TYPE_COLUMN)
    private String type;

    @DatabaseField(columnName = HEBREW_DEFINITION_COLUMN)
    private String definition;

    public Hebrew(String word,
                  String hebrew,
                  String origin,
                  String type,
                  String definition)
    {
        _id = 0;
        this.word = word;
        this.hebrew = hebrew;
        this.origin = origin;
        this.type = type;
        this.definition = definition;
    }

    protected Hebrew()
    {
        _id = 0;
        this.word = "";
        this.hebrew = "";
        this.origin = "";
        this.type = "";
        this.definition = "";
    }

    public Integer get_id()
    {
        return _id;
    }

    public String getWord()
    {
        return word;
    }

    public void setWord(String word)
    {
        this.word = word;
    }

    public String getHebrew()
    {
        return hebrew;
    }

    public void setHebrew(String hebrew)
    {
        this.hebrew = hebrew;
    }

    public String getOrigin()
    {
        return origin;
    }

    public void setOrigin(String origin)
    {
        this.origin = origin;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getDefinition()
    {
        return definition;
    }

    public void setDefinition(String definition)
    {
        this.definition = definition;
    }
}
