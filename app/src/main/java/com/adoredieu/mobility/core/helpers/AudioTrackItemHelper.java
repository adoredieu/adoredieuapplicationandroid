package com.adoredieu.mobility.core.helpers;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;

import com.adoredieu.mobility.old.R;

public class AudioTrackItemHelper
{
    public static ColorDrawable getSelectedBackgroundColor(Context context,
                                                           boolean nightMode)
    {
        return new ColorDrawable(
                ContextCompat.getColor(
                        context,
                        nightMode ? R.color.pal_blue_3 : R.color.pal_blue_4));

    }

    public static ColorDrawable getDefaultBackgroundColor(Context context,
                                                          boolean nightMode)
    {
        return new ColorDrawable(android.graphics.Color.TRANSPARENT);
    }

    public static int getTitleColor(Context context,
                                    boolean selected,
                                    boolean nightMode)
    {
        if (selected)
        {
            if (nightMode)
            {
                return ContextCompat.getColor(context, R.color.pal_gray_3);
            }
            else
            {
                return ContextCompat.getColor(context, R.color.pal_gray_2);
            }
        }
        else
        {
            if (nightMode)
            {
                return ContextCompat.getColor(context, R.color.pal_gray_3);
            }
            else
            {
                return ContextCompat.getColor(context, R.color.pal_gray_2);
            }
        }
    }

    public static int getArtistColor(Context context,
                                     boolean selected,
                                     boolean nightMode)
    {
        if (selected)
        {
            if (nightMode)
            {
                return ContextCompat.getColor(context, R.color.pal_gray_2);
            }
            else
            {
                return ContextCompat.getColor(context, R.color.pal_gray_3);
            }
        }
        else
        {
            if (nightMode)
            {
                return ContextCompat.getColor(context, R.color.pal_gray_2);
            }
            else
            {
                return ContextCompat.getColor(context, R.color.pal_gray_3);
            }
        }
    }

    public static StateListDrawable getSelectableBackground(Context ctx,
                                                            ColorDrawable defaultColorDrawable,
                                                            ColorDrawable selectedColorDrawable,
                                                            boolean animate)
    {
        StateListDrawable states = new StateListDrawable();

        states.addState(new int[]{android.R.attr.state_selected}, selectedColorDrawable);

        states.addState(new int[]{}, defaultColorDrawable);

        //if possible we enable animating across states
        if (animate && Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
        {
            int duration = ctx.getResources().getInteger(android.R.integer.config_shortAnimTime);
            states.setEnterFadeDuration(duration);
            states.setExitFadeDuration(duration);
        }

        return states;
    }
}
