package com.adoredieu.mobility.core.services;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.text.Html;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.core.cache.ImageQuotesCache;
import com.adoredieu.mobility.core.helpers.AsyncTaskHelper;
import com.adoredieu.mobility.core.helpers.NetworkHelper;
import com.adoredieu.mobility.core.helpers.WebHelper;
import com.adoredieu.mobility.core.interfaces.Action;
import com.adoredieu.mobility.core.model.website.ImageQuote;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

public class ImageQuotesServices {
    private final Context context;
    private final ImageQuotesCache imageQuotesCache;
    private DownloadArticlesTask downloadArticlesTask;

    public static final int ITEMS_PER_PAGE = 5;

    @Inject
    public ImageQuotesServices(Context context) {
        this.context = context;
        this.imageQuotesCache = new ImageQuotesCache();
    }

    public void getImageQuotesPage(int page,
                                   Action<List<ImageQuote>> onFinished,
                                   Runnable onNoMoreArticles) {
        if (imageQuotesCache.containsPage(page)) {
            onFinished.run(imageQuotesCache.getPageCache(page));
        } else {
            downloadArticlesTask = new DownloadArticlesTask(
                    context,
                    imageQuotesCache, onFinished,
                    onNoMoreArticles);

            AsyncTaskHelper.<Integer>executeAsyncTask(downloadArticlesTask, page, ITEMS_PER_PAGE);
        }
    }

    public void getLastImageQuote(final Action<ImageQuote> onFinished) {
        downloadArticlesTask = new DownloadArticlesTask(
                context,
                imageQuotesCache, new Action<List<ImageQuote>>() {
            @Override
            public void run(List<ImageQuote> object) {
                onFinished.run(object.get(0));
            }
        },
                new Runnable() {
                    @Override
                    public void run() {

                    }
                });

        AsyncTaskHelper.<Integer>executeAsyncTask(downloadArticlesTask, 0, 1);
    }

    public void clearCache() {
        imageQuotesCache.clear();
    }

    public void cancel() {
        if (downloadArticlesTask != null) {
            downloadArticlesTask.cancel(true);
            downloadArticlesTask.isDownloading = false;
        }
    }

    public boolean isDownloading() {
        if (downloadArticlesTask == null)
            return false;

        return downloadArticlesTask.isDownloading;
    }

    public void getLastImageQuoteDate(Action<DateTime> onFinished) {
        DownloadArticlesLastDateTask downloadArticlesLastDateTask = new DownloadArticlesLastDateTask(
                context,
                onFinished);

        AsyncTaskHelper.<Void>executeAsyncTask(downloadArticlesLastDateTask);
    }

    private static class DownloadArticlesTask
            extends AsyncTask<Integer, Void, List<ImageQuote>> {
        private final WeakReference<Context> contextWeakReference;
        private final Action<List<ImageQuote>> onFinished;
        private final Runnable onNoMoreArticles;
        private final ImageQuotesCache imageQuotesCache;
        private Integer page;
        private Integer itemsCount;
        private boolean isDownloading;

        public DownloadArticlesTask(Context context,
                                    ImageQuotesCache imageQuotesCache,
                                    Action<List<ImageQuote>> onFinished,
                                    Runnable onNoMoreArticles) {
            this.contextWeakReference = new WeakReference<>(context);
            this.imageQuotesCache = imageQuotesCache;
            this.onFinished = onFinished;
            this.onNoMoreArticles = onNoMoreArticles;
        }

        @Override
        @SuppressLint("StringFormatInvalid")
        protected List<ImageQuote> doInBackground(Integer... params) {
            isDownloading = true;

            List<ImageQuote> imageQuotes = new ArrayList<>();
            Context context = contextWeakReference.get();
            if (context == null)
                return imageQuotes;

            DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
            this.page = params[0];
            this.itemsCount = params[1];

            String address = context.getString(R.string.citations_images_www_address,
                    itemsCount,
                    page);
            String downloadedString = WebHelper.downloadStringBlocking(
                    context,
                    address);

            if (!downloadedString.isEmpty()) {
                try {
                    JSONArray jsonArray = new JSONArray(downloadedString);

                    for (int i = 0; i < jsonArray.length(); i++) {
                        try {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            long id = jsonObject.getLong("id");
                            DateTime publish_up = fmt.parseDateTime(jsonObject.getString(
                                    "publish_up"));
                            String article_link = jsonObject.getString("article_link");
                            String title = jsonObject.getString("title");
                            String intro_text = Html.fromHtml(jsonObject.getString("introtext"))
                                    .toString();
                            String thumbnailUri = jsonObject.getString("thumbnail");

                            String fb_link = jsonObject.getString("fb_article_link");

                            imageQuotes.add(
                                    new ImageQuote(
                                            id,
                                            publish_up,
                                            title,
                                            intro_text,
                                            thumbnailUri,
                                            context.getString(R.string.citation_image_details_www_address,
                                                    id),
                                            article_link,
                                            this.page,
                                            fb_link));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return imageQuotes;
        }

        @Override
        protected void onPostExecute(List<ImageQuote> result) {
            if (result.size() == 0) {
                onNoMoreArticles.run();
            } else {
                imageQuotesCache.add(page,
                        result);
                onFinished.run(result);
            }

            isDownloading = false;
        }
    }

    public static class DownloadArticlesLastDateTask
            extends AsyncTask<Void, Void, DateTime> {
        private final WeakReference<Context> contextWeakReference;
        private final Action<DateTime> onFinished;

        public DownloadArticlesLastDateTask(Context context,
                                            Action<DateTime> onFinished) {
            this.contextWeakReference = new WeakReference<>(context);
            this.onFinished = onFinished;
        }

        @Override
        protected DateTime doInBackground(Void... voids) {
            Context context = contextWeakReference.get();
            if (context == null)
                return null;

            try {
                if (!NetworkHelper.isNetworkAvailable(context)) {
                    return null;
                }

                String articlesLastDateUri = context.getString(R.string.citations_images_last_date_www_address);
                String downloadString = WebHelper.downloadStringBlocking(context,
                        articlesLastDateUri);

                if (downloadString.isEmpty()) {
                    return null;
                }

                DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
                DateTimeFormatter frenchFmt = fmt.withLocale(Locale.FRENCH);

                return DateTime.parse(downloadString,
                        frenchFmt);
            } catch (Exception e) {
                e.printStackTrace();

                return null;
            }
        }

        @Override
        protected void onPostExecute(DateTime dateTime) {
            onFinished.run(dateTime);
        }
    }
}
