package com.adoredieu.mobility.core.helpers;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.adoredieu.mobility.old.R;
import com.github.lzyzsd.circleprogress.DonutProgress;

public class DialogsHelper
{
    private static Dialog progressDialog;

    public static Dialog showProgressIndeterminate(Context context,
                                                   String title,
                                                   String message,
                                                   DialogInterface.OnCancelListener cancelListener)
    {
        if (progressDialog == null || !progressDialog.isShowing())
        {
            progressDialog = new Dialog(context);
            progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            progressDialog.setContentView(R.layout.dialog_progress_indeterminate);
            progressDialog.setCancelable(cancelListener != null);

            if (cancelListener != null)
            {
                progressDialog.setOnCancelListener(cancelListener);
            }

            TextView titleTextView = progressDialog.findViewById(R.id.titleTextView);
            titleTextView.setText(title);
            TextView messageTextView = progressDialog.findViewById(R.id.messageTextView);
            messageTextView.setText(message);

            ProgressBar progressBar = progressDialog.findViewById(R.id.progressBar);
            progressBar.setIndeterminate(true);

            progressDialog.show();
        }

        return progressDialog;
    }

    public static Dialog showProgressIndeterminate(Context context,
                                                   String title,
                                                   String message)
    {
        return showProgressIndeterminate(context,
                                         title,
                                         message,
                                         null);
    }

    public static Dialog showProgress(Context context,
                                      String title,
                                      String message)
    {
        if (progressDialog == null || !progressDialog.isShowing())
        {
            progressDialog = new Dialog(context);
            progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            progressDialog.setContentView(R.layout.dialog_progress);
            progressDialog.setCancelable(false);

            TextView titleTextView = progressDialog.findViewById(R.id.titleTextView);
            titleTextView.setText(title);
            TextView messageTextView = progressDialog.findViewById(R.id.messageTextView);
            messageTextView.setText(message);

            DonutProgress progressBar = progressDialog.findViewById(R.id.progressBar);
            progressBar.setMax(100);

            progressDialog.show();
        }

        return progressDialog;
    }

    public static void hideProgressDialog()
    {
        if (progressDialog != null && progressDialog.isShowing())
        {
            progressDialog.dismiss();
        }
    }

    public static void showAlert(Context context,
                                 String title,
                                 String message,
                                 final View.OnClickListener onClickListener)
    {
        final Dialog alertDialog = new Dialog(context);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCancelable(false);

        TextView titleTextView = alertDialog.findViewById(R.id.titleTextView);
        titleTextView.setText(title);

        TextView messageTextView = alertDialog.findViewById(R.id.messageTextView);
        messageTextView.setText(message);

        final Button validateButton = alertDialog.findViewById(R.id.validateButton);
        validateButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                alertDialog.dismiss();
                if (onClickListener != null)
                {
                    onClickListener.onClick(validateButton);
                }
            }
        });

        alertDialog.show();
    }

    public static void showQuestion(Context context,
                                    String title,
                                    String message,
                                    final View.OnClickListener onYesClickListener,
                                    final View.OnClickListener onNoClickListener)
    {
        final Dialog alertDialog = new Dialog(context);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_question);
        alertDialog.setCancelable(false);

        TextView titleTextView = alertDialog.findViewById(R.id.titleTextView);
        titleTextView.setText(title);

        TextView messageTextView = alertDialog.findViewById(R.id.messageTextView);
        messageTextView.setText(message);

        final Button yesButton = alertDialog.findViewById(R.id.yesButton);
        yesButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                alertDialog.dismiss();
                if (onYesClickListener != null)
                {
                    onYesClickListener.onClick(yesButton);
                }
            }
        });

        final Button noButton = alertDialog.findViewById(R.id.noButton);
        noButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                alertDialog.dismiss();
                if (onNoClickListener != null)
                {
                    onNoClickListener.onClick(noButton);
                }
            }
        });


        alertDialog.show();
    }
}
