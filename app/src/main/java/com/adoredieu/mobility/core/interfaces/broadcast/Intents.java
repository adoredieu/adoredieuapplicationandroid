package com.adoredieu.mobility.core.interfaces.broadcast;

public class Intents
{
    public static final String NOTIFICATIONS_PULLER = "com.adoredieu.mobility.NotificationsPuller";
    public static final String VERSES_HIGHLIGHTS_SYNCHRONIZATION = "com.adoredieu.mobility.VersesHighlightsSynchronization";
    public static final String FAVORITES_SYNCHRONIZATION = "com.adoredieu.mobility.FavoritesSynchronization";
}
