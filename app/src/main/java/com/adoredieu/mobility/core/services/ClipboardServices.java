package com.adoredieu.mobility.core.services;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Build;
import android.widget.Toast;

import com.adoredieu.mobility.old.R;

public class ClipboardServices
{
    @SuppressLint("ObsoleteSdkInt")
    public void copyToClipboard(Context context,
                                String text)
    {
        ClipboardManager clipboardManager = (ClipboardManager) context.getSystemService(
                Context.CLIPBOARD_SERVICE);

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.GINGERBREAD_MR1)
        {
            ClipData clip = ClipData.newPlainText(context.getString(R.string.app_name),
                                                  text);
            clipboardManager.setPrimaryClip(clip);
        }
        else
        {
            clipboardManager.setText(text);
        }

        Toast.makeText(context,
                       context.getString(R.string.texte_copie_dans_le_presse_papier),
                       Toast.LENGTH_SHORT)
             .show();
    }
}
