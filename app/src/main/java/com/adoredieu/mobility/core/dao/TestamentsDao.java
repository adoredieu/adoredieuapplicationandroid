package com.adoredieu.mobility.core.dao;

import com.adoredieu.mobility.core.db.OrmBibleDatabase;
import com.adoredieu.mobility.core.model.bible.Testament;
import com.j256.ormlite.dao.RuntimeExceptionDao;

import java.util.List;

import javax.inject.Inject;

public class TestamentsDao
{
    private final RuntimeExceptionDao<Testament, Integer> dao;

    @Inject
    public TestamentsDao(OrmBibleDatabase db)
    {
        dao = db.getTestamentDao();
    }

    public List<Testament> getTestaments()
    {
        try
        {
            return dao.queryForAll();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }
}
