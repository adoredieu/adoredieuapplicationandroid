package com.adoredieu.mobility.core.model.highlightedVerses;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

@DatabaseTable(tableName = "HighlightedVerseOperations")
public class HighlightedVerseOperation
{
    public static final String VERSE_ID_COLUMN = "_id";
    public static final String DATE_COLUMN = "date";
    public static final String VERSE_BOOK_COLUMN = "book";
    public static final String VERSE_CHAPTER_COLUMN = "chapter";
    public static final String VERSE_NUMBER_COLUMN = "verse";
    public static final String VERSE_COLOR_COLUMN = "color";
    public static final String OPERATION_TYPE_COLUMN = "operationType";

    @DatabaseField(generatedId = true, columnName = VERSE_ID_COLUMN)
    private Integer _id;
    @DatabaseField(columnName = DATE_COLUMN, index = true)
    private Date date;
    @DatabaseField(columnName = VERSE_BOOK_COLUMN, index = true)
    private Integer book;
    @DatabaseField(columnName = VERSE_CHAPTER_COLUMN, index = true)
    private Integer chapter;
    @DatabaseField(columnName = VERSE_NUMBER_COLUMN)
    private Integer verse;
    @DatabaseField(columnName = VERSE_COLOR_COLUMN)
    private Integer color;
    @DatabaseField(columnName = OPERATION_TYPE_COLUMN)
    private OperationType operationType;

    public HighlightedVerseOperation(Date date,
                                     Integer book,
                                     Integer chapter,
                                     Integer verse,
                                     Integer color,
                                     OperationType operationType)
    {
        this.date = date;
        this.book = book;
        this.chapter = chapter;
        this.verse = verse;
        this.color = color;
        this.operationType = operationType;
    }

    public HighlightedVerseOperation()
    {
    }

    public Integer getBook()
    {
        return book;
    }

    public void setBook(Integer book)
    {
        this.book = book;
    }

    public Integer getChapter()
    {
        return chapter;
    }

    public void setChapter(Integer chapter)
    {
        this.chapter = chapter;
    }

    public Integer getVerse()
    {
        return verse;
    }

    public void setVerse(Integer verse)
    {
        this.verse = verse;
    }

    public Integer getColor()
    {
        return color;
    }

    public void setColor(Integer color)
    {
        this.color = color;
    }

    public OperationType getOperationType()
    {
        return operationType;
    }

    public void setOperationType(OperationType operationType)
    {
        this.operationType = operationType;
    }

    public Date getDate()
    {
        return date;
    }

    public void setDate(Date date)
    {
        this.date = date;
    }

    public enum OperationType
    {
        CREATE,
        READ,
        UPDATE,
        DELETE,
    }
}
