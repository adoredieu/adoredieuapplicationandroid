package com.adoredieu.mobility.core.helpers;

import android.app.Activity;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.view.fragments.FragmentBase;

public class FragmentsHelper
{
    private static FragmentBase currentFragmentBase = null;

    private static void hideKeyboard(Activity activity)
    {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(
                Activity.INPUT_METHOD_SERVICE);

        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();

        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null)
        {
            view = new View(activity);
        }
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(),
                                                   0);
    }

    public static void setMainFragment(FragmentActivity activity,
                                       FragmentBase fragmentBase)
    {
        if (activity == null)
        {
            Log.e(FragmentsHelper.class.getSimpleName(), "setMainFragment : activity is null");
            return;
        }

        Log.i(FragmentsHelper.class.getSimpleName(),
              "setMainFragment : changing fragment to " + fragmentBase.getClass().getSimpleName());

        FragmentsHelper.currentFragmentBase = fragmentBase;
        hideKeyboard(activity);

        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragmentBase.setRetainInstance(true);

        fragmentTransaction.replace(R.id.fragment_container,
                                    fragmentBase,
                                    fragmentBase.getClass().getName());

        fragmentTransaction.addToBackStack(fragmentBase.getClass().getName());
        fragmentTransaction.commitAllowingStateLoss();
    }

    public static void popBackStack(FragmentActivity activity,
                                    int count)
    {
        FragmentManager fm = activity.getSupportFragmentManager();
        for (int i = 0; i < fm.getBackStackEntryCount() && i < count; ++i)
        {
            fm.popBackStack();
        }
    }

    public static FragmentBase getCurrentFragmentBase()
    {
        return currentFragmentBase;
    }
}
