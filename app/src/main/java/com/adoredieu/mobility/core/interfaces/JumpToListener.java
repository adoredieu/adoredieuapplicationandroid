package com.adoredieu.mobility.core.interfaces;

public interface JumpToListener
{
    void onJumpTo(int book,
                  int chapter);
}
