package com.adoredieu.mobility.core.interfaces;

public class BiblePosition
{
    public final long book;
    public final long chapter;
    public final long verse;

    public BiblePosition(long book,
                         long chapter,
                         long verse)
    {
        this.book = book;
        this.chapter = chapter;
        this.verse = verse;
    }
}
