package com.adoredieu.mobility.core.helpers;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Build;

public class AsyncTaskHelper {
    @SuppressLint("ObsoleteSdkInt")
    public static <Params> void executeAsyncTask(AsyncTask task, Params... params) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);
        } else {
            task.execute(params);
        }
    }
}
