package com.adoredieu.mobility.core.services;

import android.content.Context;

import com.adoredieu.mobility.core.db.OrmBibleDatabase;
import com.adoredieu.mobility.core.helpers.IOHelpers;
import com.adoredieu.mobility.core.helpers.ZipHelper;
import com.adoredieu.mobility.core.persistence.ApplicationContextPersistence;
import com.adoredieu.mobility.view.activities.ActivityBase;

import java.io.File;

import javax.inject.Inject;

public class BibleInstallationServices {
    private final ApplicationContextPersistence applicationContextPersistence;

    @Inject
    public BibleInstallationServices(ApplicationContextPersistence applicationContextPersistence) {
        this.applicationContextPersistence = applicationContextPersistence;
    }

    private long getInstalledBibleVersion() {
        return applicationContextPersistence.getInstalledBibleVersion();
    }

    public boolean isBibleInstalled(Context context,
                                    int version) {
        File bibleDatabase = context.getDatabasePath(OrmBibleDatabase.DATABASE_NAME);

        return bibleDatabase.exists() && getInstalledBibleVersion() == version;
    }

    public void installBible(final ActivityBase context,
                             File bibleXApkFile,
                             int version) {
        cleanOldFiles(context);

        File outputDirectory = context.getFilesDir()
                .getParentFile();

        ZipHelper.unzip(bibleXApkFile,
                outputDirectory.getPath(),
                null);

        applicationContextPersistence.setInstalledBibleVersion(version);
    }

    private File getOldDatabase(Context context) {
        return context.getDatabasePath("lsg_bible_ormlite.sqlite");
    }

    private File getOldLuceneIndexDirectory(Context context) {
        return new File(context.getFilesDir().getPath() + "/lucene_index");
    }

    private void cleanOldFiles(Context context) {
        File oldDatabase = getOldDatabase(context);

        if (oldDatabase.exists()) {
            oldDatabase.delete();
        }

        File luceneDataDirectory = getOldLuceneIndexDirectory(context);
        IOHelpers.deleteDir(luceneDataDirectory);
    }
}
