package com.adoredieu.mobility.core.dto;

import com.adoredieu.mobility.core.model.highlightedVerses.HighlightedVerseOperation;

import org.joda.time.DateTime;

public class HighlightedVerseDto
{
    public final DateTime date;
    public final int book;
    public final int chapter;
    public final int verse;
    public int colorIndex;
    public final HighlightedVerseOperation.OperationType operationType;

    public HighlightedVerseDto(DateTime date,
                               int book,
                               int chapter,
                               int verse,
                               int colorIndex,
                               HighlightedVerseOperation.OperationType operationType)
    {
        this.date = date;
        this.book = book;
        this.chapter = chapter;
        this.verse = verse;
        this.colorIndex = colorIndex;
        this.operationType = operationType;
    }
}
