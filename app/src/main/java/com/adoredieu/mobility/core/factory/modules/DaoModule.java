package com.adoredieu.mobility.core.factory.modules;

import com.adoredieu.mobility.core.dao.BooksDao;
import com.adoredieu.mobility.core.dao.GreekDao;
import com.adoredieu.mobility.core.dao.HebrewDao;
import com.adoredieu.mobility.core.dao.ReadingPlanDescriptionDao;
import com.adoredieu.mobility.core.dao.ReadingPlanEntriesDao;
import com.adoredieu.mobility.core.dao.TestamentsDao;
import com.adoredieu.mobility.core.dao.VerseDao;
import com.adoredieu.mobility.core.dao.VerseStrongDao;
import com.adoredieu.mobility.core.dao.VersesHighlightsDao;
import com.adoredieu.mobility.core.db.OrmBibleDatabase;
import com.adoredieu.mobility.core.db.HighlightedVersesDatabase;
import com.adoredieu.mobility.core.interfaces.DateProvider;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class DaoModule
{
    @Provides
    @Singleton
    public VerseStrongDao provideVerseStrongDao(OrmBibleDatabase ormBibleDatabase)
    {
        return new VerseStrongDao(ormBibleDatabase);
    }

    @Provides
    @Singleton
    public GreekDao provideGreekDao(OrmBibleDatabase ormBibleDatabase)
    {
        return new GreekDao(ormBibleDatabase);
    }

    @Provides
    @Singleton
    public HebrewDao provideHebrewDao(OrmBibleDatabase ormBibleDatabase)
    {
        return new HebrewDao(ormBibleDatabase);
    }

    @Provides
    @Singleton
    public BooksDao provideBookDao(OrmBibleDatabase ormBibleDatabase)
    {
        return new BooksDao(ormBibleDatabase);
    }

    @Provides
    @Singleton
    public TestamentsDao provideTestamentDao(OrmBibleDatabase ormBibleDatabase)
    {
        return new TestamentsDao(ormBibleDatabase);
    }

    @Provides
    @Singleton
    public VerseDao provideVerseDao(OrmBibleDatabase ormBibleDatabase)
    {
        return new VerseDao(ormBibleDatabase);
    }

    @Provides
    @Singleton
    public VersesHighlightsDao provideVersesHighlightsDao(HighlightedVersesDatabase highlightedVersesDatabase,
                                                          DateProvider dateProvider)
    {
        return new VersesHighlightsDao(highlightedVersesDatabase,
                                       dateProvider);
    }

    @Provides
    @Singleton
    public ReadingPlanDescriptionDao provideReadingPlanDescriptionDao(OrmBibleDatabase ormBibleDatabase)
    {
        return new ReadingPlanDescriptionDao(ormBibleDatabase);
    }

    @Provides
    @Singleton
    public ReadingPlanEntriesDao provideReadingPlanEntriesDao(OrmBibleDatabase ormBibleDatabase)
    {
        return new ReadingPlanEntriesDao(ormBibleDatabase);
    }
}
