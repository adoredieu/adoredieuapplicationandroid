package com.adoredieu.mobility.view.adapters.items;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.core.model.website.ImageQuote;
import com.adoredieu.mobility.core.services.ShareServices;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.mikepenz.fastadapter.items.AbstractItem;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.List;
import java.util.Locale;

public class ImageQuoteItem
        extends AbstractItem<ImageQuoteItem, ImageQuoteItem.ViewHolder> {
    private final ImageQuote imageQuote;
    private final ShareServices shareServices;

    public ImageQuoteItem(ImageQuote imageQuote,
                          ShareServices shareServices) {
        this.imageQuote = imageQuote;
        this.shareServices = shareServices;
    }

    //The unique ID for this type of item
    @Override
    public int getType() {
        return R.id.fastadapter_image_quote_item_id;
    }

    //The layout to be used for this type of item
    @Override
    public int getLayoutRes() {
        return R.layout.lvi_image_quote;
    }

    @Override
    public void bindView(final ViewHolder viewHolder,
                         List payloads) {
        super.bindView(viewHolder, payloads);

        //get the context
        final Context context = viewHolder.itemView.getContext();

        viewHolder.progressBar.setVisibility(View.VISIBLE);

        if (!imageQuote.getThumbnailUri().isEmpty()) {
            Glide.with(context)
                    .load(imageQuote.getThumbnailUri())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            viewHolder.progressBar.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            viewHolder.progressBar.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(viewHolder.articleThumbnailImageView);
        }
        viewHolder.articleTitleTextView.setText(imageQuote.getIntrotext());

        DateTimeFormatter fmt = DateTimeFormat.forPattern("dd/MM/yyyy");
        DateTimeFormatter frenchFmt = fmt.withLocale(Locale.FRENCH);
        viewHolder.articlePublishDateTextView.setText(imageQuote.getDateTime().toLocalDate()
                .toString(frenchFmt));

        viewHolder.shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareServices.shareImage(
                        context,
                        ((BitmapDrawable) viewHolder.articleThumbnailImageView.getDrawable()).getBitmap());
            }
        });
    }

    @Override
    public ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }

    //The viewHolder used for this item. This viewHolder is always reused by the RecyclerView so scrolling is blazing fast
    protected static class ViewHolder
            extends RecyclerView.ViewHolder {
        protected final View view;
        protected final ImageView articleThumbnailImageView;
        protected final TextView articleTitleTextView;
        protected final TextView articlePublishDateTextView;
        protected final Button shareButton;
        protected final ProgressBar progressBar;

        public ViewHolder(View view) {
            super(view);

            this.view = view;

            this.articleThumbnailImageView = view.findViewById(R.id.articleThumbnailImageView);
            this.articleTitleTextView = view.findViewById(R.id.articleTitleTextView);
            this.articlePublishDateTextView = view.findViewById(R.id.articlePublishDateTextView);

            this.shareButton = view.findViewById(R.id.shareButton);
            this.progressBar = view.findViewById(R.id.progressBar);
        }
    }
}
