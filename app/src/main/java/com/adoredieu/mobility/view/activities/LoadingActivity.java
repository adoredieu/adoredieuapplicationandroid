package com.adoredieu.mobility.view.activities;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.adoredieu.mobility.core.expansion.AdoreDieuDownloaderService;
import com.adoredieu.mobility.core.persistence.UserPreferencesPersistence;
import com.adoredieu.mobility.core.system.application.AdoreDieuApplication;
import com.adoredieu.mobility.old.BuildConfig;
import com.adoredieu.mobility.old.R;
import com.crashlytics.android.Crashlytics;
import com.github.lzyzsd.circleprogress.DonutProgress;

import javax.inject.Inject;

import io.fabric.sdk.android.Fabric;

public class LoadingActivity
        extends ActivityBase
{
    @Inject
    UserPreferencesPersistence userPreferencesPersistence;
    private ViewHolder viewHolder;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        ((AdoreDieuApplication) getApplication()).getInjector().inject(this);

        if (!BuildConfig.DEBUG)
        {
            Fabric.with(this,
                        new Crashlytics());
        }

        setTheme(userPreferencesPersistence);

        setContentView(R.layout.activity_loading);

        viewHolder = new ViewHolder();
        viewHolder.progressLinearLayout = findViewById(R.id.progressLinearLayout);
        viewHolder.progressBar = findViewById(R.id.progressBar);
        viewHolder.progressMessageTextView = findViewById(R.id.progressMessageTextView);

        new Handler().postDelayed(
                new Runnable()
                {
                    @Override
                    public void run()
                    {
                        startApplication();
                    }
                },
                200);
    }

    private void startApplication()
    {
        startActivity(new Intent(this, MainActivity.class));
    }

    public static class ViewHolder
    {
        Button cellularAuthorisationButton;
        TextView progressMessageTextView;
        DonutProgress progressBar;
        View progressLinearLayout;
    }
}
