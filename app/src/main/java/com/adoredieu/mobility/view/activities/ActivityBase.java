package com.adoredieu.mobility.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.core.persistence.UserPreferencesPersistence;
import com.adoredieu.mobility.core.system.notifications.NotificationChannelsCreator;
import com.karumi.dexter.Dexter;


public abstract class ActivityBase
        extends AppCompatActivity {
    // https://stackoverflow.com/questions/38041230/intent-migrateextrastreamtoclipdata-on-a-null-object-reference
    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        if (intent == null) {
            intent = new Intent();
        }

        try {
            super.startActivityForResult(intent, requestCode);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void setTheme(UserPreferencesPersistence userPreferencesPersistence) {
        int themeId;
        if (userPreferencesPersistence.getNightMode()) {
            themeId = R.style.AppThemeNight;
        } else {
            themeId = R.style.AppTheme;
        }
        this.setTheme(themeId);
    }

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initialize();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState,
                         @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState,
                persistentState);

        initialize();
    }

    private void initialize() {
        Dexter.withActivity(this);

        NotificationChannelsCreator.create(this);
    }
}
