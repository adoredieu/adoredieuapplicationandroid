package com.adoredieu.mobility.view.adapters.items;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.adoredieu.mobility.old.R;
import com.mikepenz.aboutlibraries.util.UIUtils;
import com.mikepenz.fastadapter.commons.utils.FastAdapterUIUtils;
import com.mikepenz.fastadapter.items.AbstractItem;

import java.util.List;

public class BibleBookItem
        extends AbstractItem<BibleBookItem, BibleBookItem.ViewHolder>
{
    public final String title;
    public final Drawable testamentIcon;

    public BibleBookItem(String title,
                         Drawable testamentIcon)
    {
        this.title = title;
        this.testamentIcon = testamentIcon;
    }

    //The unique ID for this type of item
    @Override
    public int getType()
    {
        return R.id.fastadapter_bible_book_item_id;
    }

    //The layout to be used for this type of item
    @Override
    public int getLayoutRes()
    {
        return R.layout.lvi_book;
    }

    @Override
    public void bindView(ViewHolder viewHolder,
                         List payloads)
    {
        super.bindView(viewHolder, payloads);

        //get the context
        Context context = viewHolder.itemView.getContext();

        //bind our data
        //set the text for the name
        viewHolder.titleTextView.setText(title);

        //set the text for the description or hide
        viewHolder.testamentIconImageView.setImageDrawable(testamentIcon);

        //set the background for the item
        UIUtils.setBackground(viewHolder.view,
                              FastAdapterUIUtils.getSelectableBackground(
                                      context,
                                      ContextCompat.getColor(
                                              context,
                                              R.color.pal_blue_4),
                                      true));
    }

    @Override
    public ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }

    //The viewHolder used for this item. This viewHolder is always reused by the RecyclerView so scrolling is blazing fast
    protected static class ViewHolder
            extends RecyclerView.ViewHolder
    {
        protected final View view;
        protected final TextView titleTextView;
        protected final ImageView testamentIconImageView;

        public ViewHolder(View view)
        {
            super(view);

            this.view = view;
            this.titleTextView = view.findViewById(R.id.titleTextView);
            this.testamentIconImageView = view.findViewById(R.id.testamentIconImageView);
        }
    }
}
