package com.adoredieu.mobility.view.activities;

import android.accounts.Account;
import android.accounts.AccountAuthenticatorActivity;
import android.accounts.AccountManager;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.core.helpers.AsyncTaskHelper;
import com.adoredieu.mobility.core.helpers.WebHelper;
import com.adoredieu.mobility.core.interfaces.authentication.Authentication;
import com.adoredieu.mobility.core.interfaces.authentication.ServerAuthenticate;
import com.adoredieu.mobility.core.system.application.AdoreDieuApplication;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

/**
 * The Authenticator activity.
 * Called by the Authenticator and in charge of identifying the user.
 * It sends back to the Authenticator the result.
 */
public class AuthenticatorActivity
        extends AccountAuthenticatorActivity
        implements View.OnClickListener {
    public final static String ARG_IS_ADDING_NEW_ACCOUNT = "IS_ADDING_ACCOUNT";
    private final static int REQ_SIGNUP = 1;

    @Inject
    ServerAuthenticate serverAuthenticate;

    private AccountManager mAccountManager;
    private String authTokenType;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((AdoreDieuApplication) getApplication()).getInjector().inject(this);

        setContentView(R.layout.activity_login);
        mAccountManager = AccountManager.get(getBaseContext());

        String accountName = getIntent().getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
        authTokenType = getIntent().getStringExtra(AccountManager.KEY_AUTHENTICATOR_TYPES);
        if (authTokenType == null) {
            authTokenType = Authentication.AUTHTOKEN_TYPE_FULL_ACCESS;
        }

        if (accountName != null) {
            ((TextView) findViewById(R.id.pseudonymeEditText)).setText(accountName);
        }

        findViewById(R.id.submitButton).setOnClickListener(this);
        findViewById(R.id.signUpTextView).setOnClickListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode,
                                    int resultCode,
                                    Intent data) {
        // The sign up activity returned that the user has successfully created an account
        if (requestCode == REQ_SIGNUP && resultCode == RESULT_OK) {
            finishLogin(data);
        } else {
            super.onActivityResult(requestCode,
                    resultCode,
                    data);
        }
    }

    private static class AuthenticationAsyncTask extends AsyncTask<String, Void, Intent> {
        private final ServerAuthenticate serverAuthenticate;
        private final WeakReference<AuthenticatorActivity> activityWeakReference;

        private AuthenticationAsyncTask(AuthenticatorActivity authenticatorActivity,
                                        ServerAuthenticate serverAuthenticate) {
            activityWeakReference = new WeakReference<>(authenticatorActivity);
            this.serverAuthenticate = serverAuthenticate;
        }

        @Override
        protected Intent doInBackground(String... params) {
            AuthenticatorActivity context = activityWeakReference.get();
            if (context == null)
                return null;

            final String pseudonyme = params[0];
            final String password = params[1];
            final String accountType = params[2];

            String authToken;
            Bundle data = new Bundle();
            try {
                authToken = serverAuthenticate.userSignIn(pseudonyme,
                        password);

                data.putString(AccountManager.KEY_ACCOUNT_NAME,
                        pseudonyme);
                data.putString(AccountManager.KEY_ACCOUNT_TYPE,
                        accountType);
                data.putString(AccountManager.KEY_AUTHTOKEN,
                        authToken);
                data.putString(AccountManager.KEY_PASSWORD,
                        password);
            } catch (Exception e) {
                data.putString(AccountManager.KEY_ERROR_MESSAGE,
                        e.getMessage());
            }

            final Intent res = new Intent();
            res.putExtras(data);
            return res;
        }

        @Override
        protected void onPostExecute(Intent intent) {
            AuthenticatorActivity activity = activityWeakReference.get();
            if (activity == null)
                return;
            if (intent == null)
                return;

            if (intent.hasExtra(AccountManager.KEY_ERROR_MESSAGE)) {
                Toast.makeText(activity,
                        intent.getStringExtra(AccountManager.KEY_ERROR_MESSAGE),
                        Toast.LENGTH_SHORT).show();
            } else {
                activity.finishLogin(intent);
            }
        }
    }

    public void submit() {
        final String pseudonyme = ((TextView) findViewById(R.id.pseudonymeEditText)).getText()
                .toString()
                .trim();
        final String password = ((TextView) findViewById(R.id.passwordEditText)).getText()
                .toString();
        final String accountType = getIntent().getStringExtra(AccountManager.KEY_ACCOUNT_TYPE);

        AuthenticationAsyncTask authenticationAsyncTask = new AuthenticationAsyncTask(this, serverAuthenticate);
        AsyncTaskHelper.<String>executeAsyncTask(authenticationAsyncTask, pseudonyme, password, accountType);
    }

    private void finishLogin(Intent intent) {
        String accountName = intent.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
        String accountPassword = intent.getStringExtra(AccountManager.KEY_PASSWORD);
        final Account account = new Account(accountName,
                intent.getStringExtra(AccountManager.KEY_ACCOUNT_TYPE));

        if (getIntent().getBooleanExtra(ARG_IS_ADDING_NEW_ACCOUNT,
                false)) {
            String authToken = intent.getStringExtra(AccountManager.KEY_AUTHTOKEN);

            // Creating the account on the device and setting the auth token we got
            // (Not setting the auth token will cause another call to the server to authenticate the user)
            mAccountManager.addAccountExplicitly(account,
                    accountPassword,
                    null);
            mAccountManager.setAuthToken(account,
                    authTokenType,
                    authToken);
        } else {
            mAccountManager.setPassword(account,
                    accountPassword);
        }

        setAccountAuthenticatorResult(intent.getExtras());
        setResult(RESULT_OK,
                intent);

        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.submitButton:
                submit();
                break;
            case R.id.signUpTextView:
                WebHelper.openUrlInBrowser(this,
                        getString(R.string.adoredieu_register_www_address));
                break;
        }
    }
}
