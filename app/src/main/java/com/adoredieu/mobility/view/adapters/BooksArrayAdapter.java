package com.adoredieu.mobility.view.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.core.dto.BookDto;
import com.adoredieu.mobility.core.dto.TestamentDto;
import com.adoredieu.mobility.core.services.BibleServices;

import java.util.List;


public class BooksArrayAdapter
        extends ArrayAdapterCompat<BookDto>
{
    private final Context context;
    private final List<BookDto> values;
    private final BibleServices bibleServices;

    public BooksArrayAdapter(Context context,
                             List<BookDto> values,
                             BibleServices bibleServices)
    {
        super(context,
              R.layout.lvi_book,
              values);

        this.context = context;
        this.values = values;
        this.bibleServices = bibleServices;
    }

    @Override
    public View getDropDownView(int position,
                                View convertView,
                                @NonNull ViewGroup parent)
    {
        return getCustomView(position,
                             convertView);
    }

    @NonNull
    @Override
    public View getView(int position,
                        View convertView,
                        @NonNull ViewGroup parent)
    {
        return getCustomView(position,
                             convertView);
    }

    private View getCustomView(int position,
                               View convertView)
    {
        VerseHolder viewHolder;

        if (convertView == null)
        {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.lvi_book,
                                           null);

            viewHolder = new VerseHolder();
            viewHolder.titleTextView = convertView.findViewById(R.id.titleTextView);
            viewHolder.testamentIconImageView = convertView.findViewById(R.id.testamentIconImageView);
            convertView.setTag(viewHolder);
        }
        else
        {
            viewHolder = (VerseHolder) convertView.getTag();
        }

        BookDto book = values.get(position);

        String bookName = bibleServices.getLocalizedBooksNames().get(book.getId());
        viewHolder.titleTextView.setText(bookName);

        TestamentDto testament = book.getTestament();
        if (testament != null)
        {
            Drawable testamentIcon = testament.getId() == 1 ?
                    ContextCompat.getDrawable(context,
                                              R.drawable.ic_at)
                    : ContextCompat.getDrawable(context,
                                                R.drawable.ic_nt);
            viewHolder.testamentIconImageView.setImageDrawable(testamentIcon);
            viewHolder.testamentIconImageView.setVisibility(View.VISIBLE);
        }
        else
        {
            viewHolder.testamentIconImageView.setVisibility(View.INVISIBLE);
        }

        return convertView;
    }

    private static class VerseHolder
    {
        TextView titleTextView;
        ImageView testamentIconImageView;
    }
}
