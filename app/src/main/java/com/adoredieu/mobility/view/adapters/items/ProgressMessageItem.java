package com.adoredieu.mobility.view.adapters.items;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.adoredieu.mobility.old.R;
import com.mikepenz.fastadapter.items.AbstractItem;

import java.util.List;

public class ProgressMessageItem
        extends AbstractItem<ProgressMessageItem, ProgressMessageItem.ViewHolder>
{
    private final String message;

    public ProgressMessageItem(String message)
    {
        this.message = message;
    }

    //The unique ID for this type of item
    @Override
    public int getType()
    {
        return R.id.fastadapter_progress_message_item_id;
    }

    //The layout to be used for this type of item
    @Override
    public int getLayoutRes()
    {
        return R.layout.lvi_progress_message;
    }

    @Override
    public void bindView(ViewHolder viewHolder,
                         List payloads)
    {
        //call super so the selection is already handled for you
        super.bindView(viewHolder, payloads);

        viewHolder.messageTextView.setText(message);
    }

    @Override
    public ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }

    //The viewHolder used for this item. This viewHolder is always reused by the RecyclerView so scrolling is blazing fast
    protected static class ViewHolder
            extends RecyclerView.ViewHolder
    {
        protected final View view;
        protected final TextView messageTextView;

        public ViewHolder(View view)
        {
            super(view);

            this.view = view;

            this.messageTextView = view.findViewById(R.id.messageTextView);
        }
    }
}
