package com.adoredieu.mobility.view.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adoredieu.mobility.old.R;

public abstract class ReadingPlanFragment
        extends FragmentBase
{
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_reading_plan,
                                container,
                                false);
    }
}
