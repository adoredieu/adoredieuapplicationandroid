package com.adoredieu.mobility.view.adapters.items;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.core.model.website.Article;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.mikepenz.fastadapter.items.AbstractItem;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.List;
import java.util.Locale;

public class ArticleItem
        extends AbstractItem<ArticleItem, ArticleItem.ViewHolder> {
    public final Article article;

    public ArticleItem(Article article) {
        this.article = article;
    }

    //The unique ID for this type of item
    @Override
    public int getType() {
        return R.id.fastadapter_image_quote_item_id;
    }

    //The layout to be used for this type of item
    @Override
    public int getLayoutRes() {
        return R.layout.lvi_article;
    }

    @Override
    public void bindView(final ViewHolder viewHolder,
                         List payloads) {
        //call super so the selection is already handled for you
        super.bindView(viewHolder,
                payloads);

        //get the context
        final Context context = viewHolder.itemView.getContext();

        viewHolder.progressBar.setVisibility(View.VISIBLE);

        if (!article.thumbnailUri.isEmpty()) {
            Glide.with(context)
                    .load(article.thumbnailUri)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            viewHolder.progressBar.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            viewHolder.progressBar.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(viewHolder.articleThumbnailImageView);
        } else {
            viewHolder.articleThumbnailImageView.setImageDrawable(null);
            viewHolder.progressBar.setVisibility(View.GONE);
        }

        viewHolder.articleTitleTextView.setText(article.title);
        viewHolder.articleShortDescriptionTextView.setText(article.introText);

        DateTimeFormatter fmt = DateTimeFormat.forPattern("dd/MM/yyyy");
        DateTimeFormatter frenchFmt = fmt.withLocale(Locale.FRENCH);
        viewHolder.articlePublishDateTextView.setText(article.dateTime.toLocalDate()
                .toString(frenchFmt));
    }

    @Override
    public ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }

    //The viewHolder used for this item. This viewHolder is always reused by the RecyclerView so scrolling is blazing fast
    protected static class ViewHolder
            extends RecyclerView.ViewHolder {
        protected final View view;
        public final ImageView articleThumbnailImageView;
        public final TextView articleTitleTextView;
        public final TextView articleShortDescriptionTextView;
        public final RelativeLayout articleLvi;
        public final TextView articlePublishDateTextView;
        public final ProgressBar progressBar;

        public ViewHolder(View view) {
            super(view);

            this.view = view;

            articleLvi = view.findViewById(R.id.articleLvi);
            articleThumbnailImageView = view.findViewById(R.id.articleThumbnailImageView);
            articlePublishDateTextView = view.findViewById(R.id.articlePublishDateTextView);
            articleTitleTextView = view.findViewById(R.id.articleTitleTextView);
            articleShortDescriptionTextView = view.findViewById(R.id.articleShortDescriptionTextView);
            progressBar = view.findViewById(R.id.progressBar);
        }
    }
}
