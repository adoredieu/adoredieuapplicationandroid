package com.adoredieu.mobility.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.controllers.GuiController;
import com.adoredieu.mobility.core.dao.ReadingPlanEntriesDao;
import com.adoredieu.mobility.core.helpers.FragmentsHelper;
import com.adoredieu.mobility.core.helpers.NavigationDrawerHelper;
import com.adoredieu.mobility.core.helpers.WebHelper;
import com.adoredieu.mobility.core.interfaces.VerseDefinition;
import com.adoredieu.mobility.core.persistence.ApplicationContextPersistence;
import com.adoredieu.mobility.core.persistence.UserPreferencesPersistence;
import com.adoredieu.mobility.core.services.BibleServices;
import com.adoredieu.mobility.core.system.application.AdoreDieuApplication;
import com.adoredieu.mobility.core.system.broadcastReceiver.FavoritesSynchronization;
import com.adoredieu.mobility.core.system.broadcastReceiver.NotificationsPullBroadcastReceiver;
import com.adoredieu.mobility.core.system.broadcastReceiver.VersesHighlightsSynchronization;

import java.util.ArrayList;

import javax.inject.Inject;

public class MainActivity
        extends ActivityBase {
    @Inject
    UserPreferencesPersistence userPreferencesPersistence;
    @Inject
    ApplicationContextPersistence applicationContextPersistence;
    @Inject
    GuiController guiController;
    @Inject
    ReadingPlanEntriesDao readingPlanEntriesDao;
    @Inject
    BibleServices bibleServices;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(getClass().getSimpleName(), "onCreate");

        super.onCreate(savedInstanceState);

        ((AdoreDieuApplication) getApplication()).getInjector().inject(this);

        setTheme(userPreferencesPersistence);

        setContentView(R.layout.activity_main);

        // Set a Toolbar to replace the ActionBar.
        Toolbar toolbar = findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

        final DrawerLayout drawer = findViewById(R.id.drawer_layout);

        if (drawer != null) {
            ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(
                    this,
                    drawer,
                    toolbar,
                    R.string.navigation_drawer_open,
                    R.string.navigation_drawer_close);
            drawer.addDrawerListener(actionBarDrawerToggle);
            actionBarDrawerToggle.syncState();

            NavigationView navigationView = findViewById(R.id.nav_view);
            if (navigationView != null) {
                navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        // Handle navigation view item clicks here.
                        int id = item.getItemId();

                        switch (id) {
                            case R.id.nav_home:
                                guiController.showHome(MainActivity.this);
                                break;
                            case R.id.nav_thoughts:
                                guiController.showThoughtsController(MainActivity.this);
                                break;
                            case R.id.nav_teachings:
                                guiController.showTeachingsController(MainActivity.this);
                                break;
                            case R.id.nav_articles_search:
                                guiController.showArticlesSearchController(MainActivity.this);
                                break;
                            case R.id.nav_citations_images:
                                guiController.showImageQuotesController(MainActivity.this);
                                break;
                            case R.id.nav_bible:
                                if (userPreferencesPersistence.getStrongsMode()) {
                                    guiController.showBibleStrongController(MainActivity.this,
                                            new ArrayList<VerseDefinition>());
                                } else {
                                    guiController.showBibleController(MainActivity.this,
                                            new ArrayList<VerseDefinition>());
                                }
                                break;
                            case R.id.nav_bible_search:
                                guiController.showBibleSearchController(MainActivity.this);
                                break;
                            case R.id.nav_bible_reading_plan:
                                Integer readingPlanIndex = userPreferencesPersistence.getReadingPlanIndex();
                                if (readingPlanIndex > 0) {
                                    long daysCount = readingPlanEntriesDao.getDaysCount(
                                            readingPlanIndex);

                                    if (userPreferencesPersistence.getReadingPlanCurrentDay() >= daysCount) {
                                        userPreferencesPersistence.setReadingPlanIndex(0);
                                        userPreferencesPersistence.setReadingPlanCurrentDay(1);

                                        guiController.showReadingPlanChoiceController(MainActivity.this);
                                    } else {
                                        guiController.showReadingPlanController(MainActivity.this);
                                    }
                                } else {
                                    guiController.showReadingPlanChoiceController(MainActivity.this);
                                }
                                break;
                            case R.id.nav_audio:
                                guiController.showAudioPlayerController(MainActivity.this);
                                break;
                            case R.id.nav_links:
                                guiController.showCommunityController(MainActivity.this);
                                break;
                            case R.id.nav_donation:
                                WebHelper.openUrlInBrowser(MainActivity.this,
                                        getString(R.string.donation_address));
                                break;
                            case R.id.nav_parameters:
                                guiController.showParametersController(MainActivity.this);
                                break;
                        }

                        drawer.closeDrawer(GravityCompat.START);

                        return true;
                    }
                });
            }
        }

        if (savedInstanceState == null
                || FragmentsHelper.getCurrentFragmentBase() == null) {
            guiController.showHome(this);
        }

        new Handler().postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
                        NotificationsPullBroadcastReceiver.armNotificationsPullerLoop(MainActivity.this);
                        VersesHighlightsSynchronization.armVersesHighlightsSynchronizationLoop(MainActivity.this);
                        FavoritesSynchronization.armFavoritesSynchronizationLoop(MainActivity.this);

                        try {
                            // Create cache
                            bibleServices.getLocalizedBooksNames();
                        } catch (Exception ignore) {
                        }
                    }
                },
                250);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer != null && drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_option_menu,
                menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_night_mode) {
            userPreferencesPersistence.setNightMode(!userPreferencesPersistence.getNightMode());

            setTheme(userPreferencesPersistence);

            this.recreate();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();

        Log.i(getClass().getSimpleName(),
                "onResume : activeFragment=" + FragmentsHelper.getCurrentFragmentBase()
                        .getClass()
                        .getSimpleName());

        NavigationDrawerHelper.refreshUsername(this,
                applicationContextPersistence);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        Log.i(getClass().getSimpleName(), "onNewIntent");

        if (intent == null) {
            guiController.showHome(this);
            return;
        }

        @Nullable
        String action = intent.getAction();
        Log.i(getClass().getSimpleName(), "action : " + action);

        if (action == null || action.isEmpty()) {
            guiController.showHome(this);
        } else if (action.equals(getString(R.string.notification_action_audio_player))) {
            guiController.showAudioPlayerController(this);
        } else if (action.equals(getString(R.string.notification_action_citation))) {
            guiController.showHome(this);
        } else if (action.equals(getString(R.string.notification_action_citation_image))) {
            guiController.showImageQuotesController(this);
        } else if (action.equals(getString(R.string.notification_action_enseignement))) {
            guiController.showTeachingsController(this);
        } else if (action.equals(getString(R.string.notification_action_pensee))) {
            guiController.showThoughtsController(this);
        } else {
            guiController.showHome(this);
        }
    }
}
