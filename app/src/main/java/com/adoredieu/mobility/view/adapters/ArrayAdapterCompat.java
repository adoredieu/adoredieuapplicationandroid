package com.adoredieu.mobility.view.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.widget.ArrayAdapter;

import java.util.Collection;
import java.util.List;

public class ArrayAdapterCompat<T>
        extends ArrayAdapter<T>
{
    public ArrayAdapterCompat(Context context,
                              int resource,
                              List<T> entries)
    {
        super(context,
              resource,
              entries);
    }

    /**
     * Add all elements in the collection to the end of the adapter.
     *
     * @param list to add all elements
     */
    @SuppressLint("NewApi")
    public void addAll(@NonNull Collection<? extends T> list)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
        {
            super.addAll(list);
        }
        else
        {
            for (T element : list)
            {
                super.add(element);
            }
        }
    }

    /**
     * Add all elements in the array to the end of the adapter.
     *
     * @param array to add all elements
     */
    @SafeVarargs
    @SuppressLint("NewApi")
    public final void addAll(T... array)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
        {
            super.addAll(array);
        }
        else
        {
            for (T element : array)
            {
                super.add(element);
            }
        }
    }

    public void insertAll(List<T> items)
    {
        for (int i = items.size() - 1; i >= 0; i--)
        {
            insert(items.get(i),
                   0);
        }
    }
}