package com.adoredieu.mobility.view.adapters;

import android.content.Context;

import java.util.List;

public class StringArrayAdapter
        extends ArrayAdapterCompat<String>
{
    public StringArrayAdapter(Context context,
                              int resource,
                              List<String> entries)
    {
        super(context,
              resource,
              entries);
    }
}
