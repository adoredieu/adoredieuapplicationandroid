package com.adoredieu.mobility.view.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.LayoutRes;
import android.support.v4.content.ContextCompat;
import android.widget.TextView;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.core.helpers.VersesHelper;
import com.adoredieu.mobility.core.interfaces.VerseDefinition;
import com.adoredieu.mobility.core.services.HighlightedVersesServices;
import com.adoredieu.mobility.view.controls.CheckableRelativeLayout;

import java.util.List;

public abstract class BaseVersesArrayAdapter<T>
        extends ArrayAdapterCompat<T> {
    protected final int sdk;
    protected final Context context;
    private final Integer bookId;
    private final HighlightedVersesServices highlightedVersesServices;
    private final boolean nightMode;
    HighlightedVersesServices.HighlightedVersesCollection highlightedVerses;

    public BaseVersesArrayAdapter(Context context,
                                  @LayoutRes int resource,
                                  List<T> values,
                                  Integer bookId,
                                  HighlightedVersesServices highlightedVersesServices,
                                  boolean nightMode) {
        super(context, resource, values);

        this.context = context;
        this.bookId = bookId;
        this.highlightedVersesServices = highlightedVersesServices;
        this.nightMode = nightMode;
        sdk = android.os.Build.VERSION.SDK_INT;
        highlightedVerses = new HighlightedVersesServices.HighlightedVersesCollection();
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();

        highlightedVerses = highlightedVersesServices.getHighlightedVerses(bookId);
    }

    @Override
    public void notifyDataSetInvalidated() {
        super.notifyDataSetInvalidated();

        highlightedVerses = highlightedVersesServices.getHighlightedVerses(bookId);
    }

    protected void restoreCheckedState(BaseVersesArrayAdapter.VerseHolder holder,
                                       VerseDefinition verse,
                                       boolean checked) {
        restoreCheckedState(holder, verse.getBookId(), verse.getChapterNumber(), verse.getVerseNumber(), checked);
    }

    protected void restoreCheckedState(BaseVersesArrayAdapter.VerseHolder holder,
                                       Integer book,
                                       Integer chapter,
                                       Integer verse,
                                       boolean checked) {
        // Restore the checked state properly
        int highlightIndex = highlightedVerses.getHighlightColorIndex(book,
                chapter,
                verse);

        int verseBackgroundColor = VersesHelper.getVerseBackgroundColor(highlightIndex,
                nightMode,
                checked);

        setItemColors(
                holder,
                verseBackgroundColor,
                nightMode ? R.color.pal_gray_3 : R.color.pal_gray_2);
    }

    @SuppressWarnings("deprecation")
    @SuppressLint("NewApi")
    protected void setItemColors(VerseHolder holder,
                                 @DrawableRes int background,
                                 @ColorRes int textColor) {
        holder.verseTextTextView.setTextColor(ContextCompat.getColor(context, textColor));

        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            holder.checkableRelativeLayout.setBackgroundDrawable(ContextCompat.getDrawable(
                    context,
                    background));
        } else {
            holder.checkableRelativeLayout.setBackground(ContextCompat.getDrawable(context,
                    background));
        }
    }

    public boolean isNightMode() {
        return nightMode;
    }

    protected static class VerseHolder {
        public TextView verseTextTextView;
        public CheckableRelativeLayout checkableRelativeLayout;
    }
}
