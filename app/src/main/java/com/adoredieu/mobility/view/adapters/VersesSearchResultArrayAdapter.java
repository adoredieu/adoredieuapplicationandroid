package com.adoredieu.mobility.view.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.core.dto.SearchResultBibleVerseDto;
import com.adoredieu.mobility.core.helpers.StringHelper;
import com.adoredieu.mobility.core.services.BibleServices;
import com.adoredieu.mobility.core.services.HighlightedVersesServices;

import java.util.List;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;


public class VersesSearchResultArrayAdapter
        extends BaseVersesArrayAdapter<SearchResultBibleVerseDto>
        implements StickyListHeadersAdapter {
    private final List<SearchResultBibleVerseDto> values;
    private final BibleServices bibleServices;
    private final int numberColor;

    public VersesSearchResultArrayAdapter(Context context,
                                          List<SearchResultBibleVerseDto> values,
                                          HighlightedVersesServices highlightedVersesServices,
                                          boolean nightMode,
                                          BibleServices bibleServices) {
        super(context,
                R.layout.lvi_verse_search_result,
                values,
                0,
                highlightedVersesServices,
                nightMode);

        this.values = values;
        this.bibleServices = bibleServices;
        numberColor = ContextCompat.getColor(
                context,
                R.color.pal_blue_2);
    }

    @NonNull
    @SuppressLint("SetTextI18n")
    @Override
    public View getView(int position,
                        View convertView,
                        @NonNull ViewGroup parent) {
        VerseHolder holder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.lvi_verse_search_result,
                    null);

            holder = new VerseHolder();
            holder.verseTextTextView = convertView.findViewById(R.id.verseTextTextView);
            holder.checkableRelativeLayout = convertView.findViewById(R.id.checkableRelativeLayout);
            convertView.setTag(holder);
        } else {
            holder = (VerseHolder) convertView.getTag();
        }

        SearchResultBibleVerseDto verse = values.get(position);

        Integer verseNumber = verse.getVerse();
        Integer verseChapter = verse.getChapter();
        String verseNumberText = verseChapter + ":" + verseNumber;
        String verseText = verseNumberText + " " + verse.getHighlightedText();

        final ListView listView = (ListView) parent;

        restoreCheckedState(holder, verse, listView.isItemChecked(position));

        holder.verseTextTextView.setText(StringHelper.createColoredText(Html.fromHtml(verseText),
                0,
                verseNumberText.length(),
                numberColor),
                TextView.BufferType.SPANNABLE);

        return convertView;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getHeaderView(int position,
                              View convertView,
                              ViewGroup viewGroup) {
        BookHolder holder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.lvi_header_verse_search,
                    null);

            holder = new BookHolder();
            holder.bookTitleTextView = convertView.findViewById(R.id.headerTitleTextView);
            holder.testamentIconImageView = convertView.findViewById(R.id.testamentIconImageView);

            convertView.setTag(holder);
        } else {
            holder = (BookHolder) convertView.getTag();
        }

        SearchResultBibleVerseDto verse = values.get(position);
        String bookName = bibleServices.getLocalizedBooksNames().get(verse.getBook().getId());
        holder.bookTitleTextView.setText(bookName);

        holder.testamentIconImageView.setImageResource(
                verse.getBook().getTestament().getId() == 1 ? R.drawable.ic_at : R.drawable.ic_nt);

        return convertView;
    }

    @Override
    public long getHeaderId(int position) {
        SearchResultBibleVerseDto verse = values.get(position);

        return verse.getBook()
                .getId();
    }

    private static class BookHolder {
        TextView bookTitleTextView;
        ImageView testamentIconImageView;
    }
}
