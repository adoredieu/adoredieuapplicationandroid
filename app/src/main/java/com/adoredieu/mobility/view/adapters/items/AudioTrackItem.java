package com.adoredieu.mobility.view.adapters.items;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.core.helpers.AudioTrackItemHelper;
import com.adoredieu.mobility.core.model.website.AudioTrack;
import com.mikepenz.fastadapter.items.AbstractItem;
import com.mikepenz.materialize.util.UIUtils;

import java.util.List;

public class AudioTrackItem
        extends AbstractItem<AudioTrackItem, AudioTrackItem.ViewHolder>
{
    public final AudioTrack audioTrack;
    private final boolean nightMode;

    public AudioTrackItem(AudioTrack audioTrack,
                          boolean nightMode)
    {
        this.audioTrack = audioTrack;
        this.nightMode = nightMode;
    }

    //The unique ID for this type of item
    @Override
    public int getType()
    {
        return R.id.fastadapter_audio_track_item_id;
    }

    //The layout to be used for this type of item
    @Override
    public int getLayoutRes()
    {
        return R.layout.lvi_audio_track;
    }

    @Override
    public void bindView(ViewHolder viewHolder,
                         List payloads)
    {
        super.bindView(viewHolder, payloads);

        //get the context
        Context context = viewHolder.itemView.getContext();

        viewHolder.titleTextView.setText(audioTrack.title);
        viewHolder.artistTextView.setText(audioTrack.artist);

        viewHolder.titleTextView.setTextColor(AudioTrackItemHelper.getTitleColor(
                context,
                isSelected(),
                nightMode));
        viewHolder.artistTextView.setTextColor(AudioTrackItemHelper.getArtistColor(
                context,
                isSelected(),
                nightMode));

        ColorDrawable defaultBackgroundColor = AudioTrackItemHelper.getDefaultBackgroundColor(
                context,
                nightMode);
        ColorDrawable selectedBackgroundColor = AudioTrackItemHelper.getSelectedBackgroundColor(
                context,
                nightMode);

        UIUtils.setBackground(viewHolder.mainLayout,
                              AudioTrackItemHelper.getSelectableBackground(
                                      context,
                                      defaultBackgroundColor,
                                      selectedBackgroundColor,
                                      true));
    }

    @Override
    public ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }

    //The viewHolder used for this item. This viewHolder is always reused by the RecyclerView so scrolling is blazing fast
    protected static class ViewHolder
            extends RecyclerView.ViewHolder
    {
        protected final View view;
        public final ViewGroup mainLayout;
        public final TextView titleTextView;
        public final TextView artistTextView;

        public ViewHolder(View view)
        {
            super(view);

            this.view = view;

            mainLayout = view.findViewById(R.id.mainLayout);
            titleTextView = view.findViewById(R.id.titleTextView);
            artistTextView = view.findViewById(R.id.artistTextView);
        }
    }
}
