package com.adoredieu.mobility.view.activities;

import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.core.helpers.AsyncTaskHelper;
import com.adoredieu.mobility.core.interfaces.authentication.ServerAuthenticate;
import com.adoredieu.mobility.core.system.application.AdoreDieuApplication;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

public class SignUpActivity
        extends Activity {
    @Inject
    ServerAuthenticate serverAuthenticate;

    private String accountType;

    public SignUpActivity() {
    }

    private static class SignUpAsyncTask extends AsyncTask<String, Void, Intent> {
        private final WeakReference<SignUpActivity> signUpActivityWeakReference;
        private final ServerAuthenticate serverAuthenticate;


        public SignUpAsyncTask(SignUpActivity signUpActivity,
                               ServerAuthenticate serverAuthenticate) {
            this.signUpActivityWeakReference = new WeakReference<>(signUpActivity);
            this.serverAuthenticate = serverAuthenticate;
        }

        @Override
        protected Intent doInBackground(String... params) {
            SignUpActivity signUpActivity = signUpActivityWeakReference.get();
            if (signUpActivity == null)
                return null;

            final String name = params[0];
            final String email = params[1];
            final String pseudonyme = params[2];
            final String password = params[3];
            final String accountType = params[4];

            String authToken;
            Bundle data = new Bundle();
            try {
                authToken = serverAuthenticate.userSignUp(name,
                        pseudonyme,
                        password,
                        email);

                data.putString(AccountManager.KEY_ACCOUNT_NAME,
                        pseudonyme);
                data.putString(AccountManager.KEY_ACCOUNT_TYPE,
                        accountType);
                data.putString(AccountManager.KEY_AUTHTOKEN,
                        authToken);
                data.putString(AccountManager.KEY_PASSWORD,
                        password);
            } catch (Exception e) {
                data.putString(AccountManager.KEY_ERROR_MESSAGE,
                        e.getMessage());
            }

            final Intent res = new Intent();
            res.putExtras(data);
            return res;
        }

        @Override
        protected void onPostExecute(Intent intent) {
            if (intent == null)
                return;

            SignUpActivity signUpActivity = signUpActivityWeakReference.get();
            if (signUpActivity == null)
                return;

            if (intent.hasExtra(AccountManager.KEY_ERROR_MESSAGE)) {
                Toast.makeText(signUpActivity,
                        intent.getStringExtra(AccountManager.KEY_ERROR_MESSAGE),
                        Toast.LENGTH_SHORT).show();
            } else {
                signUpActivity.setResult(RESULT_OK,
                        intent);
                signUpActivity.finish();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((AdoreDieuApplication) getApplication()).getInjector().inject(this);

        accountType = getIntent().getStringExtra(AccountManager.KEY_ACCOUNT_TYPE);

        setContentView(R.layout.activity_register);

        findViewById(R.id.loginTextView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        findViewById(R.id.submitButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createAccount();
            }
        });
    }

    private void createAccount() {
        final String name = ((TextView) findViewById(R.id.nameEditText)).getText()
                .toString()
                .trim();
        final String pseudonyme = ((TextView) findViewById(R.id.pseudonymeEditText)).getText()
                .toString()
                .trim();
        final String email = ((TextView) findViewById(R.id.emailEditText)).getText()
                .toString()
                .trim();
        final String password = ((TextView) findViewById(R.id.passwordEditText)).getText()
                .toString()
                .trim();

        SignUpAsyncTask signUpAsyncTask = new SignUpAsyncTask(this, serverAuthenticate);
        AsyncTaskHelper.<String>executeAsyncTask(signUpAsyncTask, name, email, pseudonyme, password, accountType);
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        super.onBackPressed();
    }
}
