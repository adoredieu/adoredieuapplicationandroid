package com.adoredieu.mobility.view.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.core.services.BibleSearchService;
import com.adoredieu.mobility.core.services.BibleServices;
import com.adoredieu.mobility.core.services.HighlightedVersesServices;

import java.util.List;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;


public class HighlightedHitArrayAdapter
        extends BaseVersesArrayAdapter<BibleSearchService.HighlightedHit>
        implements StickyListHeadersAdapter {
    private final List<BibleSearchService.HighlightedHit> values;
    private final BibleServices bibleServices;
    private String numberColor;
    private String textColor;
    private String hitsColor;

    public HighlightedHitArrayAdapter(Context context,
                                      List<BibleSearchService.HighlightedHit> values,
                                      HighlightedVersesServices highlightedVersesServices,
                                      boolean nightMode,
                                      BibleServices bibleServices) {
        super(context,
                R.layout.lvi_verse_search_result,
                values,
                0,
                highlightedVersesServices,
                nightMode);

        this.values = values;
        this.bibleServices = bibleServices;

        numberColor = "#" + Integer.toHexString(ContextCompat.getColor(context, R.color.pal_blue_2) & 0x00ffffff);

        hitsColor = "#" + Integer.toHexString((nightMode ?
                ContextCompat.getColor(context, R.color.pal_strongs_night)
                : ContextCompat.getColor(context, R.color.pal_strongs_day)) & 0x00ffffff);

        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = context.getTheme();
        theme.resolveAttribute(R.attr.textColorInverted,
                typedValue,
                true);
        String color = typedValue.coerceToString()
                .toString();
        color = color.substring(3);
        textColor = "#" + color;
    }

    @NonNull
    @SuppressLint("SetTextI18n")
    @Override
    public View getView(int position,
                        View convertView,
                        @NonNull ViewGroup parent) {
        VerseHolder holder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.lvi_verse_search_result,
                    null);

            holder = new VerseHolder();
            holder.verseTextTextView = convertView.findViewById(R.id.verseTextTextView);
            holder.checkableRelativeLayout = convertView.findViewById(R.id.checkableRelativeLayout);
            convertView.setTag(holder);
        } else {
            holder = (VerseHolder) convertView.getTag();
        }

        BibleSearchService.HighlightedHit verse = values.get(position);

        final ListView listView = (ListView) parent;

        restoreCheckedState(holder, verse.getBook(), verse.getChapter(), verse.getVerse(), listView.isItemChecked(position));

        // Colorize hits
        String colorizedText = verse.getText();
        for (int i = verse.getOffsets().size() - 1; i >= 0; i--) {
            BibleSearchService.HighlightedHit.Offset offset = verse.getOffsets().get(i);

            colorizedText = colorizedText.substring(0, offset.getStart())
                    + "<font color='" + hitsColor + "'>"
                    + colorizedText.substring(offset.getStart(), offset.getStart() + offset.getLength())
                    + "</font> "
                    + colorizedText.substring(offset.getStart() + offset.getLength());
        }

        String verseText = "<span style=\"color:'" + textColor + "'\">"
                + "<font color='" + numberColor + "'>"
                + verse.getChapterNumber() + ":" + verse.getVerseNumber()
                + "</font> "
                + colorizedText
                + "</span> ";

        holder.verseTextTextView.setText(Html.fromHtml(verseText), TextView.BufferType.SPANNABLE);

        return convertView;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getHeaderView(int position,
                              View convertView,
                              ViewGroup viewGroup) {
        BookHolder holder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.lvi_header_verse_search,
                    null);

            holder = new BookHolder();
            holder.bookTitleTextView = convertView.findViewById(R.id.headerTitleTextView);
            holder.testamentIconImageView = convertView.findViewById(R.id.testamentIconImageView);

            convertView.setTag(holder);
        } else {
            holder = (BookHolder) convertView.getTag();
        }

        BibleSearchService.HighlightedHit verse = values.get(position);
        String bookName = bibleServices.getLocalizedBooksNames().get(verse.getBook());
        holder.bookTitleTextView.setText(bookName);

        holder.testamentIconImageView.setImageResource(
                verse.getTestament() == 1 ? R.drawable.ic_at : R.drawable.ic_nt);

        return convertView;
    }

    @Override
    public long getHeaderId(int position) {
        BibleSearchService.HighlightedHit verse = values.get(position);

        return verse.getBook();
    }

    private static class BookHolder {
        TextView bookTitleTextView;
        ImageView testamentIconImageView;
    }
}
