package com.adoredieu.mobility.view.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.core.interfaces.VerseDefinition;
import com.adoredieu.mobility.core.services.BibleServices;
import com.adoredieu.mobility.core.services.HighlightedVersesServices;

import java.util.List;


public class ReadingPlanVersesArrayAdapter
        extends VersesArrayAdapter
{
    private final BibleServices bibleServices;

    public ReadingPlanVersesArrayAdapter(Context context,
                                         List<VerseDefinition> values,
                                         Integer bookId,
                                         HighlightedVersesServices highlightedVersesServices,
                                         boolean nightMode,
                                         BibleServices bibleServices)
    {
        super(context,
              values,
              bookId,
              highlightedVersesServices,
              nightMode);

        this.bibleServices = bibleServices;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getHeaderView(int position,
                              View convertView,
                              ViewGroup viewGroup)
    {
        ChapterHolder holder;
        if (convertView == null)
        {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.lvi_header_verse,
                                           null);

            holder = new ChapterHolder();
            holder.chapterTitleTextView = convertView.findViewById(R.id.headerTitleTextView);

            convertView.setTag(holder);
        }
        else
        {
            holder = (ChapterHolder) convertView.getTag();
        }

        VerseDefinition verse = values.get(position);

        if (verse.getBookId() == 0)
        {
            holder.chapterTitleTextView.setText(context.getString(R.string.bravo));
        }
        else
        {
            holder.chapterTitleTextView.setText(
                    bibleServices.getLocalizedBooksNames().get(verse.getBookId())
                    + " " + verse.getChapter());
        }

        return convertView;
    }
}
