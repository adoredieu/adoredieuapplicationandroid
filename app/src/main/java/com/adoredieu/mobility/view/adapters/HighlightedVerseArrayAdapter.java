package com.adoredieu.mobility.view.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.core.dao.VerseDao;
import com.adoredieu.mobility.core.dto.HighlightedVerseDto;
import com.adoredieu.mobility.core.helpers.StringHelper;
import com.adoredieu.mobility.core.helpers.VersesHelper;
import com.adoredieu.mobility.core.services.BibleServices;
import com.adoredieu.mobility.view.controls.CheckableRelativeLayout;

import java.util.List;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

public class HighlightedVerseArrayAdapter
        extends ArrayAdapterCompat<HighlightedVerseDto>
        implements StickyListHeadersAdapter {
    protected final int sdk;
    private final List<HighlightedVerseDto> values;
    private final boolean nightMode;
    private final BibleServices bibleServices;
    private final Context context;
    private final List<String> booksName;
    private final VerseDao verseDao;
    private final int numberColor;

    public HighlightedVerseArrayAdapter(Context context,
                                        List<HighlightedVerseDto> values,
                                        VerseDao verseDao,
                                        boolean nightMode,
                                        BibleServices bibleServices) {
        super(context,
                R.layout.lvi_verse,
                values);

        this.context = context;
        this.values = values;
        this.verseDao = verseDao;
        this.nightMode = nightMode;
        this.bibleServices = bibleServices;

        sdk = android.os.Build.VERSION.SDK_INT;
        booksName = bibleServices.getLocalizedBooksNames();

        numberColor = ContextCompat.getColor(
                context,
                R.color.pal_blue_2);
    }

    @NonNull
    @SuppressLint("SetTextI18n")
    @Override
    public View getView(int position,
                        View convertView,
                        @NonNull ViewGroup parent) {
        VerseHolder holder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.lvi_verse,
                    null);

            holder = new VerseHolder();
            holder.verseTextTextView = convertView.findViewById(R.id.verseTextTextView);
            holder.checkableRelativeLayout = convertView.findViewById(R.id.checkableRelativeLayout);
            convertView.setTag(holder);
        } else {
            holder = (VerseHolder) convertView.getTag();
        }

        HighlightedVerseDto verse = values.get(position);

        Integer verseNumber = verse.verse;
        Integer verseChapter = verse.chapter;
        String verseNumberText = verseChapter + ":" + verseNumber;
        String verseText = verseNumberText + " " + verseDao.getVerse(verse.book,
                verse.chapter,
                verse.verse).getText();

        int verseBackgroundColor = VersesHelper.getVerseBackgroundColor(verse.colorIndex,
                nightMode,
                false);

        setItemColors(
                holder,
                verseBackgroundColor,
                nightMode ? R.color.pal_gray_3 : R.color.pal_gray_2);

        holder.verseTextTextView.setText(StringHelper.createColoredText(Html.fromHtml(verseText),
                0,
                verseNumberText.length(),
                numberColor),
                TextView.BufferType.SPANNABLE);

        return convertView;
    }

    @SuppressWarnings("deprecation")
    @SuppressLint("NewApi")
    protected void setItemColors(VerseHolder holder,
                                 @DrawableRes int background,
                                 @ColorRes int textColor) {
        holder.verseTextTextView.setTextColor(ContextCompat.getColor(context, textColor));

        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            holder.checkableRelativeLayout.setBackgroundDrawable(ContextCompat.getDrawable(
                    context,
                    background));
        } else {
            holder.checkableRelativeLayout.setBackground(ContextCompat.getDrawable(context,
                    background));
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getHeaderView(int position,
                              View convertView,
                              ViewGroup viewGroup) {
        BookHolder holder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.lvi_header_verse_search,
                    null);

            holder = new BookHolder();
            holder.bookTitleTextView = convertView.findViewById(R.id.headerTitleTextView);
            holder.testamentIconImageView = convertView.findViewById(R.id.testamentIconImageView);

            convertView.setTag(holder);
        } else {
            holder = (BookHolder) convertView.getTag();
        }

        HighlightedVerseDto highlightedVerseDto = values.get(position);
        String bookName = booksName.get(highlightedVerseDto.book);
        holder.bookTitleTextView.setText(bookName);

        holder.testamentIconImageView.setImageResource(
                bibleServices.getBook(highlightedVerseDto.book)
                        .getTestament()
                        .getId() == 1 ? R.drawable.ic_at : R.drawable.ic_nt);

        return convertView;
    }

    @Override
    public long getHeaderId(int position) {
        HighlightedVerseDto highlightedVerseDto = values.get(position);

        return highlightedVerseDto.book;
    }

    private static class VerseHolder {
        public TextView verseTextTextView;
        public CheckableRelativeLayout checkableRelativeLayout;
    }

    private static class BookHolder {
        TextView bookTitleTextView;
        ImageView testamentIconImageView;
    }
}
