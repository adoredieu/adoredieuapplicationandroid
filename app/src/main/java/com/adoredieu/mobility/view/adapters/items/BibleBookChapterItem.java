package com.adoredieu.mobility.view.adapters.items;


import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.adoredieu.mobility.old.R;
import com.mikepenz.aboutlibraries.util.UIUtils;
import com.mikepenz.fastadapter.commons.utils.FastAdapterUIUtils;
import com.mikepenz.fastadapter.items.AbstractItem;

import java.util.List;

public class BibleBookChapterItem
        extends AbstractItem<BibleBookChapterItem, BibleBookChapterItem.ViewHolder>
{
    public final Integer chapter;

    public BibleBookChapterItem(Integer chapter)
    {
        this.chapter = chapter;
    }

    //The unique ID for this type of item
    @Override
    public int getType()
    {
        return R.id.fastadapter_bible_book_chapter_id;
    }

    //The layout to be used for this type of item
    @Override
    public int getLayoutRes()
    {
        return R.layout.lvi_chapter;
    }

    //The logic to bind your data to the view
    @SuppressLint("SetTextI18n")
    @Override
    public void bindView(ViewHolder viewHolder,
                         List payloads)
    {
        super.bindView(viewHolder, payloads);

        //get the context
        Context context = viewHolder.itemView.getContext();

        viewHolder.chapterTextView.setText("" + chapter);

        if (chapter % 50 == 0)
        {
            //set the background for the item
            UIUtils.setBackground(viewHolder.view,
                                  FastAdapterUIUtils.getSelectableBackground(
                                          context,
                                          ContextCompat.getColor(
                                                  context,
                                                  R.color.pal_blue_4),
                                          true));
        }
        else if (chapter % 10 == 0)
        {
            //set the background for the item
            UIUtils.setBackground(viewHolder.view,
                                  FastAdapterUIUtils.getSelectableBackground(
                                          context,
                                          ContextCompat.getColor(
                                                  context,
                                                  R.color.pal_blue_4),
                                          true));
        }
        else
        {
            //set the background for the item
            UIUtils.setBackground(viewHolder.view,
                                  FastAdapterUIUtils.getSelectableBackground(
                                          context,
                                          ContextCompat.getColor(
                                                  context,
                                                  R.color.pal_blue_4),
                                          true));
        }

    }

    @Override
    public ViewHolder getViewHolder(View v) {
        return new ViewHolder(v);
    }

    //The viewHolder used for this item. This viewHolder is always reused by the RecyclerView so scrolling is blazing fast
    protected static class ViewHolder
            extends RecyclerView.ViewHolder
    {
        protected final View view;
        protected final TextView chapterTextView;

        public ViewHolder(View view)
        {
            super(view);

            this.view = view;
            this.chapterTextView = view.findViewById(R.id.chapterTextView);
        }
    }
}
