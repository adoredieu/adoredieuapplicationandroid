package com.adoredieu.mobility.view.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.core.interfaces.VerseDefinition;
import com.adoredieu.mobility.core.services.HighlightedVersesServices;

import java.util.List;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;


public class VersesArrayAdapter
        extends BaseVersesArrayAdapter<VerseDefinition>
        implements StickyListHeadersAdapter
{
    protected final List<VerseDefinition> values;
    private final String numberColor;
    private final String textColor;

    public VersesArrayAdapter(Context context,
                              List<VerseDefinition> values,
                              Integer bookId,
                              HighlightedVersesServices highlightedVersesServices,
                              boolean nightMode)
    {
        super(context,
              R.layout.lvi_verse,
              values,
              bookId,
              highlightedVersesServices,
              nightMode);

        this.values = values;
        numberColor = "#" + Integer.toHexString(ContextCompat.getColor(context,
                                                                       R.color.pal_blue_2) & 0x00ffffff);

        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = context.getTheme();
        theme.resolveAttribute(R.attr.textColorInverted,
                               typedValue,
                               true);
        String color = typedValue.coerceToString()
                                 .toString();
        color = color.substring(3);
        textColor = "#" + color;
    }

    @NonNull
    @SuppressLint("SetTextI18n")
    @Override
    public View getView(int position,
                        View convertView,
                        @NonNull ViewGroup parent)
    {
        VerseHolder holder;

        if (convertView == null)
        {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.lvi_verse,
                                           null);

            holder = new VerseHolder();
            holder.verseTextTextView = convertView.findViewById(R.id.verseTextTextView);
            holder.checkableRelativeLayout = convertView.findViewById(R.id.checkableRelativeLayout);
            convertView.setTag(holder);
        }
        else
        {
            holder = (VerseHolder) convertView.getTag();
        }

        VerseDefinition verse = values.get(position);

        String verseText;
        if (verse.getVerse() != 0)
        {
            verseText = "<font color='" + numberColor + "'>"
                        + verse.getVerse().toString()
                        + "</font> "
                        + "<font color='" + textColor + "'>"
                        + verse.getText()
                        + "</font> ";
        }
        else
        {
            verseText = "<font color='" + textColor + "'>"
                        + verse.getText()
                        + "</font> ";
        }

        holder.verseTextTextView.setText(Html.fromHtml(verseText), TextView.BufferType.SPANNABLE);

        final ListView listView = (ListView) parent;

        restoreCheckedState(holder,
                            verse,
                            listView.isItemChecked(position));

        return convertView;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getHeaderView(int position,
                              View convertView,
                              ViewGroup viewGroup)
    {
        ChapterHolder holder;
        if (convertView == null)
        {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.lvi_header_verse,
                                           null);

            holder = new ChapterHolder();
            holder.chapterTitleTextView = convertView.findViewById(R.id.headerTitleTextView);

            convertView.setTag(holder);
        }
        else
        {
            holder = (ChapterHolder) convertView.getTag();
        }

        VerseDefinition verse = values.get(position);
        holder.chapterTitleTextView.setText(context.getString(R.string.chapitre) + " " + verse.getChapter());

        return convertView;
    }

    @Override
    public long getHeaderId(int position)
    {
        try
        {
            VerseDefinition verse = values.get(position);

            return verse.getChapter();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return 0;
        }
    }

    protected static class ChapterHolder
    {
        TextView chapterTitleTextView;
    }
}
