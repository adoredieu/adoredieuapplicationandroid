package com.adoredieu.mobility.view.fragments;

import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.core.persistence.UserPreferencesPersistence;
import com.adoredieu.mobility.core.system.application.AdoreDieuApplication;

import javax.inject.Inject;

public abstract class FragmentBase
        extends Fragment
{
    @Inject
    UserPreferencesPersistence userPreferencesPersistence;

    public abstract void synchronizeNavigationDrawer(NavigationView navigationView);

    @Override
    @CallSuper
    public void onViewCreated(@NonNull View view,
                              Bundle savedInstanceState)
    {
        ((AdoreDieuApplication) getActivity().getApplication()).getInjector().inject(this);

        Log.i(getClass().getSimpleName(), "onViewCreated");
    }

    @Override
    @CallSuper
    public void onResume()
    {
        super.onResume();

        if (userPreferencesPersistence.getFullscreenPreference())
        {
            getActivity().getWindow()
                         .addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        else
        {
            getActivity().getWindow()
                         .clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }

        NavigationView navigationView = getActivity().findViewById(R.id.nav_view);
        synchronizeNavigationDrawer(navigationView);
    }
}
