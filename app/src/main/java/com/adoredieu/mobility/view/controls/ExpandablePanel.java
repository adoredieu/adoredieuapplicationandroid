package com.adoredieu.mobility.view.controls;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.adoredieu.mobility.old.R;

public class ExpandablePanel
        extends LinearLayout
{
    private ViewHolder viewHolder;
    private boolean expanded = true;

    public ExpandablePanel(Context context)
    {
        this(context, null);
    }

    public ExpandablePanel(Context context,
                           AttributeSet attrs)
    {
        this(context,
             attrs, 0);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public ExpandablePanel(Context context,
                           AttributeSet attrs,
                           int defStyle)
    {
        super(context,
              attrs,
              defStyle);

        inflate(context);

        initViewHolder();

        init(context,
             attrs,
             defStyle);
    }

    protected void inflate(Context context)
    {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.component_expandable_panel,
                         this,
                         true);
    }

    protected void initViewHolder()
    {
        viewHolder = new ViewHolder();
        viewHolder.containerLinearLayout = findViewById(R.id.containerLinearLayout);
        viewHolder.expandImageButton = findViewById(R.id.expandImageButton);
        viewHolder.titleTextView = findViewById(R.id.titleTextView);
    }

    private void init(Context context,
                      AttributeSet attrs,
                      int defStyle)
    {
        TypedArray a = context.obtainStyledAttributes(attrs,
                                                      R.styleable.ExpandablePanel,
                                                      defStyle,
                                                      0);

        final int attributeCount = a.getIndexCount();
        for (int i = 0; i < attributeCount; i++)
        {
            int curAttr = a.getIndex(i);

            if (curAttr == R.styleable.ExpandablePanel_android_text)
            {
                String text = a.getString(curAttr);
                viewHolder.titleTextView.setText(text);
            }
            else if (curAttr == R.styleable.ExpandablePanel_android_textColor)
            {
                int textColor = a.getColor(curAttr,
                                           0);
                viewHolder.titleTextView.setTextColor(textColor);
            }
            else if (curAttr == R.styleable.ExpandablePanel_android_textSize)
            {
                int textSize = a.getDimensionPixelSize(curAttr,
                                                       1);
                viewHolder.titleTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                                                     textSize);
            }
            else if (curAttr == R.styleable.ExpandablePanel_android_textStyle)
            {
                int textStyle = a.getInt(curAttr,
                                         0);
                viewHolder.titleTextView.setTypeface(null,
                                                     textStyle);
            }
        }

        a.recycle();

        viewHolder.expandImageButton.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (isExpanded())
                {
                    collapse();
                }
                else
                {
                    expand();
                }
            }
        });
    }

    private void expand()
    {
        viewHolder.containerLinearLayout.setVisibility(VISIBLE);
        viewHolder.expandImageButton.setBackgroundResource(R.drawable.ic_collapse);
        expanded = true;
    }

    public void collapse()
    {
        viewHolder.containerLinearLayout.setVisibility(GONE);
        viewHolder.expandImageButton.setBackgroundResource(R.drawable.ic_expand);
        expanded = false;
    }

    private boolean isExpanded()
    {
        return expanded;
    }

    // This way children can be added from xml.
    @Override
    protected void onFinishInflate()
    {
        // They suggest to call it no matter what.
        super.onFinishInflate();

        View v;
        while ((v = getChildAt(1)) != null)
        {
            removeView(v);
            viewHolder.containerLinearLayout.addView(v);
        }
    }

    private class ViewHolder
    {
        ImageButton expandImageButton;
        ViewGroup containerLinearLayout;
        TextView titleTextView;
    }
}
