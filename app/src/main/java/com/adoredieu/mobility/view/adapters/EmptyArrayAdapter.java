package com.adoredieu.mobility.view.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.adoredieu.mobility.old.R;

import java.util.ArrayList;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

public class EmptyArrayAdapter
        extends ArrayAdapterCompat
        implements StickyListHeadersAdapter
{
    public EmptyArrayAdapter(Context context)
    {
        super(context,
              R.layout.lvi_verse,
              new ArrayList());
    }

    @Override
    public View getHeaderView(int position,
                              View convertView,
                              ViewGroup parent)
    {
        return null;
    }

    @Override
    public long getHeaderId(int position)
    {
        return 0;
    }
}
