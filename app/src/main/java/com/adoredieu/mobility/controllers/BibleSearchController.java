package com.adoredieu.mobility.controllers;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.core.adapter.BookAdapter;
import com.adoredieu.mobility.core.helpers.KeyboardHelper;
import com.adoredieu.mobility.core.interfaces.Action;
import com.adoredieu.mobility.core.interfaces.SearchOperator;
import com.adoredieu.mobility.core.interfaces.VerseDefinition;
import com.adoredieu.mobility.core.model.bible.Book;
import com.adoredieu.mobility.core.persistence.ApplicationContextPersistence;
import com.adoredieu.mobility.core.persistence.UserPreferencesPersistence;
import com.adoredieu.mobility.core.services.BibleSearchService;
import com.adoredieu.mobility.core.services.BibleServices;
import com.adoredieu.mobility.core.services.HighlightedVersesServices;
import com.adoredieu.mobility.core.system.application.AdoreDieuApplication;
import com.adoredieu.mobility.view.adapters.BooksArrayAdapter;
import com.adoredieu.mobility.view.adapters.EmptyArrayAdapter;
import com.adoredieu.mobility.view.adapters.HighlightedHitArrayAdapter;
import com.adoredieu.mobility.view.controls.ExpandablePanel;
import com.adoredieu.mobility.view.fragments.BibleSearchFragment;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import se.emilsjolander.stickylistheaders.StickyListHeadersListView;


public class BibleSearchController
        extends BibleSearchFragment
        implements AdapterView.OnItemClickListener {
    @Inject
    BibleServices bibleServices;
    @Inject
    HighlightedVersesServices highlightedVersesServices;
    @Inject
    ApplicationContextPersistence applicationContextPersistence;
    @Inject
    UserPreferencesPersistence userPreferencesPersistence;
    @Inject
    GuiController guiController;

    private List<BibleSearchService.HighlightedHit> highlightedHits;
    private ViewHolder viewHolder;

    @Override
    public void synchronizeNavigationDrawer(NavigationView navigationView) {
        navigationView.setCheckedItem(R.id.nav_bible_search);
    }

    @Override
    public void onViewCreated(@NonNull View view,
                              Bundle savedInstanceState) {
        super.onViewCreated(view,
                savedInstanceState);
        ((AdoreDieuApplication) getActivity().getApplication()).getInjector().inject(this);

        viewHolder = new ViewHolder();
        viewHolder.searchButton = view.findViewById(R.id.searchButton);
        viewHolder.searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startSearch();
            }
        });

        viewHolder.searchEditText = view.findViewById(R.id.searchEditText);
        viewHolder.searchEditText.setText(applicationContextPersistence.getLastBibleSearch());
        viewHolder.searchEditText.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v,
                                 int keyCode,
                                 KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    startSearch();
                    return true;
                }
                return false;
            }
        });

        ImageButton clearSearchEditTextButton = view.findViewById(R.id.clearSearchEditTextButton);
        clearSearchEditTextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewHolder.searchEditText.setText("");
            }
        });

        viewHolder.progressBar = view.findViewById(R.id.progressBar);
        viewHolder.versesListView = view.findViewById(R.id.versesListView);
        viewHolder.versesListView.setOnItemClickListener(this);

        viewHolder.resultCountTextView = view.findViewById(R.id.resultCountTextView);
        viewHolder.resultCountTextView.setVisibility(View.GONE);

        viewHolder.exactTermsCheckBox = view.findViewById(R.id.exactTermsCheckBox);

        viewHolder.bookFilterOperatorSpinner = view.findViewById(R.id.bookFilterOperatorSpinner);
        final List<Book> books = bibleServices.getBooks();
        List<Book> finalBooks = new ArrayList<>();
        finalBooks.add(new Book(null,
                "",
                "")); // Fake empty book
        finalBooks.addAll(books);
        viewHolder.bookFilterOperatorSpinner.setAdapter(new BooksArrayAdapter(
                getActivity(),
                BookAdapter.booksToBookDtos(finalBooks),
                bibleServices));

        viewHolder.strongSearchButton = view.findViewById(R.id.strongSearchButton);
        viewHolder.strongSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                guiController.showBibleStrongSearchController(getActivity(),
                        BibleStrongSearchController.StrongLanguage.Hebrew,
                        0);
            }
        });

        viewHolder.advancedSearchExpandablePanel = view.findViewById(R.id.advancedSearchExpandablePanel);
        viewHolder.advancedSearchExpandablePanel.collapse();

        if (highlightedHits != null && highlightedHits.size() > 0) {
            publishResults(highlightedHits);
        }
    }

    private void startSearch() {
        String search = viewHolder.searchEditText.getText().toString();

        if (!search.isEmpty()) {
            viewHolder.versesListView.setAdapter(new EmptyArrayAdapter(getActivity()));

            applicationContextPersistence.setLastBibleSearch(search);

            viewHolder.strongSearchButton.setVisibility(View.INVISIBLE);
            viewHolder.progressBar.setVisibility(View.VISIBLE);
            viewHolder.searchButton.setEnabled(false);
            KeyboardHelper.hide_keyboard(getActivity());

            SearchOperator searchOperator = SearchOperator.AND;
            if (viewHolder.exactTermsCheckBox.isChecked())
                searchOperator = SearchOperator.EXACTLY;

            Book bookFilter = null;

            if (viewHolder.bookFilterOperatorSpinner.getSelectedItemPosition() > 0) {
                bookFilter = bibleServices.getBook(viewHolder.bookFilterOperatorSpinner.getSelectedItemPosition());
            }

            bibleServices.startSearch(
                    search,
                    searchOperator,
                    bookFilter,
                    new Action<List<BibleSearchService.HighlightedHit>>() {
                        @SuppressLint("SetTextI18n")
                        @Override
                        public void run(List<BibleSearchService.HighlightedHit> highlightedHits) {
                            publishResults(highlightedHits);
                        }
                    });
        }
    }

    @SuppressLint("SetTextI18n")
    private void publishResults(List<BibleSearchService.HighlightedHit> highlightedHits) {
        this.highlightedHits = highlightedHits;

        viewHolder.strongSearchButton.setVisibility(View.INVISIBLE);

        HighlightedHitArrayAdapter versesArrayAdapter = new HighlightedHitArrayAdapter(
                getActivity(),
                highlightedHits,
                highlightedVersesServices,
                userPreferencesPersistence.getNightMode(),
                bibleServices);

        viewHolder.versesListView.setAdapter(versesArrayAdapter);

        if (versesArrayAdapter.getCount() > 1) {
            viewHolder.resultCountTextView.setText(versesArrayAdapter.getCount() + " " + getString(R.string.resultats));
        } else {
            viewHolder.resultCountTextView.setText(versesArrayAdapter.getCount() + " " + getString(R.string.resultat));
        }

        viewHolder.searchButton.setEnabled(true);
        viewHolder.progressBar.setVisibility(View.INVISIBLE);
        viewHolder.resultCountTextView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView,
                            View view,
                            int position,
                            long id) {
        BibleSearchService.HighlightedHit highlightedHit = (BibleSearchService.HighlightedHit) adapterView.getItemAtPosition(position);

        // Enlever le mode strong
        userPreferencesPersistence.setStrongsMode(false);

        List<VerseDefinition> verseDefinitions = new ArrayList<>();
        verseDefinitions.add(highlightedHit);

        guiController.showBibleController(getActivity(), verseDefinitions);
    }

    private class ViewHolder {
        public ProgressBar progressBar;
        public StickyListHeadersListView versesListView;
        ImageButton searchButton;
        EditText searchEditText;
        TextView resultCountTextView;
        ImageButton strongSearchButton;
        Spinner bookFilterOperatorSpinner;
        ExpandablePanel advancedSearchExpandablePanel;
        CheckBox exactTermsCheckBox;
    }
}

