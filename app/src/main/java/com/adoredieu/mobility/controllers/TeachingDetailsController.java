package com.adoredieu.mobility.controllers;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.view.View;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.controllers.base.ArticleDetailsBaseController;
import com.adoredieu.mobility.core.system.application.AdoreDieuApplication;

public class TeachingDetailsController
        extends ArticleDetailsBaseController
{
    @Override
    public void synchronizeNavigationDrawer(NavigationView navigationView)
    {
        navigationView.setCheckedItem(R.id.nav_teachings);
    }

    @Override
    public void onViewCreated(@NonNull View view,
                              Bundle savedInstanceState)
    {
        super.onViewCreated(view,
                            savedInstanceState);

        ((AdoreDieuApplication) getActivity().getApplication()).getInjector().inject(this);
    }
}
