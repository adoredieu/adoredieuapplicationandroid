package com.adoredieu.mobility.controllers;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.controllers.extensions.scroll.VerseListViewInfiniteScrollManager;
import com.adoredieu.mobility.core.dao.ReadingPlanEntriesDao;
import com.adoredieu.mobility.core.expansion.BibleExpansionPackageInfo;
import com.adoredieu.mobility.core.helpers.DialogsHelper;
import com.adoredieu.mobility.core.interfaces.MoveDirection;
import com.adoredieu.mobility.core.interfaces.VerseDefinition;
import com.adoredieu.mobility.core.model.bible.ReadingPlanEntry;
import com.adoredieu.mobility.core.persistence.UserPreferencesPersistence;
import com.adoredieu.mobility.core.services.BibleInstallationServices;
import com.adoredieu.mobility.core.services.BibleServices;
import com.adoredieu.mobility.core.services.HighlightedVersesServices;
import com.adoredieu.mobility.core.system.application.AdoreDieuApplication;
import com.adoredieu.mobility.view.adapters.ReadingPlanVersesArrayAdapter;
import com.adoredieu.mobility.view.fragments.ReadingPlanFragment;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

public class ReadingPlanController
        extends ReadingPlanFragment
{
    @Inject
    ReadingPlanEntriesDao readingPlanEntriesDao;
    @Inject
    UserPreferencesPersistence userPreferencesPersistence;
    @Inject
    GuiController guiController;
    @Inject
    BibleInstallationServices bibleInstallationServices;
    @Inject
    HighlightedVersesServices highlightedVersesServices;
    @Inject
    BibleServices bibleServices;

    Integer readingPlanCurrentDay;
    ViewHolder viewHolder;
    VerseListViewInfiniteScrollManager verseListViewInfiniteScrollManager;

    @Override
    public void synchronizeNavigationDrawer(NavigationView navigationView)
    {
        navigationView.setCheckedItem(R.id.nav_bible_reading_plan);
    }

    @Override
    public void onViewCreated(@NonNull View view,
                              Bundle savedInstanceState)
    {
        super.onViewCreated(view,
                            savedInstanceState);
        ((AdoreDieuApplication) getActivity().getApplication()).getInjector().inject(this);

        if (!bibleInstallationServices.isBibleInstalled(getActivity(),
                                                        BibleExpansionPackageInfo.getBibleXApkFile().fileVersion))
        {
            DialogsHelper.showAlert(getActivity(),
                                    getString(R.string.probleme_d_installation),
                                    getString(R.string.error_bible_not_installed),
                                    new View.OnClickListener()
                                    {
                                        @Override
                                        public void onClick(View v)
                                        {
                                            guiController.showHome(getActivity());
                                        }
                                    });
            return;
        }

        Integer readingPlanIndex = userPreferencesPersistence.getReadingPlanIndex();
        readingPlanCurrentDay = userPreferencesPersistence.getReadingPlanCurrentDay();

        long daysCount = readingPlanEntriesDao.getDaysCount(readingPlanIndex);

        if (readingPlanCurrentDay >= daysCount)
        {
            guiController.showHome(getActivity());
            return;
        }

        verseListViewInfiniteScrollManager = new VerseListViewInfiniteScrollManager()
        {
            @Override
            protected void onMouseDown(float downX,
                                       float downY)
            {

            }

            @Override
            public void onScrolling(MoveDirection moveDirection,
                                    int firstVisible,
                                    int visibleCount,
                                    int totalCount)
            {
                int lastItemIndex = firstVisible + visibleCount;

                if (lastItemIndex >= (totalCount - 1))
                {
                    userPreferencesPersistence.setReadingPlanCurrentDay(readingPlanCurrentDay + 1);
                }
            }

            @Override
            public void onMouseUp(MoveDirection moveDirection)
            {
            }
        };

        viewHolder = new ViewHolder();
        viewHolder.versesListView = view.findViewById(R.id.versesListView);
        viewHolder.versesListView.setChoiceMode(ListView.CHOICE_MODE_NONE);

        viewHolder.titleTextView = view.findViewById(R.id.titleTextView);

        viewHolder.previousDayButton = view.findViewById(R.id.previousDayButton);
        viewHolder.previousDayButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                userPreferencesPersistence.setReadingPlanCurrentDay(readingPlanCurrentDay - 1);

                Integer readingPlanIndex = userPreferencesPersistence.getReadingPlanIndex();
                showVerses(readingPlanIndex, userPreferencesPersistence.getReadingPlanCurrentDay());
            }
        });
        viewHolder.nextDayButton = view.findViewById(R.id.nextDayButton);
        viewHolder.nextDayButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                userPreferencesPersistence.setReadingPlanCurrentDay(readingPlanCurrentDay + 1);

                Integer readingPlanIndex = userPreferencesPersistence.getReadingPlanIndex();
                showVerses(readingPlanIndex, userPreferencesPersistence.getReadingPlanCurrentDay());
            }
        });

        showVerses(readingPlanIndex, readingPlanCurrentDay);
    }

    private void showVerses(Integer readingPlanIndex,
                            Integer readingPlanCurrentDay)
    {
        this.readingPlanCurrentDay = readingPlanCurrentDay;
        List<VerseDefinition> allVerses = new ArrayList<>();
        setTitle(readingPlanCurrentDay);

        List<ReadingPlanEntry> readingPlanEntries = readingPlanEntriesDao.getReadingPlanEntriesForDay(
                readingPlanIndex,
                readingPlanCurrentDay);

        for (int entryIndex = 0; entryIndex < readingPlanEntries.size(); entryIndex++)
        {
            ReadingPlanEntry readingPlanEntry = readingPlanEntries.get(entryIndex);
            List<VerseDefinition> chapter = getVersesOfEntry(readingPlanEntry);

            allVerses.addAll(chapter);
        }

        ReadingPlanVersesArrayAdapter arrayAdapter = new ReadingPlanVersesArrayAdapter(
                getActivity(),
                allVerses,
                allVerses.get(0)
                         .getBookId(),
                highlightedVersesServices,
                userPreferencesPersistence.getNightMode(),
                bibleServices);

        viewHolder.versesListView.setAdapter(arrayAdapter);
        viewHolder.versesListView.setOnScrollListener(verseListViewInfiniteScrollManager);
        viewHolder.versesListView.setOnTouchListener(verseListViewInfiniteScrollManager);

        viewHolder.previousDayButton.setEnabled(readingPlanCurrentDay > 1);
        long daysCount = readingPlanEntriesDao.getDaysCount(
                readingPlanIndex);
        viewHolder.nextDayButton.setEnabled(readingPlanCurrentDay < daysCount);

        if (readingPlanCurrentDay >= daysCount)
        {
            arrayAdapter.add(new VerseDefinition()
            {
                @Override
                public Integer getChapter()
                {
                    return 0;
                }

                @Override
                public Integer getVerse()
                {
                    return 0;
                }

                @Override
                public String getText()
                {
                    return getString(R.string.felicitations);
                }

                @Override
                public Integer getId()
                {
                    return 0;
                }

                @Override
                public Integer getBookId()
                {
                    return 0;
                }

                @Override
                public Integer getChapterNumber()
                {
                    return 0;
                }

                @Override
                public Integer getVerseNumber()
                {
                    return 0;
                }
            });
        }
    }

    private List<VerseDefinition> getVersesOfEntry(ReadingPlanEntry readingPlanEntry)
    {
        List<VerseDefinition> chapter = new ArrayList<>();

        if (readingPlanEntry.getStartChapter().equals(readingPlanEntry.getEndChapter())
            || readingPlanEntry.getEndChapter() == 0)
        {
            chapter = bibleServices.getChapter(readingPlanEntry.getBook(),
                                               readingPlanEntry.getStartChapter());

            chapter = resizeChapter(chapter,
                                    readingPlanEntry.getStartVerse(),
                                    readingPlanEntry.getEndVerse());
        }
        else
        {
            for (int chapterIndex = readingPlanEntry.getStartChapter();
                 chapterIndex <= readingPlanEntry.getEndChapter();
                 chapterIndex++)
            {
                List<VerseDefinition> currentChapter = bibleServices.getChapter(readingPlanEntry.getBook(),
                                                                                chapterIndex);

                if (chapterIndex == readingPlanEntry.getStartChapter()
                    && readingPlanEntry.getStartVerse() > 1)
                {
                    currentChapter = resizeChapter(currentChapter,
                                                   readingPlanEntry.getStartVerse(),
                                                   0);
                }

                if (chapterIndex == readingPlanEntry.getEndChapter()
                    && readingPlanEntry.getEndVerse() > 1)
                {
                    currentChapter = resizeChapter(currentChapter,
                                                   0,
                                                   readingPlanEntry.getEndVerse());
                }

                chapter.addAll(currentChapter);
            }
        }

        return chapter;
    }

    private List<VerseDefinition> resizeChapter(List<VerseDefinition> chapter,
                                                Integer startVerse,
                                                Integer endVerse)
    {
        if (endVerse != 0)
        {
            chapter = chapter.subList(0, endVerse);
        }

        if (startVerse > 1)
        {
            chapter = chapter.subList(startVerse - 1,
                                      chapter.size());
        }

        return chapter;
    }

    protected void setTitle(Integer readingPlanCurrentDay)
    {
        viewHolder.titleTextView.setText(getString(R.string.jour) + " " + readingPlanCurrentDay);
    }

    public static class ViewHolder
    {
        TextView titleTextView;
        StickyListHeadersListView versesListView;
        ImageButton previousDayButton;
        ImageButton nextDayButton;
    }
}
