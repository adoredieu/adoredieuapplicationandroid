package com.adoredieu.mobility.controllers;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.view.View;
import android.widget.AdapterView;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.core.dao.VerseDao;
import com.adoredieu.mobility.core.dto.HighlightedVerseDto;
import com.adoredieu.mobility.core.dto.VerseDto;
import com.adoredieu.mobility.core.interfaces.VerseDefinition;
import com.adoredieu.mobility.core.persistence.UserPreferencesPersistence;
import com.adoredieu.mobility.core.services.BibleServices;
import com.adoredieu.mobility.core.services.HighlightedVersesServices;
import com.adoredieu.mobility.core.system.application.AdoreDieuApplication;
import com.adoredieu.mobility.view.adapters.HighlightedVerseArrayAdapter;
import com.adoredieu.mobility.view.fragments.HighlightsListFragment;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

public class HighlightsListController
        extends HighlightsListFragment
        implements AdapterView.OnItemClickListener {
    @Inject
    HighlightedVersesServices highlightedVersesServices;
    @Inject
    VerseDao verseDao;
    @Inject
    UserPreferencesPersistence userPreferencesPersistence;
    @Inject
    GuiController guiController;
    @Inject
    BibleServices bibleServices;

    @Override
    public void onViewCreated(@NonNull View view,
                              Bundle savedInstanceState) {
        super.onViewCreated(view,
                savedInstanceState);
        ((AdoreDieuApplication) getActivity().getApplication()).getInjector().inject(this);

        StickyListHeadersListView highlightsListView = view.findViewById(
                R.id.highlightsListView);
        highlightsListView.setOnItemClickListener(this);

        List<HighlightedVerseDto> highlightedVerses = highlightedVersesServices.getHighlightedVersesCondensed();
        highlightsListView.setAdapter(new HighlightedVerseArrayAdapter(
                getActivity(),
                highlightedVerses,
                verseDao,
                userPreferencesPersistence.getNightMode(),
                bibleServices));
    }

    @Override
    public void synchronizeNavigationDrawer(NavigationView navigationView) {
        navigationView.setCheckedItem(R.id.nav_parameters);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView,
                            View view,
                            int position,
                            long id) {
        HighlightedVerseDto highlightedVerseDto = (HighlightedVerseDto) adapterView
                .getItemAtPosition(
                        position);

        // Enlever le mode strong
        userPreferencesPersistence.setStrongsMode(false);

        List<VerseDefinition> verseDefinitions = new ArrayList<>();
        verseDefinitions.add(VerseDto.fromVerse(verseDao.getVerse(highlightedVerseDto.book,
                highlightedVerseDto.chapter,
                highlightedVerseDto.verse)));

        guiController.showBibleController(getActivity(), verseDefinitions);
    }
}
