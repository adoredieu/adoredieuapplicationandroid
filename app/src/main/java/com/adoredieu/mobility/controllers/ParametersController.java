package com.adoredieu.mobility.controllers;

import android.annotation.SuppressLint;
import android.app.TimePickerDialog;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.core.helpers.LocalTimeHelper;
import com.adoredieu.mobility.core.helpers.NavigationDrawerHelper;
import com.adoredieu.mobility.core.helpers.WebHelper;
import com.adoredieu.mobility.core.interfaces.AdoreDieuActionResultListener;
import com.adoredieu.mobility.core.interfaces.FacebookActionResultListener;
import com.adoredieu.mobility.core.persistence.ApplicationContextPersistence;
import com.adoredieu.mobility.core.persistence.UserPreferencesPersistence;
import com.adoredieu.mobility.core.services.AdoreDieuServices;
import com.adoredieu.mobility.core.services.FacebookServices;
import com.adoredieu.mobility.core.system.application.AdoreDieuApplication;
import com.adoredieu.mobility.view.adapters.StringArrayAdapter;
import com.adoredieu.mobility.view.fragments.ParametersFragment;
import com.mikepenz.aboutlibraries.LibsBuilder;

import org.joda.time.LocalTime;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;


public class ParametersController
        extends ParametersFragment
        implements CompoundButton.OnCheckedChangeListener,
                   View.OnClickListener,
                   AdoreDieuActionResultListener,
                   FacebookActionResultListener,
                   AdapterView.OnItemSelectedListener
{
    @Inject
    ApplicationContextPersistence applicationContextPersistence;
    @Inject
    UserPreferencesPersistence userPreferencesPersistence;
    @Inject
    AdoreDieuServices adoreDieuServices;
    @Inject
    FacebookServices facebookServices;
    @Inject
    GuiController guiController;

    private ViewHolder viewHolder;

    @Override
    public void synchronizeNavigationDrawer(NavigationView navigationView)
    {
        navigationView.setCheckedItem(R.id.nav_parameters);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onViewCreated(@NonNull View view,
                              Bundle savedInstanceState)
    {
        super.onViewCreated(view,
                            savedInstanceState);
        ((AdoreDieuApplication) getActivity().getApplication()).getInjector().inject(this);

        viewHolder = new ViewHolder();

        CheckBox keepScreenOnCheckBox = view.findViewById(R.id.keepScreenOnCheckBox);
        keepScreenOnCheckBox.setChecked(userPreferencesPersistence.getKeepScreenOnPreference());
        keepScreenOnCheckBox.setOnCheckedChangeListener(this);

        CheckBox fullscreenCheckBox = view.findViewById(R.id.fullscreenCheckBox);
        fullscreenCheckBox.setChecked(userPreferencesPersistence.getFullscreenPreference());
        fullscreenCheckBox.setOnCheckedChangeListener(this);

        CheckBox dailyVerseCheckBox = view.findViewById(R.id.dailyVerseCheckBox);
        dailyVerseCheckBox.setChecked(userPreferencesPersistence.getDailyVerseNotificationEnabledPreference());
        dailyVerseCheckBox.setOnCheckedChangeListener(this);

        CheckBox citationCheckBox = view.findViewById(R.id.citationCheckBox);
        citationCheckBox.setChecked(userPreferencesPersistence.getQuoteOfTheDayNotificationEnabledPreference());
        citationCheckBox.setOnCheckedChangeListener(this);

        CheckBox citationImageCheckBox = view.findViewById(R.id.citationImageCheckBox);
        citationImageCheckBox.setChecked(userPreferencesPersistence.getImageQuoteOfTheDayNotificationEnabledPreference());
        citationImageCheckBox.setOnCheckedChangeListener(this);

        CheckBox thoughtsCheckBox = view.findViewById(R.id.thoughtsCheckBox);
        thoughtsCheckBox.setChecked(userPreferencesPersistence.getThoughtsNotificationEnabledPreference());
        thoughtsCheckBox.setOnCheckedChangeListener(this);

        CheckBox teachingsCheckBox = view.findViewById(R.id.teachingsCheckBox);
        teachingsCheckBox.setChecked(userPreferencesPersistence.getTeachingsNotificationEnabledPreference());
        teachingsCheckBox.setOnCheckedChangeListener(this);

        Button highlightListButton = view.findViewById(R.id.highlightListButton);
        highlightListButton.setOnClickListener(this);

        Button readingPlanResetButton = view.findViewById(R.id.readingPlanResetButton);
        readingPlanResetButton.setOnClickListener(this);

        final TextView startTextView = view.findViewById(R.id.startTextView);
        startTextView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                LocalTime notificationStartTime = userPreferencesPersistence.getNotificationQuietStartTime();
                TimePickerDialog timePickerDialog = new TimePickerDialog(
                        getActivity(),
                        new TimePickerDialog.OnTimeSetListener()
                        {
                            @Override
                            public void onTimeSet(TimePicker timePicker,
                                                  int hourOfDay,
                                                  int minute)
                            {
                                LocalTime localTime = new LocalTime(hourOfDay,
                                                                    minute);
                                userPreferencesPersistence.setNotificationQuietStartTime(
                                        localTime);

                                setTimeTextViewText(startTextView,
                                                    localTime);
                            }
                        },
                        notificationStartTime.getHourOfDay(),
                        notificationStartTime.getMinuteOfHour(),
                        true);
                timePickerDialog.show();
            }
        });
        LocalTime notificationStartTime = userPreferencesPersistence.getNotificationQuietStartTime();
        setTimeTextViewText(startTextView,
                            notificationStartTime);

        final TextView endTextView = view.findViewById(R.id.endTextView);
        endTextView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                LocalTime notificationEndTime = userPreferencesPersistence.getNotificationQuietEndTime();
                TimePickerDialog timePickerDialog = new TimePickerDialog(
                        getActivity(),
                        new TimePickerDialog.OnTimeSetListener()
                        {
                            @Override
                            public void onTimeSet(TimePicker timePicker,
                                                  int hourOfDay,
                                                  int minute)
                            {
                                LocalTime localTime = new LocalTime(hourOfDay,
                                                                    minute);
                                userPreferencesPersistence.setNotificationQuietEndTime(
                                        localTime);

                                setTimeTextViewText(endTextView,
                                                    localTime);
                            }
                        },
                        notificationEndTime.getHourOfDay(),
                        notificationEndTime.getMinuteOfHour(),
                        true);
                timePickerDialog.show();
            }
        });
        LocalTime notificationEndTime = userPreferencesPersistence.getNotificationQuietEndTime();
        setTimeTextViewText(endTextView,
                            notificationEndTime);

        viewHolder.loginAdoreDieuButton = view.findViewById(R.id.loginAdoreDieuButton);
        viewHolder.loginAdoreDieuButton.setOnClickListener(this);

        viewHolder.loginFacebookButton = view.findViewById(R.id.loginFacebookButton);
        viewHolder.loginFacebookButton.setOnClickListener(this);

        viewHolder.maxSearchResultsSpinner = view.findViewById(R.id.maxSearchResultsSpinner);
        Integer maxSearchResults = userPreferencesPersistence.getMaxSearchResults();

        List<String> max_search_results_list = Arrays.asList(getResources().getStringArray(R.array.max_search_results_list));
        viewHolder.maxSearchResultsSpinner.setAdapter(new StringArrayAdapter(getActivity(),
                                                                             R.layout.spi_simple,
                                                                             max_search_results_list));
        viewHolder.maxSearchResultsSpinner.setOnItemSelectedListener(this);

        if (maxSearchResults == 0)
        {
            viewHolder.maxSearchResultsSpinner.setSelection(viewHolder.maxSearchResultsSpinner.getCount() - 1);
        }
        else
        {
            for (int i = 0; i < max_search_results_list.size(); i++)
            {
                if (max_search_results_list.get(i)
                                           .equals(maxSearchResults.toString()))
                {
                    viewHolder.maxSearchResultsSpinner.setSelection(i);
                    break;
                }
            }
        }

        TextView confidentialityPoliticsTextView = view.findViewById(R.id.confidentialityPoliticsTextView);
        SpannableString confidentialityPoliticsTextViewContent = new SpannableString(
                confidentialityPoliticsTextView
                        .getText());
        confidentialityPoliticsTextViewContent.setSpan(new UnderlineSpan(),
                                                       0,
                                                       confidentialityPoliticsTextViewContent.length(),
                                                       0);
        confidentialityPoliticsTextView.setText(confidentialityPoliticsTextViewContent);
        confidentialityPoliticsTextView.setOnClickListener(this);

        TextView appVersionTextView = view.findViewById(R.id.appVersionTextView);
        String versionText = "";
        try
        {
            PackageInfo packageInfo = getActivity().getPackageManager()
                                                   .getPackageInfo(getActivity().getPackageName(),
                                                                   0);
            versionText = "v" + packageInfo.versionName;
        }
        catch (PackageManager.NameNotFoundException e)
        {
            e.printStackTrace();
        }

        SpannableString appVersionTextViewContent = new SpannableString(versionText);
        appVersionTextViewContent.setSpan(new UnderlineSpan(),
                                          0,
                                          appVersionTextViewContent.length(),
                                          0);
        appVersionTextView.setText(appVersionTextViewContent);
        appVersionTextView.setOnClickListener(this);

        refreshLoginButton();
    }

    private void setTimeTextViewText(TextView textView,
                                     LocalTime localTime)
    {
        String localTimeString = LocalTimeHelper.toShortString(localTime);
        SpannableString content = new SpannableString(localTimeString);
        content.setSpan(new UnderlineSpan(),
                        0,
                        localTimeString.length(),
                        0);
        textView.setText(content);
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton,
                                 boolean checked)
    {
        switch (compoundButton.getId())
        {
            case R.id.fullscreenCheckBox:
                userPreferencesPersistence.setFullscreenPreference(checked);

                if (userPreferencesPersistence.getFullscreenPreference())
                {
                    getActivity().getWindow()
                                 .addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
                }
                else
                {
                    getActivity().getWindow()
                                 .clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
                }
                break;
            case R.id.keepScreenOnCheckBox:
                userPreferencesPersistence.setKeepScreenOnPreference(checked);
                break;
            case R.id.citationCheckBox:
                userPreferencesPersistence.setQuoteOfTheDayNotificationEnabledPreference(
                        checked);
                break;
            case R.id.citationImageCheckBox:
                userPreferencesPersistence.setImageQuoteOfTheDayNotificationEnabledPreference(
                        checked);
                break;
            case R.id.dailyVerseCheckBox:
                userPreferencesPersistence.setDailyVerseNotificationEnabledPreference(checked);
                break;
            case R.id.thoughtsCheckBox:
                userPreferencesPersistence.setThoughtsNotificationEnabledPreference(checked);
                break;
            case R.id.teachingsCheckBox:
                userPreferencesPersistence.setTeachingsNotificationEnabledPreference(checked);
                break;
        }
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.loginFacebookButton:
                if (facebookServices.isLoggedIn())
                {
                    facebookServices.logout(this);
                }
                else
                {
                    facebookServices.login(this);
                }
                break;
            case R.id.loginAdoreDieuButton:
                if (!adoreDieuServices.isLoggedIn())
                {
                    adoreDieuServices.login(getActivity(),
                                            this);
                }
                else
                {
                    adoreDieuServices.logout(this);
                }
                break;
            case R.id.confidentialityPoliticsTextView:
                WebHelper.openUrlInBrowser(getActivity(),
                                           "https://www.adoredieu.com/license_2017-03-02.html");
                break;
            case R.id.appVersionTextView:
                new LibsBuilder()
                        //Pass the fields of your application to the lib so it can find all external lib information
                        .withFields(R.string.class.getFields())
                        .withAboutIconShown(true)
                        .withAboutVersionShown(true)
                        .start(getActivity());
                break;
            case R.id.readingPlanResetButton:
                userPreferencesPersistence.setReadingPlanIndex(0);
                userPreferencesPersistence.setReadingPlanCurrentDay(1);

                guiController.showReadingPlanChoiceController(getActivity());
                break;
            case R.id.highlightListButton:
                guiController.showHighlightListController(getActivity());
                break;
        }
    }

    private void refreshLoginButton()
    {
        getActivity().runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
                NavigationDrawerHelper.refreshUsername(getActivity(),
                                                       applicationContextPersistence);

                if (adoreDieuServices.isLoggedIn())
                {
                    viewHolder.loginAdoreDieuButton.setText(getString(R.string.deconnexion));
                }
                else
                {
                    viewHolder.loginAdoreDieuButton.setText(getString(R.string.connexion));
                }

                if (facebookServices.isLoggedIn())
                {
                    viewHolder.loginFacebookButton.setText(getString(R.string.deconnexion));
                }
                else
                {
                    viewHolder.loginFacebookButton.setText(getString(R.string.connexion));
                }
            }
        });
    }

    @Override
    public void onFacebookLoginSuccess()
    {
        refreshLoginButton();
    }

    @Override
    public void onFacebookLoginFail(String message)
    {
        Toast.makeText(getActivity(),
                       message,
                       Toast.LENGTH_SHORT)
             .show();
        refreshLoginButton();
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView,
                               View view,
                               int position,
                               long id)
    {
        String selectedItem = viewHolder.maxSearchResultsSpinner.getSelectedItem()
                                                                .toString();

        int maxSearchResults = 0;
        try
        {
            maxSearchResults = Integer.parseInt(selectedItem);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        userPreferencesPersistence.setMaxSearchResults(maxSearchResults);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView)
    {

    }

    @Override
    public void onAdoreDieuLoginSuccess()
    {
        refreshLoginButton();
    }

    public class ViewHolder
    {
        Button loginAdoreDieuButton;
        Button loginFacebookButton;
        Spinner maxSearchResultsSpinner;
    }
}
