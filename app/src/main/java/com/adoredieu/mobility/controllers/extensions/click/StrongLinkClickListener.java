package com.adoredieu.mobility.controllers.extensions.click;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.text.method.LinkMovementMethod;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.controllers.BibleStrongSearchController;
import com.adoredieu.mobility.controllers.GuiController;
import com.adoredieu.mobility.core.dao.GreekDao;
import com.adoredieu.mobility.core.dao.HebrewDao;
import com.adoredieu.mobility.core.dto.StrongDto;
import com.adoredieu.mobility.core.helpers.StrongsHelper;
import com.adoredieu.mobility.core.system.mmi.ClickSpan;

import java.util.ArrayList;
import java.util.List;

public class StrongLinkClickListener
        implements ClickSpan.OnClickListener,
        View.OnClickListener {
    private final GuiController guiController;
    private final StrongsHelper strongsHelper;
    private final GreekDao greekDao;
    private final HebrewDao hebrewDao;
    private final FragmentActivity activity;
    private PopupWindow popupWindow;
    private final List<String> strongClickStack;

    public StrongLinkClickListener(FragmentActivity activity,
                                   GuiController guiController,
                                   StrongsHelper strongsHelper,
                                   GreekDao greekDao,
                                   HebrewDao hebrewDao) {
        this.activity = activity;
        this.guiController = guiController;
        this.strongsHelper = strongsHelper;
        this.greekDao = greekDao;
        this.hebrewDao = hebrewDao;

        strongClickStack = new ArrayList<>();
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onClick(View view,
                        final String url) {
        try {
            strongClickStack.add(url);

            String[] split = url.split("#");
            String strongType = split[0];
            final int strongCode = Integer.parseInt(split[1]);
            final BibleStrongSearchController.StrongLanguage strongLanguage;

            StrongDto strong;
            switch (strongType) {
                case "H":
                    strong = hebrewDao.getStrong(strongCode);
                    strongLanguage = BibleStrongSearchController.StrongLanguage.Hebrew;
                    break;
                case "G":
                    strong = greekDao.getStrong(strongCode);
                    strongLanguage = BibleStrongSearchController.StrongLanguage.Greek;
                    break;
                default:
                    return;
            }

            if (popupWindow == null) {
                LayoutInflater layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View popupView = layoutInflater.inflate(R.layout.popup_strong_details,
                        null);
                popupWindow = new PopupWindow(
                        popupView,
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT);

                popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
                    @Override
                    public void onDismiss() {
                        strongClickStack.clear();
                    }
                });
            }

            ViewGroup layout = popupWindow.getContentView().findViewById(R.id.layout);
            layout.setOnClickListener(this);

            TextView wordTextView = popupWindow.getContentView()
                    .findViewById(R.id.wordTextView);
            wordTextView.setText(strongsHelper.formatTextWithStrongs(strong.getWord(),
                    true,
                    this));
            wordTextView.setMovementMethod(LinkMovementMethod.getInstance());

            TextView originalWordTextView = popupWindow.getContentView()
                    .findViewById(R.id.originalWordTextView);
            originalWordTextView.setText("(" + strong.getOriginalWord() + ")");
            originalWordTextView.setVisibility(strong.getOriginalWord().isEmpty() ?
                    View.GONE : View.VISIBLE);

            TextView strongNumberTextView = popupWindow.getContentView()
                    .findViewById(R.id.strongNumberTextView);
            strongNumberTextView.setText("" + strong.getCode());

            TextView originTextView = popupWindow.getContentView()
                    .findViewById(R.id.originTextView);
            originTextView.setText(strongsHelper.formatTextWithStrongs(strong.getOrigin(),
                    true,
                    this));
            originTextView.setMovementMethod(LinkMovementMethod.getInstance());

            ViewGroup originLayout = popupWindow.getContentView()
                    .findViewById(R.id.originLayout);
            originLayout.setVisibility(strong.getOrigin().isEmpty() ?
                    View.GONE : View.VISIBLE);

            TextView typeTextView = popupWindow.getContentView()
                    .findViewById(R.id.typeTextView);
            typeTextView.setText(strong.getType());
            ViewGroup typeLayout = popupWindow.getContentView()
                    .findViewById(R.id.typeLayout);
            typeLayout.setVisibility(strong.getType().isEmpty() ?
                    View.GONE : View.VISIBLE);

            TextView definitionTextView = popupWindow.getContentView()
                    .findViewById(R.id.definitionTextView);
            definitionTextView.setText(strongsHelper.formatTextWithStrongs(strong.getDefinition(),
                    true,
                    this));
            definitionTextView.setMovementMethod(LinkMovementMethod.getInstance());

            ImageButton strongNumberSearchImageButton = popupWindow.getContentView()
                    .findViewById(R.id.strongNumberSearchImageButton);
            strongNumberSearchImageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dismissPopup();

                    guiController.showBibleStrongSearchController(activity,
                            strongLanguage,
                            strongCode);
                }
            });

            if (!isPopupDisplayed()) {
                popupWindow.showAtLocation(view,
                        Gravity.CENTER,
                        0,
                        0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onClick(View v) {
        dismissPopup();
    }

    public void dismissPopup() {
        if (popupWindow != null) {
            popupWindow.dismiss();
        }
    }

    public List<String> getStrongClickStack() {
        return strongClickStack;
    }

    public boolean isPopupDisplayed() {
        return popupWindow != null && popupWindow.isShowing();
    }
}
