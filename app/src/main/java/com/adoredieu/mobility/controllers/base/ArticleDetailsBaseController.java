package com.adoredieu.mobility.controllers.base;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.util.TypedValue;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.controllers.GuiController;
import com.adoredieu.mobility.core.helpers.DialogsHelper;
import com.adoredieu.mobility.core.helpers.StringHelper;
import com.adoredieu.mobility.core.interfaces.Action;
import com.adoredieu.mobility.core.model.website.Article;
import com.adoredieu.mobility.core.persistence.UserPreferencesPersistence;
import com.adoredieu.mobility.core.services.ArticleServices;
import com.adoredieu.mobility.core.services.FavouriteServices;
import com.adoredieu.mobility.core.services.ShareServices;
import com.adoredieu.mobility.core.system.application.AdoreDieuApplication;
import com.adoredieu.mobility.view.fragments.ArticleDetailsFragment;
import com.bumptech.glide.Glide;

import javax.inject.Inject;

public abstract class ArticleDetailsBaseController
        extends ArticleDetailsFragment
        implements View.OnClickListener {
    protected Article article;
    @Inject
    UserPreferencesPersistence userPreferencesPersistence;
    @Inject
    ShareServices shareServices;
    @Inject
    ArticleServices articleServices;
    @Inject
    FavouriteServices favouriteServices;
    @Inject
    GuiController guiController;

    private int currentFontSize;
    private ViewHolder viewHolder;

    @Override
    @SuppressLint("SetJavaScriptEnabled")
    @CallSuper
    public void onViewCreated(@NonNull View view,
                              Bundle savedInstanceState) {
        super.onViewCreated(view,
                savedInstanceState);
        ((AdoreDieuApplication) getActivity().getApplication()).getInjector().inject(this);

        if (article == null) {
            guiController.showHome(getActivity());
            return;
        }

        viewHolder = new ViewHolder();

        currentFontSize = userPreferencesPersistence.getWebViewFontSize();

        viewHolder.articleThumbnailImageView = view.findViewById(R.id.articleThumbnailImageView);

        if (!article.thumbnailUri.isEmpty()) {
            Glide.with(getActivity())
                    .load(article.thumbnailUri)
                    .into(viewHolder.articleThumbnailImageView);
        } else {
            viewHolder.articleThumbnailImageView.setImageDrawable(null);
        }

        viewHolder.articleTitleTextView = view.findViewById(R.id.articleTitleTextView);
        viewHolder.articleTitleTextView.setText(article.title);

        viewHolder.shareButton = view.findViewById(R.id.shareButton);
        viewHolder.shareButton.setOnClickListener(this);

        viewHolder.articleDetailsWebView = view.findViewById(R.id.articleDetailsWebView);
        viewHolder.articleDetailsWebView.getSettings()
                .setJavaScriptEnabled(true);
        viewHolder.articleDetailsWebView.getSettings()
                .setLoadsImagesAutomatically(true);
        viewHolder.articleDetailsWebView.setBackgroundColor(0x00000000);
        viewHolder.articleDetailsWebView.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view,
                                       String url) {
                DialogsHelper.hideProgressDialog();
            }
        });


        ImageButton zoomOutButton = view.findViewById(R.id.zoomOutButton);
        zoomOutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentFontSize -= 2;
                if (currentFontSize < 2) {
                    currentFontSize = 2;
                }

                userPreferencesPersistence.setWebViewFontSize(currentFontSize);
                ArticleDetailsBaseController.this.setWebViewFontSize(currentFontSize);
            }
        });

        ImageButton zoomInButton = view.findViewById(R.id.zoomInButton);
        zoomInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentFontSize += 2;
                if (currentFontSize > 72) {
                    currentFontSize = 72;
                }

                userPreferencesPersistence.setWebViewFontSize(currentFontSize);
                ArticleDetailsBaseController.this.setWebViewFontSize(currentFontSize);
            }
        });

        viewHolder.favouriteButton = view.findViewById(R.id.favouriteButton);
        viewHolder.favouriteButton.setOnClickListener(this);
        //viewHolder.favouriteButton.setBackgroundResource(favouriteServices.isFavourite(article) ? R.drawable.ic_favourite_on : R.drawable.ic_favourite_off);

        setWebViewFontSize(currentFontSize);

        DialogsHelper.showProgressIndeterminate(getActivity(),
                getString(R.string.veuillez_patienter),
                getString(R.string.chargement_de_l_article));

        articleServices.getArticleContent(
                article.detailsUri,
                new Action<ArticleServices.ArticleContent>() {
                    @Override
                    public void run(ArticleServices.ArticleContent object) {
                        String articleContent = object.getContent();
                        String facebookLikeUri = object.getContent();

                        ArticleDetailsBaseController.this.showArticleContent(articleContent,
                                facebookLikeUri);

                        DialogsHelper.hideProgressDialog();
                    }
                },
                new Runnable() {
                    @Override
                    public void run() {
                        DialogsHelper.hideProgressDialog();
                    }
                });
    }

    String savedCleanArticleContent = "";

    private void showArticleContent(String articleContent,
                                    String facebookLikeUri) {
        savedCleanArticleContent = StringHelper.stripHtml(articleContent);

        if (userPreferencesPersistence.getNightMode()) {
            TypedValue typedValue = new TypedValue();
            Resources.Theme theme = getActivity().getTheme();
            theme.resolveAttribute(R.attr.textColorInverted,
                    typedValue,
                    true);
            String color = typedValue.coerceToString()
                    .toString();
            color = color.substring(3);
            articleContent = "<span style=\"color:#" + color + ";\">" + articleContent + "</span>";
        }

        viewHolder.articleDetailsWebView.loadDataWithBaseURL(
                article.detailsUri,
                articleContent,
                "text/html",
                "UTF-8",
                article.detailsUri);
    }

    private void setWebViewFontSize(int fontSize) {
        viewHolder.articleDetailsWebView.getSettings()
                .setDefaultFontSize(fontSize);
        viewHolder.articleDetailsWebView.getSettings()
                .setDefaultFixedFontSize(fontSize);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.shareButton:
                shareServices.share(getActivity(), savedCleanArticleContent);
                break;
            case R.id.favouriteButton:
                favouriteServices.toggleFavourite(
                        article,
                        new Runnable() {
                            @Override
                            public void run() {
                            }
                        },
                        new Runnable() {
                            @Override
                            public void run() {
                            }
                        });
                break;
        }
    }

    public ArticleDetailsBaseController withArticle(Article article) {
        this.article = article;
        return this;
    }

    private class ViewHolder {
        ImageView articleThumbnailImageView;
        TextView articleTitleTextView;
        WebView articleDetailsWebView;
        Button shareButton;
        ImageButton favouriteButton;
    }
}
