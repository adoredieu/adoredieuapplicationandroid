package com.adoredieu.mobility.controllers;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.util.SparseBooleanArray;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.controllers.extensions.click.StrongLinkClickListener;
import com.adoredieu.mobility.controllers.extensions.scroll.VerseListViewInfiniteScrollManager;
import com.adoredieu.mobility.core.dao.GreekDao;
import com.adoredieu.mobility.core.dao.HebrewDao;
import com.adoredieu.mobility.core.dao.VersesHighlightsDao;
import com.adoredieu.mobility.core.expansion.BibleExpansionPackageInfo;
import com.adoredieu.mobility.core.helpers.DialogsHelper;
import com.adoredieu.mobility.core.helpers.FragmentsHelper;
import com.adoredieu.mobility.core.helpers.StrongsHelper;
import com.adoredieu.mobility.core.interfaces.BiblePosition;
import com.adoredieu.mobility.core.interfaces.MoveDirection;
import com.adoredieu.mobility.core.interfaces.VerseDefinition;
import com.adoredieu.mobility.core.persistence.ApplicationContextPersistence;
import com.adoredieu.mobility.core.persistence.UserPreferencesPersistence;
import com.adoredieu.mobility.core.services.BibleInstallationServices;
import com.adoredieu.mobility.core.services.BibleServices;
import com.adoredieu.mobility.core.services.BibleStrongServices;
import com.adoredieu.mobility.core.services.FacebookServices;
import com.adoredieu.mobility.core.services.HighlightedVersesServices;
import com.adoredieu.mobility.core.system.application.AdoreDieuApplication;
import com.adoredieu.mobility.view.adapters.VersesStrongsArrayAdapter;
import com.adoredieu.mobility.view.fragments.BibleFragment;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

import static com.adoredieu.mobility.core.helpers.VersesHelper.getCheckedVerses;

public class BibleStrongController
        extends BibleFragment
        implements View.OnClickListener {
    protected ViewHolder viewHolder;
    protected VerseListViewInfiniteScrollManager verseListViewInfiniteScrollManager;
    protected List<VerseDefinition> selectedVerses = new ArrayList<>();

    @Inject
    HighlightedVersesServices highlightedVersesServices;
    @Inject
    GreekDao greekDao;
    @Inject
    HebrewDao hebrewDao;
    @Inject
    StrongsHelper strongsHelper;
    @Inject
    VersesHighlightsDao versesHighlightsDao;
    @Inject
    BibleInstallationServices bibleInstallationServices;
    @Inject
    FacebookServices facebookServices;
    @Inject
    GuiController guiController;
    @Inject
    BibleStrongServices bibleStrongServices;
    @Inject
    BibleServices bibleServices;
    @Inject
    ApplicationContextPersistence applicationContextPersistence;
    @Inject
    UserPreferencesPersistence userPreferencesPersistence;
    private StrongLinkClickListener strongLinkClickListener;

    @SuppressLint("NewApi")
    @Override
    public void onViewCreated(@NonNull View view,
                              Bundle savedInstanceState) {
        super.onViewCreated(view,
                savedInstanceState);
        ((AdoreDieuApplication) getActivity().getApplication()).getInjector().inject(this);

        viewHolder = new ViewHolder();

        // Back management
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v,
                                 int keyCode,
                                 KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK
                        && event.getAction() == KeyEvent.ACTION_UP) {
                    if (strongLinkClickListener != null) {
                        List<String> strongClickStack = strongLinkClickListener.getStrongClickStack();

                        if (strongClickStack.size() > 1) {
                            strongClickStack.remove(strongClickStack.size() - 1);
                            String lastElement = strongClickStack.get(strongClickStack.size() - 1);
                            strongClickStack.remove(strongClickStack.size() - 1);

                            strongLinkClickListener.onClick(null, lastElement);

                            return true;
                        } else if (strongLinkClickListener.isPopupDisplayed()) {
                            strongLinkClickListener.dismissPopup();
                            return true;
                        }
                    }

                    return false;
                }

                return false;
            }
        });

        if (!bibleInstallationServices.isBibleInstalled(getActivity(),
                BibleExpansionPackageInfo.getBibleXApkFile().fileVersion)) {
            DialogsHelper.showAlert(getActivity(),
                    getString(R.string.probleme_d_installation),
                    getString(R.string.error_bible_not_installed),
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            guiController.showHome(getActivity());
                        }
                    });
            return;
        }

        final List<VerseDefinition> verses;
        BiblePosition position = applicationContextPersistence.getLastBiblePosition();
        if (position == null) {
            position = new BiblePosition(1, 1, 1);
        }

        if (selectedVerses.size() > 0) {
            verses = bibleStrongServices.getChapter(selectedVerses.get(0).getBookId(),
                    selectedVerses.get(0).getChapterNumber());
        } else {
            verses = bibleStrongServices.getChapter(position.book,
                    position.chapter);
        }

        strongLinkClickListener = new StrongLinkClickListener(getActivity(),
                guiController,
                strongsHelper,
                greekDao,
                hebrewDao);
        VersesStrongsArrayAdapter arrayAdapter = new VersesStrongsArrayAdapter(
                getActivity(),
                verses,
                strongsHelper,
                verses.get(0)
                        .getBookId(),
                highlightedVersesServices,
                userPreferencesPersistence
                        .getNightMode(),
                strongLinkClickListener);

        viewHolder.titleTextView = view.findViewById(R.id.titleTextView);

        if (verses.size() > 0) {
            setTitle(bibleServices.getLocalizedBooksNames().get(verses.get(0).getBookId()));
        }

        viewHolder.versesListView = view.findViewById(R.id.versesListView);
        viewHolder.versesArrayAdapter = arrayAdapter;

        verseListViewInfiniteScrollManager = new VerseListViewInfiniteScrollManager() {
            @Override
            protected void onMouseDown(float downX,
                                       float downY) {

            }

            @Override
            public void onScrolling(MoveDirection moveDirection,
                                    int firstVisible,
                                    int visibleCount,
                                    int totalCount) {
                if (moveDirection == MoveDirection.None) {
                    return;
                }

                switch (moveDirection) {
                    case North:
                        int lastItemIndex = firstVisible + visibleCount;

                        if (lastItemIndex >= totalCount) {
                            addVersesToBottom(lastItemIndex);
                        }
                        break;
                    case South:

                        if (firstVisible <= 0) {
                            addVerseToTop(viewHolder.versesListView,
                                    firstVisible);
                        }
                        break;
                }

            }

            @Override
            public void onMouseUp(MoveDirection moveDirection) {
            }
        };

        viewHolder.versesListView.setAdapter(arrayAdapter);
        viewHolder.versesListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        viewHolder.versesListView.setOnScrollListener(verseListViewInfiniteScrollManager);
        viewHolder.versesListView.setOnTouchListener(verseListViewInfiniteScrollManager);

        if (selectedVerses.size() > 0) {
            for (int i = 0; i < selectedVerses.size(); i++) {
                viewHolder.versesListView.setItemChecked(selectedVerses.get(i).getVerseNumber() - 1,
                        true);
            }
            viewHolder.versesArrayAdapter.notifyDataSetChanged();
        }

        final BiblePosition finalPosition = position;
        viewHolder.versesListView.post(new Runnable() {
            @Override
            public void run() {
                if (selectedVerses.size() > 0) {
                    viewHolder.versesListView.smoothScrollToPositionFromTop(selectedVerses.get(0).getVerseNumber() - 1,
                            0,
                            0);
                    viewHolder.versesListView.setSelection(selectedVerses.get(0).getVerseNumber() - 1);
                } else {
                    viewHolder.versesListView.smoothScrollToPositionFromTop((int) (finalPosition.verse - 1),
                            0,
                            0);
                    viewHolder.versesListView.setSelection((int) (finalPosition.verse - 1));
                }
            }
        });

        ImageButton strongsButton = view.findViewById(R.id.strongsButton);
        strongsButton.setImageDrawable(userPreferencesPersistence.getStrongsMode() ?
                ContextCompat.getDrawable(getActivity(),
                        R.drawable.ic_strongs_selected)
                : ContextCompat.getDrawable(getActivity(),
                R.drawable.ic_strongs_normal));
        strongsButton.setOnClickListener(this);

        ImageButton searchButton = view.findViewById(R.id.searchButton);
        searchButton.setOnClickListener(this);

        ImageButton jumpButton = view.findViewById(R.id.jumpButton);
        jumpButton.setOnClickListener(this);
    }

    protected void setTitle(String bookName) {
        viewHolder.titleTextView.setText(bookName);
    }

    public BibleStrongController withSelectedVerses(@NonNull List<VerseDefinition> selectedVerses) {
        this.selectedVerses = selectedVerses;
        return this;
    }

    protected void addVerseToTop(StickyListHeadersListView listView,
                                 int firstVisible) {
        VerseDefinition firstVisibleVerse = viewHolder.versesArrayAdapter.getItem(firstVisible);
        if (firstVisibleVerse.getChapter() <= 1) {
            return;
        }

        List<VerseDefinition> verses = bibleStrongServices.getChapter(
                firstVisibleVerse.getBookId(),
                firstVisibleVerse.getChapter() - 1);

        if (verses.size() > 0) {
            addOffsetToCheckedItems(verses.size());

            // save index and top position
            View v = listView.getChildAt(0);
            int top = (v == null) ? 0 : v.getTop();

            viewHolder.versesArrayAdapter.insertAll(verses);
            viewHolder.versesArrayAdapter.notifyDataSetChanged();

            // restore index and position
            viewHolder.versesListView.setSelectionFromTop(firstVisible + verses.size(),
                    top + verses.size());
        }
    }

    protected void addVersesToBottom(int lastItemIndex) {
        VerseDefinition lastVerse = viewHolder.versesArrayAdapter.getItem(lastItemIndex - 1);

        List<VerseDefinition> verses = bibleStrongServices.getChapter(lastVerse.getBookId(),
                lastVerse.getChapter() + 1);

        if (verses.size() > 0) {
            viewHolder.versesArrayAdapter.addAll(verses);
            viewHolder.versesArrayAdapter.notifyDataSetChanged();
        }
    }

    private void addOffsetToCheckedItems(int offset) {
        SparseBooleanArray oldCheckedItemPositions = viewHolder.versesListView.getCheckedItemPositions()
                .clone();
        for (int i = 0; i < oldCheckedItemPositions.size(); i++) {
            if (oldCheckedItemPositions.valueAt(i)) {
                viewHolder.versesListView.setItemChecked(oldCheckedItemPositions.keyAt(i),
                        false);
                viewHolder.versesListView.setItemChecked(oldCheckedItemPositions.keyAt(i) + offset,
                        true);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (userPreferencesPersistence.getKeepScreenOnPreference()) {
            getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }

        if (verseListViewInfiniteScrollManager != null) {
            verseListViewInfiniteScrollManager.reinit();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.strongsButton:
                userPreferencesPersistence.setStrongsMode(!userPreferencesPersistence.getStrongsMode());

                List<VerseDefinition> checkedVerses = getCheckedVerses(viewHolder.versesListView, viewHolder.versesArrayAdapter);

                FragmentsHelper.popBackStack(getActivity(),
                        1);

                if (userPreferencesPersistence.getStrongsMode()) {
                    guiController.showBibleStrongController(getActivity(),
                            checkedVerses);
                } else {
                    guiController.showBibleController(getActivity(),
                            checkedVerses);
                }
                break;
            case R.id.jumpButton:
                guiController.showBibleBookChoiceController(getActivity());
                break;
            case R.id.searchButton:
                guiController.showBibleSearchController(getActivity());
                break;
        }
    }

    private void savePosition() {
        try {
            int firstVisiblePosition = viewHolder.versesListView.getFirstVisiblePosition();

            VerseDefinition verse = (VerseDefinition) viewHolder.versesListView.getAdapter()
                    .getItem(
                            firstVisiblePosition);

            BiblePosition position = new BiblePosition(verse.getBookId(),
                    verse.getChapter(),
                    verse.getVerse());
            applicationContextPersistence.setLastBiblePosition(position);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void synchronizeNavigationDrawer(NavigationView navigationView) {
        navigationView.setCheckedItem(R.id.nav_bible);
    }

    @Override
    public void onPause() {
        super.onPause();

        getActivity().getWindow()
                .clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        savePosition();

        if (strongLinkClickListener != null && strongLinkClickListener.isPopupDisplayed()) {
            strongLinkClickListener.dismissPopup();
        }
    }

    public class ViewHolder {
        public TextView titleTextView;
        public StickyListHeadersListView versesListView;
        public VersesStrongsArrayAdapter versesArrayAdapter;
    }
}
