package com.adoredieu.mobility.controllers.extensions.click;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.util.SparseBooleanArray;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupWindow;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.controllers.BibleController;
import com.adoredieu.mobility.core.dao.VersesHighlightsDao;
import com.adoredieu.mobility.core.helpers.VersesHelper;
import com.adoredieu.mobility.core.helpers.WidgetHelper;
import com.adoredieu.mobility.core.interfaces.BibleVerseWidgetContent;
import com.adoredieu.mobility.core.interfaces.VerseDefinition;
import com.adoredieu.mobility.core.persistence.ApplicationContextPersistence;
import com.adoredieu.mobility.core.persistence.UserPreferencesPersistence;
import com.adoredieu.mobility.core.services.BibleServices;
import com.adoredieu.mobility.core.services.ClipboardServices;
import com.adoredieu.mobility.core.services.ShareServices;

import java.util.List;

import static com.adoredieu.mobility.core.helpers.VersesHelper.getCheckedVerses;

public class VerseItemClickListener
        implements AdapterView.OnItemLongClickListener,
        AdapterView.OnItemClickListener,
        View.OnClickListener {
    private final ShareServices shareServices;
    private final ApplicationContextPersistence applicationContextPersistence;
    private final UserPreferencesPersistence userPreferencesPersistence;
    private final VersesHighlightsDao highlightedVersesServices;
    private final BibleServices bibleServices;
    private final FragmentActivity activity;
    private final BibleController.ViewHolder viewHolder;
    private final ClipboardServices clipboardServices;
    private PopupWindow popupWindow;

    public VerseItemClickListener(FragmentActivity activity,
                                  BibleController.ViewHolder viewHolder,
                                  ShareServices shareServices,
                                  ClipboardServices clipboardServices,
                                  ApplicationContextPersistence applicationContextPersistence,
                                  UserPreferencesPersistence userPreferencesPersistence,
                                  VersesHighlightsDao highlightedVersesServices,
                                  BibleServices bibleServices) {
        this.activity = activity;

        this.viewHolder = viewHolder;
        this.shareServices = shareServices;
        this.applicationContextPersistence = applicationContextPersistence;
        this.userPreferencesPersistence = userPreferencesPersistence;
        this.highlightedVersesServices = highlightedVersesServices;
        this.bibleServices = bibleServices;
        this.clipboardServices = clipboardServices;
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView,
                                   View view,
                                   int position,
                                   long id) {
        viewHolder.versesListView.setItemChecked(position,
                true);

        LayoutInflater layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View popupView = layoutInflater.inflate(R.layout.popup_verse_menu,
                null);
        popupWindow = new PopupWindow(
                popupView,
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);

        ViewGroup layout = popupView.findViewById(R.id.layout);
        layout.setOnClickListener(this);

        Button copyButton = popupView.findViewById(R.id.copyButton);
        copyButton.setOnClickListener(this);

        Button shareButton = popupView.findViewById(R.id.shareButton);
        shareButton.setOnClickListener(this);

        Button widgetButton = popupView.findViewById(R.id.widgetButton);
        widgetButton.setOnClickListener(this);

        boolean nightMode = userPreferencesPersistence.getNightMode();
        ImageView color1ImageView = popupView.findViewById(R.id.color1ImageView);
        color1ImageView.setImageResource(nightMode ? R.color.bible_verse_color_1_night : R.color.bible_verse_color_1);
        color1ImageView.setOnClickListener(this);

        ImageView color2ImageView = popupView.findViewById(R.id.color2ImageView);
        color2ImageView.setImageResource(nightMode ? R.color.bible_verse_color_2_night : R.color.bible_verse_color_2);
        color2ImageView.setOnClickListener(this);

        ImageView color3ImageView = popupView.findViewById(R.id.color3ImageView);
        color3ImageView.setImageResource(nightMode ? R.color.bible_verse_color_3_night : R.color.bible_verse_color_3);
        color3ImageView.setOnClickListener(this);

        ImageView color4ImageView = popupView.findViewById(R.id.color4ImageView);
        color4ImageView.setImageResource(nightMode ? R.color.bible_verse_color_4_night : R.color.bible_verse_color_4);
        color4ImageView.setOnClickListener(this);

        ImageView color5ImageView = popupView.findViewById(R.id.color5ImageView);
        color5ImageView.setImageResource(nightMode ? R.color.bible_verse_color_5_night : R.color.bible_verse_color_5);
        color5ImageView.setOnClickListener(this);

        ImageView colorDeleteImageView = popupView.findViewById(R.id.colorDeleteImageView);
        colorDeleteImageView.setOnClickListener(this);

        popupWindow.showAtLocation(view,
                Gravity.CENTER,
                0,
                0);

        return true;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView,
                            View view,
                            int i,
                            long l) {
        viewHolder.versesArrayAdapter.notifyDataSetInvalidated();
    }

    private void clearCheckedItems() {
        SparseBooleanArray checkedItemPositions = viewHolder.versesListView.getCheckedItemPositions();
        for (int i = 0; i < checkedItemPositions.size(); i++) {
            viewHolder.versesListView.setItemChecked(checkedItemPositions.keyAt(i),
                    false);
        }
    }

    private VersesHelper.VersesText getCheckedVersesAsVersesText() {
        List<VerseDefinition> checkedVerses = getCheckedVerses(viewHolder.versesListView, viewHolder.versesArrayAdapter);

        return VersesHelper.getVersesAsText(activity,
                bibleServices,
                checkedVerses);
    }

    private String getCheckedVersesAsString() {
        List<VerseDefinition> checkedVerses = getCheckedVerses(viewHolder.versesListView, viewHolder.versesArrayAdapter);

        VersesHelper.VersesText versesAsText = VersesHelper.getVersesAsText(activity,
                bibleServices,
                checkedVerses);

        return versesAsText.getTitle() + "\n" + versesAsText.getContent();
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.copyButton:
                String clipboardText = getCheckedVersesAsString();

                clipboardServices.copyToClipboard(activity, clipboardText);

                clearCheckedItems();
                break;
            case R.id.shareButton:
                String shareText = getCheckedVersesAsString();

                shareServices.share(activity, shareText);

                clearCheckedItems();
                break;
            case R.id.widgetButton:
                VersesHelper.VersesText versesText = getCheckedVersesAsVersesText();
                applicationContextPersistence.setBibleVerseWidgetVerse(new BibleVerseWidgetContent(
                        versesText.getTitle(),
                        versesText.getContent()));
                clearCheckedItems();

                WidgetHelper.refreshWidget(activity);
                break;

            case R.id.color1ImageView:
                saveVersesColoration(1);
                clearCheckedItems();
                viewHolder.versesArrayAdapter.notifyDataSetInvalidated();
                break;
            case R.id.color2ImageView:
                saveVersesColoration(2);
                clearCheckedItems();
                viewHolder.versesArrayAdapter.notifyDataSetInvalidated();
                break;
            case R.id.color3ImageView:
                saveVersesColoration(3);
                clearCheckedItems();
                viewHolder.versesArrayAdapter.notifyDataSetInvalidated();
                break;
            case R.id.color4ImageView:
                saveVersesColoration(4);
                clearCheckedItems();
                viewHolder.versesArrayAdapter.notifyDataSetInvalidated();
                break;
            case R.id.color5ImageView:
                saveVersesColoration(5);
                clearCheckedItems();
                viewHolder.versesArrayAdapter.notifyDataSetInvalidated();
                break;
            case R.id.colorDeleteImageView:
                List<VerseDefinition> checkedVerses = getCheckedVerses(viewHolder.versesListView, viewHolder.versesArrayAdapter);
                for (VerseDefinition verse : checkedVerses) {
                    highlightedVersesServices.deleteVerseHighlight(verse.getBookId(),
                            verse.getChapterNumber(),
                            verse.getVerseNumber());
                }
                clearCheckedItems();
                viewHolder.versesArrayAdapter.notifyDataSetInvalidated();
                break;
        }

        dismissPopup();
    }

    private void saveVersesColoration(Integer colorIndex) {
        List<VerseDefinition> checkedVerses = getCheckedVerses(viewHolder.versesListView, viewHolder.versesArrayAdapter);
        for (VerseDefinition verse : checkedVerses) {
            highlightedVersesServices.addOrUpdateVerseHighlight(verse.getBookId(),
                    verse.getChapterNumber(),
                    verse.getVerseNumber(),
                    colorIndex);
        }
    }

    public void dismissPopup() {
        if (popupWindow != null) {
            popupWindow.dismiss();
        }
    }

    public boolean isPopupDisplayed() {
        return popupWindow != null && popupWindow.isShowing();
    }
}
