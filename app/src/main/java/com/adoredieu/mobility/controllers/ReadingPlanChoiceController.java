package com.adoredieu.mobility.controllers;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.core.adapter.ReadingPlanDescriptionAdapter;
import com.adoredieu.mobility.core.dao.ReadingPlanDescriptionDao;
import com.adoredieu.mobility.core.dto.ReadingPlanDescriptionDto;
import com.adoredieu.mobility.core.expansion.BibleExpansionPackageInfo;
import com.adoredieu.mobility.core.helpers.DialogsHelper;
import com.adoredieu.mobility.core.model.bible.ReadingPlanDescription;
import com.adoredieu.mobility.core.persistence.UserPreferencesPersistence;
import com.adoredieu.mobility.core.services.BibleInstallationServices;
import com.adoredieu.mobility.core.system.application.AdoreDieuApplication;
import com.adoredieu.mobility.view.adapters.items.ReadingPlanDescriptionItem;
import com.adoredieu.mobility.view.fragments.ReadingPlanChoiceFragment;
import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.IAdapter;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class ReadingPlanChoiceController
        extends ReadingPlanChoiceFragment
{
    @Inject
    ReadingPlanDescriptionDao readingPlanDescriptionDao;
    @Inject
    UserPreferencesPersistence userPreferencesPersistence;
    @Inject
    GuiController guiController;
    @Inject
    BibleInstallationServices bibleInstallationServices;

    @Override
    public void synchronizeNavigationDrawer(NavigationView navigationView)
    {
        navigationView.setCheckedItem(R.id.nav_bible_reading_plan);
    }

    @Override
    public void onViewCreated(@NonNull View view,
                              Bundle savedInstanceState)
    {
        super.onViewCreated(view,
                            savedInstanceState);
        ((AdoreDieuApplication) getActivity().getApplication()).getInjector().inject(this);

        if (!bibleInstallationServices.isBibleInstalled(getActivity(),
                                                        BibleExpansionPackageInfo.getBibleXApkFile().fileVersion))
        {
            DialogsHelper.showAlert(getActivity(),
                                    getString(R.string.probleme_d_installation),
                                    getString(R.string.error_bible_not_installed),
                                    new View.OnClickListener()
                                    {
                                        @Override
                                        public void onClick(View v)
                                        {
                                            guiController.showHome(getActivity());
                                        }
                                    });
            return;
        }

        List<ReadingPlanDescription> readingPlanDescriptions = readingPlanDescriptionDao.getReadingPlanDescriptions();
        final List<ReadingPlanDescriptionDto> readingPlanDescriptionDtos = ReadingPlanDescriptionAdapter
                .readingPlanDescriptionsToReadingPlanDescriptionDtos(readingPlanDescriptions);

        FastItemAdapter fastItemAdapter = new FastItemAdapter();
        fastItemAdapter.withOnClickListener(new FastAdapter.OnClickListener<ReadingPlanDescriptionItem>()
        {
            @Override
            public boolean onClick(View v,
                                   IAdapter<ReadingPlanDescriptionItem> adapter,
                                   ReadingPlanDescriptionItem item,
                                   int position)
            {
                ReadingPlanDescriptionDto readingPlanDescriptionDto = readingPlanDescriptionDtos.get(
                        position);
                userPreferencesPersistence.setReadingPlanIndex(readingPlanDescriptionDto.getId());

                guiController.showReadingPlanController(getActivity());
                return false;
            }
        });

        RecyclerView readingPlansListView = view.findViewById(R.id.readingPlansListView);
        readingPlansListView.setLayoutManager(new LinearLayoutManager(getActivity()));
        readingPlansListView.setItemAnimator(new DefaultItemAnimator());

        List<ReadingPlanDescriptionItem> items = new ArrayList<>();

        for (ReadingPlanDescriptionDto readingPlanDescriptionDto : readingPlanDescriptionDtos)
        {
            items.add(new ReadingPlanDescriptionItem(readingPlanDescriptionDto.getName(),
                                                     readingPlanDescriptionDto.getDescription()));
        }

        fastItemAdapter.add(items);
        readingPlansListView.setAdapter(fastItemAdapter);
    }
}
