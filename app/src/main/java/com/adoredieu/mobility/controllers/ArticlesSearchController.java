package com.adoredieu.mobility.controllers;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.core.helpers.DialogsHelper;
import com.adoredieu.mobility.core.helpers.KeyboardHelper;
import com.adoredieu.mobility.core.helpers.NetworkHelper;
import com.adoredieu.mobility.core.interfaces.Action;
import com.adoredieu.mobility.core.interfaces.ArticleType;
import com.adoredieu.mobility.core.model.website.Article;
import com.adoredieu.mobility.core.services.ArticleSearchServices;
import com.adoredieu.mobility.core.system.application.AdoreDieuApplication;
import com.adoredieu.mobility.view.adapters.items.ArticleItem;
import com.adoredieu.mobility.view.adapters.items.ProgressMessageItem;
import com.adoredieu.mobility.view.fragments.ArticlesSearchFragment;
import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.IAdapter;
import com.mikepenz.fastadapter.IItem;
import com.mikepenz.fastadapter.adapters.FooterAdapter;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;

import java.util.ArrayList;

import javax.inject.Inject;

public class ArticlesSearchController
        extends ArticlesSearchFragment
{
    @Inject
    ArticleSearchServices articleSearchServices;
    @Inject
    GuiController guiController;

    private ArrayList<Article> currentArticles;
    private ViewHolder viewHolder;

    // save our FastAdapter
    private FastItemAdapter<IItem> fastItemAdapter;
    private FooterAdapter<IItem> footerAdapter;

    @Override
    public void synchronizeNavigationDrawer(NavigationView navigationView)
    {
        navigationView.setCheckedItem(R.id.nav_articles_search);
    }

    @Override
    public void onViewCreated(@NonNull View view,
                              Bundle savedInstanceState)
    {
        super.onViewCreated(view,
                            savedInstanceState);
        ((AdoreDieuApplication) getActivity().getApplication()).getInjector().inject(this);

        viewHolder = new ViewHolder();
        viewHolder.searchButton = view.findViewById(R.id.searchButton);
        viewHolder.searchButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                startSearch();
            }
        });

        viewHolder.searchEditText = view.findViewById(R.id.searchEditText);
        viewHolder.searchEditText.setOnKeyListener(new View.OnKeyListener()
        {
            public boolean onKey(View v,
                                 int keyCode,
                                 KeyEvent event)
            {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN)
                    && (keyCode == KeyEvent.KEYCODE_ENTER))
                {
                    startSearch();
                    return true;
                }
                return false;
            }
        });

        ImageButton clearSearchEditTextButton = view.findViewById(R.id.clearSearchEditTextButton);
        clearSearchEditTextButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                viewHolder.searchEditText.setText("");
            }
        });

        fastItemAdapter = new FastItemAdapter<>();
        fastItemAdapter.withSelectable(false);
        fastItemAdapter.withOnClickListener(new FastAdapter.OnClickListener<IItem>()
        {
            @Override
            public boolean onClick(View v,
                                   IAdapter<IItem> adapter,
                                   IItem item,
                                   int position)
            {
                if (item instanceof ArticleItem)
                {
                    ArticleItem articleItem = (ArticleItem) item;
                    if (articleItem.article.type == ArticleType.Teaching)
                    {
                        guiController.showTeachingDetailsController(getActivity(),
                                                                    articleItem.article);
                    }
                    else
                    {
                        guiController.showThoughtDetailsController(getActivity(),
                                                                   articleItem.article);
                    }
                }

                return false;
            }
        });

        footerAdapter = new FooterAdapter<>();

        //get our recyclerView and do basic setup
        RecyclerView recyclerView = view.findViewById(R.id.articlesListView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(footerAdapter.wrap(fastItemAdapter));

        if (currentArticles != null && currentArticles.size() > 0)
        {
            publishResults(currentArticles);
        }

        if (NetworkHelper.isNetworkAvailable(getActivity()))
        {
            TextView internetErrorTextView = view.findViewById(R.id.internetErrorTextView);
            internetErrorTextView.setVisibility(View.GONE);
        }
    }

    private void startSearch()
    {
        if (!viewHolder.searchEditText.getText()
                                      .toString()
                                      .isEmpty())
        {
            footerAdapter.clear();
            footerAdapter.add(new ProgressMessageItem(getString(R.string.searching)));

            fastItemAdapter.clear();
            viewHolder.searchButton.setEnabled(false);
            KeyboardHelper.hide_keyboard(getActivity());

            articleSearchServices.search(viewHolder.searchEditText.getText().toString().trim(),
                                         new Action<ArrayList<Article>>()
                                         {
                                             @Override
                                             public void run(ArrayList<Article> object)
                                             {
                                                 publishResults(object);
                                                 DialogsHelper.hideProgressDialog();
                                             }
                                         });
        }
    }

    private void publishResults(ArrayList<Article> articles)
    {
        currentArticles = articles;

        footerAdapter.clear();

        for (Article article : articles)
        {
            fastItemAdapter.add(fastItemAdapter.getAdapterItemCount(),
                                new ArticleItem(article));
        }

        viewHolder.searchButton.setEnabled(true);
    }

    protected class ViewHolder
    {
        EditText searchEditText;
        ImageButton searchButton;
    }
}
