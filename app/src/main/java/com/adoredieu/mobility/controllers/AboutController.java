package com.adoredieu.mobility.controllers;

import android.annotation.SuppressLint;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.TextView;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.core.persistence.UserPreferencesPersistence;
import com.adoredieu.mobility.core.system.application.AdoreDieuApplication;
import com.adoredieu.mobility.view.fragments.AboutFragment;
import com.mikepenz.aboutlibraries.LibsBuilder;
import com.mikepenz.aboutlibraries.ui.LibsSupportFragment;

import javax.inject.Inject;


public class AboutController
        extends AboutFragment
{
    @Inject
    GuiController guiController;
    @Inject
    UserPreferencesPersistence userPreferencesPersistence;

    @Override
    public void synchronizeNavigationDrawer(NavigationView navigationView)
    {
        navigationView.setCheckedItem(R.id.nav_parameters);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onViewCreated(@NonNull View view,
                              Bundle savedInstanceState)
    {
        super.onViewCreated(view,
                            savedInstanceState);
        ((AdoreDieuApplication) getActivity().getApplication()).getInjector().inject(this);

        LibsSupportFragment fragment = new LibsBuilder()
                //Pass the fields of your application to the lib so it can find all external lib information
                .withFields(R.string.class.getFields())
                //get the fragment
                .supportFragment();

        FragmentTransaction transaction = getActivity().getSupportFragmentManager()
                                                       .beginTransaction();
        transaction.replace(R.id.libsAboutFrameLayout,
                            fragment);
        transaction.commit();

        TextView appVersionTextView = view.findViewById(R.id.appVersionTextView);
        String versionText = "";
        try
        {
            PackageInfo packageInfo = getActivity().getPackageManager()
                                                   .getPackageInfo(getActivity().getPackageName(),
                                                                   0);
            versionText = "v" + packageInfo.versionName;
        }
        catch (PackageManager.NameNotFoundException e)
        {
            e.printStackTrace();
        }
        appVersionTextView.setText(versionText);
    }
}
