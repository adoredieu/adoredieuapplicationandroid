package com.adoredieu.mobility.controllers;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.controllers.base.ArticlesListBaseController;
import com.adoredieu.mobility.core.helpers.DialogsHelper;
import com.adoredieu.mobility.core.helpers.NetworkHelper;
import com.adoredieu.mobility.core.interfaces.Action;
import com.adoredieu.mobility.core.interfaces.ArticleType;
import com.adoredieu.mobility.core.model.website.Article;
import com.adoredieu.mobility.core.model.website.ImageQuote;
import com.adoredieu.mobility.core.services.ImageQuotesServices;
import com.adoredieu.mobility.core.services.ShareServices;
import com.adoredieu.mobility.core.system.application.AdoreDieuApplication;
import com.adoredieu.mobility.view.adapters.items.ImageQuoteItem;
import com.adoredieu.mobility.view.adapters.items.ProgressMessageItem;
import com.mikepenz.fastadapter.IItem;
import com.mikepenz.fastadapter.adapters.FooterAdapter;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.mikepenz.fastadapter_extensions.scroll.EndlessRecyclerOnScrollListener;

import java.util.List;

import javax.inject.Inject;

public class ImageQuotesListController
        extends ArticlesListBaseController {
    @Inject
    ShareServices shareServices;
    @Inject
    ImageQuotesServices imageQuotesServices;

    private boolean noMoreArticles;

    //save our FastAdapter
    private FastItemAdapter<IItem> fastItemAdapter;
    private FooterAdapter<IItem> footerAdapter;
    private RecyclerView recyclerView;

    public ImageQuotesListController() {
        super(ArticleType.ImageQuotes);
    }

    @Override
    protected int getDeltaOfIndex() {
        return -1;
    }

    @Override
    public void onViewCreated(@NonNull View view,
                              Bundle savedInstanceState) {
        super.onViewCreated(view,
                savedInstanceState);

        ((AdoreDieuApplication) getActivity().getApplication()).getInjector().inject(this);

        fastItemAdapter = new FastItemAdapter<>();
        fastItemAdapter.withSelectable(false);
        footerAdapter = new FooterAdapter<>();

        //get our recyclerView and do basic setup
        recyclerView = view.findViewById(R.id.articlesListView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(footerAdapter.wrap(fastItemAdapter));
        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener() {
            @Override
            public void onLoadMore(final int currentPage) {
                if (!noMoreArticles && !imageQuotesServices.isDownloading()) {
                    downloadPage((footerAdapter.getItemCount() / ImageQuotesServices.ITEMS_PER_PAGE) - getDeltaOfIndex());
                }
            }
        });

        noMoreArticles = false;
        if (NetworkHelper.isNetworkAvailable(getActivity())) {
            TextView internetErrorTextView = view.findViewById(R.id.internetErrorTextView);
            internetErrorTextView.setVisibility(View.GONE);

            downloadPage(1);
        }
    }

    @Override
    public void synchronizeNavigationDrawer(NavigationView navigationView) {
        navigationView.setCheckedItem(R.id.nav_citations_images);
    }

    private void downloadPage(final Integer page) {
        recyclerView.post(new Runnable() {
            @Override
            public void run() {
                footerAdapter.clear();
                footerAdapter.add(new ProgressMessageItem(getString(R.string.downloading_articles_list)));
            }
        });

        imageQuotesServices.getImageQuotesPage(
                page - 1,
                new Action<List<ImageQuote>>() {
                    @Override
                    public void run(List<ImageQuote> object) {
                        showImageQuotes(object);
                        DialogsHelper.hideProgressDialog();
                    }
                },
                new Runnable() {
                    @Override
                    public void run() {
                        noMoreArticles = true;
                        DialogsHelper.hideProgressDialog();
                    }
                });
    }

    @Override
    protected void onItemClick(Article article) {
    }

    private void showImageQuotes(List<ImageQuote> imageQuotes) {
        footerAdapter.clear();

        for (ImageQuote imageQuote : imageQuotes) {
            fastItemAdapter.add(fastItemAdapter.getAdapterItemCount(),
                    new ImageQuoteItem(imageQuote,
                            shareServices));
        }
    }
}
