package com.adoredieu.mobility.controllers;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.core.adapter.BookAdapter;
import com.adoredieu.mobility.core.dto.BookDto;
import com.adoredieu.mobility.core.model.bible.Book;
import com.adoredieu.mobility.core.services.BibleServices;
import com.adoredieu.mobility.core.system.application.AdoreDieuApplication;
import com.adoredieu.mobility.view.adapters.items.BibleBookItem;
import com.adoredieu.mobility.view.fragments.BibleBookChoiceFragment;
import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.IAdapter;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class BibleBookChoiceController
        extends BibleBookChoiceFragment
{
    @Inject
    BibleServices bibleServices;
    @Inject
    GuiController guiController;

    @Override
    public void synchronizeNavigationDrawer(NavigationView navigationView)
    {
        navigationView.setCheckedItem(R.id.nav_bible);
    }

    @Override
    public void onViewCreated(@NonNull View view,
                              Bundle savedInstanceState)
    {
        super.onViewCreated(view,
                            savedInstanceState);
        ((AdoreDieuApplication) getActivity().getApplication()).getInjector().inject(this);

        List<Book> books = bibleServices.getBooks();
        final List<BookDto> bookDtos = BookAdapter.booksToBookDtos(books);

        FastItemAdapter fastItemAdapter = new FastItemAdapter();
        fastItemAdapter.withOnClickListener(new FastAdapter.OnClickListener<BibleBookItem>()
        {
            @Override
            public boolean onClick(View v,
                                   IAdapter<BibleBookItem> adapter,
                                   BibleBookItem item,
                                   int position)
            {
                BookDto book = bookDtos.get(position);
                guiController.showBibleChapterChoiceController(getActivity(),
                                                               book);
                return false;
            }
        });

        RecyclerView booksListView = view.findViewById(R.id.booksListView);
        booksListView.setLayoutManager(new LinearLayoutManager(getActivity()));
        booksListView.setItemAnimator(new DefaultItemAnimator());

        List<BibleBookItem> items = new ArrayList<>();

        for (BookDto bookDto : bookDtos)
        {
            String bookName = bibleServices.getLocalizedBooksNames().get(bookDto.getId());

            items.add(new BibleBookItem(bookName,
                                        bookDto.getTestament()
                                               .getId() == 1 ?
                                                ContextCompat.getDrawable(getActivity(),
                                                                          R.drawable.ic_at)
                                                : ContextCompat.getDrawable(getActivity(),
                                                                            R.drawable.ic_nt)));
        }

        fastItemAdapter.add(items);
        booksListView.setAdapter(fastItemAdapter);
        booksListView.scrollToPosition(items.size() / 2);
    }
}
