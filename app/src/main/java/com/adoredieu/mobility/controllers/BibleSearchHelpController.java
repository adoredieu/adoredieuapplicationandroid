package com.adoredieu.mobility.controllers;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.view.View;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.core.system.application.AdoreDieuApplication;
import com.adoredieu.mobility.view.fragments.BibleSearchHelpFragment;

public class BibleSearchHelpController
        extends BibleSearchHelpFragment
{
    @Override
    public void synchronizeNavigationDrawer(NavigationView navigationView)
    {
        navigationView.setCheckedItem(R.id.nav_bible_search);
    }

    @Override
    public void onViewCreated(@NonNull View view,
                              Bundle savedInstanceState)
    {
        super.onViewCreated(view,
                            savedInstanceState);
        ((AdoreDieuApplication) getActivity().getApplication()).getInjector().inject(this);
    }
}
