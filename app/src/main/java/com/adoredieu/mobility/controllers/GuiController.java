package com.adoredieu.mobility.controllers;

import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;

import com.adoredieu.mobility.core.dto.BookDto;
import com.adoredieu.mobility.core.factory.Injector;
import com.adoredieu.mobility.core.helpers.FragmentsHelper;
import com.adoredieu.mobility.core.interfaces.VerseDefinition;
import com.adoredieu.mobility.core.model.website.Article;

import java.util.List;

import javax.inject.Inject;

public class GuiController {
    private final Injector injector;

    @Inject
    public GuiController(Injector injector) {
        this.injector = injector;
    }

    public void showHome(FragmentActivity activity) {
        HomeController homeController = injector.provideHomeController();

        FragmentsHelper.setMainFragment(activity,
                homeController);
    }

    public void showAbout(FragmentActivity activity) {
        AboutController aboutController = injector.provideAboutController();

        FragmentsHelper.setMainFragment(activity,
                aboutController);
    }

    public void showThoughtDetailsController(FragmentActivity activity,
                                             Article article) {
        ThoughtDetailsController thoughtDetailsController = injector.provideThoughtDetailsController();
        thoughtDetailsController.withArticle(article);

        FragmentsHelper.setMainFragment(activity,
                thoughtDetailsController);
    }

    public void showTeachingDetailsController(FragmentActivity activity,
                                              Article article) {
        TeachingDetailsController teachingDetailsController = injector.provideTeachingDetailsController();
        teachingDetailsController.withArticle(article);

        FragmentsHelper.setMainFragment(activity,
                teachingDetailsController);
    }

    public void showBibleSearchController(FragmentActivity activity) {
        BibleSearchController bibleSearchController = injector.provideBibleSearchController();

        FragmentsHelper.setMainFragment(activity,
                bibleSearchController);
    }

    public void showBibleStrongSearchController(FragmentActivity activity,
                                                BibleStrongSearchController.StrongLanguage language,
                                                Integer strongCode) {
        BibleStrongSearchController bibleStrongSearchController = injector.provideBibleStrongSearchController();
        bibleStrongSearchController.withSearch(language, strongCode);

        FragmentsHelper.setMainFragment(activity,
                bibleStrongSearchController);
    }

    public void showCommunityController(FragmentActivity activity) {
        FragmentsHelper.setMainFragment(activity,
                injector.provideCommunityController());
    }

    public void showThoughtsController(FragmentActivity activity) {
        ThoughtsListController thoughtsListController = injector.provideThoughtsListController();
        FragmentsHelper.setMainFragment(activity,
                thoughtsListController);
    }

    public void showImageQuotesController(FragmentActivity activity) {
        ImageQuotesListController imageQuotesController = injector.provideImageQuotesListController();

        FragmentsHelper.setMainFragment(activity,
                imageQuotesController);
    }

    public void showTeachingsController(FragmentActivity activity) {
        TeachingsListController teachingsListController = injector.provideTeachingsListController();
        FragmentsHelper.setMainFragment(activity,
                teachingsListController);
    }

    public void showBibleController(FragmentActivity activity,
                                    @NonNull List<VerseDefinition> selectedVerses) {
        BibleController bibleController = injector.provideBibleController();
        bibleController.withSelectedVerses(selectedVerses);

        FragmentsHelper.setMainFragment(activity,
                bibleController);
    }

    public void showBibleStrongController(FragmentActivity activity,
                                          @NonNull List<VerseDefinition> selectedVerses) {
        BibleStrongController bibleStrongController = injector.provideBibleStrongController();
        bibleStrongController.withSelectedVerses(selectedVerses);

        FragmentsHelper.setMainFragment(activity,
                bibleStrongController);
    }

    public void showParametersController(FragmentActivity activity) {
        FragmentsHelper.setMainFragment(activity,
                injector.provideParametersController());
    }

    public void showBibleBookChoiceController(FragmentActivity activity) {
        FragmentsHelper.setMainFragment(activity,
                injector.provideBibleBookChoiceController());
    }

    public void showBibleChapterChoiceController(FragmentActivity activity,
                                                 BookDto book) {
        BibleChapterChoiceController bibleChapterChoiceController = injector.provideBibleChapterChoiceController();
        bibleChapterChoiceController.withBook(book);

        FragmentsHelper.setMainFragment(activity,
                bibleChapterChoiceController);
    }

    public void showBibleSearchHelpController(FragmentActivity activity) {
        FragmentsHelper.setMainFragment(activity,
                injector.provideBibleSearchHelpController());
    }

    public void showAudioPlayerController(FragmentActivity activity) {
        FragmentsHelper.setMainFragment(activity,
                injector.provideAudioPlayerController());
    }

    public void showArticlesSearchController(FragmentActivity activity) {
        ArticlesSearchController articlesSearchController = injector.provideArticlesSearchController();
        FragmentsHelper.setMainFragment(activity,
                articlesSearchController);
    }

    public void showHighlightListController(FragmentActivity activity) {
        HighlightsListController highlightListController = injector.provideHighlightListController();
        FragmentsHelper.setMainFragment(activity,
                highlightListController);
    }

    public void showReadingPlanChoiceController(FragmentActivity activity) {
        ReadingPlanChoiceController readingPlanChoiceController = injector.provideReadingPlanChoiceController();
        FragmentsHelper.setMainFragment(activity,
                readingPlanChoiceController);
    }

    public void showReadingPlanController(FragmentActivity activity) {
        ReadingPlanController readingPlanController = injector.provideReadingPlanController();
        FragmentsHelper.setMainFragment(activity,
                readingPlanController);
    }
}
