package com.adoredieu.mobility.controllers;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.TextView;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.core.dto.BookDto;
import com.adoredieu.mobility.core.interfaces.BiblePosition;
import com.adoredieu.mobility.core.interfaces.VerseDefinition;
import com.adoredieu.mobility.core.persistence.ApplicationContextPersistence;
import com.adoredieu.mobility.core.persistence.UserPreferencesPersistence;
import com.adoredieu.mobility.core.services.BibleServices;
import com.adoredieu.mobility.core.system.application.AdoreDieuApplication;
import com.adoredieu.mobility.view.adapters.items.BibleBookChapterItem;
import com.adoredieu.mobility.view.fragments.BibleChapterChoiceFragment;
import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.IAdapter;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class BibleChapterChoiceController
        extends BibleChapterChoiceFragment {
    @Inject
    BibleServices bibleServices;
    @Inject
    ApplicationContextPersistence applicationContextPersistence;
    @Inject
    UserPreferencesPersistence userPreferencesPersistence;
    @Inject
    GuiController guiController;

    private BookDto book;

    public BibleChapterChoiceController withBook(BookDto book) {
        this.book = book;
        return this;
    }

    @Override
    public void synchronizeNavigationDrawer(NavigationView navigationView) {
        navigationView.setCheckedItem(R.id.nav_bible);

    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onViewCreated(@NonNull View view,
                              Bundle savedInstanceState) {
        super.onViewCreated(view,
                savedInstanceState);
        ((AdoreDieuApplication) getActivity().getApplication()).getInjector().inject(this);

        String bookName = bibleServices.getLocalizedBooksNames().get(book.getId());

        TextView titleTextView = view.findViewById(R.id.titleTextView);
        titleTextView.setText(bookName + ", " + getResources().getString(R.string.chapitre) + " ?");

        FastItemAdapter fastItemAdapter = new FastItemAdapter();
        fastItemAdapter.withOnClickListener(new FastAdapter.OnClickListener<BibleBookChapterItem>() {
            @Override
            public boolean onClick(View v,
                                   IAdapter<BibleBookChapterItem> adapter,
                                   BibleBookChapterItem item,
                                   int position) {
                applicationContextPersistence.setLastBiblePosition(new BiblePosition(book.getId(),
                        position + 1,
                        1));

                if (userPreferencesPersistence.getStrongsMode()) {
                    guiController.showBibleStrongController(getActivity(),
                            new ArrayList<VerseDefinition>());
                } else {
                    guiController.showBibleController(getActivity(),
                            new ArrayList<VerseDefinition>());
                }

                return false;
            }
        });

        final RecyclerView booksListView = view.findViewById(R.id.chaptersGridView);
        booksListView.setHasFixedSize(true);
        final GridLayoutManager layoutManager = new GridLayoutManager(getActivity(),
                1);
        booksListView.setItemAnimator(new DefaultItemAnimator());
        booksListView.setLayoutManager(layoutManager);
        booksListView.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        try {
                            int viewWidth = booksListView.getMeasuredWidth();
                            float cardViewWidth = getActivity().getResources()
                                    .getDimension(R.dimen.chapter_square_size) + 4;
                            int newSpanCount = (int) Math.floor(viewWidth / cardViewWidth);
                            layoutManager.setSpanCount(newSpanCount);
                            layoutManager.requestLayout();
                        } catch (Exception ignore) {
                        }
                    }
                });

        List<BibleBookChapterItem> items = new ArrayList<>();
        int chaptersCount = (int) bibleServices.getChaptersCount(book.getId());

        for (int i = 0; i < chaptersCount; i++) {
            items.add(new BibleBookChapterItem(i + 1));
        }

        fastItemAdapter.add(items);
        booksListView.setAdapter(fastItemAdapter);
    }
}
