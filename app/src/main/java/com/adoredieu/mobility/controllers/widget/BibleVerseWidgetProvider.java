package com.adoredieu.mobility.controllers.widget;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.widget.RemoteViews;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.core.interfaces.BibleVerseWidgetContent;
import com.adoredieu.mobility.core.persistence.ApplicationContextPersistence;

public class BibleVerseWidgetProvider
        extends AppWidgetProvider
{
    private static void updateAppWidget(Context context,
                                        AppWidgetManager appWidgetManager,
                                        int appWidgetId,
                                        BibleVerseWidgetContent bibleVerseWidgetVerse)
    {

        // Prepare widget views
        RemoteViews views = new RemoteViews(context.getPackageName(),
                                            R.layout.widget_bible_verse);
        views.setTextViewText(R.id.titleTextView,
                              bibleVerseWidgetVerse.getTitle());
        views.setTextViewText(R.id.contentTextView,
                              bibleVerseWidgetVerse.getContent());
        appWidgetManager.updateAppWidget(appWidgetId,
                                         views);
    }

    @Override
    public void onUpdate(Context context,
                         AppWidgetManager appWidgetManager,
                         int[] appWidgetIds)
    {
        ApplicationContextPersistence applicationContextPersistence = new ApplicationContextPersistence(
                context);
        BibleVerseWidgetContent bibleVerseWidgetVerse = applicationContextPersistence.getBibleVerseWidgetVerse(
                context);

        // Perform this loop procedure for each App Widget that belongs to this
        // provider
        for (int appWidgetId : appWidgetIds)
        {
            updateAppWidget(context,
                            appWidgetManager,
                            appWidgetId,
                            bibleVerseWidgetVerse);
        }
    }
}
