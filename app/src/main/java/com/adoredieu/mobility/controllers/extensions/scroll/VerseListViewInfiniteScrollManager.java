package com.adoredieu.mobility.controllers.extensions.scroll;

import android.view.MotionEvent;
import android.view.View;
import android.widget.AbsListView;

import com.adoredieu.mobility.core.interfaces.MoveDirection;

public abstract class VerseListViewInfiniteScrollManager
        implements View.OnTouchListener,
                   AbsListView.OnScrollListener
{
    private static final int SPEED_THRESHOLD = 5;
    private int firstVisible;
    private int visibleCount;
    private int totalCount;
    private MoveDirection moveDirection = MoveDirection.None;
    private float _downX = -1;
    private float _downY = -1;
    private float _lastX = -1;
    private float _lastY = -1;

    @Override
    public void onScrollStateChanged(AbsListView absListView,
                                     int i)
    {

    }

    @Override
    public void onScroll(AbsListView absListView,
                         int firstVisible,
                         int visibleCount,
                         int totalCount)
    {
        this.firstVisible = firstVisible;
        this.visibleCount = visibleCount;
        this.totalCount = totalCount;
    }

    @Override
    public boolean onTouch(View v,
                           MotionEvent event)
    {
        switch (event.getAction())
        {
            case MotionEvent.ACTION_DOWN:
                moveDirection = MoveDirection.None;
                _downX = event.getX();
                _downY = event.getY();
                _lastX = _downX;
                _lastY = _downY;

                onMouseDown(_downX,
                            _downY);
                break;
            case MotionEvent.ACTION_MOVE:
                float speedX = _lastX - event.getX();
                _lastX = event.getX();

                float speedY = _lastY - event.getY();
                _lastY = event.getY();

                if (Math.abs(speedX) > Math.abs(speedY))
                {
                    if (speedX >= SPEED_THRESHOLD)
                    {
                        moveDirection = MoveDirection.West;
                    }
                    else if (speedX <= -SPEED_THRESHOLD)
                    {
                        moveDirection = MoveDirection.East;
                    }
                }
                else
                {
                    if (speedY >= SPEED_THRESHOLD)
                    {
                        moveDirection = MoveDirection.North;
                    }
                    else if (speedY <= -SPEED_THRESHOLD)
                    {
                        moveDirection = MoveDirection.South;
                    }
                }

                onScrolling(moveDirection,
                            firstVisible,
                            visibleCount,
                            totalCount);
                break;
            case MotionEvent.ACTION_UP:
                onMouseUp(moveDirection);

                _downX = -1;
                _downY = -1;
                _lastX = -1;
                _lastY = -1;
                moveDirection = MoveDirection.None;
                break;
        }

        return false;
    }

    protected abstract void onMouseDown(float downX,
                                        float downY);

    public abstract void onScrolling(MoveDirection moveDirection,
                                     int firstVisible,
                                     int visibleCount,
                                     int totalCount);

    public abstract void onMouseUp(MoveDirection moveDirection);

    public void reinit()
    {
        moveDirection = MoveDirection.None;
    }
}
