package com.adoredieu.mobility.controllers;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.core.dao.VerseDao;
import com.adoredieu.mobility.core.dto.VerseDto;
import com.adoredieu.mobility.core.dto.VerseOfTheDayDto;
import com.adoredieu.mobility.core.helpers.ApplicationHelper;
import com.adoredieu.mobility.core.helpers.DialogsHelper;
import com.adoredieu.mobility.core.helpers.DisplayHelper;
import com.adoredieu.mobility.core.helpers.NetworkHelper;
import com.adoredieu.mobility.core.helpers.SwipeDetector;
import com.adoredieu.mobility.core.helpers.WebHelper;
import com.adoredieu.mobility.core.interfaces.Action;
import com.adoredieu.mobility.core.interfaces.DateProvider;
import com.adoredieu.mobility.core.interfaces.VerseDefinition;
import com.adoredieu.mobility.core.persistence.ApplicationContextPersistence;
import com.adoredieu.mobility.core.persistence.UserPreferencesPersistence;
import com.adoredieu.mobility.core.services.ClipboardServices;
import com.adoredieu.mobility.core.services.FacebookServices;
import com.adoredieu.mobility.core.services.QuoteOfTheDayServices;
import com.adoredieu.mobility.core.services.ShareServices;
import com.adoredieu.mobility.core.services.VerseOfTheDayServices;
import com.adoredieu.mobility.core.system.application.AdoreDieuApplication;
import com.adoredieu.mobility.view.fragments.HomeFragment;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class HomeController
        extends HomeFragment
        implements View.OnClickListener {
    @Inject
    ApplicationContextPersistence applicationContextPersistence;
    @Inject
    UserPreferencesPersistence userPreferencesPersistence;
    @Inject
    GuiController guiController;
    @Inject
    VerseOfTheDayServices verseOfTheDayServices;
    @Inject
    QuoteOfTheDayServices quoteOfTheDayServices;
    @Inject
    DateProvider dateProvider;
    @Inject
    FacebookServices facebookServices;
    @Inject
    ClipboardServices clipboardServices;
    @Inject
    ShareServices shareServices;
    @Inject
    VerseDao verseDao;

    private ViewHolder viewHolder;
    private PopupWindow popupWindow;
    private String tempText;

    @Override
    public void onPause() {
        super.onPause();

        dismissPopup();
    }

    @Override
    public void synchronizeNavigationDrawer(NavigationView navigationView) {
        navigationView.setCheckedItem(R.id.nav_home);
    }

    public boolean isPopupDisplayed() {
        return popupWindow != null && popupWindow.isShowing();
    }

    public void dismissPopup() {
        if (popupWindow != null) {
            popupWindow.dismiss();
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public void onViewCreated(@NonNull final View view,
                              Bundle savedInstanceState) {
        super.onViewCreated(view,
                savedInstanceState);
        ((AdoreDieuApplication) getActivity().getApplication()).getInjector().inject(this);

        viewHolder = new ViewHolder();
        viewHolder.messageWebView = view.findViewById(R.id.messageWebView);
        viewHolder.messageWebView.getSettings()
                .setJavaScriptEnabled(true);
        viewHolder.messageWebView.getSettings()
                .setLoadsImagesAutomatically(true);
        viewHolder.messageWebView.setBackgroundColor(0x00000000);

        // Back management
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v,
                                 int keyCode,
                                 KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK
                        && event.getAction() == KeyEvent.ACTION_UP) {
                    if (isPopupDisplayed()) {
                        dismissPopup();
                        return true;
                    }

                    // Ask the user if he wants to quit
                    DialogsHelper.showQuestion(getActivity(),
                            getString(R.string.quitter),
                            getString(R.string.voulez_vous_vraiment_quitter),
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    ApplicationHelper.exit(getActivity());
                                }
                            },
                            null);
                    return true;
                }

                return false;
            }
        });

        TextView noInternetTextView = view.findViewById(R.id.internetErrorTextView);
        if (!NetworkHelper.isNetworkAvailable(getActivity())) {
            noInternetTextView.setVisibility(View.VISIBLE);
        } else {
            noInternetTextView.setVisibility(View.GONE);
        }

        viewHolder.verseOfTheDayImageButton = view.findViewById(R.id.verseOfTheDayImageButton);
        viewHolder.verseOfTheDayTextView = view.findViewById(R.id.verseOfTheDayTextView);
        viewHolder.verseOfTheDayTitleTextView = view.findViewById(R.id.verseOfTheDayTitleTextView);

        viewHolder.verseOfTheDayLinearLayout = (LinearLayout) view.findViewById(R.id.verseOfTheDayLinearLayout);
        viewHolder.verseOfTheDayProgressBar = view.findViewById(R.id.verseOfTheDayProgressBar);
        SwipeDetector verseOfTheDaySwipeDetector = new SwipeDetector(viewHolder.verseOfTheDayLinearLayout);
        verseOfTheDaySwipeDetector.setOnLongClickListener(new SwipeDetector.OnLongPressListener() {
            @Override
            public void longPressDetected(View v) {
                tempText = viewHolder.verseOfTheDayTextView.getText().toString();

                showPopup(view);
            }
        });
        verseOfTheDaySwipeDetector.setOnSwipeListener(new SwipeDetector.OnSwipeListener() {
            int dayDelta = 0;

            @Override
            public void swipeEventDetected(View v,
                                           SwipeDetector.SwipeTypeEnum swipeType) {
                switch (swipeType) {
                    case LEFT_TO_RIGHT:
                        dayDelta--;
                        getVerseOfTheDay(dayDelta);
                        break;
                    case RIGHT_TO_LEFT:
                        dayDelta++;
                        if (dayDelta > 0) {
                            dayDelta = 0;
                        } else {
                            getVerseOfTheDay(dayDelta);
                        }
                        break;
                }
            }
        });
        viewHolder.quoteTextView = view.findViewById(R.id.citationTextView);
        viewHolder.quoteTitleTextView = view.findViewById(R.id.citationTitleTextView);

        viewHolder.quoteLinearLayout = (LinearLayout) view.findViewById(R.id.citationLinearLayout);
        viewHolder.quoteProgressBar = view.findViewById(R.id.citationProgressBar);
        SwipeDetector citationSwipeDetector = new SwipeDetector(viewHolder.quoteLinearLayout);
        citationSwipeDetector.setOnLongClickListener(new SwipeDetector.OnLongPressListener() {
            @Override
            public void longPressDetected(View v) {
                tempText = viewHolder.quoteTextView.getText().toString();
                showPopup(view);
            }
        });
        citationSwipeDetector.setOnSwipeListener(new SwipeDetector.OnSwipeListener() {
            int dayDelta = 0;

            @Override
            public void swipeEventDetected(View v,
                                           SwipeDetector.SwipeTypeEnum swipeType) {
                switch (swipeType) {
                    case LEFT_TO_RIGHT:
                        dayDelta--;
                        getQuoteOfTheDay(dayDelta);
                        break;
                    case RIGHT_TO_LEFT:
                        dayDelta++;
                        if (dayDelta > 0) {
                            dayDelta = 0;
                        } else {
                            getQuoteOfTheDay(dayDelta);
                        }
                        break;
                }
            }
        });

        if (!applicationContextPersistence.getHomeTutorialSeen()) {
            ViewTreeObserver vto = viewHolder.verseOfTheDayLinearLayout.getViewTreeObserver();
            vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    viewHolder.verseOfTheDayLinearLayout.getViewTreeObserver()
                            .removeGlobalOnLayoutListener(this);

                    int[] l = new int[2];
                    viewHolder.verseOfTheDayLinearLayout.getLocationOnScreen(l);
                    Rect hitRect = new Rect(l[0],
                            l[1],
                            l[0] + viewHolder.verseOfTheDayLinearLayout.getWidth(),
                            l[1] + viewHolder.verseOfTheDayLinearLayout.getHeight());

                    Rect bounds = new Rect(hitRect.centerX() - DisplayHelper.dpToPx(80),
                            hitRect.centerY() - DisplayHelper.dpToPx(80),
                            hitRect.centerX() + DisplayHelper.dpToPx(80),
                            hitRect.centerY() + DisplayHelper.dpToPx(80));

                    Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
                    new TapTargetSequence(getActivity())
                            .targets(
                                    TapTarget.forToolbarNavigationIcon(toolbar,
                                            getString(R.string.adore_dieu_evolue),
                                            getString(R.string.aide_menu))
                                            .cancelable(false),
                                    TapTarget.forToolbarNavigationIcon(toolbar,
                                            "",
                                            getString(R.string.aide_menu2))
                                            .cancelable(false),
                                    TapTarget.forBounds(bounds,
                                            "",
                                            getString(R.string.aide_swipe_verse))
                                            .cancelable(false)
                                            .icon(ContextCompat.getDrawable(getActivity(),
                                                    R.drawable.ic_gesture_swipe_right_small)))
                            .listener(new TapTargetSequence.Listener() {
                                @Override
                                public void onSequenceFinish() {
                                    applicationContextPersistence.setHomeTutorialSeen(true);
                                }

                                @Override
                                public void onSequenceStep(TapTarget lastTarget,
                                                           boolean targetClicked) {

                                }

                                @Override
                                public void onSequenceCanceled(TapTarget lastTarget) {

                                }
                            }).start();
                }
            });
        }
    }

    private void getMessage() {
        WebHelper.downloadString(getActivity(),
                getString(R.string.message_www_address),
                new Action<String>() {
                    @Override
                    public void run(String content) {
                        if (content != null
                                && !content.isEmpty()) {
                            viewHolder.messageWebView.setVisibility(View.VISIBLE);
                            viewHolder.messageWebView.loadDataWithBaseURL(
                                    getString(R.string.message_www_address),
                                    content,
                                    "text/html",
                                    "UTF-8",
                                    getString(R.string.message_www_address));
                        } else {
                            viewHolder.messageWebView.setVisibility(View.GONE);
                        }
                    }
                });
    }

    private void showPopup(View view) {
        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View popupView = layoutInflater.inflate(R.layout.popup_home_menu,
                null);

        popupWindow = new PopupWindow(popupView,
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);

        ViewGroup layout = popupView.findViewById(R.id.layout);
        layout.setOnClickListener(HomeController.this);

        Button copyButton = popupView.findViewById(R.id.copyButton);
        copyButton.setOnClickListener(HomeController.this);

        Button shareButton = popupView.findViewById(R.id.shareButton);
        shareButton.setOnClickListener(HomeController.this);

        popupWindow.showAtLocation(view,
                Gravity.CENTER,
                0,
                0);
    }

    @Override
    public void onResume() {
        super.onResume();

        getMessage();

        getVerseOfTheDay(0);

        getQuoteOfTheDay(0);
    }

    private void getQuoteOfTheDay(int dayDelta) {
        final DateTime dateTime = dateProvider.getDateTime().plusDays(dayDelta);

        viewHolder.quoteProgressBar.setVisibility(View.VISIBLE);
        quoteOfTheDayServices.getQuoteOfTheDay(
                new Action<String>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void run(String object) {
                        try {
                            viewHolder.quoteLinearLayout.setVisibility(View.VISIBLE);
                            viewHolder.quoteProgressBar.setVisibility(View.GONE);
                            viewHolder.quoteTextView.setText(object);

                            if (dateTime.toLocalDate()
                                    .equals(dateProvider.getDateTime().toLocalDate())) {
                                viewHolder.quoteTitleTextView.setText(getString(R.string.citation_du_jour));
                            } else {
                                DateTimeFormatter fmt = DateTimeFormat.forPattern("dd MMM yyyy");
                                viewHolder.quoteTitleTextView.setText(
                                        getString(R.string.citation_du)
                                                + " "
                                                + fmt.print(dateTime));
                            }
                        } catch (Exception ignore) {
                        }
                    }
                },
                new Runnable() {
                    @Override
                    public void run() {
                        viewHolder.quoteLinearLayout.setVisibility(View.GONE);
                    }
                },
                dateTime);
    }

    private void getVerseOfTheDay(int dayDelta) {
        final DateTime dateTime = dateProvider.getDateTime().plusDays(dayDelta);

        viewHolder.verseOfTheDayProgressBar.setVisibility(View.VISIBLE);
        verseOfTheDayServices.getVerseOfTheDay(
                new Action<VerseOfTheDayDto>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void run(final VerseOfTheDayDto verseOfTheDay) {
                        try {
                            viewHolder.verseOfTheDayLinearLayout.setVisibility(View.VISIBLE);
                            viewHolder.verseOfTheDayProgressBar.setVisibility(View.GONE);
                            viewHolder.verseOfTheDayTextView.setText(verseOfTheDay.getVerse());

                            if (dateTime.toLocalDate()
                                    .equals(dateProvider.getDateTime().toLocalDate())) {
                                viewHolder.verseOfTheDayTitleTextView.setText(getString(R.string.verset_du_jour));
                            } else {
                                DateTimeFormatter fmt = DateTimeFormat.forPattern("dd MMM yyyy");
                                viewHolder.verseOfTheDayTitleTextView.setText(
                                        getString(R.string.verset_du)
                                                + " "
                                                + fmt.print(dateTime));
                            }

                            viewHolder.verseOfTheDayImageButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    userPreferencesPersistence.setStrongsMode(false);

                                    List<VerseDefinition> verseDefinitions = new ArrayList<>();
                                    verseDefinitions.add(VerseDto.fromVerse(
                                            verseDao.getVerse(verseOfTheDay.getBook(),
                                                    verseOfTheDay.getChapter_start(),
                                                    verseOfTheDay.getVerse_start())));

                                    guiController.showBibleController(
                                            getActivity(),
                                            verseDefinitions);
                                }
                            });
                        } catch (Exception ignore) {
                        }
                    }
                },
                new Runnable() {
                    @Override
                    public void run() {
                        viewHolder.verseOfTheDayLinearLayout.setVisibility(View.GONE);
                    }
                },
                dateTime);
    }

    @SuppressLint("ObsoleteSdkInt")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.copyButton:
                clipboardServices.copyToClipboard(getActivity(), tempText);
                break;
            case R.id.shareButton:
                shareServices.share(getActivity(), tempText);
                break;
        }

        dismissPopup();
    }

    class ViewHolder {
        ViewGroup verseOfTheDayLinearLayout;
        TextView verseOfTheDayTextView;
        TextView verseOfTheDayTitleTextView;
        ProgressBar verseOfTheDayProgressBar;

        ViewGroup quoteLinearLayout;
        TextView quoteTextView;
        TextView quoteTitleTextView;
        ProgressBar quoteProgressBar;
        WebView messageWebView;
        ImageButton verseOfTheDayImageButton;
    }
}
