package com.adoredieu.mobility.controllers;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.view.View;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.controllers.base.ArticlesListBaseController;
import com.adoredieu.mobility.core.helpers.DialogsHelper;
import com.adoredieu.mobility.core.interfaces.ArticleType;
import com.adoredieu.mobility.core.model.website.Article;
import com.adoredieu.mobility.core.system.application.AdoreDieuApplication;

import javax.inject.Inject;

public class ThoughtsListController
        extends ArticlesListBaseController
{
    @Inject
    GuiController guiController;

    public ThoughtsListController()
    {
        super(ArticleType.Thought);
    }

    @Override
    public void synchronizeNavigationDrawer(NavigationView navigationView)
    {
        navigationView.setCheckedItem(R.id.nav_thoughts);
    }

    @Override
    protected void onItemClick(final Article article)
    {
        DialogsHelper.showProgressIndeterminate(getActivity(),
                                                getString(R.string.veuillez_patienter),
                                                getString(R.string.chargement_de_l_article));

        new Handler().postDelayed(new Runnable()
                                  {
                                      @Override
                                      public void run()
                                      {
                                          guiController.showThoughtDetailsController(getActivity(),
                                                                                     article);
                                      }
                                  },
                                  100);
    }

    @Override
    public void onViewCreated(@NonNull View view,
                              Bundle savedInstanceState)
    {
        super.onViewCreated(view,
                            savedInstanceState);

        ((AdoreDieuApplication) getActivity().getApplication()).getInjector().inject(this);
    }
}
