package com.adoredieu.mobility.controllers;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.core.dao.GreekDao;
import com.adoredieu.mobility.core.dao.HebrewDao;
import com.adoredieu.mobility.core.dto.SearchResultBibleVerseDto;
import com.adoredieu.mobility.core.dto.StrongDto;
import com.adoredieu.mobility.core.helpers.KeyboardHelper;
import com.adoredieu.mobility.core.interfaces.Action;
import com.adoredieu.mobility.core.interfaces.VerseDefinition;
import com.adoredieu.mobility.core.persistence.UserPreferencesPersistence;
import com.adoredieu.mobility.core.services.BibleServices;
import com.adoredieu.mobility.core.services.BibleStrongServices;
import com.adoredieu.mobility.core.services.HighlightedVersesServices;
import com.adoredieu.mobility.core.system.application.AdoreDieuApplication;
import com.adoredieu.mobility.view.adapters.EmptyArrayAdapter;
import com.adoredieu.mobility.view.adapters.StringArrayAdapter;
import com.adoredieu.mobility.view.adapters.VersesSearchResultArrayAdapter;
import com.adoredieu.mobility.view.fragments.BibleStrongSearchFragment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import se.emilsjolander.stickylistheaders.StickyListHeadersListView;


public class BibleStrongSearchController
        extends BibleStrongSearchFragment
        implements AdapterView.OnItemClickListener {
    @Inject
    BibleStrongServices bibleStrongServices;
    @Inject
    BibleServices bibleServices;
    @Inject
    HighlightedVersesServices highlightedVersesServices;
    @Inject
    UserPreferencesPersistence userPreferencesPersistence;
    @Inject
    GuiController guiController;
    @Inject
    GreekDao greekDao;
    @Inject
    HebrewDao hebrewDao;

    private ArrayList<SearchResultBibleVerseDto> currentVerses;
    private ViewHolder viewHolder;
    private StrongLanguage language = StrongLanguage.Hebrew;
    private StrongLanguage lastLanguage = StrongLanguage.Hebrew;
    private Integer strongCode = 0;
    private Integer lastStrongCode = 0;

    @Override
    public void synchronizeNavigationDrawer(NavigationView navigationView) {
        navigationView.setCheckedItem(R.id.nav_bible_search);
    }

    @Override
    public void onViewCreated(@NonNull View view,
                              Bundle savedInstanceState) {
        super.onViewCreated(view,
                savedInstanceState);
        ((AdoreDieuApplication) getActivity().getApplication()).getInjector().inject(this);

        viewHolder = new ViewHolder();
        viewHolder.searchButton = view.findViewById(R.id.searchButton);
        viewHolder.searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startSearch();
            }
        });

        viewHolder.searchEditText = view.findViewById(R.id.searchEditText);
        if (strongCode > 0) {
            viewHolder.searchEditText.setText("" + strongCode);
        }
        viewHolder.searchEditText.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v,
                                 int keyCode,
                                 KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    startSearch();
                    return true;
                }
                return false;
            }
        });

        ImageButton clearSearchEditTextButton = view.findViewById(R.id.clearSearchEditTextButton);
        clearSearchEditTextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewHolder.searchEditText.setText("");
            }
        });

        viewHolder.progressBar = view.findViewById(R.id.progressBar);
        viewHolder.versesListView = view.findViewById(R.id.versesListView);
        viewHolder.versesListView.setOnItemClickListener(this);

        viewHolder.resultCountTextView = view.findViewById(R.id.resultCountTextView);
        viewHolder.resultCountTextView.setVisibility(View.GONE);

        viewHolder.strongDefinitionTextView = view.findViewById(R.id.strongDefinitionTextView);
        viewHolder.strongDefinitionTextView.setVisibility(View.GONE);

        viewHolder.strongsLanguagesSpinner = view.findViewById(R.id.strongsLanguagesSpinner);
        List<String> strongs_languages_list = Arrays.asList(getResources().getStringArray(R.array.strongs_languages_list));
        viewHolder.strongsLanguagesSpinner.setAdapter(new StringArrayAdapter(getActivity(),
                R.layout.spi_simple,
                strongs_languages_list));
        viewHolder.strongsLanguagesSpinner.setSelection(language == StrongLanguage.Hebrew ? 0 : 1);

        if (currentVerses != null && currentVerses.size() > 0
                && lastStrongCode != null && lastStrongCode.equals(strongCode)
                && lastLanguage != null && lastLanguage.equals(language)) {
            publishResults(currentVerses);
        } else {
            startSearch();
        }
    }

    private void startSearch() {
        try {
            strongCode = Integer.parseInt(viewHolder.searchEditText.getText().toString());

            if (strongCode > 0) {
                language = viewHolder.strongsLanguagesSpinner.getSelectedItemPosition() == 0 ? StrongLanguage.Hebrew : StrongLanguage.Greek;

                String strongDefinition;
                if (language == StrongLanguage.Greek) {
                    StrongDto strong = greekDao.getStrong(strongCode);
                    strongDefinition = strong.getOriginalWord() + " : " + strong.getWord() + " (strong n°" + strongCode + ")";
                } else {
                    StrongDto strong = hebrewDao.getStrong(strongCode);
                    strongDefinition = strong.getOriginalWord() + " : " + strong.getWord() + " (strong n°" + strongCode + ")";
                }

                viewHolder.strongDefinitionTextView.setText(strongDefinition);
                viewHolder.strongDefinitionTextView.setVisibility(View.VISIBLE);

                viewHolder.versesListView.setAdapter(new EmptyArrayAdapter(getActivity()));

                viewHolder.progressBar.setVisibility(View.VISIBLE);
                KeyboardHelper.hide_keyboard(getActivity());

                bibleStrongServices.startSearch(
                        getActivity(),
                        language,
                        strongCode,
                        new Action<ArrayList<SearchResultBibleVerseDto>>() {
                            @SuppressLint("SetTextI18n")
                            @Override
                            public void run(ArrayList<SearchResultBibleVerseDto> verses) {
                                publishResults(verses);
                            }
                        });
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            // TODO : Aucun resultat
        }
    }

    @SuppressLint("SetTextI18n")
    private void publishResults(ArrayList<SearchResultBibleVerseDto> verses) {
        currentVerses = verses;
        lastStrongCode = strongCode;
        lastLanguage = language;

        VersesSearchResultArrayAdapter versesArrayAdapter = new VersesSearchResultArrayAdapter(
                getActivity(),
                verses,
                highlightedVersesServices,
                userPreferencesPersistence.getNightMode(),
                bibleServices);

        viewHolder.versesListView.setAdapter(versesArrayAdapter);

        if (versesArrayAdapter.getCount() > 1) {
            viewHolder.resultCountTextView.setText(versesArrayAdapter.getCount() + " " + getString(R.string.resultats));
        } else {
            viewHolder.resultCountTextView.setText(versesArrayAdapter.getCount() + " " + getString(R.string.resultat));
        }

        viewHolder.progressBar.setVisibility(View.INVISIBLE);
        viewHolder.resultCountTextView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView,
                            View view,
                            int position,
                            long id) {
        SearchResultBibleVerseDto searchResultBibleVerseDto = (SearchResultBibleVerseDto) adapterView
                .getItemAtPosition(
                        position);

        // Forcer le mode strong
        userPreferencesPersistence.setStrongsMode(true);

        List<VerseDefinition> verseDefinitions = new ArrayList<>();
        verseDefinitions.add(searchResultBibleVerseDto);

        guiController.showBibleStrongController(getActivity(), verseDefinitions);
    }

    public void withSearch(StrongLanguage language, Integer strongCode) {
        this.language = language;
        this.strongCode = strongCode;
    }

    private class ViewHolder {
        public ProgressBar progressBar;
        Spinner strongsLanguagesSpinner;
        ImageButton searchButton;
        EditText searchEditText;
        StickyListHeadersListView versesListView;
        TextView resultCountTextView;
        TextView strongDefinitionTextView;
    }

    public enum StrongLanguage {
        Hebrew,
        Greek;

        public String toCode() {
            if (this == StrongLanguage.Greek)
                return "G";

            return "H";
        }
    }
}

