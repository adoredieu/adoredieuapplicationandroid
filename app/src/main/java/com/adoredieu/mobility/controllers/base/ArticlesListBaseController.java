package com.adoredieu.mobility.controllers.base;

import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.core.helpers.DialogsHelper;
import com.adoredieu.mobility.core.helpers.NetworkHelper;
import com.adoredieu.mobility.core.interfaces.Action;
import com.adoredieu.mobility.core.interfaces.ArticleType;
import com.adoredieu.mobility.core.model.website.Article;
import com.adoredieu.mobility.core.services.ArticlesServices;
import com.adoredieu.mobility.view.adapters.items.ArticleItem;
import com.adoredieu.mobility.view.adapters.items.ProgressMessageItem;
import com.adoredieu.mobility.view.fragments.ArticlesListFragment;
import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.IAdapter;
import com.mikepenz.fastadapter.IItem;
import com.mikepenz.fastadapter.adapters.FooterAdapter;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;
import com.mikepenz.fastadapter_extensions.scroll.EndlessRecyclerOnScrollListener;

import java.util.ArrayList;
import java.util.List;

public abstract class ArticlesListBaseController
        extends ArticlesListFragment {
    private final ArticleType articleType;
    private ArrayList<Article> currentArticles;
    private ArticlesServices articlesServices;
    private boolean noMoreArticles;
    // save our FastAdapter
    private FastItemAdapter<IItem> fastItemAdapter;
    private FooterAdapter<IItem> footerAdapter;
    private RecyclerView recyclerView;

    public ArticlesListBaseController(ArticleType articleType) {
        this.articleType = articleType;
    }

    @Override
    @CallSuper
    public void onViewCreated(@NonNull View view,
                              Bundle savedInstanceState) {
        super.onViewCreated(view,
                savedInstanceState);
        fastItemAdapter = new FastItemAdapter<>();
        fastItemAdapter.withSelectable(false);
        fastItemAdapter.withOnClickListener(new FastAdapter.OnClickListener<IItem>() {
            @Override
            public boolean onClick(View v,
                                   IAdapter<IItem> adapter,
                                   IItem item,
                                   int position) {
                if (item instanceof ArticleItem) {
                    onItemClick(((ArticleItem) item).article);
                }

                return false;
            }
        });
        footerAdapter = new FooterAdapter<>();

        //get our recyclerView and do basic setup
        recyclerView = view.findViewById(R.id.articlesListView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(footerAdapter.wrap(fastItemAdapter));
        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener() {
            @Override
            public void onLoadMore(final int currentPage) {
                if (!noMoreArticles && !articlesServices.isDownloading()) {
                    downloadPage((footerAdapter.getItemCount() / ArticlesServices.ITEMS_PER_PAGE) - getDeltaOfIndex());
                }
            }
        });

        noMoreArticles = false;

        articlesServices = new ArticlesServices(getActivity(),
                articleType);


        if (NetworkHelper.isNetworkAvailable(getActivity())) {
            TextView internetErrorTextView = view.findViewById(R.id.internetErrorTextView);
            internetErrorTextView.setVisibility(View.GONE);
        }

        if (currentArticles != null && currentArticles.size() > 0) {
            List<Article> articles = new ArrayList<>(currentArticles);
            currentArticles = new ArrayList<>();
            showArticles(articles);
        } else {
            if (NetworkHelper.isNetworkAvailable(getActivity())) {
                currentArticles = new ArrayList<>();
                downloadPage(0);
            }
        }
    }

    protected int getDeltaOfIndex() {
        return 0;
    }

    private void downloadPage(final Integer page) {
        recyclerView.post(new Runnable() {
            @Override
            public void run() {
                footerAdapter.clear();
                footerAdapter.add(new ProgressMessageItem(getString(R.string.downloading_articles_list)));
            }
        });

        articlesServices.getArticlesPage(
                page,
                new Action<List<Article>>() {
                    @Override
                    public void run(List<Article> object) {
                        showArticles(object);
                        DialogsHelper.hideProgressDialog();
                    }
                },
                new Runnable() {
                    @Override
                    public void run() {
                        noMoreArticles = true;
                        DialogsHelper.hideProgressDialog();
                    }
                });
    }

    protected abstract void onItemClick(Article article);

    private void showArticles(List<Article> articles) {
        currentArticles.addAll(articles);

        footerAdapter.clear();

        for (Article article : articles) {
            fastItemAdapter.add(fastItemAdapter.getAdapterItemCount(),
                    new ArticleItem(article));
        }
    }
}
