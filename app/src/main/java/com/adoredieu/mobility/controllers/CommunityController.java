package com.adoredieu.mobility.controllers;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.view.View;
import android.widget.Button;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.core.helpers.WebHelper;
import com.adoredieu.mobility.core.system.application.AdoreDieuApplication;
import com.adoredieu.mobility.view.fragments.CommunityFragment;

public class CommunityController
        extends CommunityFragment
{
    @Override
    public void synchronizeNavigationDrawer(NavigationView navigationView)
    {
        navigationView.setCheckedItem(R.id.nav_links);
    }

    @Override
    public void onViewCreated(@NonNull View view,
                              Bundle savedInstanceState)
    {
        super.onViewCreated(view,
                            savedInstanceState);
        ((AdoreDieuApplication) getActivity().getApplication()).getInjector().inject(this);

        Button facebookAccessButton = view.findViewById(R.id.facebookButton);
        facebookAccessButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                WebHelper.openUrlInBrowser(getActivity(),
                                           getString(R.string.facebook_address));
            }
        });

        Button twitterAccessButton = view.findViewById(R.id.twitterButton);
        twitterAccessButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                WebHelper.openUrlInBrowser(getActivity(),
                                           getString(R.string.twitter_address));
            }
        });

        Button instagramButton = view.findViewById(R.id.instagramButton);
        instagramButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                WebHelper.openUrlInBrowser(getActivity(),
                                           getString(R.string.instagram_address));
            }
        });

        Button websiteButton = view.findViewById(R.id.websiteButton);
        websiteButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                WebHelper.openUrlInBrowser(getActivity(),
                                           getString(R.string.website_address));
            }
        });
    }
}
