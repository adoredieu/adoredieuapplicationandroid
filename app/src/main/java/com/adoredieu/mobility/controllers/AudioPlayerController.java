package com.adoredieu.mobility.controllers;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.NotificationCompat;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.adoredieu.mobility.old.R;
import com.adoredieu.mobility.core.helpers.DialogsHelper;
import com.adoredieu.mobility.core.helpers.NetworkHelper;
import com.adoredieu.mobility.core.helpers.WebHelper;
import com.adoredieu.mobility.core.helpers.WifiHelper;
import com.adoredieu.mobility.core.interfaces.Action;
import com.adoredieu.mobility.core.interfaces.MusicServiceEventsListener;
import com.adoredieu.mobility.core.model.website.AudioTrack;
import com.adoredieu.mobility.core.persistence.UserPreferencesPersistence;
import com.adoredieu.mobility.core.services.AudioTracksServices;
import com.adoredieu.mobility.core.services.MusicServices;
import com.adoredieu.mobility.core.system.application.AdoreDieuApplication;
import com.adoredieu.mobility.core.system.notifications.NotificationMediaDescriptionAdapter;
import com.adoredieu.mobility.view.adapters.items.AudioTrackItem;
import com.adoredieu.mobility.view.fragments.AudioPlayerFragment;
import com.google.android.exoplayer2.ui.PlayerControlView;
import com.google.android.exoplayer2.ui.PlayerNotificationManager;
import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.IAdapter;
import com.mikepenz.fastadapter.IItem;
import com.mikepenz.fastadapter.adapters.FooterAdapter;
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter;

import java.util.List;

import javax.inject.Inject;

import static com.adoredieu.mobility.core.helpers.NotificationHelper.MUSIC_PLAYER_CHANNEL_ID;

public class AudioPlayerController
        extends AudioPlayerFragment
        implements MusicServiceEventsListener {
    @Inject
    AudioTracksServices audioTracksServices;
    @Inject
    MusicServices musicServices = null;
    @Inject
    UserPreferencesPersistence userPreferencesPersistence;

    private PlayerNotificationManager playerNotificationManager = null;
    private NotificationMediaDescriptionAdapter notificationMediaDescriptionAdapter = null;
    private MediaSessionCompat mediaSession = null;

    private ViewHolder viewHolder = null;

    //save our FastAdapter
    private FastItemAdapter<IItem> fastItemAdapter;
    private FooterAdapter<IItem> footerAdapter;

    @Override
    public void synchronizeNavigationDrawer(NavigationView navigationView) {
        navigationView.setCheckedItem(R.id.nav_audio);
    }

    @Override
    public void onViewCreated(@NonNull final View view,
                              Bundle savedInstanceState) {
        super.onViewCreated(view,
                savedInstanceState);

        ((AdoreDieuApplication) getActivity().getApplication()).getInjector().inject(this);

        viewHolder = new ViewHolder();

        viewHolder.playerControlView = view.findViewById(R.id.playerControlView);
        viewHolder.loadingProgressBar = view.findViewById(R.id.loadingProgressBar);
        viewHolder.albumImageView = view.findViewById(R.id.albumImageView);

        viewHolder.titleTextView = view.findViewById(R.id.titleTextView);
        viewHolder.titleTextView.setText("");

        viewHolder.artistTextView = view.findViewById(R.id.artistTextView);
        viewHolder.artistTextView.setText("");

        fastItemAdapter = new FastItemAdapter<>();
        footerAdapter = new FooterAdapter<>();

        fastItemAdapter.withSelectable(true);
        fastItemAdapter.withOnClickListener(new FastAdapter.OnClickListener<IItem>() {
            @Override
            public boolean onClick(View v,
                                   IAdapter<IItem> adapter,
                                   IItem item,
                                   int position) {
                if (item instanceof AudioTrackItem) {
                    musicServices.playTrack(position);
                }

                return false;
            }
        });

        //get our recyclerView and do basic setup
        viewHolder.recyclerView = view.findViewById(R.id.audioTracksListView);
        viewHolder.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        viewHolder.recyclerView.setItemAnimator(new DefaultItemAnimator());
        viewHolder.recyclerView.setAdapter(footerAdapter.wrap(fastItemAdapter));

        musicServices.setMusicServiceEventsListener(this);

        musicServices.getPlayer().setShuffleModeEnabled(userPreferencesPersistence.getAudioPlayerRandomState());

        if (notificationMediaDescriptionAdapter == null) {
            notificationMediaDescriptionAdapter = new NotificationMediaDescriptionAdapter(getActivity());
            playerNotificationManager = new PlayerNotificationManager(getActivity(),
                    MUSIC_PLAYER_CHANNEL_ID,
                    1,
                    notificationMediaDescriptionAdapter);

            // omit fast forward action by setting the increment to zero
            playerNotificationManager.setFastForwardIncrementMs(0);
            // omit rewind action by setting the increment to zero
            playerNotificationManager.setRewindIncrementMs(0);
            playerNotificationManager.setStopAction(null);
            playerNotificationManager.setOngoing(false);
            playerNotificationManager.setColor(getResources().getColor(R.color.pal_blue_3));
            playerNotificationManager.setColorized(true);
            playerNotificationManager.setUseChronometer(false);
            playerNotificationManager.setSmallIcon(R.drawable.ic_notification);
            playerNotificationManager.setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL);
            playerNotificationManager.setVisibility(NotificationCompat.VISIBILITY_PRIVATE);
            playerNotificationManager.setPriority(NotificationCompat.PRIORITY_LOW);
            playerNotificationManager.setPlayer(musicServices.getPlayer());
        }

        viewHolder.playerControlView.setPlayer(musicServices.getPlayer());

        if (mediaSession == null) {
            // Media session
            mediaSession = new MediaSessionCompat(getActivity(), MUSIC_PLAYER_CHANNEL_ID);
            mediaSession.setActive(true);

            PlaybackStateCompat playbackStateCompat = new PlaybackStateCompat.Builder()
                    .setActions(
                            PlaybackStateCompat.ACTION_SEEK_TO
                                    | PlaybackStateCompat.ACTION_SKIP_TO_PREVIOUS
                                    | PlaybackStateCompat.ACTION_SKIP_TO_NEXT
                                    | PlaybackStateCompat.ACTION_PLAY
                                    | PlaybackStateCompat.ACTION_PAUSE
                                    | PlaybackStateCompat.ACTION_STOP
                    )
                    .setState(
                            PlaybackStateCompat.STATE_PAUSED,
                            0,
                            1.0f)
                    .build();
            mediaSession.setPlaybackState(playbackStateCompat);
            playerNotificationManager.setMediaSessionToken(mediaSession.getSessionToken());
        }

        if (NetworkHelper.isNetworkAvailable(getActivity())) {
            TextView internetErrorTextView = view.findViewById(R.id.internetErrorTextView);
            internetErrorTextView.setVisibility(View.GONE);
        }

        if (musicServices.getAudioTracks().size() > 0) {
            populateTracks(musicServices.getAudioTracks());
            onTrackChanged(musicServices.getCurrentIndex());
        } else {
            DialogsHelper.showProgressIndeterminate(getActivity(),
                    getString(R.string.veuillez_patienter),
                    getString(R.string.downloading_audio_tracks_list));
            audioTracksServices.getTracks(
                    new Action<List<AudioTrack>>() {
                        @Override
                        public void run(List<AudioTrack> audioTracks) {
                            if (audioTracks == null) {
                                return;
                            }

                            musicServices.setTracks(audioTracks);
                            populateTracks(audioTracks);

                            DialogsHelper.hideProgressDialog();


                            if (!WifiHelper.isConnected(getActivity())) {
                                DialogsHelper.showAlert(getActivity(),
                                        getString(R.string.warning),
                                        getString(R.string.audio_player_no_wifi),
                                        null);
                            }
                        }
                    },
                    new Runnable() {
                        @Override
                        public void run() {
                            footerAdapter.clear();
                            DialogsHelper.hideProgressDialog();
                        }
                    });
        }
    }

    private void updateMediaSessionMetaData() {
        MediaMetadataCompat.Builder metaDataBuilder = new MediaMetadataCompat.Builder();
        Bitmap albumArtBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ic_album_white);

        metaDataBuilder.putBitmap(MediaMetadataCompat.METADATA_KEY_ALBUM_ART, albumArtBitmap);
        metaDataBuilder.putBitmap(MediaMetadataCompat.METADATA_KEY_ART, albumArtBitmap);

        mediaSession.setMetadata(metaDataBuilder.build());
    }

    private void populateTracks(List<AudioTrack> audioTracks) {
        if (audioTracks.size() > 0) {
            footerAdapter.clear();
            fastItemAdapter.clear();

            for (AudioTrack audioTrack : audioTracks) {
                fastItemAdapter.add(new AudioTrackItem(audioTrack,
                        userPreferencesPersistence.getNightMode()));
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (musicServices != null) {
            mediaSession.setActive(false);
            mediaSession.release();
            viewHolder.playerControlView.setPlayer(null);
            playerNotificationManager.setPlayer(null);
            playerNotificationManager.setOngoing(false);
            musicServices.destroyAudioPlayer();
        }
    }

    @Override
    public void onTrackChanged(int index) {
        fastItemAdapter.deselect();
        fastItemAdapter.select(index);
        viewHolder.recyclerView.getLayoutManager().scrollToPosition(index);
        viewHolder.albumImageView.setImageResource(R.drawable.ic_album);

        AudioTrack audioTrack = this.musicServices.getAudioTracks().get(index);
        viewHolder.titleTextView.setText(audioTrack.title);
        viewHolder.artistTextView.setText(audioTrack.artist);
        getCdCover(audioTrack);

        notificationMediaDescriptionAdapter.setCurrentTrack(audioTrack);

        updateMediaSessionMetaData();
    }

    private void getCdCover(final AudioTrack audioTrack) {
        if (audioTrack.cover != null) {
            onAudioTrackCoverReceived(audioTrack.cover);
        } else {
            if (audioTrack.coverUri.isEmpty()) {
                onAudioTrackCoverFailed();
            } else {
                Log.d(getClass().getSimpleName(), "Downloading " + audioTrack.coverUri);

                WebHelper.downloadImage(
                        getActivity(),
                        audioTrack.coverUri,
                        new Action<Bitmap>() {
                            @Override
                            public void run(Bitmap bitmap) {
                                audioTrack.cover = bitmap;
                                onAudioTrackCoverReceived(audioTrack.cover);
                            }
                        });
            }
        }
    }

    public void onAudioTrackCoverReceived(Bitmap bitmap) {
        viewHolder.albumImageView.setImageBitmap(bitmap);
    }

    public void onAudioTrackCoverFailed() {
        viewHolder.albumImageView.setImageResource(R.drawable.ic_album);
    }

    @Override
    public void onShuffleModeEnabledChanged(boolean mode) {
        userPreferencesPersistence.setAudioPlayerRandomState(mode);
    }

    @Override
    public void onPlayerPause() {
        // playerNotificationManager.setOngoing(false);
    }

    @Override
    public void onPlayerPlay() {
        // playerNotificationManager.setOngoing(true);
    }

    @Override
    public void onPlayerBuffering() {
        viewHolder.loadingProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onPlayerRecoverFromError() {
        playerNotificationManager.setPlayer(musicServices.getPlayer());
        viewHolder.playerControlView.setPlayer(musicServices.getPlayer());

        populateTracks(musicServices.getAudioTracks());
        musicServices.playTrack(0);
    }

    @Override
    public void onPlayerReady() {
        viewHolder.loadingProgressBar.setVisibility(View.INVISIBLE);
    }

    private class ViewHolder {
        public ImageView albumImageView;
        public TextView titleTextView;
        public TextView artistTextView;
        public RecyclerView recyclerView;
        public PlayerControlView playerControlView;
        public View loadingProgressBar;
    }
}
