package com.adoredieu.mobility.core.helpers;

import junit.framework.Assert;

import org.joda.time.LocalTime;
import org.junit.Test;

public class DateTimeHelperTest
{
    @Test
    public void testIsTimeBetweenIntervalSimple()
    {
        boolean result = DateTimeHelper.isTimeBetweenInterval(new LocalTime(10,
                                                                            0),
                                                              new LocalTime(8,
                                                                            0),
                                                              new LocalTime(12,
                                                                            0));
        Assert.assertTrue(result);

        result = DateTimeHelper.isTimeBetweenInterval(new LocalTime(8,
                                                                    0),
                                                      new LocalTime(8,
                                                                    0),
                                                      new LocalTime(12,
                                                                    0));
        Assert.assertFalse(result);

        result = DateTimeHelper.isTimeBetweenInterval(new LocalTime(8,
                                                                    1),
                                                      new LocalTime(8,
                                                                    0),
                                                      new LocalTime(12,
                                                                    0));
        Assert.assertTrue(result);

        result = DateTimeHelper.isTimeBetweenInterval(new LocalTime(12,
                                                                    1),
                                                      new LocalTime(8,
                                                                    0),
                                                      new LocalTime(12,
                                                                    1));
        Assert.assertFalse(result);

        result = DateTimeHelper.isTimeBetweenInterval(new LocalTime(12,
                                                                    2),
                                                      new LocalTime(8,
                                                                    0),
                                                      new LocalTime(12,
                                                                    1));
        Assert.assertFalse(result);
    }

    @Test
    public void testIsTimeBetweenIntervalAcrossMidnight()
    {
        boolean result = DateTimeHelper.isTimeBetweenInterval(new LocalTime(22,
                                                                            30),
                                                              new LocalTime(22,
                                                                            30),
                                                              new LocalTime(8,
                                                                            0));
        Assert.assertFalse(result);

        result = DateTimeHelper.isTimeBetweenInterval(new LocalTime(22,
                                                                    31),
                                                      new LocalTime(22,
                                                                    30),
                                                      new LocalTime(8,
                                                                    0));
        Assert.assertTrue(result);

        result = DateTimeHelper.isTimeBetweenInterval(new LocalTime(23,
                                                                    59,
                                                                    58),
                                                      new LocalTime(22,
                                                                    30),
                                                      new LocalTime(8,
                                                                    0));
        Assert.assertTrue(result);

        result = DateTimeHelper.isTimeBetweenInterval(new LocalTime(23,
                                                                    59,
                                                                    59),
                                                      new LocalTime(22,
                                                                    30),
                                                      new LocalTime(8,
                                                                    0));
        Assert.assertTrue(result);

        result = DateTimeHelper.isTimeBetweenInterval(new LocalTime(0,
                                                                    0,
                                                                    0),
                                                      new LocalTime(22,
                                                                    30),
                                                      new LocalTime(8,
                                                                    0));
        Assert.assertTrue(result);

        result = DateTimeHelper.isTimeBetweenInterval(new LocalTime(0,
                                                                    0,
                                                                    1),
                                                      new LocalTime(22,
                                                                    30),
                                                      new LocalTime(8,
                                                                    0));
        Assert.assertTrue(result);

        result = DateTimeHelper.isTimeBetweenInterval(new LocalTime(1,
                                                                    0),
                                                      new LocalTime(22,
                                                                    30),
                                                      new LocalTime(8,
                                                                    0));
        Assert.assertTrue(result);

        result = DateTimeHelper.isTimeBetweenInterval(new LocalTime(8,
                                                                    0),
                                                      new LocalTime(22,
                                                                    30),
                                                      new LocalTime(8,
                                                                    0));
        Assert.assertFalse(result);

        result = DateTimeHelper.isTimeBetweenInterval(new LocalTime(9,
                                                                    0),
                                                      new LocalTime(22,
                                                                    30),
                                                      new LocalTime(8,
                                                                    0));
        Assert.assertFalse(result);
    }
}
